<?php
namespace app\channel\controller;

use site\myDb;
use site\myCache;
use site\myValidate;
use app\common\model\mdLogin;
use app\common\model\mdMessage;
use app\common\model\mdConsole;
use site\mySearch;
use site\myHttp;

class Index extends Common
{
    
    //后台首页
    public function index(){
    	global $loginId;
        $cur = myCache::getChannel($loginId);
        $menu = myCache::getChannelMenu(2);
        $variable = [
        	'cur' => $cur,
            'menu' => $menu,
            'site_name' => $cur['name']
        ];
        return $this->fetch('common@index/index-channel',$variable);
    }
    
    //设置是否接收消息
    public function setIsReceive(){
    	global $loginId;
    	$cur = myCache::getChannel($loginId);
    	if(!$cur){
    		res_api('登录信息异常');
    	}
    	$field = 'is_receive';
    	$is_receive = myValidate::getData(['is_receive'=>['require|in:1,2',['require'=>'参数错误','in'=>'参数错误']]]);
    	$res = myDb::setField('Channel', [['id','=',$loginId]], $field, $is_receive);
    	if($res){
    		myCache::doChannel($loginId,$field,$is_receive);
    		res_api();
    	}else{
    		res_api('设置失败');
    	}
    }
    
    //账号绑定列表
    public function account(){
    	global $loginId;
    	if($this->request->isAjax()){
    		$cur = myCache::getChannel($loginId);
    		if($cur['relation_code']){
    			$config = [
    				'like' => 'keyword:b.name',
    				'default' => [['a.code','=',$cur['relation_code']]]
    			];
    			$where = mySearch::getWhere($config);
    			$pages = myHttp::getPageParams();
    			$res = mdConsole::getRelAccountList($where,$pages,$loginId);
    			res_table($res['data'],$res['count']);
    		}else{
    			res_table([]);
    		}
    	}else{
    			
    		return $this->fetch('common@index/account');
    	}
    }
    
    //处理账号事件
    public function doAccountEvent(){
    	global $loginId;
    	$rules = [
    		'id' => ['require|number|gt:0',['require'=>'账号参数错误','number'=>'账号参数错误','gt'=>'账号参数错误']],
    		'event' => ['require|in:location,delete',['require'=>'事件参数错误','in'=>'未指定该事件']]
    	];
    	$data = myValidate::getData($rules);
    	$cur = myCache::getChannel($loginId);
    	$channel = myCache::getChannel($data['id']);
    	if($cur['relation_code'] !== $channel['relation_code']){
    		res_api('非法访问');
    	}
    	switch ($data['event']){
    		case 'location':
    			session('CHANNEL_LOGIN_ID',$data['id']);
    			break;
    		case 'delete':
    			$res = mdConsole::deleteAccount($loginId, $channel['id'], $cur['relation_code']);
    			if(!$res){
    				res_api('解除绑定失败');
    			}
    			break;
    	}
    	res_api();
    }
    
    //绑定账号
    public function doBind(){
    	if($this->request->isAjax()){
    		global $loginId;
    		$rules = [
    			'login_name' => ["require|alphaDash|length:5,12",["require"=>"请输入登陆账户名","alphaDash"=>'登陆账户名必须是英文、数字、下划线和破折号',"length"=>"请输入5至12位符合规范的登陆账户名"]],
    			'password' => ["require|length:6,16",["require"=>"请输入登陆密码","length"=>"请输入6-16位登陆密码"]],
    		];
    		$post = myValidate::getData($rules);
    		$channel = myDb::getCur('Channel', [['login_name','=',$post['login_name']]],'id,is_wx,password,status,relation_code');
    		if(!$channel){
    			res_api('用户名或密码输入错误');
    		}
    		$pwd = createPwd($post['password']);
    		if($pwd != $channel['password']){
    			res_api('用户名或密码输入错误');
    		}
    		if($channel['status'] != 1){
    			res_api('该用户尚未启用');
    		}
    		if($channel['is_wx'] != 1){
    			res_api('不支持绑定代理账号');
    		}
    		if($channel['relation_code']){
    			res_api('该账号已绑定其他关联组');
    		}
    		$res = mdConsole::bindAccount($loginId, $channel['id']);
    		if($res){
    			res_api();
    		}else{
    			res_api('绑定失败,请重试');
    		}
    	}else{
    		
    		return $this->fetch('common@public/doBind');
    	}
    }
    
    //控制台
    public function console(){
    	global $loginId;
    	$number = mdConsole::getChannelNumbersData();
    	$charge_rank = mdConsole::getChargeRank([['a.status','=',2],['a.is_count','=',1],['a.channel_id','=',$loginId]]);
    	$complaint_rank = mdConsole::getComplaintRank($loginId);
    	$variable = [
    		'number' => $number,
    		'charge_rank' => $charge_rank,
    		'complaint_rank' => $complaint_rank,
    	];
    	return $this->fetch('common@index/console-channel',$variable);
    }
    
    //检查是否有未读公告
    public function getReadMessage(){
    	$res = mdMessage::checkMessage();
    	if($res['id'] > 0){
    		$res['url'] = my_url('Message/showInfo',['id'=>$res['id']]);
    	}
        res_api($res);
    }
    
    //获取用户趋势图
    public function getUserChartData(){
    	global $loginId;
    	$data = mdConsole::getUserChartData($loginId);
    	res_api($data);
    }
    
    
    //基础信息
    public function userinfo(){
    	global $loginId;
    	if($this->request->isAjax()){
    		$rules = ['name' =>  ["require|max:20",["require"=>"请输入用户名称",'max'=>'用户名称最多支持20个字符']]];
    		$name = myValidate::getData($rules);
    		$re = myDb::setField('Channel',[['id','=',$loginId]], 'name', $name);
    		if($re){
    			myCache::rmChannel($loginId);
    			$cur = myCache::getChannel($loginId);
    			if($cur['url']){
    				myCache::rmUrlInfo($cur['url']);
    			}
    			if($cur['location_url']){
    				myCache::rmUrlInfo($cur['location_url']);
    			}
    			res_api();
    		}else{
    			res_api('保存失败');
    		}
    	}else{
    		$cur = myCache::getChannel($loginId);
    		$login_msg = mdLogin::getLastLoginMsg($cur['login_name'],2);
    		if($login_msg){
    			$cur = array_merge($cur,$login_msg);
    		}
    		$variable = [
    			'cur' => $cur,
    			'js' => getJs('index.userinfo','channel')
    		];
    		return $this->fetch('common@index/userinfo',$variable);
    	}
    }
    
    //修改密码
    public function password(){
    	if($this->request->isAjax()){
    		global $loginId;
    		global $loginId;
    		$re = mdLogin::changePwd($loginId,2);
    		if($re){
    			res_api();
    		}else{
    			res_api('保存失败');
    		}
    	}else{
    		
    		return $this->fetch('common@index/password');
    	}
    }
    
    //退出登录
    public function logOut(){
    	session('CHANNEL_LOGIN_ID',null);
    	$url = my_url('Login/index');
    	echo "<script language=\"javascript\">window.open('".$url."','_top');</script>";
    	exit;
    }

}
