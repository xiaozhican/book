<?php
namespace app\channel\controller;

use site\myHttp;
use site\myAliyunoss;

class Upload extends Common{
    
    //裁剪图片
    public function crop(){
        if($this->request->isAjax()){
        	$post = myHttp::getData('img');
            $image = $post['img'];
            if(!empty($image)){
            	$config = getMyConfig('alioss');
            	if(!$config){
            		res_api('您尚未配置阿里云oss参数');
            	}
                myAliyunoss::$config = $config;
                $content = base64_decode($image);
                $name = md5(microtime().mt_rand(10000,99999)).'.jpg';
                $savename = 'images/'.date('Ymd').'/'.$name;
                $url = myAliyunoss::putObject($savename, $content);
                if($url){
                    res_api(['url'=>$url]);
                }else{
                    res_api('上传失败,请重试');
                }
            }else{
                res_api('未检测到上传文件');
            }
        }else{
            $get = myHttp::getData('crop_size','get');
            $crop_size = $get['crop_size'];
            $reg = '/^[\d]{1,}[x][\d]{1,}$/';
            if(!preg_match($reg, $crop_size)){
                res_api('裁剪尺寸格式错误');
            }
            $arr = explode('x', $crop_size);
            $config = ['width'=>$arr[0],'height'=>$arr[1],'ratio'=>($arr[0]/$arr[1])];
            $this->assign('config',$config);
            return $this->fetch('common@upload/crop');
        }
    }
    
}