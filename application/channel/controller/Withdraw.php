<?php
namespace app\channel\controller;

use site\myDb;
use site\myHttp;
use site\mySearch;
use site\myValidate;
use app\common\model\mdWithdraw;

class Withdraw extends Common{
    
	//提现申请列表
	public function index(){
		global $loginId;
		$status = myValidate::getData(['status'=>['in:0,1',['in'=>'非法访问']]],'get');
		$status = $status ? : 0;
		if($this->request->isAjax()){
			$config = [
				'between' => 'between_time:cur_date%date',
				'default' => [['to_channel_id','=',$loginId],['status','=',$status]]
			];
			$where = mySearch::getWhere($config);
			switch ($status){
				case 0:
					$list = mdWithdraw::getWaitList($where);
					res_table($list);
					break;
				case 1:
					$pages = myHttp::getPageParams();
					$res = mdWithdraw::getPassList($where, $pages,true);
					res_table($res['data'],$res['count']);
					break;
			}
			res_api('非法访问');
		}else{
			switch ($status){
				case 0:
					$js = getJs('withdraw.wait','admin',['date'=>'20200310']);
					break;
				case 1:
					$js = getJs('withdraw.pass','admin',['date'=>'20200310']);
					break;
			}
			$cur = mdWithdraw::getChildCountData($loginId);
			return $this->fetch('common@withdraw/index-channel',['cur'=>$cur,'status'=>$status,'js'=>$js]);
		}
	}
	
	//获取导出数据
	public function exportData(){
		global $loginId;
		$where = [['to_channel_id','=',$loginId],['status','=',0]];
		$data = mdWithdraw::getExportData($where);
		res_table($data);
	}
	
	//处理提现申请
	public function doWithdraw(){
		global $loginId;
		$rules = ['sign' => ['require|array',['require'=>'账单参数错误','array'=>'账单格式不规范']]];
		$signs = myValidate::getData($rules);
		$num = 0;
		foreach ($signs as $v){
			$re = mdWithdraw::doPass($v,$loginId);
			if($re){
				$num++;
			}
		}
		res_api(['num'=>$num]);
	}
    
    //我的结算
    public function mine(){
    	global $loginId;
        if($this->request->isAjax()){
            $config = [
                'default' => [['channel_id','=',$loginId],['status','between',[0,1]]],
                'eq' => 'status:status',
            	'between' => 'between_time:cur_date%date'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = myDb::getPageList('Withdraw', $where, '*', $pages);
            if($res['data']){
            	$opts = [];
                foreach ($res['data'] as &$v){
                	$v['bank_info'] = json_decode($v['bank_info'],true);
                	if($v['status'] == 1){
                		$v['opt_name'] = '未知';
                		$v['pay_time'] = $v['pay_time'] ? date('Y-m-d H:i:s',$v['pay_time']) : '--';
                		if($v['opt_id']){
                			if(!array_key_exists($v['opt_id'], $opts)){
                				$opts[$v['opt_id']] = myDb::getValue('Manage',[['id','=',$v['opt_id']]],'name');
                			}
                			if($opts[$v['opt_id']]){
                				$v['opt_name'] = $opts[$v['opt_id']];
                			}
                		}
                	}
                    $v['create_time'] = date('Y-m-d H:i:s',$v['create_time']);
                }
            }
            res_table($res['data'],$res['count']);
        }else{
        	$cur = mdWithdraw::getMyCountData($loginId);
            return $this->fetch('common@withdraw/mine',['cur'=>$cur]);
        }
    }
    
}
