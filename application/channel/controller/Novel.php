<?php
namespace app\channel\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use app\common\model\mdBook;
use app\common\model\mdSpread;


class Novel extends Common{
    
    //小说列表
    public function index(){
        if($this->request->isAjax()){
            $config = [
            	'default' => [['type','=',1],['status','=',1]],
            	'eq' => 'gender_type,over_type,free_type,is_god',
            	'like' => 'keyword:name,category'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdBook::getBookPageList($where, $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                	$free_time = '';
                	if(myCache::checkBookFree($v['id'])){
                		$free_time = '未知';
                		$freeItem = myDb::getCur('BookFree', [['book_id','=',$v['id']]],'start_date,end_date');
                		if($freeItem){
                			$free_time = $freeItem['start_date'].'~'.$freeItem['end_date'];
                		}
                	}
                	$share_info = myCache::getBookShareInfo($v['id']);
                	$v['is_share'] = $share_info ? 1 : 2;
                	$share_info = null;
                	$v['free_time'] = $free_time;
                	$v['total_chapter'] = myCache::getBookChapterNum($v['id']);
                    $v['spread_url'] = my_url('Spread/createLink',['book_id'=>$v['id']]);
                    $v['guide_url'] = my_url('Guide/index',['book_id'=>$v['id']]);
                    $v['copy_url'] = my_url('copyLink',['id'=>$v['id']]);
                    $v['customer_url'] = my_url('Custom/addCustom',['book_id'=>$v['id']]);
                    $v['share_url'] = my_url('bookShare',['id'=>$v['id']]);
                }
            }
            res_table($res['data'],$res['count']);
        }else{
        	$category = getMyConfig('novel_category');
        	$keyword = $this->request->get('keyword');
        	$variable = ['js'=>getJs('novel.index','channel',['date'=>'20200226']),'category'=>$category,'keyword'=>$keyword];
        	return $this->fetch('common@book/index',$variable);
        }
    }
    
    //复制链接
    public function copyLink(){
    	global $loginId;
        $id = myHttp::getId('小说');
        $book = myDb::getById('Book',$id,'id,name');
        if(!$book){
            res_api('书籍参数错误');
        }
        $url = mdSpread::getSpreadUrl($loginId);
        $url .= '/Index/Book/info.html?book_id='.$id;
        $data = ['links' => [['title'=>'小说链接','val'=>$url]]];
        return $this->fetch('common@public/copyLink',$data);
    }
    
    
    //分享话术
    public function bookShare(){
    	$book_id = myHttp::getId('小说');
    	$cur = myDb::getCur('BookShare',[['book_id','=',$book_id]]);
    	if(!$cur){
    		$cur = ['title'=>'','content'=>''];
    	}
    	return $this->fetch('common@public/bookShare',['cur'=>$cur]);
    }
}