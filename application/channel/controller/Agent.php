<?php
namespace app\channel\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use site\myValidate;
use app\common\model\mdPlatform;

class Agent extends Common{
    
    //代理列表
    public function index(){
        if($this->request->isAjax()){
            global $loginId;
            $config = [
                'default' => [['status','between',[0,3]],['type','=',2],['parent_id','=',$loginId]],
                'eq' => 'status',
                'like' => 'keyword:name'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdPlatform::getList($where, $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                    $v['status_name'] = mdPlatform::getStatusName($v['status']);
                    $v['money'] = myCache::getChannelAmount($v['id']);
                    $v['do_url'] = my_url('doAgent',['id'=>$v['id']]);
                    $v['ratio'] .= '%';
                }
            }
            res_table($res['data'],$res['count']);
            
        }else{
            
        	return $this->fetch('common@agent/index',['js'=>getJs('agent.index','channel')]);
        }
    }
    
    //新增代理
    public function addAgent(){
    	global $loginId;
        if($this->request->isAjax()){
        	$rules = mdPlatform::getRules(0);
        	$data = myValidate::getData($rules);
        	$data['parent_id'] = $loginId;
        	$data['status'] = 0;
        	$data['type'] = 2;
        	mdPlatform::doneAgent($data);
        }else{
            $channel = myDb::getById('Channel', $loginId,'id,ratio');
            $field = 'id,name,login_name,url,ratio:80,bank_user,bank_name,bank_no';
            $cur = myDb::buildArr($field);
            $variable = ['cur'=>$cur,'backUrl'=>my_url('index'),'ratio'=>$channel['ratio']];
            return $this->fetch('common@agent/doAgent',$variable);
        }
    }
    
    //编辑代理
    public function doAgent(){
        if($this->request->isAjax()){
        	$rules = mdPlatform::getRules(0,0);
        	$data = myValidate::getData($rules);
        	mdPlatform::doneAgent($data);
        }else{
            global $loginId;
            $id = myHttp::getId('代理');
            $channel = myDb::getById('Channel', $loginId,'id,ratio');
            $cur = myDb::getById('Channel',$id);
            if(!$cur){
                res_api('渠道不存在');
            }
            $variable = ['cur'=>$cur,'backUrl'=>my_url('index'),'ratio'=>$channel['ratio']];
            return $this->fetch('common@agent/doAgent',$variable);
        }
    }
    
    //处理代理状态
    public function doAgentEvent(){
    	$rules = mdPlatform::getEventRules();
        $data = myValidate::getData($rules);
        $cur = myDb::getById('Channel',$data['id'],'id,url');
        if(!$cur){
            res_api('代理信息异常');
        }
        $key = 'status';
        switch ($data['event']){
            case 'on':
                $value = 1;
                break;
            case 'off':
                $value = 2;
                break;
            case 'delete':
                $value = 4;
                break;
            case 'resetpwd':
                $key = 'password';
                $value = createPwd(123456);
                break;
            default:
                res_api('未指定该事件');
                break;
        }
        if($value === 4){
        	$saveData = ['status'=>4,'url'=>'','login_name'=>''];
        	$re = myDb::save('Channel',[['id','=',$cur['id']]],$saveData);
        }else{
        	$re = myDb::setField('Channel', [['id','=',$cur['id']]],$key, $value);
        }
        if($re){
            if($key === 'status'){
                myCache::rmChannel($cur['id']);
                if($cur['url']){
                    myCache::rmUrlInfo($cur['url']);
                }
            }
            res_api();
        }else{
            res_api('操作失败,请重试');
        }
    }
    
    
}