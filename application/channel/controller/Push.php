<?php
namespace app\channel\controller;

use site\myDb;
use site\myValidate;
use app\common\model\mdPush;
use app\common\model\mdMaterial;

class Push extends Common{
    
    
    //智能推送消息
    public function index(){
    	
        global $loginId;
        $variable = mdPush::getPushInfo($loginId);
        return $this->fetch('common@push/index',$variable);
    }
    
    //编辑推送消息
    public function doPush(){
        global $loginId;
        $rules = ['type'=>['require|between:1,5',['require'=>'类型错误','between'=>'未定义该类型']]];
        if($this->request->isAjax()){
        	$rules['hours'] = ['require|between:1,48',['require'=>'请输入延迟推送小时数','between'=>'小时数必须为1-48小时']];
        	$data = myValidate::getData($rules);
        	$data['channel_id'] = $loginId;
            $material = mdMaterial::getCustomMsg();
            $data['material'] = json_encode($material,JSON_UNESCAPED_UNICODE);
            $cur = myDb::getCur('TaskMessage',[['type','=',$data['type']],['channel_id','=',$loginId]],'id,status');
            if($cur){
                $re = myDb::save('TaskMessage', [['id','=',$cur['id']]], $data);
            }else{
                $data['status'] = 2;
                $re = myDb::add('TaskMessage', $data);
            }
            if($re){
                res_api();
            }else{
                res_api('配置失败，请重试');
            }
        }else{
            $type = myValidate::getData($rules,'get');
            $data = myDb::getCur('TaskMessage',[['type','=',$type],['channel_id','=',$loginId]]);
            $material = mdMaterial::getMaterialGroup();
            if(!$material){
            	res_api('您尚未配置文案图片');
            }
            $cur = ['type'=>$type];
            if($data){
                $content = json_decode($data['material'],true);
                $cur['material'] = $content[0];
                $cur['hours'] = $data['hours'];
            }else{
                $random = $material['count'] > 1 ? mt_rand(1,$material['count'])-1 : 0;
                $cur['material'] = [
                    'title' => $material['title'][$random],
                    'picurl' => $material['cover'][$random],
                    'url' => '',
                    'description' => ''
                ];
                $cur['hours'] = 4;
            }
            $variable = [
                'cur' => $cur,
                'material' => $material,
                'backUrl' => my_url('index')
            ];
            return $this->fetch('common@push/doPush',$variable);
        }
    }
    
    //处理推送消息状态
    public function doEvent(){
    	global $loginId;
    	$rules = mdPush::getEventRules();
    	$data = myValidate::getData($rules);
        $res = mdPush::donePushEvent($data,$loginId);
        if($res){
            res_api();
        }else{
            res_api('操作失败，请重试');
        }
    }
    
}