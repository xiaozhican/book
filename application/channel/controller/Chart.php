<?php
namespace app\channel\controller;

use site\mySearch;
use app\common\model\mdChart;

class Chart extends Common{
	
	//用户统计
	public function member(){
		global $loginId;
		$data = mdChart::getMemberCount($loginId);
		$this->assign($data);
		return $this->fetch('common@chart/member');
	}
    
    //投诉统计
    public function complaint(){
       if($this->request->isAjax()){
           global $loginId;
           $config = ['default'=>[['a.channel_id','=',$loginId]],'like'=>'keyword:b.name'];
           $where = mySearch::getWhere($config);
           $list = mdChart::getComplaintList($where);
           res_table($list);
       }else{
       	
       		return $this->fetch('common@chart/complaint');
       }
    }
    
    //订单统计
    public function order(){
    	global $loginId;
        if($this->request->isAjax()){
        	$data = mdChart::getTodayOrderMsg($loginId);
        	res_table($data);
        }else{
        	$data = mdChart::getOrderCount($loginId);
        	return $this->fetch('common@chart/order',$data);
        }
    }
}