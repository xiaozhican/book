<?php
namespace app\channel\controller;

use site\myDb;
use site\myCache;
use site\myValidate;
use app\common\model\mdCharge;

class Charge extends Common{
	
	//充值配置
	public function index(){
		if($this->request->isAjax()){
			global $loginId;
			$status = myValidate::getData(['status'=>['in:1,2',['in'=>'未指定该状态']]],'get');
			$where = [['channel_id','=',$loginId]];
			if($status){
				$where[] = ['status','=',$status];
			}
			$list = myDb::getList('Charge', $where,'id,pid,status,is_hot,is_check',['sort_num'=>'desc']);
			if($list){
				$charge = myCache::getSiteCharge();
				foreach ($list as &$v){
					if(isset($charge[$v['pid']])){
						$v['is_on'] = 1;
						$v['type'] = $charge[$v['pid']]['type'];
						$v['money'] = $charge[$v['pid']]['money'];
						$v['content'] = $charge[$v['pid']]['content'];
						$v['pay_times'] = $charge[$v['pid']]['pay_times'];
					}else{
						$v['is_on'] = 2;
					}
				}
			}
			res_table($list);
		}else{
			
			return $this->fetch('common@charge/index-channel');
		}
	}
	
	//选择列表
	public function selectList(){
		if($this->request->isAjax()){
			$where = [['channel_id','=',0]];
			$list = myDb::getList('Charge', $where,'id,money,type,content,pay_times',['sort_num'=>'desc']);
			if($list){
				foreach ($list as &$v){
					$v['content'] = json_decode($v['content'],true);
				}
			}
			res_table($list);
		}else{
			return $this->fetch('common@charge/selectList');
		}
	}
	
	//新增套餐
	public function addCharge(){
		global $loginId;
		$rules = ['ids'=>['require|array',['require'=>'请选择要添加的套餐','array'=>'套餐格式错误']]];
		$ids = myValidate::getData($rules);
		$content = myCache::getSiteCharge();
		$data = [];
		$one = [
			'status' => 1,
			'channel_id' => $loginId
		];
		foreach ($ids as $v){
			$cur = myDb::getById('Charge',$v,'id,type');
			if(!$cur){
				res_api('所选套餐存在已删除记录，请重新选择');
			}
			$repeat = myDb::getValue('Charge', [['channel_id','=',$loginId],['pid','=',$cur['id']]],'id');
			if(!$repeat){
				$one['pid'] = $cur['id'];
				$one['type'] = $cur['type'];
				$one['sort_num'] = $cur['id'];
				$data[] = $one;
			}
		}
		if($data){
			$res = myDb::addAll('Charge', $data);
			if($res){
				myCache::rmCharge($loginId);
				res_api();
			}else{
				res_api('新增失败');
			}
		}else{
			res_api('没有数据被插入');
		}
	}
	
	//处理充值套餐事件
	public function doChargeEvent(){
		global $loginId;
		$data = mdCharge::getEventData();
		$cur = myDb::getById('Charge', $data['id'],'id,channel_id');
		if(!$cur || $cur['channel_id'] != $loginId){
			res_api('非法操作');
		}
		switch ($data['event']){
			case 'on':
			case 'off':
				$value = $data['event'] === 'on' ? 1 : 2;
				$re = myDb::setField('Charge', [['id','=',$cur['id']]], 'status', $value);
				break;
			case 'sethot':
				$re = mdCharge::setField($cur['id'],'is_hot',$loginId);
				break;
			case 'setcheck':
				$re = mdCharge::setField($cur['id'],'is_check',$loginId);
				break;
			case 'delete':
				$re = myDb::delById('Charge',$cur['id']);
				break;
			case 'sortUp':
			case 'sortDown':
				$re = mdCharge::doSort($data);
				break;
		}
		if($re){
			myCache::rmCharge($loginId);
			res_api();
		}else{
			res_api('操作失败,请重试');
		}
	}
}