<?php
namespace app\channel\controller;

use site\myHttp;
use site\mySearch;
use app\common\model\mdOrder;

class Order extends Common{
    
    //充值订单
    public function index(){
        if($this->request->isAjax()){
        	global $loginId;
        	$config = [
        		'eq' => 'status:status',
        		'like' => 'keyword:uid|order_no',
        		'between' => 'between_time:create_time',
        		'default' => [['status','between',[0,2]],['is_count','=',1],['channel_id','=',$loginId]],
        	];
        	$pages = myHttp::getPageParams();
        	$where = mySearch::getWhere($config);
        	$res = mdOrder::getChargeOrder($where, $pages);
        	res_table($res['data'],$res['count']);
        }else{
        	
        	return $this->fetch('common@order/index',['js'=>getJs('order.index','channel',['date'=>20200301])]);
        }
    }
    
}
