<?php
namespace app\channel\controller;

use site\myDb;
use site\myHttp;
use site\mySearch;
use app\common\model\mdMessage;

class Message extends Common{
    
    //渠道公告
    public function index(){
        if($this->request->isAjax()){
        	global $loginId;
            $config = [
                'default' => [['a.channel_id','=',$loginId]],
                'like' => 'keyword:b.title'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdMessage::getChannelMsgList($where, $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                    $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
                    $v['show_url'] = my_url('showInfo',['id'=>$v['id']]);
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            
        	return $this->fetch('common@message/index');
        }
    }
    
    //查看公告详情
    public function showInfo(){
    	global $loginId;
        $id = myHttp::getId('公告');
        $content = getBlockContent($id,'message');
        myDb::setField('MessageRead', [['message_id','=',$id],['channel_id','=',$loginId]], 'is_read', 1);
        return $this->fetch('common@message/showInfo',['content'=>$content]);
    }
    
}