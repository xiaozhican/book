<?php
namespace app\channel\controller;

use think\Controller;

class Common extends Controller{
    
    //初始化
    public function __construct(){
        parent::__construct();
        global $loginId;
        $loginId = session('CHANNEL_LOGIN_ID');
        if(!$loginId){
            $url = my_url('Login/index');
            if($this->request->isAjax()){
                res_api('登录已失效');
            }else{
                echo "<script language=\"javascript\">window.open('".$url."','_top');</script>";
                exit;
            }
        }
    }
    
    public function _empty(){
        res_api('页面不存在');
    }
}