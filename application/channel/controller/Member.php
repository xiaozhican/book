<?php
namespace app\channel\controller;

use site\myDb;
use site\myHttp;
use site\mySearch;
use site\myValidate;
use app\common\model\mdMember;
use app\common\model\mdFeedback;

class Member extends Common{
    
    //用户列表
    public function index(){
        if($this->request->isAjax()){
            global $loginId;
            $config = [
                'default' => [['a.status','between',[1,2]],['a.channel_id','=',$loginId]],
                'eq' => 'status:a.status,subscribe:a.subscribe',
                'like' => 'keyword:a.nickname|a.phone|a.id'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdMember::getMemberList($where, $pages);
            res_table($res['data'],$res['count']);
        }else{
            
            return $this->fetch('common@member/index',['js'=>getJs('member.index','channel')]);
        }
    }
    
    //用户详情
    public function info(){
    	global $loginId;
        $id = myHttp::getId('用户');
        $cur = myDb::getById('Member',$id,'id,nickname,headimgurl,money,total_money,subscribe,viptime,create_time');
        if(empty($cur)){
            res_api('用户信息异常');
        }
        $count = mdMember::getMemberCountMsg($id,$loginId);
        $cur['charge_money'] = $count['charge'];
        $cur['consume_money'] = $count['consume'];
        $variable = [
            'cur' => $cur,
            'url' => [
            	'charge' => my_url('getRecordList',['uid'=>$id,'type'=>1]),
            	'reward' => my_url('getRecordList',['uid'=>$id,'type'=>2]),
            	'read' => my_url('getRecordList',['uid'=>$id,'type'=>3]),
            	'sign' => my_url('getRecordList',['uid'=>$id,'type'=>4]),
            	'consume' => my_url('getRecordList',['uid'=>$id,'type'=>5]),
            	'invite' => my_url('getRecordList',['uid'=>$id,'type'=>6]),
            ],
        	'js' => getJs('member.info','channel',['date'=>'20200617'])
        ];
        return $this->fetch('common@member/info',$variable);
    }
    
    //获取各种记录列表
    public function getRecordList(){
    	global $loginId;
    	$rules = ['type'=>['require|between:1,6',['require'=>'请选择查看类型','between'=>'未指定该查看类型']]];
    	$type = myValidate::getData($rules,'get');
    	$pages = myHttp::getPageParams();
        switch ($type){
            case 1:
                $config = [
                    'default' => [['channel_id','=',$loginId],['is_count','=',1]],
                    'eq' => 'uid,status'
                ];
                $where = mySearch::getWhere($config);
                $res = mdMember::getChargeOrder($where,$pages);
                break;
            case 2:
                $config = ['eq' => 'uid'];
                $where = mySearch::getWhere($config);
                $res = mdMember::getRewardOrder($where,$pages);
                break;
            case 3:
            	$uid = $this->request->get('uid');
            	$list = mdMember::getReadHistory($uid);
            	$res = ['data'=>$list,'count'=>0];
                break;
            case 4:
                $config = ['eq' => 'uid'];
                $where = mySearch::getWhere($config);
                $res = mdMember::getSignList($where,$pages);
                break;
            case 5:
                $config = ['eq' => 'uid'];
                $where = mySearch::getWhere($config);
                $res = mdMember::getConsumeList($where,$pages);
                break;
            case 6:
            	$config = ['eq' => 'uid:a.from_uid'];
            	$where = mySearch::getWhere($config);
            	$res = mdMember::getInviteList($where,$pages);
            	break;
            default:
                res_api('请求数据异常');
                break;
        }
        res_table($res['data'],$res['count']);
    }
    
    //用户投诉列表
    public function complaint(){
        if($this->request->isAjax()){
            global $loginId;
            $config = [
                'default' => [['a.channel_id','=',$loginId]],
                'eq' => 'keyword:a.uid',
                'between' => 'between_time:a.create_time'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdFeedback::getComplaintList($where, $pages);
            res_table($res['data'],$res['count']);
        }else{
            
            return $this->fetch('common@member/feedback',['js'=>getJs('member.complaint')]);
        }
    }
    
}