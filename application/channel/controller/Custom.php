<?php
namespace app\channel\controller;

use weixin\wx;
use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use site\myValidate;
use app\common\model\mdCustom;
use app\common\model\mdSpread;
use app\common\model\mdMaterial;

class Custom extends Common{
	
	//客服消息
	public function index(){
		if($this->request->isAjax()){
			global $loginId;
			$config = [
				'eq' => 'status',
				'like' => 'keyword:name',
				'between' => 'between_time:send_time',
				'default' => [['channel_id','=',$loginId],['status','between','1,2'],['type','=',1]],
			];
			$pages = myHttp::getPageParams();
			$where = mySearch::getWhere($config);
			$res = myDb::getPageList('Custom',$where,'id,name,status,send_time,suc_num,fail_num,create_time',$pages);
			if($res['data']){
				foreach ($res['data'] as &$v){
					$v['send_time'] = date('Y-m-d H:i',$v['send_time']);
					$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
					$v['del_url'] = url('delCustom');
					$v['do_url'] = my_url('doCustom',['id'=>$v['id']]);
				}
			}
			res_table($res['data'],$res['count']);
		}else{
			
			return $this->fetch('common@index');
		}
	}
	
	//新增任务消息
	public function addCustom(){
		global $loginId;
		if($this->request->isAjax()){
			$data = mdCustom::getCustomData();
			$data['channel_id'] = $loginId;
			mdCustom::doneCustom($data);
		}else{
			$channel = myCache::getChannel($loginId);
			if(!$channel){
				res_error('渠道信息异常');
			}
			$rules = ['book_id' =>  ["number|gt:0",['number'=>'书籍参数错误',"gt"=>"书籍参数不规范"]]];
			$book_id = myValidate::getData($rules,'get');
			$backUrl = url('index');
			$url = '';
			if($book_id){
				$url = mdSpread::getSpreadUrl($loginId);
				$url .= '/index/Book/read.html?book_id='.$book_id;
				$backUrl = my_url('Novel/index');
			}
			$field = 'id,name,is_all:1,send_time';
			$cur = myDb::buildArr($field);
			$material = mdMaterial::getMaterialGroup();
			if(!$material){
				res_api('您尚未配置文案信息');
			}
			$random = $material['count'] > 1 ? mt_rand(1,$material['count'])-1 : 0;
			$cur['material'] = [
				'title' => $material['title'][$random],
				'picurl' => $material['cover'][$random],
				'url' => $url,
				'description' => ''
			];
			$cur['condition'] = myDb::buildArr('sex:-1,is_charge:-1,money:-1,subscribe_time:-1');
			$variable = [
				'cur' => $cur,
				'backUrl' => $backUrl,
				'material' => $material,
				'book_id' => $book_id ? : 0,
				'option' => mdCustom::getWhereOption(),
			];
			return $this->fetch('common@doCustom',$variable);
		}
	}
	
	//选择推广链接
	public function selLink(){
		if($this->request->isAjax()){
			global $loginId;
			$where = [['channel_id','=',$loginId],['status','=',1]];
			$list = myDb::getLimitList('Spread', $where,'id,name,book_name,url',20);
			$list = $list ? : [];
			res_table($list);
		}else{
			
			return $this->fetch('common@selLink');
		}
	}
	
	//更新客服消息
	public function doCustom(){
		if($this->request->isAjax()){
			global $loginId;
			$data = mdCustom::getCustomData(0);
			$data['channel_id'] = $loginId;
			mdCustom::doneCustom($data);
		}else{
			$id = myHttp::getId('客服消息');
			$cur = myDb::getById('Custom', $id);
			if(!$cur){
				res_api('该消息不存在');
			}
			switch ($cur['status']){
				case 1:
					res_api('该消息已发送，禁止编辑');
					break;
				case 3:
					res_api('该消息已被删除');
					break;
			}
			$material = mdMaterial::getMaterialGroup();
			if(!$material){
				res_api('您尚未配置文案信息');
			}
			$content = json_decode($cur['material'],true);
			$cur['material'] = $content[0];
			$cur['condition'] = json_decode($cur['condition'],true);
			$cur['send_time'] = date('Y-m-d H:i:s',$cur['send_time']);
			$variable = [
				'cur' => $cur,
				'backUrl' => url('index'),
				'book_id' => 0,
				'option' => mdCustom::getWhereOption(),
				'material' => $material
			];
			return $this->fetch('common@doCustom',$variable);
		}
	}
	
	//测试发送客服消息
	public function sendCustom(){
		global $loginId;
		$content = mdMaterial::getCustomMsg();
		$uid = myHttp::postId('用户','member_id');
		$member = myDb::getById('Member', $uid,'id,channel_id,openid');
		if(isset($member['openid']) && $member['openid']){
			if($member['channel_id'] != $loginId){
				res_api('该用户不属于您的公众号');
			}
			$config = myCache::getChannel($loginId);
			if(!$config['appid']){
				res_api('尚未配置微信参数');
			}
			wx::$config = $config;
			$res = wx::sendCustomMessage($member['openid'], $content);
			if($res){
				res_api();
			}else{
				res_api('发送失败');
			}
		}else{
			res_api('用户信息异常');
		}
	}
	
	//删除任务消息
	public function delCustom(){
		$id = myHttp::postId('客服消息');
		$re = myDb::setField('Custom', [['id','=',$id],['type','=',1]], 'status', 3);
		if($re){
			res_api();
		}else{
			res_api('删除失败');
		}
	}
	
	//获取筛选用户数量
	public function getUserCount(){
		global $loginId;
		$info = mdCustom::getSendWhere($loginId);
		$count = myDb::getCount('Member', $info['where']);
		res_api(['count'=>$count]);
	}
	
	//模版消息
	public function template(){
		if($this->request->isAjax()){
			global $loginId;
			$config = [
				'eq' => 'status',
				'like' => 'keyword:name',
				'between' => 'between_time:send_time',
				'default' => [['channel_id','=',$loginId],['status','between','1,2'],['type','=',2]],
			];
			$pages = myHttp::getPageParams();
			$where = mySearch::getWhere($config);
			$res = myDb::getPageList('Custom',$where,'id,name,status,suc_num,fail_num,send_time,create_time',$pages);
			if($res['data']){
				foreach ($res['data'] as &$v){
					$v['send_time'] = date('Y-m-d H:i',$v['send_time']);
					$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
					$v['del_url'] = url('delTemplate');
					$v['do_url'] = my_url('doTemplate',['id'=>$v['id']]);
				}
			}
			res_table($res['data'],$res['count']);
		}else{
			
			return $this->fetch('common@template');
		}
	}
	
	//新增模版消息
	public function addTemplate(){
		global $loginId;
		if($this->request->isAjax()){
			$data = mdCustom::getTemplateData();
			$data['channel_id'] = $loginId;
			mdCustom::doneTemplate($data);
		}else{
			$field = 'id,name,is_all:1,send_time';
			$cur = myDb::buildArr($field);
			$cur['content'] = ['url'=>'','template_id'=>''];
			$template = myDb::getList('ChannelTemplate', [['channel_id','=',$loginId]],'template_id as id,title as name');
			$cur['condition'] = myDb::buildArr('sex:-1,is_charge:-1,money:-1,subscribe_time:-1');
			$variable = [
				'cur' => $cur,
				'templates' => $template,
				'option' => mdCustom::getWhereOption(),
			];
			return $this->fetch('common@doTemplate',$variable);
		}
	}
	
	//编辑模版消息
	public function doTemplate(){
		global $loginId;
		if($this->request->isAjax()){
			$data = mdCustom::getTemplateData(0);
			$data['channel_id'] = $loginId;
			mdCustom::doneTemplate($data);
		}else{
			$id = myHttp::getId('模版消息');
			$cur = myDb::getById('Custom', $id);
			if(!$cur){
				res_error('该消息不存在');
			}
			if($cur['type'] != 2){
				res_error('消息数据有误');
			}
			switch ($cur['status']){
				case 1:
					res_api('该消息已发送，禁止编辑');
					break;
				case 3:
					res_api('该消息已被删除');
					break;
			}
			$template = myDb::getList('ChannelTemplate', [['channel_id','=',$loginId]],'template_id as id,title as name');
			$cur['content'] = json_decode($cur['material'],true);
			$cur['condition'] = json_decode($cur['condition'],true);
			$cur['send_time'] = date('Y-m-d H:i:s',$cur['send_time']);
			$variable = [
				'cur' => $cur,
				'templates' => $template,
				'option' => mdCustom::getWhereOption(),
				'template_html' => mdCustom::createTemplateHtml($cur['content']['template_id'],$cur['content']['data']),
			];
			return $this->fetch('common@doTemplate',$variable);
		}
	}
	
	//删除模版消息
	public function delTemplate(){
		$id = myHttp::postId('模版消息');
		$re = myDb::setField('Custom', [['id','=',$id],['type','=',2]], 'status', 3);
		if($re){
			res_api();
		}else{
			res_api('删除失败');
		}
	}
	
	//刷新模版
	public function doRefresh(){
		global $loginId;
		$config = myCache::getChannel($loginId);
		if(!$config['appid']){
			res_api('您尚未配置公众号信息');
		}
		wx::$config = $config;
		$template = wx::getTemplateList();
		$list = mdCustom::doSyncTemplate($template,$loginId);
		res_table($list);
	}
	
	//设置模版值
	public function setTemplateValue(){
		
		return $this->fetch('common@setTemplateValue');
	}
	
	//获取编辑消息框html
	public function getContentHtml(){
		$template_id = $this->request->post('template_id');
		if(!$template_id){
			res_api('模版参数异常');
		}
		$str = mdCustom::createTemplateHtml($template_id);
		res_table($str);
	}
	
	//测试发送模版消息
	public function sendTemplate(){
		global $loginId;
		$rules = [
			'url' => ['url',['url'=>'原文链接格式不规范']],
			'template_id' => ['require',['require'=>'未选择发送模版']],
			'content' => ['require|array',['require'=>'模版信息未配置','array'=>'模版信息格式不规范']],
			'member_id' => ['require|number|gt:0',['require'=>'请输入测试用户ID','number'=>'用户id格式不规范','gt'=>'用户ID格式不规范']]
		];
		$data = myValidate::getData($rules);
		$member = myDb::getById('Member',$data['member_id'],'id,channel_id,openid,nickname');
		if(isset($member['openid']) && $member['openid']){
			if($member['channel_id'] != $loginId){
				res_api('该用户不属于您的公众号');
			}
			$config = myCache::getChannel($loginId);
			if(!$config['appid']){
				res_api('尚未配置微信参数');
			}
			$content = $data['content'];
			foreach ($content as &$v){
				if(strpos($v['value'], '{username}') !== false){
					$v['value'] = str_replace('{username}', $member['nickname'], $v['value']);
				}
			}
			wx::$config = $config;
			$content = [
				'template_id' => $data['template_id'],
				'url' => $data['url'],
				'data' => $content
			];
			$res = wx::sendTemplateMessage($member['openid'], $content);
			if($res){
				res_api();
			}else{
				res_api('发送失败');
			}
		}else{
			res_api('用户信息异常');
		}
	}
}