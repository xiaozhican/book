<?php
namespace app\channel\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use app\common\model\mdActivity;
use app\common\model\mdSpread;

class Activity extends Common{
    
    //活动列表
    public function index(){
        if($this->request->isAjax()){
        	global $loginId;
            $config = [
                'default' => [['a.status','=',1]],
                'like' => 'keyword:a.name'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdActivity::getListInChannel($where,$pages);
            $time = time();
            if($res['data']){
            	$ids = myCache::getActivityDelIds($loginId);
            	$ids = $ids ? : [];
                foreach ($res['data'] as &$v){
                	$v['status'] = in_array($v['id'], $ids) ? 2 : 1;
                    if($time > $v['end_time']){
                        $v['between_time'] = '<font class="text-red">活动已结束</font>';
                    }elseif ($time < $v['start_time']){
                        $v['between_time'] = '<font class="text-red">活动未开始</font>';
                    }else{
                        $v['between_time'] = date('Y-m-d H:i',$v['start_time']).'~'.date('Y-m-d H:i',$v['end_time']);
                    }
                    $v['content'] = '充'.$v['money'].'元送'.$v['send_money'].'书币';
                    $v['copy_url'] = my_url('copyLink',['id'=>$v['id']]);
                    $v['first_str'] = $v['is_first'] == 1 ? '仅限一次' : '不限次数'; 
                }
            }
            res_table($res['data'],$res['count']);
        }else{
        	
        	return $this->fetch('common@activity/index',['js'=>getJs('activity.index','channel')]);
        }
    }
    
    //复制链接
    public function copyLink(){
    	global $loginId;
        $id = myHttp::getId('活动');
        $cur = myDb::getById('Activity',$id,'id,name');
        if(!$cur){
            res_api('活动参数错误');
        }
        $url = mdSpread::getSpreadUrl($loginId);
        $code = mdActivity::getActivityCode($cur['id'],$loginId);
        $short_url = '/Index/Activity/index.html?kc_code='.$code;
        $data = [
            'links' => [
                ['title'=>'活动链接','val'=>$url.$short_url]
            ]
        ];
        return $this->fetch('common@public/copyLink',$data);
    }
    
    //处理活动事件
    public function doActivityEvent(){
    	global $loginId;
    	$data = mdActivity::getEventData();
    	switch ($data['event']){
    		case 'on':
    			$status = 1;
    			break;
    		case 'off':
    			$status = 2;
    			break;
    		default:
    			res_api('未指定该事件');
    			break;
    	}
    	$where =  [['channel_id','=',$loginId],['act_id','=',$data['id']]];
    	$id = myDb::getValue('ActivityStatus',$where,'id');
    	if($id){
    		$flag = myDb::setField('ActivityStatus', [['id','=',$id]], 'status',$status);
    	}else{
    		$sData = ['status'=>$status,'act_id'=>$data['id'],'channel_id'=>$loginId];
    		$flag = myDb::add('ActivityStatus', $sData);
    	}
    	if($flag){
    		myCache::rmActivityDelIds($loginId);
    		res_api();
    	}else{
    		res_api('操作失败');
    	}
    }
    
}