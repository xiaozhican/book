<?php
namespace app\agent\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use app\common\model\mdSpread;
use app\common\model\mdActivity;


class Activity extends Common{
    
    //活动列表
    public function index(){
        if($this->request->isAjax()){
        	$key = parent::getCurKeyId();
            $config = [
                'default' => [['a.status','=',1]],
                'like' => 'keyword:a.name'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdActivity::getListInChannel($where,$pages,$key);
            $time = time();
            if($res['data']){
                foreach ($res['data'] as &$v){
                    if($time > $v['end_time']){
                        $v['between_time'] = '<font class="text-red">活动已结束</font>';
                    }elseif ($time < $v['start_time']){
                        $v['between_time'] = '<font class="text-red">活动未开始</font>';
                    }else{
                        $v['between_time'] = date('Y-m-d H:i',$v['start_time']).'~'.date('Y-m-d H:i',$v['end_time']);
                    }
                    $v['content'] = '充'.$v['money'].'元送'.$v['send_money'].'书币';
                    $v['copy_url'] = my_url('copyLink',['id'=>$v['id']]);
                    $v['first_str'] = $v['is_first'] == 1 ? '仅限一次' : '不限次数'; 
                }
            }
            res_table($res['data'],$res['count']);
        }else{
        	
            $js = getJs('activity.index','agent');
            return $this->fetch('common@activity/index',['js'=>$js]);
        }
    }
    
    //复制链接
    public function copyLink(){
    	global $loginId;
        $id = myHttp::getId('活动');
        $cur = myDb::getById('Activity',$id,'id,name');
        if(!$cur){
            res_api('活动参数错误');
        }
        $code = myCache::getActivityCode($id,$loginId);
        if(!$code){
        	res_api('初始化失败,请重试');
        }
        $url = mdSpread::getSpreadUrl($loginId);
        $url .= '/Index/Activity/index.html?kc_code='.$code;
        $data = [
            'links' => [
                ['title'=>'活动链接','val'=>$url]
            ]
        ];
        return $this->fetch('common@public/copyLink',$data);
    }
    
}