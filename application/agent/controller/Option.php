<?php
namespace app\agent\controller;

use site\myDb;

class Option extends Common{
    
    
    //选择推广链接
    public function selLink(){
    	if($this->request->isAjax()){
    		global $loginId;
    		$where = [['channel_id','=',$loginId],['status','=',1]];
    		$list = myDb::getLimitList('Spread', $where,'id,name,book_name,url',20);
    		$list = $list ? : [];
    		res_table($list);
    	}else{
    		
    		return $this->fetch('common@selLink');
    	}
    }
}