<?php
namespace app\agent\controller;

use site\myCache;
use think\Controller;

class Common extends Controller{
    
    //初始化
    public function __construct(){
        parent::__construct();
        global $loginId;
        $loginId = session('AGENT_LOGIN_ID');
        if(!$loginId){
            $url = my_url('Login/index');
            if($this->request->isAjax()){
                res_api('登录已失效');
            }else{
                echo "<script language=\"javascript\">window.open('".$url."','_top');</script>";
                exit;
            }
        }
    }
    
    //获取当前代理类别key
    protected function getCurKeyId(){
    	global $loginId;
    	$cur = myCache::getChannel($loginId);
    	$key = $cur['type'] == 1 ? 'channel_id' : 'agent_id';
    	return $key;
    }
    
    public function _empty(){
        res_api('页面不存在');
    }
}