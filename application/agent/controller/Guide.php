<?php
namespace app\agent\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\myValidate;
use app\common\model\mdBook;
use app\common\model\mdSpread;

class Guide extends Common{
	
	public function index(){
		$book_id = myHttp::getId('小说','book_id');
		$book = myCache::getBook($book_id);
		if(!$book){
			res_error('书籍信息异常');
		}
		$material = myDb::getList('Material',[]);
		if(empty($material)){
			res_api('您尚未配置文案信息');
		}
		$title = array_column($material, 'title');
		$cover = array_column($material, 'cover');
		$chatper = mdBook::getTenChapter($book_id);
		$ids = range(1,12);
		$variable = [
			'book' => $book,
			'titles' => $title,
			'covers' => $cover,
			'chapter' => $chatper,
			'template_ids' => $ids,
			'footer_ids' => $ids,
			'qr_ids' => range(1, 5)
		];
		return $this->fetch('common@index',$variable);
	}
	
	//获取章节列表
	public function getGuideChapter(){
		$template_id = myHttp::postId('正文模版','template_id');
		$template_ids = range(1, 12);
		if(!in_array($template_id,$template_ids)){
			res_api('未选择正确的模版');
		}
		$list = mdBook::getGuideChapter();
		$tpl = 'blockBody'.$template_id;
		$html = $this->fetch('common@'.$tpl,['list'=>$list]);
		$html = htmlspecialchars_decode($html);
		res_api(['info'=>$html]);
	}
	
	//推广链接
	public function createLink(){
		if($this->request->isAjax()){
			global $loginId;
			$rules = mdSpread::getRules();
			$data = myValidate::getData($rules);
			$data['channel_id'] = $loginId;
			mdSpread::doneSpread($data);
		}else{
			$rules = [
				'number' =>  ["require|number|between:1,10",["require"=>"请求章节异常",'number'=>'请求章节异常',"between"=>"请求章节异常"]],
				'book_id' =>  ["require|number|gt:0",["require"=>"书籍参数错误",'number'=>'书籍参数错误',"gt"=>"书籍参数错误"]],
			];
			$data = myValidate::getData($rules,'get');
			$book = myCache::getBook($data['book_id']);
			if(!$book){
				res_error('书籍不存在');
			}
			$options = mdSpread::getOptions($book['id']);
			$sub_number = $data['number'] + 5;
			$field = 'id,name,chapter_number:'.$data['number'].',click_type:1,number:'.$sub_number.',cost_money:0,remark';
			$options['cur'] = myDb::buildArr($field);
			$options['book'] = $book;
			$options['is_layer'] = 'yes';
			return $this->fetch('common@spread/createLink',$options);
		}
	}
	
	//编辑链接
	public function editLink(){
		if($this->request->isAjax()){
			$rules = mdSpread::getRules(0);
			$data = myValidate::getData($rules);
			mdSpread::doneSpread($data);
		}else{
			$id = myHttp::getId('推广链接','spread_id');
			$cur = myDb::getById('Spread',$id);
			if(!$cur){
				res_error('推广链接参数异常');
			}
			$book = myCache::getBook($cur['book_id']);
			if(!$book){
				res_error('书籍不存在');
			}
			$options = mdSpread::getOptions($cur['book_id']);
			$options['cur'] = $cur;
			$options['book'] = $book;
			$options['is_layer'] = 'yes';
			return $this->fetch('common@spread/createLink',$options);
		}
	}
	
	//生成二维码
	public function createQrcode(){
		$id = myHttp::postId('推广链接','spread_id');
		mdSpread::createQrcode($id);
		$file = './qrcode/'.$id.'/qrcode.png';
		if(@is_file($file)){
			res_api();
		}else{
			res_api('生成二维码失败');
		}
	}
}