<?php
namespace app\agent\controller;

use site\myDb;
use site\myCache;
use site\myValidate;
use app\common\model\mdLogin;
use app\common\model\mdMessage;
use app\common\model\mdConsole;
use app\common\model\mdSpread;

class Index extends Common
{
    
    //后台首页
    public function index(){
    	global $loginId;
        $cur = myCache::getChannel($loginId);
        $menu = myCache::getChannelMenu(3);
        if($cur['type'] == 1){
        	$menu[] = [
        		'name' => '代理管理',
        		'url' => '',
        		'icon' => 'layui-icon-app',
        		'child' => [
        			['name'=>'代理列表','url'=>'Agent/index'],
        			['name'=>'代理结算','url'=>'Agent/withdraw'],
       			]
        	];
        }
        $site_url = mdSpread::getSpreadUrl($loginId);
        $code = myCache::getAgentCode($loginId);
        if(!$code){
        	res_api('数据有误，请联系管理员');
        }
        $site_url .= '?kc_code='.$code;
        $variable = [
        	'cur' => $cur,
            'menu' => $menu,
        	'site_url' => $site_url,
            'site_name' => $cur['name']
        ];
        return $this->fetch('common@index/index-channel',$variable);
    }
    
    //设置是否接收消息
    public function setIsReceive(){
    	global $loginId;
    	$cur = myCache::getChannel($loginId);
    	if(!$cur){
    		res_api('登录信息异常');
    	}
    	$field = 'is_receive';
    	$is_receive = myValidate::getData(['is_receive'=>['require|in:1,2',['require'=>'参数错误','in'=>'参数错误']]]);
    	$res = myDb::setField('Channel', [['id','=',$loginId]], $field, $is_receive);
    	if($res){
    		myCache::doChannel($loginId,$field,$is_receive);
    		res_api();
    	}else{
    		res_api('设置失败');
    	}
    }
    
    //控制台
    public function console(){
    	global $loginId;
        $number = mdConsole::getAgentNumbersData();
        $key = parent::getCurKeyId();
        $charge_rank = mdConsole::getChargeRank([['a.status','=',2],['a.is_count','=',1],['a.'.$key,'=',$loginId]]);
        $variable = [
            'number' => $number,
            'charge_rank' => $charge_rank,
        ];
        return $this->fetch('common@index/console-agent',$variable);
    }
    
    //检查是否有未读公告
    public function getReadMessage(){
        $res = mdMessage::checkMessage();
        if($res['id'] > 0){
            $res['url'] = my_url('Message/showInfo',['id'=>$res['id']]);
        }
        res_api($res);
    }
    
    //获取用户趋势图
    public function getUserChartData(){
    	global $loginId;
        $data = mdConsole::getUserChartData($loginId);
        res_api($data);
    }
    
    
    //基础信息
    public function userinfo(){
        global $loginId;
        if($this->request->isAjax()){
        	$rules = ['name' =>  ["require|max:20",["require"=>"请输入用户名称",'max'=>'用户名称最多支持20个字符']]];
        	$name = myValidate::getData($rules);
            $re = myDb::setField('Channel',[['id','=',$loginId]], 'name', $name);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = myCache::getChannel($loginId);
            $login_msg = mdLogin::getLastLoginMsg($cur['login_name'],2);
            if($login_msg){
                $cur = array_merge($cur,$login_msg);
            }
            $variable = [
            	'cur' => $cur,
            	'js' => getJs('index.userinfo','channel')
            ];
            return $this->fetch('common@index/userinfo',$variable);
        }
    }
    
    //修改密码
    public function password(){
        if($this->request->isAjax()){
            global $loginId;
            $rules = [
            	"old_pwd" => ["require|length:6,16",["require"=>"请输入原密码","length"=>"请输入6到16位原密码"]],
            	"new_pwd" => ["require|length:6,16",["require"=>"请输入新密码","length"=>"请输入6到16位新密码"]],
            	"re_pwd"  => ["require|confirm:new_pwd",["require"=>"请再次输入新密码","confirm"=>"两次输入密码不一致"]],
            ];
            $data = myValidate::getData($rules);
            $cur = myDb::getById('Channel', $loginId,'id,password');
            if(createPwd($data['old_pwd']) !== $cur['password']){
                res_api('原密码输入不正确');
            }
            $password = createPwd($data['new_pwd']);
            $re = myDb::setField('Channel',[['id','=',$loginId]],'password', $password);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            
        	return $this->fetch('common@index/password');
        }
    }
    
    //退出登录
    public function logOut(){
        session('AGENT_LOGIN_ID',null);
        $url = my_url('Login/index');
        echo "<script language=\"javascript\">window.open('".$url."','_top');</script>";
        exit;
    }

}
