<?php
namespace app\agent\controller;

use site\myCache;
use site\myLinks;
use app\common\model\mdSpread;

class Links extends Common{
	
	//获取链接
	public function index(){
		global $loginId;
		$list = myLinks::getAll();
		$url = mdSpread::getSpreadUrl($loginId);
		$code = myCache::getAgentCode($loginId);
		foreach ($list as &$v){
			foreach ($v['links'] as &$val){
				$param = ['kc_code'=>$code];
				if(isset($val['param']) && !empty($val['param'])){
					$param = array_merge($param,$val['param']);
				}
				$str = $param ? '?'.http_build_query($param) : '';
				$val['url'] .= $str;
				$val['long_url'] = $url.$val['url'];
			}
		}
		return $this->fetch('common@wechat/links',['list'=>$list]);
	}
}