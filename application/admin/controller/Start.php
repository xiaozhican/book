<?php
namespace app\admin\controller;

use think\Db;
use think\Controller;
use site\myAliyunoss;

class Start extends Controller{
	
	private $images;
	private $ossConfig = [
		'url' => 'oss-cn-chengdu.aliyuncs.com',
		'bucket' => 'jztestbook',
		'accessKey' => 'LTAIp8UkXlKHvfST',
		'secretKey' => '4cTaUYAqHmfHennRmF1QB3fC031r15',
        'cdn' => ''
	];
	
	//执行程序初始化
	public function index(){
		$aliConfig = $this->ossConfig;
		if(!$aliConfig['accessKey']){
			res_error('尚未配置阿里云oss存储');
		}
		myAliyunoss::$config = $aliConfig;
		self::images();
		self::config();
	}
	
	//初始化图片
	public function images(){
		$imgs = [
			'category'   	=> ['src'=>'./static/resource/index/images/index_item1.png','savename'=>'images/common/icon1.png','url'=>''],
			'rank' 			=> ['src'=>'./static/resource/index/images/index_item2.png','savename'=>'images/common/icon2.png','url'=>''],
			'publish'  		=> ['src'=>'./static/resource/index/images/index_item3.png','savename'=>'images/common/icon3.png','url'=>''],
			'activity'  	=> ['src'=>'./static/resource/index/images/index_item4.png','savename'=>'images/common/icon4.png','url'=>''],
			'free'  		=> ['src'=>'./static/resource/index/images/index_item5.png','savename'=>'images/common/icon5.png','url'=>''],
		];
		foreach ($imgs as &$v){
			$url = myAliyunoss::putLocalFile($v['savename'], $v['src']);
			if(!$url){
				res_error('初始化图片过程遇到失败');
			}
			$v['url'] = $url;
		}
		$this->images = $imgs;
	}
	
	//初始化配置
	public function config(){
		$index_nav_man = [
				['src' => $this->images['category']['url'],'link'=>'/index/Book/category.html','text'=>'书库'],
				['src' => $this->images['rank']['url'],'link'=>'/index/Book/rank.html','text'=>'榜单'],
				['src' => $this->images['publish']['url'],'link'=>'/index/Book/publish.html?gender_type=1','text'=>'出版'],
				['src' => $this->images['activity']['url'],'link'=>'/index/Activity/cur.html','text'=>'活动'],
				['src' => $this->images['free']['url'],'link'=>'/index/Book/free.html?gender_type=1','text'=>'限免']
		];
		$index_nav_woman = [
				['src' => $this->images['category']['url'],'link'=>'/index/Book/category.html','text'=>'书库'],
				['src' => $this->images['rank']['url'],'link'=>'/index/Book/rank.html','text'=>'榜单'],
				['src' => $this->images['publish']['url'],'link'=>'/index/Book/publish.html?gender_type=2','text'=>'出版'],
				['src' => $this->images['activity']['url'],'link'=>'/index/Activity/cur.html','text'=>'活动'],
				['src' => $this->images['free']['url'],'link'=>'/index/Book/free.html?gender_type=2','text'=>'限免']
		];
		
		$config = [
				['key'=>'alioss','value'=>json_encode($this->ossConfig,JSON_UNESCAPED_UNICODE),'status'=>1],
				['key'=>'novel_category','value'=>'["都市", "言情", "校园", "总裁", "穿越", "历史", "异能", "玄幻", "武侠", "科幻", "恐怖", "悬疑", "灵异", "军事"]','status'=>1],
				['key'=>'novel_area','value'=>'["新书精选", "经典小说", "本周强推", "异能兵王", "豪门总裁", "美女上司", "穿越重生"]','status'=>1],
				['key'=>'index_nav_man','value'=>json_encode($index_nav_man,JSON_UNESCAPED_UNICODE),'status'=>1],
				['key'=>'index_nav_woman','value'=>json_encode($index_nav_woman,JSON_UNESCAPED_UNICODE),'status'=>1],
				['key'=>'wxjspay','value'=>'{"APPID": "wxa28dd265f2745121", "MCHID": "1512777761", "APIKEY": "22e3r3t4y5y6u7j7ue6ur6u6u5y4twt4"}','status'=>1],
				['key'=>'website','value'=>'{"ip": "47.108.50.210", "url": "test.baidu-yingxiao.com", "name": "江总旗舰版", "pay_type": "1", "is_location": "2", "share_money": "166", "location_url": ""}','status'=>1],
				['key'=>'weixin_param','value'=>'{"appid": "wxa28dd265f2745121", "qrcode": "https://jztestbook.oss-cn-chengdu.aliyuncs.com/images/20200203/3ef09be6b2707b17fa1fe2d52fadeedb.jpg", "apptoken": "123456", "appsecret": "341f2471e179631571f9a4d584aa3f85"}','status'=>1],
		];
		$res = [];
		foreach ($config as $v){
			$re = Db::name('Config')->insert($v);
			if($re){
				$res[] = $v['key'].'----->配置成功';
				switch ($v['key']){
					case 'index_nav_man':
						$html = $this->fetch('common@block/navMenu',['list'=>$index_nav_man]);
						saveBlock($html,$v['key'],'other');
						break;
					case 'index_nav_woman':
						$html = $this->fetch('common@block/navMenu',['list'=>$index_nav_woman]);
						saveBlock($html,$v['key'],'other');
						break;
				}
			}else{
				$res[] = $v['key'].'----->配置失败';
			}
		}
		my_print($res);
	}

	
	
	//初始化数据
	public function clearData(){
		if(true){
			$list = [
					['name'=>'activity'],
					['name'=>'activity_status'],
					['name'=>'book'],
					['name'=>'book_chapter'],
					['name'=>'book_free'],
					['name'=>'book_share'],
                    ['name'=>'book_sync'],
					['name'=>'channel'],
					['name'=>'channel_chart'],
					['name'=>'channel_money','data'=>[
						'channel_id' => 0
					]],
					['name'=>'channel_rel'],
					['name'=>'channel_template'],
					['name'=>'charge'],
					['name'=>'code'],
					['name'=>'comments'],
					['name'=>'comments_zan'],
					['name'=>'complaint'],
					['name'=>'config'],
					['name'=>'custom'],
					['name'=>'login_log'],
					['name'=>'manage','data'=>[
							'id' => 1,
							'role_id' => 0,
							'name' => '超级管理员',
							'login_name' => 'manage',
							'password' => createPwd(123456),
							'status' => 1,
							'create_time' => time()
					]],
					['name'=>'material'],
					['name'=>'member'],
					['name'=>'member_charge'],
					['name'=>'member_charge_log'],
					['name'=>'member_collect'],
					['name'=>'member_consume'],
					['name'=>'member_sign'],
					['name'=>'message'],
					['name'=>'message_read'],
					['name'=>'order'],
					['name'=>'order_reward'],
					['name'=>'plan_record'],
					['name'=>'plan_withdraw'],
					['name'=>'push_first_charge'],
					['name'=>'push_likes'],
					['name'=>'push_notpay'],
					['name'=>'push_read'],
					['name'=>'push_sub_after'],
					['name'=>'read_history'],
					['name'=>'role'],
					['name'=>'search_record'],
					['name'=>'share'],
					['name'=>'spread'],
					['name'=>'spread_chart'],
					['name'=>'spread_view'],
					['name'=>'task_member'],
					['name'=>'task_message'],
					['name'=>'task_order'],
					['name'=>'video'],
					['name'=>'vip_channel'],
					['name'=>'vip_user'],
					['name'=>'withdraw'],
					['name'=>'withdraw_merge'],
					['name'=>'wx_menu'],
					['name'=>'wx_reply']
			];
			$prefix = 'sy_';
			foreach ($list as $v){
				$table = $prefix.$v['name'];
				$sql = 'TRUNCATE TABLE '.$table;
				$re = Db::execute($sql);
				if($re !== false){
					if(isset($v['data'])){
						Db::name($v['name'])->insert($v['data']);
					}
				}
			}
		}
		echo 'ok';
		exit;
	}
}
