<?php
namespace app\admin\controller;

use app\common\model\mdLinks;
use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use site\myValidate;
use app\common\model\mdNode;
use app\common\model\mdConfig;

class System extends Common{

    //渠道菜单
    public function channelMenu(){
        if($this->request->isAjax()){
            $list = mdNode::getChannelNode(2);
            if($list){
                foreach ($list as &$v){
                    $v['add_url'] = my_url('addChannelMenu',['parent_id'=>$v['id']]);
                    $v['do_url'] = my_url('doChannelMenu',['id'=>$v['id']]);
                }
            }
            res_table($list);
        }else{
            $variable = ['add_url'=>url('addChannelMenu'),'event_url'=>url('doChannelMenuEvent')];
            return $this->fetch('common@system/menu',$variable);
        }
    }

    //添加节点
    public function addChannelMenu(){
        if($this->request->isAjax()){
            $rules = mdNode::getRules();
            $data = myValidate::getData($rules);
            $data['is_menu'] = 1;
            $data['type'] = 2;
            $re = mdNode::doneNodes($data);
            if($re){
                myCache::rmChannelMenu(2);
                res_api();
            }else{
                res_api('编辑节点失败');
            }
        }else{
            $field = 'id,pid,name,icon,url';
            $cur = myDb::buildArr($field);
            $cur['parent_name'] = '作为一级节点';
            $parent_id = myValidate::getData(['parent_id'=>['number',['number'=>'参数错误']]],'get');
            $parent_id = $parent_id ? : 0;
            if($parent_id){
                $parent = myDb::getById('Nodes',$parent_id,'id,name');
                $cur['parent_name'] = $parent['name'];
            }
            $cur['pid'] = $parent_id;
            $variable = [
                'cur' => $cur,
                'backUrl' => my_url('channelMenu')
            ];
            return $this->fetch('common@system/doMenu',$variable);
        }
    }

    //编辑节点
    public function doChannelMenu(){
        if($this->request->isAjax()){
            $rules = mdNode::getRules(0);
            $data = myValidate::getData($rules);
            $re = mdNode::doneNodes($data);
            if($re){
                myCache::rmChannelMenu(2);
                res_api();
            }else{
                res_api('编辑节点失败');
            }
        }else{
            $id = myHttp::getId('渠道菜单');
            $cur = myDb::getById('Nodes', $id);
            if(!$cur){
                res_api('节点不存在');
            }
            $cur['parent_name'] = '作为一级节点';
            if($cur['pid'] > 0){
                $parent = myDb::getById('Nodes',$cur['pid'],'id,name');
                $cur['parent_name'] = $parent['name'];
            }
            $variable = [
                'cur' => $cur,
                'backUrl' => my_url('channelMenu')
            ];
            return $this->fetch('common@system/doMenu',$variable);
        }
    }

    //处理节点排序及启用禁用
    public function doChannelMenuEvent(){
        $rules = mdNode::getEventRules();
        $data = myValidate::getData($rules);
        if(in_array($data['event'],['sortDown','sortUp'])){
            $re = mdNode::doNodeSort($data);
        }else{
            if(in_array($data['event'], ['on','off'])){
                $status = 2;
                if($data['event'] === 'on'){
                    $status = 1;
                }
                $re = myDb::setField('Nodes',[['id','=',$data['id']]], 'status',$status);
            }else{
                res_api('该按钮尚未绑定事件');
            }
        }
        if($re){
            myCache::rmChannelMenu(2);
            res_api();
        }else{
            res_api('操作失败,请重试');
        }
    }

    //代理菜单
    public function agentMenu(){
        if($this->request->isAjax()){
            $list = mdNode::getChannelNode(3);
            if($list){
                foreach ($list as &$v){
                    $v['add_url'] = my_url('addAgentMenu',['parent_id'=>$v['id']]);
                    $v['do_url'] = my_url('doAgentMenu',['id'=>$v['id']]);
                }
            }
            res_table($list);
        }else{
            $variable = ['add_url'=>url('addAgentMenu'),'event_url'=>url('doAgentMenuEvent')];
            return $this->fetch('common@system/menu',$variable);
        }
    }

    //添加节点
    public function addAgentMenu(){
        if($this->request->isAjax()){
            $rules = mdNode::getRules();
            $data = myValidate::getData($rules);
            $data['type'] = 3;
            $data['is_menu'] = 1;
            $re = mdNode::doneNodes($data);
            if($re){
                myCache::rmChannelMenu(3);
                res_api();
            }else{

                res_api('编辑节点失败');
            }
        }else{
            $field = 'id,pid,name,icon,url';
            $cur = myDb::buildArr($field);
            $cur['parent_name'] = '作为一级节点';
            $parent_id = myValidate::getData(['parent_id'=>['number',['number'=>'参数错误']]],'get');
            $parent_id = $parent_id ? : 0;
            if($parent_id){
                $parent = myDb::getById('Nodes',$parent_id,'id,name');
                $cur['parent_name'] = $parent['name'];
            }
            $cur['pid'] = $parent_id;
            $variable = [
                'cur' => $cur,
                'backUrl' => my_url('agentMenu')
            ];
            return $this->fetch('common@system/doMenu',$variable);
        }
    }

    //编辑节点
    public function doAgentMenu(){
        if($this->request->isAjax()){
            $rules = mdNode::getRules(0);
            $data = myValidate::getData($rules);
            $re = mdNode::doneNodes($data);
            if($re){
                myCache::rmChannelMenu(3);
                res_api();
            }else{
                res_api('编辑节点失败');
            }
        }else{
            $id = myHttp::getId('代理菜单');
            $cur = myDb::getById('Nodes', $id);
            if(!$cur){
                res_api('菜单不存在');
            }
            $cur['parent_name'] = '作为一级节点';
            if($cur['pid'] > 0){
                $parent = myDb::getById('Nodes',$cur['pid'],'id,name');
                $cur['parent_name'] = $parent['name'];
            }
            $variable = [
                'cur' => $cur,
                'backUrl' => my_url('agentMenu')
            ];
            return $this->fetch('common@system/doMenu',$variable);
        }
    }

    //处理节点排序及删除节点
    public function doAgentMenuEvent(){
        $rules = mdNode::getEventRules();
        $data = myValidate::getData($rules);
        if(in_array($data['event'],['sortDown','sortUp'])){
            $re = mdNode::doNodeSort($data);
        }else{
            if(in_array($data['event'], ['on','off'])){
                $status = 2;
                if($data['event'] === 'on'){
                    $status = 1;
                }
                $re = myDb::setField('Nodes',[['id','=',$data['id']]], 'status',$status);
            }else{
                res_api('该按钮尚未绑定事件');
            }
        }
        if($re){
            myCache::rmChannelMenu(3);
            res_api();
        }else{
            res_api('操作失败,请重试');
        }
    }

    //网址基础信息
    public function website(){
        $key = 'website';
        $cur = mdConfig::getConfig($key);
        if($this->request->isAjax()){
            $data = mdConfig::getWebsiteData();
            $res = mdConfig::saveConfig($key,$data);
            if($res){
                if($cur['url']){
                    myCache::rmUrlInfo($cur['url']);
                }
                if($cur['location_url']){
                    myCache::rmUrlInfo($cur['location_url']);
                }
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            if(!$cur){
                $field = 'logo,name,url,is_location:2,is_https:1,is_cartoon:1,ip,location_url,pay_type:1,app_pay_type:21,download_url,sms_type:1';
                $cur = myDb::buildArr($field);
                $re = mdConfig::addConfig($key, $cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }else{
                if(!isset($cur['app_pay_type'])){
                    $cur['app_pay_type'] = 21;
                }
                if (!isset($cur['sms_type'])){
                    $cur['sms_type'] = 1;
                }
                if (!isset($cur['is_https'])){
                    $cur['is_https'] = 1;
                }
            }
            $variable = mdConfig::getWebsiteOptions();
            $variable['cur'] = $cur;
            return $this->fetch('common@system/website',$variable);
        }
    }

    //分享配置
    public function share(){
        $key = 'site_share';
        if($this->request->isAjax()){
            $data = mdConfig::getShareData();
            $res = mdConfig::saveConfig($key,$data);
            $str = $res ? 'ok' : '保存失败';
            res_api($str);
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = ['bg'=>'','reg_money'=>'','title'=>'','desc'=>''];
                $re = mdConfig::addConfig($key, $cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }else{
                if(!isset($cur['title'])){
                    $cur['title'] = '';
                    $cur['desc'] = '';
                }
            }
            return $this->fetch('common@system/share',['cur'=>$cur]);
        }
    }

    //联系方式
    public function contact(){
        $key = 'site_contact';
        if($this->request->isAjax()){
            $data = mdConfig::getContactData();
            $res = mdConfig::saveConfig($key,$data);
            $str = $res ? 'ok' : '保存失败';
            res_api($str);
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = ['qrcode'=>'','tel'=>'','email'=>''];
                $re = mdConfig::addConfig($key, $cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            return $this->fetch('common@system/contact',['cur'=>$cur]);
        }
    }

    //签到配置
    public function sign(){
        $key = 'site_sign';
        if($this->request->isAjax()){
            $data = mdConfig::getSignData();
            $res = mdConfig::saveConfig($key,$data);
            $str = $res ? 'ok' : '保存失败';
            res_api($str);
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = ['is_sign'=>2];
                $cur['sign_config'] = ['day1'=>0,'day2'=>0,'day3'=>0,'day4'=>0,'day5'=>0,'day6'=>0,'day7'=>0];
                $re = mdConfig::addConfig($key, $cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            $variable = [
                'cur' => $cur,
                'sign' => ['name' => 'is_sign','option' => [['val'=>1,'text'=>'开启','default'=>1],['val'=>2,'text'=>'关闭','default'=>0]]]
            ];
            return $this->fetch('common@system/sign',$variable);
        }
    }

    //打赏金额配置
    public function reward(){
        $key = 'reward';
        if($this->request->isAjax()){
            $data = mdConfig::getRewardData();
            $res = mdConfig::saveConfig($key,$data);
            if($res){
                $html = $this->fetch('common@block/reward',['list'=>array_values($data)]);
                saveBlock($html, $key,'other');
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if($cur === false){
                $cur = [];
                $re = mdConfig::addConfig($key, $cur);
                if(!$re){
                    res_api('初始化数据失败，请重试');
                }
            }
            return $this->fetch('common@system/reward',['cur'=>$cur]);
        }
    }

    //充值金额配置
    public function charge(){
        $key = 'charge';
        if($this->request->isAjax()){
            $data = mdConfig::getChargeData();
            $res = mdConfig::saveConfig($key,$data);
            if($res){
                $html = $this->fetch('common@block/charge',['list'=>$data]);
                saveBlock($html,'charge_config','other');
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if($cur === false){
                $cur = [];
                $re = mdConfig::addConfig($key, $cur);
                if(!$re){
                    res_api('初始化数据失败，请重试');
                }
            }
            return $this->fetch('common@system/charge',['cur'=>$cur]);
        }
    }

    //微信公众号支付
    public function wxJsPay(){
        $key = 'wxjspay';
        if($this->request->isAjax()){
            $data = mdConfig::getWxPayData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = myDb::buildArr('APPID,MCHID,APIKEY');
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            return $this->fetch('common@system/wxPay',['cur'=>$cur]);
        }
    }

    //微信app支付
    public function wxappPay(){
        $key = 'wxapppay';
        if($this->request->isAjax()){
            $data = mdConfig::getWxAppPayData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = myDb::buildArr('APPID,MCHID,APIKEY,is_on');
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }else{
                if(!isset($cur['is_on'])){
                    $cur['is_on'] = 1;
                }
            }
            $is_on = [
                'name' => 'is_on',
                'option' => [
                    ['val'=>1,'text'=>'启用','default'=>1],
                    ['val'=>2,'text'=>'禁用','default'=>0]
                ]
            ];
            $variable = ['is_on'=>$is_on,'cur'=>$cur];
            return $this->fetch('common@system/wxAppPay',$variable);
        }
    }

    //支付宝APP配置
    public function alipayApp(){
        $key = 'aliapppay';
        if($this->request->isAjax()){
            $data = mdConfig::getAliAppPayData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = myDb::buildArr('appId,rsaPrivateKey,alipayrsaPublicKey,is_on');
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            $is_on = [
                'name' => 'is_on',
                'option' => [
                    ['val'=>1,'text'=>'启用','default'=>1],
                    ['val'=>2,'text'=>'禁用','default'=>0]
                ]
            ];
            $variable = ['is_on'=>$is_on,'cur'=>$cur];
            return $this->fetch('common@alipay',$variable);
        }
    }

    //支付宝扫码支付配置
    public function alipaySm(){
        $key = 'alismpay';
        if($this->request->isAjax()){
            $data = mdConfig::getAliAppPayData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = myDb::buildArr('appId,rsaPrivateKey,alipayrsaPublicKey,is_on');
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            $is_on = [
                'name' => 'is_on',
                'option' => [
                    ['val'=>1,'text'=>'启用','default'=>1],
                    ['val'=>2,'text'=>'禁用','default'=>0]
                ]
            ];
            $variable = ['is_on'=>$is_on,'cur'=>$cur];
            return $this->fetch('common@alipay',$variable);
        }
    }

    //payjq支付
    public function payjq(){
        $key = 'site_payjq';
        if($this->request->isAjax()){
            $rules = [
                'mchid' => ['require',['require'=>'请输入商户号']],
                'api_key' => ['require',['require'=>'请输入支付key']],
            ];
            $data = myValidate::getData($rules);
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $field = 'mchid,api_key';
                $cur = myDb::buildArr($field);
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            return $this->fetch('common@system/payjq',['cur'=>$cur]);
        }
    }

    //米花支付配置
    public function mihuaPay(){
        $key = 'mihuaPay';
        if($this->request->isAjax()){
            $data = mdConfig::getMihuaPayData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = myDb::buildArr('merAccount,merNo,privateKey,publicKey');
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            return $this->fetch('common@system/mihuaPay',['cur'=>$cur]);
        }
    }

    //赛迪短信接口配置
    public function message(){
        $key = 'saiyou_message';
        if($this->request->isAjax()){
            $data = mdConfig::getSaiyouData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = myDb::buildArr('appid,appkey,sign,content');
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            return $this->fetch('common@system/saiyou',['cur'=>$cur]);
        }
    }

    //dxton短信接口配置
    public function messageDxton(){
        $key = 'dxton_message';
        if($this->request->isAjax()){
            $data = mdConfig::getDxtonData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = myDb::buildArr('account,password,content');
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            return $this->fetch('common@system/dxton',['cur'=>$cur]);
        }
    }

    //阿里云oss配置
    public function alioss(){
        $key = 'alioss';
        if($this->request->isAjax()){
            $data = mdConfig::getAliossData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $field = 'accessKey,secretKey,bucket,url,cdn';
                $cur = myDb::buildArr($field);
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            return $this->fetch('common@system/alioss',['cur'=>$cur]);
        }
    }

    //版本管理
    public function version(){
        $key = 'site_version';
        if($this->request->isAjax()){
            $data = mdConfig::getVersionData();
            $re = mdConfig::saveConfig($key,$data);
            if($re){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $field = 'android_code,ios_code,android_download_url,ios_download_url,summary';
                $cur = myDb::buildArr($field);
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }
            return $this->fetch('common@system/version',['cur'=>$cur]);
        }
    }

    //app启动广告
    public function appStartImg(){
        $key = 'app_start_img';
        if($this->request->isAjax()){
            $rules = [
                'img' => ['require|url',['require'=>'请上传启动广告图片','url'=>'启动广告图片地址无效']],
                'logo' => ['require|url',['require'=>'请上传广告logo','url'=>'广告logo地址无效']],
                'sec' => ['require|number|gt:0',['require'=>'请输入广告展示秒数','number'=>'展示时间数大于0的数字','gt'=>'展示时间数大于0的数字']],
                'url' => ['require|url',['require'=>'请输入跳转链接','url'=>'跳转链接地址无效']],
                'is_on' => ['require|in:1,2',['require'=>'请选择开启状态','in'=>'未指定该状态']]
            ];
            $data = myValidate::getData($rules);
            $res = mdConfig::saveConfig($key,$data);
            if($res){
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if($cur === false){
                $cur = myDb::buildArr('img,logo,sec:3,url,is_on');
                $re = mdConfig::addConfig($key, $cur);
                if(!$re){
                    res_error('初始化数据失败，请重试');
                }
            }else{
                if(!isset($cur['is_on'])){
                    $cur['is_on'] = 1;
                }
            }
            $is_on = [
                'name' => 'is_on',
                'option' => [
                    ['val'=>1,'text'=>'启用','default'=>1],
                    ['val'=>2,'text'=>'禁用','default'=>0]
                ]
            ];
            $variable = ['is_on'=>$is_on,'cur'=>$cur];
            return $this->fetch('common@system/appStartImg',$variable);
        }
    }

    //协议管理
    public function doc(){
        $cate = $this->request->get('cate');
        $cate = in_array($cate, ['privacypolicy','agreement']) ? $cate : 'agreement';
        if($this->request->isAjax()){
            $content = $this->request->post('content');
            if(!$content){
                res_api('您尚未上传内容');
            }
            saveBlock($content, $cate,'other');
            res_api();
        }else{
            $content = getBlockContent($cate,'other');
            return $this->fetch('common@doc',['cate'=>$cate,'content'=>$content]);
        }
    }

    //友情链接列表
    public function links(){
        if($this->request->isAjax()){
            $pages = myHttp::getPageParams();
            $where = [];
            $res = mdLinks::getLinksList($where, $pages);
            res_table($res['data'],$res['count']);
        }else{

            return $this->fetch('common@system/links');
        }
    }

    //新增友情链接
    public function addLinks(){
        if($this->request->isAjax()){
            $rules = mdLinks::getRules();
            $data = myValidate::getData($rules);
            mdLinks::doneLinks($data);
        }else{
            $field = 'id,title,desc,url,sort';
            $option = mdLinks::getOptions();
            $option['cur'] = myDb::buildArr($field);
            return $this->fetch('common@system/doLinks',$option);
        }
    }

    //编辑友情链接
    public function doLinks(){
        if($this->request->isAjax()){
            $rules = mdLinks::getRules(0);
            $data = myValidate::getData($rules);
            mdLinks::doneLinks($data);
        }else{
            $id = myHttp::getId('友情链接');
            $cur = myDb::getById('Links',$id);
            if(!$cur){
                res_api('该友情链接不存在');
            }
            $option = mdLinks::getOptions();
            $option['cur'] = $cur;
            return $this->fetch('common@system/doLinks',$option);
        }
    }

    //处理友情链接事件
    public function doLinksEvent(){
        $rules = mdLinks::getEventRules();
        $data = myValidate::getData($rules);
        $key = 'status';
        if($data['event'] === 'delete'){
            $re = myDb::delById('Links', $data['id']);
        }else{
            switch ($data['event']){
                case 'on':
                    $value = 1;
                    break;
                case 'off':
                    $value = 2;
                    break;
                case 'resetpwd':
                    $key = 'password';
                    $value = createPwd(123456);
                    break;
            }
            $re = myDb::setField('Links', [['id','=',$data['id']]],$key, $value);
        }
        if($re){
            myCache::rmLinksList();
            res_api();
        }else{
            res_api('操作失败,请重试');
        }
    }
}
