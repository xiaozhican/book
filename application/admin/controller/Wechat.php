<?php
namespace app\admin\controller;

use weixin\wx;
use site\myDb;
use site\myHttp;
use site\myLinks;
use site\mySearch;
use site\myValidate;
use app\common\model\mdWx;
use app\common\model\mdConfig;
use app\common\model\mdSpread;
use app\common\model\mdMaterial;


class Wechat extends Common{
    
    //菜单列表
    public function menu(){
        if($this->request->isAjax()){
            $list = mdWx::getMenuList();
            if($list){
                foreach ($list as &$v){
                    $v['type_name'] = mdWx::getMenuTypeName($v['type']);
                    $v['value'] = '';
                    if($v['content']){
                        $content = json_decode($v['content'],true);
                        $v['value'] = $content['value'];
                    }
                    $v['do_url'] = my_url('doMenu',['id'=>$v['id']]);
                    $v['add_url'] = my_url('addMenu',['parent_id'=>$v['id']]);
                }
            }
            res_table($list);
        }else{
            
            return $this->fetch('common@wechat/menu');
        }
    }
    
    //添加菜单
    public function addMenu(){
        if($this->request->isAjax()){
        	$rules = mdWx::getMenuRules();
        	$data = myValidate::getData($rules);
        	$data['channel_id'] = 0;
            mdWx::doneMenu($data);
        }else{
            $field = 'id,pid,name,type:0,content';
            $cur = myDb::buildArr($field);
            $cur['parent_name'] = '作为顶级菜单';
            $parent_id = myValidate::getData(['parent_id'=>['number',['number'=>'参数错误']]],'get');
            $parent_id = $parent_id ? : 0;
            $count = mdWx::getMenuCount($parent_id);
            if($parent_id){
                $parent = myDb::getById('WxMenu',$parent_id,'id,name');
                if(!$parent){
                    res_api('顶级菜单不存在');
                }
                if($count >= 5){
                    res_api('最多允许添加5个子菜单');
                }
                $cur['parent_name'] = $parent['name'];
            }else{
                if($count >= 3){
                    res_api('最多允许添加3个顶级菜单');
                }
            }
            $option = mdWx::getMenuOption();
            $cur['pid'] = $parent_id;
            $cur['content'] = ['value'=>'','appid'=>''];
            $option['cur'] = $cur;
            $this->assign($option);
            return $this->fetch('common@wechat/doMenu');
        }
    }
    
    //编辑菜单
    public function doMenu(){
        if($this->request->isAjax()){
        	$rules = mdWx::getMenuRules(0);
        	$data = myValidate::getData($rules);
        	mdWx::doneMenu($data);
        }else{
            $id = myHttp::getId('菜单');
            $cur = myDb::getById('WxMenu', $id);
            if(!$cur){
                res_api('菜单不存在');
            }
            $cur['parent_name'] = '作为顶级菜单';
            if($cur['pid'] > 0){
                $parent = myDb::getById('WxMenu',$cur['pid'],'id,name');
                $cur['parent_name'] = $parent['name'];
            }
            $cur['content'] = $cur['content'] ? json_decode($cur['content'],true) : ['value'=>'','appid'=>''];
            $option = mdWx::getMenuOption();
            $option['cur'] = $cur;
            $this->assign($option);
            return $this->fetch('common@wechat/doMenu');
        }
    }
    
    //处理菜单排序及删除节点
    public function doMenuEvent(){
        $rules = mdWx::getMenuEventRules();
        $data = myValidate::getData($rules);
        if($data['event'] === 'delete'){
            $re = mdWx::deleteMenu($data['id']);
        }else{
            $re = mdWx::doMenuSort($data);
        }
        if($re){
            res_api();
        }else{
            res_api('操作失败,请重试');
        }
    }
    
    //发布菜单
    public function pushMenu(){
        $data = mdWx::getMenuList();
        $treeData = list_to_tree($data,'id','pid','child');
        $config = getMyConfig('weixin_param');
        wx::$config = $config;
        $res = wx::createMenu($treeData);
        if($res){
            res_api();
        }else{
            res_api('菜单发布失败');
        }
    }
    
    //创建默认微信菜单
    public function createDefaultMenu(){
    	$flag = mdWx::createDefaultMenu();
    	if($flag){
    		res_api();
    	}else{
    		res_api('设置默认菜单失败');
    	}
    }
    
    
    //自动回复
    public function reply(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['cate','=',1],['status','between',[1,2]],['channel_id','=',0]],
                'eq' => 'status',
                'like' => 'keyword'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = myDb::getPageList('WxReply',$where, 'id,keyword,type,status', $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                    $v['do_url'] = my_url('doReply',['id'=>$v['id']]);
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            
        	return $this->fetch('common@wechat/reply');
        }
    }
    
    //添加关键字
    public function addReply(){
        if($this->request->isAjax()){
        	$rules = mdWx::getReplyRules();
        	$data = myValidate::getData($rules);
        	$data['cate'] = 1;
            $re = mdWx::doneReply($data);
            if($re){
            	res_api();
            }else{
            	res_api('保存失败，请重试');
            }
        }else{
            $field = 'id,type,status,keyword,content';
            $option = mdWx::getReplyOption();
            $cur = myDb::buildArr($field);
            $material = mdMaterial::getMaterialGroup();
            if(!$material){
                res_api('您尚未配置文案信息');
            }
            $random = $material['count'] > 1 ? mt_rand(1,$material['count'])-1 : 0;
            $cur['material'] = [
                'title' => $material['title'][$random],
                'picurl' => $material['cover'][$random],
                'url' => '',
                'description' => ''
            ];
            $option['material'] = $material;
            $option['cur'] = $cur;
            $option['backUrl'] = my_url('reply');
            $this->assign($option);
            return $this->fetch('common@wechat/doReply');
        }
    }
    
    //编辑关键字
    public function doReply(){
        if($this->request->isAjax()){
            $rules = mdWx::getReplyRules(0);
            $data = myValidate::getData($rules);
            $data['cate'] = 1;
            $re = mdWx::doneReply($data);
            if($re){
            	res_api();
            }else{
            	res_api('保存失败，请重试');
            }
        }else{
            $id = myHttp::getId('关键字');
            $cur = myDb::getById('WxReply',$id,'id,type,status,keyword,content,material');
            if(!$cur){
                res_api('关键字不存在');
            }
            $option = mdWx::getReplyOption();
            $material = mdMaterial::getMaterialGroup();
            if(!$material){
                res_api('您尚未配置文案信息');
            }
            $cur['material'] = json_decode($cur['material'],true);
            if($cur['material']){
                $cur['material'] = $cur['material'][0];
            }else{
                $random = $material['count'] > 1 ? mt_rand(1,$material['count'])-1 : 0;
                $cur['material'] = [
                    'title' => $material['title'][$random],
                    'picurl' => $material['cover'][$random],
                    'url' => '',
                    'description' => ''
                ];
            }
            $option['material'] = $material;
            $option['cur'] = $cur;
            $option['backUrl'] = my_url('reply');
            $this->assign($option);
            return $this->fetch('common@wechat/doReply');
        }
    }
    
    
    
    //处理关键字事件
    public function doReplyEvent(){
        $rules = mdWx::getReplyEventRules();
        $data = myValidate::getData($rules);
        if(in_array($data['event'], ['on','off'])){
        	$status = $data['event'] === 'on' ? 1 : 2;
        	$re = myDb::setField('WxReply', [['id','=',$data['id']]], 'status', $status);
        }else{
        	if($data['event'] === 'delete'){
        		$re = myDb::delById('WxReply', $data['id']);
        	}
        }
        if($re){
        	res_api();
        }else{
        	res_api('操作失败,请重试');
        }
    }
    
    //特殊回复
    public function special(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['cate','=',2],['status','between',[1,2]],['channel_id','=',0]],
                'eq' => 'status',
                'like' => 'keyword'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = myDb::getPageList('WxReply',$where, 'id,keyword,type,status', $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                    $v['do_url'] = my_url('doSpecial',['id'=>$v['id']]);
                }
            }
            $this->success('ok','',$res['data'],$res['count']);
        }else{
            
        	return $this->fetch('common@wechat/special');
        }
    }
    
    //添加特殊回复
    public function addSpecial(){
        if($this->request->isAjax()){
        	$rules = mdWx::getReplyRules();
        	$data = myValidate::getData($rules);
        	$data['cate'] = 2;
        	$re = mdWx::doneReply($data);
        	if($re){
        		res_api();
        	}else{
        		res_api('保存失败，请重试');
        	}
        }else{
            $option = mdWx::getReplyOption();
            $cur = myDb::buildArr('id,type,status,keyword,content');
            $material = mdMaterial::getMaterialGroup();
            if(!$material){
                res_api('您尚未配置文案信息');
            }
            $random = $material['count'] > 1 ? mt_rand(1,$material['count'])-1 : 0;
            $cur['material'] = [
                'main' => [
                    'title' => $material['title'][$random],
                    'picurl' => $material['cover'][$random],
                    'url' => '',
                    'description' => ''
                ]
            ];
            $option['cur'] = $cur;
            $option['material'] = $material;
            $option['backUrl'] = my_url('special');
            $this->assign($option);
            return $this->fetch('common@wechat/doSpecial');
        }
    }
    
    //编辑特殊回复
    public function doSpecial(){
        if($this->request->isAjax()){
        	$rules = mdWx::getReplyRules(0);
        	$data = myValidate::getData($rules);
        	$data['cate'] = 2;
        	$re = mdWx::doneReply($data);
        	if($re){
        		res_api();
        	}else{
        		res_api('保存失败，请重试');
        	}
        }else{
            $id = myHttp::getId('回复');
            $cur = myDb::getById('WxReply',$id,'id,type,status,keyword,content,material');
            if(!$cur){
                res_api('关键字不存在');
            }
            $option = mdWx::getReplyOption();
            $material = mdMaterial::getMaterialGroup();
            if(!$material){
                res_api('您尚未配置文案信息');
            }
            $cur['material'] = json_decode($cur['material'],true);
            if($cur['material']){
                $main = $child = [];
                $key = 1;
                foreach ($cur['material'] as $v){
                    if($key == 1){
                        $main = $v;
                        $key++;
                    }else{
                        $child[] = $v;
                    }
                }
                $cur['material'] = ['main' => $main,'child' => $child];
            }else{
                $random = $material['count'] > 1 ? mt_rand(1,$material['count'])-1 : 0;
                $cur['material'] = [
                    'main' => [
                        'title' => $material['title'][$random],
                        'picurl' => $material['cover'][$random],
                        'url' => '',
                        'description' => ''
                    ]
                ];
            }
            $option['cur'] = $cur;
            $option['material'] = $material;
            $option['backUrl'] = my_url('special');
            $this->assign($option);
            return $this->fetch('common@wechat/doSpecial');
        }
    }
    
    //处理关键字事件
    public function doSpecialEvent(){
        $rules = mdWx::getReplyEventRules();
        $data = myValidate::getData($rules);
        if(in_array($data['event'], ['on','off'])){
        	$status = $data['event'] === 'on' ? 1 : 2;
        	$re = myDb::setField('WxReply', [['id','=',$data['id']]], 'status', $status);
        }else{
        	if($data['event'] === 'delete'){
        		$re = myDb::delById('WxReply', $data['id']);
        	}
        }
        if($re){
        	res_api();
        }else{
        	res_api('操作失败,请重试');
        }
    }
    
    //公众号参数配置
    public function param(){
        $key = 'weixin_param';
        if($this->request->isAjax()){
        	$rules = mdWx::getWxParamRules();
            $data = myValidate::getData($rules);
            $re = mdConfig::saveConfig($key, $data);
            if($re){
                $website = getMyConfig('website');
                if($website['url']){
                    cache(md5($website['url']),null);
                }
                if($website['location_url']){
                    cache(md5($website['location_url']),null);
                }
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if(!$cur){
                $cur = myDb::buildArr('appid,appsecret,apptoken,qrcode');
                $re = mdConfig::addConfig($key, $cur);
                if(!$re){
                    res_api('初始化数据失败，请重试');
                }
            }
            $website = getMyConfig('website');
            $ip = '';
            $url = $callback = '';
            if($website && $website['url']){
                $url = $website['url'];
                $callback = 'http://'.$url.'/Index/Open/callback.html';
                $ip = $website['ip'];
            }
            $variable = [
                'cur' => $cur,
                'url' => $url,
            	'ip' => $website['ip'],
                'callback' => $callback
            ];
            $this->assign($variable);
            return $this->fetch('common@wechat/param');
        }
    }
    
    //获取链接
    public function links(){
        $list = myLinks::getAll();
        $url = mdSpread::getSpreadUrl();
        foreach ($list as &$v){
            foreach ($v['links'] as &$val){
            	$param = [];
                if(isset($val['param']) && !empty($val['param'])){
                	$param = $val['param'];
                }
                $str = $param ? '?'.http_build_query($param) : '';
                $val['url'] .= $str;
                $val['long_url'] = $url.$val['url'];
            }
        }
        return $this->fetch('common@wechat/links',['list'=>$list]);
    }
}