<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use site\myValidate;
use app\common\model\mdMember;
use app\common\model\mdFeedback;

class Member extends Common{
    
    //用户列表
    public function index(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['a.status','between',[1,2]]],
                'eq' => 'status:a.status,subscribe:a.subscribe',
                'like' => 'keyword:a.id|a.nickname|a.phone'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdMember::getMemberList($where, $pages);
            res_table($res['data'],$res['count']);
        }else{
        	
            return $this->fetch('common@member/index',['js'=>getJs('member.index','admin',['v'=>1])]);
        }
    }
    
    //处理用户事件
    public function doMemberEvent(){
    	$rules = mdMember::getEventRules();
        $data = myValidate::getData($rules);
        $viptime = -1;
        $key = '';
        $value = 0;
        switch ($data['event']){
            case 'charge':
            	$money = $data['money'] ? : 0;
                if($money == 0){
                    res_api('用户书币未变动');
                }
                if($money < 0){
                    $cur = myDb::getById('Member',$data['id'],'id,money');
                    if(!$cur){
                        res_api('用户数据异常');
                    }
                    if($cur['money'] <= 0){
                        res_api('不能设置此调整书币金额');
                    }
                    $abs_money = abs($money);
                    if($abs_money > $cur['money']){
                        $money = '-'.$cur['money'];
                    }
                }
                $re = mdMember::setMemberMoney($data['id'], $money);
                break;
            case 'vipon':
            	$key = 'viptime';
                $month = $data['month'];
                $value = time()+$month*30*86400;
                $re = myDb::setField('Member', [['id','=',$data['id']]],'viptime',$value);
                if($re){
                	mdMember::addChargeLog($data['id'], '+ '.$month.' 月会员','平台赠送');
                }
                break;
            case 'vipoff':
            	$key = 'viptime';
            	$value = 0;
                $re = myDb::setField('Member', [['id','=',$data['id']]],'viptime',$value);
                break;
            case 'statuson':
            	$key = 'status';
            	$value = 1;
            	$re = myDb::setField('Member', [['id','=',$data['id']]],'status', $value);
            	break;
            case 'statusoff':
            	$key = 'status';
            	$value = 2;
            	$re = myDb::setField('Member', [['id','=',$data['id']]],'status', $value);
            	break;
            default:
                res_api('按钮绑定事件错误');
                break;
        }
        if($re){
        	if($key){
        		myCache::updateMember($data['id'], $key, $value);
        	}
            res_api();
        }else{
            res_api('操作失败,请重试');
        }
    }
    
    //用户详情
    public function info(){
        $id = myHttp::getId('用户');
        $cur = myDb::getById('Member',$id,'id,nickname,headimgurl,money,total_money,subscribe,viptime,create_time');
        if(empty($cur)){
            res_api('用户信息异常');
        }
        $count = mdMember::getMemberCountMsg($id);
        $cur['charge_money'] = $count['charge'];
        $cur['consume_money'] = $count['consume'];
        $variable = [
            'cur' => $cur,
            'url' => [
                'charge' => my_url('getRecordList',['uid'=>$id,'type'=>1]),
                'reward' => my_url('getRecordList',['uid'=>$id,'type'=>2]),
            	'read' => my_url('getRecordList',['uid'=>$id,'type'=>3]),
                'sign' => my_url('getRecordList',['uid'=>$id,'type'=>4]),
            	'consume' => my_url('getRecordList',['uid'=>$id,'type'=>5]),
            	'invite' => my_url('getRecordList',['uid'=>$id,'type'=>6]),
            ],
        	'js' => getJs('member.info','admin',['date'=>'20200617'])
        ];
        return $this->fetch('common@member/info',$variable);
    }
    
    //获取各种记录列表
    public function getRecordList(){
    	$rules = ['type'=>['require|between:1,6',['require'=>'请选择查看类型','between'=>'未指定该查看类型']]];
        $type = myValidate::getData($rules,'get');
        $pages = myHttp::getPageParams();
        switch ($type){
            case 1:
                $config = ['eq' => 'uid,status'];
                $where = mySearch::getWhere($config);
                $res = mdMember::getChargeOrder($where,$pages);
                break;
            case 2:
                $config = ['eq' => 'uid'];
                $where = mySearch::getWhere($config);
                $res = mdMember::getRewardOrder($where,$pages);
                break;
            case 3:
            	$uid = $this->request->get('uid');
            	$list = mdMember::getReadHistory($uid);
            	$res = ['data'=>$list,'count'=>0];
                break;
            case 4:
                $config = ['eq' => 'uid'];
                $where = mySearch::getWhere($config);
                $res = mdMember::getSignList($where,$pages);
                break;
            case 5:
                $config = ['eq' => 'uid'];
                $where = mySearch::getWhere($config);
                $res = mdMember::getConsumeList($where,$pages);
                break;
            case 6:
                $config = ['eq' => 'uid:a.from_uid'];
                $where = mySearch::getWhere($config);
                $res = mdMember::getInviteList($where,$pages);
                break;
            default:
                res_api('请求数据异常');
                break;
        }
        res_table($res['data'],$res['count']);
    }
    
    //用户投诉列表
    public function complaint(){
        if($this->request->isAjax()){
            $config = [
                'default' => [],
                'eq' => 'keyword:a.uid',
                'between' => 'between_time:a.create_time'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdFeedback::getComplaintList($where, $pages);
            res_table($res['data'],$res['count']);
        }else{
            $js = getJs('member.complaint');
            return $this->fetch('common@member/feedback',['js'=>$js]);
        }
    }
    
    //意见反馈
    public function feedback(){
    	if($this->request->isAjax()){
    		$config = [
    			'default' => [],
    			'eq' => 'keyword:a.uid',
    			'between' => 'between_time:a.create_time'
    		];
    		$pages = myHttp::getPageParams();
    		$where = mySearch::getWhere($config);
    		$res = mdFeedback::getFeedbackList($where, $pages);
    		res_table($res['data'],$res['count']);
    	}else{
    		$js = getJs('member.feedback','admin',['v'=>1]);
    		return $this->fetch('common@member/feedback',['js'=>$js]);
    	}
    }
    
    //采纳意见
    public function doAdopt(){
    	$rules = [
    		'id' => ['require|number|gt:0',['require'=>'意见参数异常','number'=>'意参数D格式不规范','gt'=>'意见参数格式不规范']],
    		'adopt_coin' => ['require|number|gt:0',['require'=>'请输入采纳奖励书币','number'=>'书币格式不规范','gt'=>'书币格式不规范']]
    	];
    	$data = myValidate::getData($rules);
    	$re = mdFeedback::doAdopt($data);
    	if($re){
    		res_api();
    	}else{
    		res_api('操作失败，请重试');
    	}
    }
    
}