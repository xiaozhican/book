<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\mySearch;
use site\myValidate;
use app\common\model\mdBook;
use site\myCache;

class Free extends Common{

    public function index(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['b.status','=',1]],
                'eq' => 'type:b.type',
                'like' => 'keyword:b.name'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdBook::getFreeBookList($where, $pages);
            if($res['data']){
                $date = date('Y-m-d');
                foreach ($res['data'] as &$v){
                    if($date >= $v['start_date'] && $date <= $v['end_date']){
                        $free_status = 1;
                    }else{
                        if($v['end_date'] > $date){
                            $free_status = 2;
                        }else{
                            $free_status = 3;
                        }
                    }
                    $v['free_status'] = $free_status;
                    $v['do_url'] = my_url('doFree',['id'=>$v['id']]);
                }
            }
            res_table($res['data'],$res['count']);
        }else{

            return $this->fetch('common@free/index');
        }
    }

    //公告资讯
    public function news(){
        if($this->request->isAjax()){
            $config = [
                'like' => 'keyword:title'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $where =[];
            $res = mdBook::getNewsList($where, $pages);
            res_table($res['data'],$res['count']);
        }else{

            return $this->fetch('common@free/news');
        }
    }

    //新增限免书籍
    public function addFree(){
        if($this->request->isAjax()){
            $rules = [
                'start_date' => ['require|date',['require'=>'请选择开始日期','date'=>'日期格式不正确']],
                'end_date' => ['require|date',['require'=>'请结束开始日期','date'=>'日期格式不正确']],
                'book_ids' => ['require|array',['require'=>'请选择书籍','array'=>'书籍格式不规范']],
            ];
            $date = date('Y-m-d');
            $data = myValidate::getData($rules);
            $bookIds = $data['book_ids'];
            unset($data['book_ids']);
            $num = 0;
            foreach ($bookIds as $v){
                if(is_numeric($v)){
                    $cur_id = myDb::getValue('BookFree', [['book_id','=',$v]], 'id');
                    if($cur_id){
                        myDb::save('BookFree', [['id','=',$cur_id]], $data);
                    }else{
                        $data['book_id'] = $v;
                        $re = myDb::add('BookFree', $data);
                        if($re){
                            $num++;
                        }
                    }
                }
            }
            myCache::rmFreeBookIds();
            res_api(['num'=>$num]);
        }else{

            return $this->fetch('common@free/addFree');
        }
    }

    //新增公告资讯
    public function addNews(){
        if($this->request->isAjax()){
            $rules = [
                'title' => ['require|min:2',['require'=>'资讯标题必须填写','min'=>'不少于两个字符']],
                'content' => ['min:5',['min'=>'不少于5个字符']],
            ];
            $data = myValidate::getData($rules);
            $data['create_time'] = time();
            $re = myDb::add('news', $data);
            if ($re){
                res_api();
            }else{
                res_api('新增失败',2);
            }
        }else{

            return $this->fetch('common@free/addNews');
        }
    }
	
	//选择书籍
	public function selBook(){
		if($this->request->isAjax()){
			$config = [
				'eq' => 'type',
				'like' => 'keyword:name',
				'default' => [['status','=',1],['free_type','=',2]],
			];
			$where = mySearch::getWhere($config);
			$list = myDb::getLimitList('Book', $where,'id,name,cover,free_chapter,money,hot_num',20);
			res_table($list);
		}else{
			
			return $this->fetch('common@free/selBook');
		}
	}

    //编辑限免书籍
    public function doFree(){
        if($this->request->isAjax()){
            $rules = [
                'start_date' => ['require|date',['require'=>'请选择开始日期','date'=>'日期格式不正确']],
                'end_date' => ['require|date',['require'=>'请结束开始日期','date'=>'日期格式不正确']],
                'id' => ['require|number|gt:0',['require'=>'参数错误','number'=>'参数错误','gt'=>'参数错误']],
            ];
            $data = myValidate::getData($rules);
            $date = date('Y-m-d');
            if($data['start_date'] > $data['end_date']){
                res_api('开始时间不能大于结束时间');
            }
            $re = myDb::saveIdData('BookFree', $data);
            if($re){
                myCache::rmFreeBookIds();
                res_api();
            }else{
                res_api('更新失败');
            }
        }else{
            $id = myHttp::getId('限免书籍');
            $cur = myDb::getById('BookFree', $id);
            $cur['book'] = myCache::getBook($cur['book_id']);
            return $this->fetch('common@free/doFree',['cur'=>$cur]);
        }
    }

    //编辑公告资讯
    public function doNews(){
        if($this->request->isAjax()){
            $rules = [
                'title' => ['require|min:2',['require'=>'资讯标题必须填写','min'=>'不少于两个字符']],
                'content' => ['min:5',['min'=>'不少于5个字符']],
                'id' => ['require|number|gt:0',['require'=>'参数错误','number'=>'参数错误','gt'=>'参数错误']],
            ];
            $data = myValidate::getData($rules);
            $re = myDb::saveIdData('news', $data);
            if($re){
                res_api();
            }else{
                res_api('更新失败');
            }
        }else{
            $id = myHttp::getId('公告资讯');
            $cur = myDb::getById('news', $id);
            return $this->fetch('common@free/doNews',['cur'=>$cur]);
        }
    }

    //删除限免书籍
    public function delFree(){
        $id = myHttp::postId('限免书籍');
        $re = myDb::delById('BookFree', $id);
        if($re){
            myCache::rmFreeBookIds();
            res_api();
        }else{
            res_api('删除失败');
        }
    }

    //删除公告资讯
    public function delNews(){
        $id = myHttp::postId('公告资讯');
        $re = myDb::delById('news', $id);
        if($re){
            res_api();
        }else{
            res_api('删除失败');
        }
    }
}