<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\mySearch;
use site\myValidate;
use app\common\model\mdMaterial;

class Material extends Common{
	
	//文案素材列表
	public function index(){
		if($this->request->isAjax()){
			$config = [
					'like' => 'keyword:title'
			];
			$pages = myHttp::getPageParams();
			$where = mySearch::getWhere($config);
			$res = myDb::getPageList('Material',$where, '*', $pages);
			if($res['data']){
				foreach ($res['data'] as &$v){
					$v['do_url'] = my_url('doMaterial',['id'=>$v['id']]);
				}
			}
			res_table($res['data'],$res['count']);
		}else{
			
			return $this->fetch('common@material/index');
		}
	}
	
	//新增素材
	public function addMaterial(){
		if($this->request->isAjax()){
			$rules = mdMaterial::getRules();
			$data = myValidate::getData($rules);
			mdMaterial::doneMaterial($data);
		}else{
			$field = 'id,title,cover';
			$cur = myDb::buildArr($field);
			$variable = [
					'cur' => $cur,
					'backUrl' => url('index')
			];
			return $this->fetch('common@material/doMaterial',$variable);
		}
	}
	
	//编辑素材
	public function doMaterial(){
		if($this->request->isAjax()){
			$rules = mdMaterial::getRules(0);
			$data = myValidate::getData($rules);
			mdMaterial::doneMaterial($data);
		}else{
			$id = myHttp::getId('素材');
			$cur = myDb::getById('Material',$id);
			if(!$cur){
				res_api('文案不存在');
			}
			$variable = [
					'cur' => $cur,
					'backUrl' => url('index')
			];
			return $this->fetch('common@material/doMaterial',$variable);
		}
	}
	
	//删除素材
	public function delMaterial(){
		$id = myHttp::postId('素材');
		$re = myDb::delById('Material',$id);
		if($re){
			res_api();
		}else{
			res_api('操作失败,请重试');
		}
	}
}