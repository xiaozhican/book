<?php
namespace app\admin\controller;

use app\common\model\mdNode;
use site\myValidate;
use site\myDb;

class Option extends Common{
    
    //获取更新角色页面节点列表
    public function getNodeSelectList(){
        $list = mdNode::getOptionList();
        if($list){
        	$role_id = myValidate::getData(['role_id'=>['number',['number'=>'参数错误']]]);
        	$role_id = $role_id ? : 0;
            if($role_id > 0){
                $cur_role = myDb::getById('Role',$role_id,'id,content');
                $ids = $cur_role['content'] ? json_decode($cur_role['content'],true) : [];
                if($ids){
                    foreach ($list as &$v){
                        if(in_array($v['id'], $ids)){
                            $v['checked'] = true;
                        }
                    }
                }
            }
            $lists = list_to_tree($list,'id','pid','children');
            res_table($lists);
        }else{
        	
            res_api('未发现数据');
        }
    }
    
    //选择推广链接
    public function selLink(){
    	if($this->request->isAjax()){
    		$where = [['channel_id','=',0],['status','=',1]];
    		$list = myDb::getLimitList('Spread', $where,'id,name,book_name,url',20);
    		$list = $list ? : [];
    		res_table($list);
    	}else{
    		
    		return $this->fetch('common@selLink');
    	}
    }
}