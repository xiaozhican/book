<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\mySearch;
use app\common\model\mdVip;

class Vip extends Common{
	
	//vip管理
	public function index(){
		if($this->request->isAjax()){
			$config = [
				'eq' => 'status',
				'like' => 'keyword:name',
				'default' => [['status','between',[1,2]]],
			];
			$pages = myHttp::getPageParams();
			$where = mySearch::getWhere($config);
			$res = myDb::getPageList('VipUser', $where, '*', $pages);
			if($res['data']){
				foreach ($res['data'] as &$v){
					$v['do_url'] = my_url('doUser',['id'=>$v['id']]);
				}
			}
			res_table($res['data'],$res['count']);
		}else{
			$config = getMyConfig('website');
			$url = ($config && $config['url']) ? 'http://'.$config['url'].'/vip' : '';
			return $this->fetch('common@vip/index',['url'=>$url]);
		}
	}
	
	//新增用户
	public function addUser(){
		if($this->request->isAjax()){
			$data = mdVip::getData();
			$data['create_time'] = time();
			$data['password'] = createPwd($data['password']);
			$re = myDb::add('VipUser', $data);
			$str = $re ? 'ok' : '新增失败';
			res_api($str);
		}else{
			$field = 'id,login_name,status,remark';
			$option = mdVip::getOptions();
			$option['cur'] = myDb::buildArr($field);
			return $this->fetch('common@vip/doUser',$option);
		}
	}
	
	//编辑用户
	public function doUser(){
		if($this->request->isAjax()){
			$data = mdVip::getData(0);
			$re = myDb::saveIdData('VipUser', $data);
			$str = $re ? 'ok' : '更新失败';
			res_api($str);
		}else{
			$id = myHttp::getId('用户');
			$cur = myDb::getById('VipUser',$id);
			if(!$cur){
				res_api('用户不存在');
			}
			$option = mdVip::getOptions();
			$option['cur'] = $cur;
			return $this->fetch('common@vip/doUser',$option);
		}
	}
	
	//处理用户事件
	public function doVipEvent(){
		$data = mdVip::getEventData();
		$key = 'status';
		if($data['event'] === 'delete'){
			$re = myDb::delById('VipUser', $data['id']);
		}else{
			switch ($data['event']){
				case 'on':
					$value = 1;
					break;
				case 'off':
					$value = 2;
					break;
				case 'resetpwd':
					$key = 'password';
					$value = createPwd(123456);
					break;
			}
			$re = myDb::setField('VipUser', [['id','=',$data['id']]],$key, $value);
		}
		if($re){
			res_api();
		}else{
			res_api('操作失败,请重试');
		}
	}
}