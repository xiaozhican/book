<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\mySearch;
use site\myValidate;
use app\common\model\mdComments;
use site\myCache;
use think\Db;


class Comments extends Common{

    //评论列表
    public function index(){
        if($this->request->isAjax()){
            $config = [
                'eq' => 'status:a.status',
                'like' => 'keyword:a.content',
                'default' => [['a.status','between',[0,2]]],
                'rules' => ['status'=>'between:0,2','type'=>'in:1,2,3']
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdComments::getCommentsList($where,$pages);
            res_table($res['data'],$res['count']);
        }else{

            return $this->fetch('common@comments/index');
        }
    }

    //处理评论事件
    public function doState(){
        $rules = [
            'id' =>  ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
            'event' => ["require|in:pass,refuse,on,off,delete",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']]
        ];
        $data = myValidate::getData($rules);
        $cur = myDb::getById('Comments', $data['id'],'id,pid');
        if(!$cur){
            res_api('评论不存在');
        }
        switch ($data['event']){
            case 'pass':
            case 'on':
                $status = 1;
                break;
            case 'refuse':
                $status = 3;
                break;
            case 'off':
                $status = 2;
                break;
            case 'delete':
                $status = 4;
                break;
            default:
                res_api('未指定该事件');
                break;
        }
        $re = myDb::setField('Comments', [['id','=',$data['id']]], 'status', $status);
        if($re){
            myCache::rmBookComments($cur['pid']);
            res_api();
        }else{
            res_api('操作失败');
        }
    }

    //pc首页导航
    public function pcNav(){
        if($this->request->isAjax()){
            $config =[];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdComments::getpcNavList($where,$pages);
            res_table($res['data'],$res['count']);
        }else{
            return $this->fetch('common@comments/pcNav');
        }
    }

    //编辑首页导航栏
    public function doPcnav(){
        if($this->request->isAjax()){
            $rules = [
                'name' => ['require|max:4',['require'=>'导航栏目名称必须填写','max'=>'栏目名称不多于4个字符']],
                'url' => ['min:1',['min'=>'不少于1个字符']],
                'is_show' => ['require|in:1,2',['require'=>'栏目状态必选','in'=>'栏目状态参数异常']],
                'key' => ['require|unique:pc_nav',['require'=>'active标识必填','unique'=>'active标识已经存在']],
                'id' => ['require|number|gt:0',['require'=>'参数错误','number'=>'参数错误','gt'=>'参数错误']],
            ];
            $data = myValidate::getData($rules);
            $re = myDb::saveIdData('pc_nav', $data);
            if($re){
                myCache::rmPcNavList();
                res_api();
            }else{
                res_api('更新失败');
            }
        }else{
            $id = myHttp::getId('首页导航栏目');
            $cur = myDb::getById('pc_nav', $id);
            if ($cur['pid'] == 0){
                $cur['pname'] = '顶级栏目';
            }else{
                $nav = myDb::getById('pc_nav', $cur['pid'],'name');
                $cur['pname'] = $nav['name'];
            }
            return $this->fetch('common@comments/doPcnav',['cur'=>$cur]);
        }
    }

    //添加首页导航栏子栏目
    public function addChildNav(){
        if($this->request->isAjax()){
            $rules = [
                'name' => ['require|max:4',['require'=>'导航栏目名称必须填写','max'=>'栏目名称不多于4个字符']],
                'url' => ['min:1',['min'=>'不少于1个字符']],
                'is_show' => ['require|in:1,2',['require'=>'栏目状态必选','in'=>'栏目状态参数异常']],
                'key' => ['require|unique:pc_nav',['require'=>'active标识必填','unique'=>'active标识已经存在']],
                'pid' => ['require|number|gt:0',['require'=>'参数错误','number'=>'参数错误','gt'=>'参数错误']],
            ];
            $data = myValidate::getData($rules);
            $re = myDb::add('pc_nav', $data);
            if($re){
                myCache::rmPcNavList();
                res_api();
            }else{
                res_api('新增子栏目失败');
            }
        }else{
            $id = myHttp::getId('首页导航栏目');
            $cur = myDb::getById('pc_nav', $id,'id,name');
            return $this->fetch('common@comments/addChildNav',['cur'=>$cur]);
        }
    }

    //处理PC首页导航事件
    public function doStates(){
        $rules = [
            'id' =>  ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
            'event' => ["require|in:pass,refuse,on,off,delete",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']]
        ];
        $data = myValidate::getData($rules);
        $cur = myDb::getById('pc_nav', $data['id'],'id');
        if(!$cur){
            res_api('栏目不存在');
        }
        switch ($data['event']){
            case 'on':
                $status = 1;
                break;
            case 'refuse':
                $status = 3;
                break;
            case 'off':
                $status = 2;
                break;
            case 'delete':
                $status = 4;
                break;
            default:
                res_api('未指定该事件');
                break;
        }
        if (in_array($status,[1,2])){
            $re = myDb::setField('pc_nav', [['id','=',$data['id']]], 'is_show', $status);
        }

        if ($status == 4){
            $res = Db::name('pc_nav')->where(['pid'=>$data['id']])->field('id')->find();
            if ($res){
                res_api('该栏目下存在子栏目，不允许删除');
            }else{
                $re = myDb::delById('pc_nav',$data['id']);
            }
        }

        if($re){
            myCache::rmPcNavList();
            res_api();
        }else{
            res_api('操作失败');
        }
    }

}