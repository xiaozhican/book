<?php
namespace app\admin\controller;

use site\myHttp;
use site\mySearch;
use app\common\model\mdOrder;

class Order extends Common{
    
    //充值订单
    public function index(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['status','between',[0,2]]],
                'eq' => 'status:status',
                'like' => 'keyword:uid|order_no',
                'between' => 'between_time:create_time'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdOrder::getChargeOrder($where, $pages);
            res_table($res['data'],$res['count']);
        }else{
            
            return $this->fetch('common@order/index',['js'=>getJs('order.index','admin',['date'=>20200618])]);
        }
    }

    //打赏订单
    public function reward(){
        if($this->request->isAjax()){
            $config = [
                'like' => 'keyword:uid',
                'between' => 'between_time:create_time'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdOrder::getRewardOrder($where, $pages);
            res_table($res['data'],$res['count']);
        }else{
            
        	return $this->fetch('common@order/reward',['js'=>getJs('order.reward','admin',['date'=>20200229])]);
        }
    }
    
}
