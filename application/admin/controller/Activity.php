<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use site\myValidate;
use app\common\model\mdSpread;
use app\common\model\mdActivity;

class Activity extends Common{
    
    //活动列表
    public function index(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['a.status','between',[1,2]]],
                'eq' => 'status:a.status',
                'like' => 'keyword:a.name',
                'rules' => ['status'=>'in:1,2']
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdActivity::getListInSite($where,$pages);
            $time = time();
            if($res['data']){
                foreach ($res['data'] as &$v){
                    if($time > $v['end_time']){
                        $v['between_time'] = '<font class="text-red">活动已结束</font>';
                    }elseif ($time < $v['start_time']){
                        $v['between_time'] = '<font class="text-red">活动未开始</font>';
                    }else{
                        $v['between_time'] = date('Y-m-d H:i',$v['start_time']).'~'.date('Y-m-d H:i',$v['end_time']);
                    }
                    $v['content'] = '充'.$v['money'].'元送'.$v['send_money'].'书币';
                    $v['do_url'] = my_url('doActivity',['id'=>$v['id']]);
                    $v['copy_url'] = my_url('copyLink',['id'=>$v['id']]);
                    $v['first_str'] = $v['is_first'] == 1 ? '仅限一次' : '不限次数'; 
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            
            return $this->fetch('common@activity/index-admin');
        }
    }
    
    //复制链接
    public function copyLink(){
        $id = myHttp::getId('活动');
        $cur = myDb::getById('Activity',$id,'id,name');
        if(!$cur){
            res_api('活动参数错误');
        }
        $code = myCache::getActivityCode($cur['id']);
        if(!$code){
        	res_api('初始化失败,请重试');
        }
        $url = mdSpread::getSpreadUrl();
        $short_url = '/Index/Activity/index.html?kc_code='.$code;
        $data = [
            'notice' => '温馨提示 : 相对链接只能应用到页面跳转链接中，如轮播图链接等，渠道用户点击后不会跳转到总站',
            'links' => [
                ['title'=>'相对链接','val'=>$short_url],
                ['title'=>'绝对链接','val'=>$url.$short_url]
            ]
        ];
        return $this->fetch('common@public/copyLink',$data);
    }
    
    //添加活动
    public function addActivity(){
    	if($this->request->isAjax()){
    		$rules = mdActivity::getRules();
    		$data = myValidate::getData($rules);
    		$flag = mdActivity::doneActivity($data);
    		if($flag){
    			myCache::rmLastActivity();
    		}
    		$str = $flag ? 'ok' : '更新失败';
    		res_api($str);
    	}else{
    		$field = 'id,name,money,send_money,status,is_first,start_time,end_time';
    		$option = mdActivity::getOptions();
    		$option['cur'] = myDb::buildArr($field);
    		return $this->fetch('common@activity/doActivity-admin',$option);
    	}
    }
    
    //更新活动
    public function doActivity(){
    	if($this->request->isAjax()){
    		$rules = mdActivity::getRules(0);
    		$data = myValidate::getData($rules);
    		$flag = mdActivity::doneActivity($data);
    		if($flag){
    			myCache::rmLastActivity();
    			myCache::rmActivity($data['id']);
    			res_api();
    		}else{
    			res_api('更新失败');
    		}
    	}else{
    		$id = myHttp::getId('活动');
    		$cur = myDb::getById('Activity', $id);
    		if(!$cur){
    			res_api('活动不存在');
    		}
    		$cur['start_time'] = date('Y-m-d H:i:s',$cur['start_time']);
    		$cur['end_time'] = date('Y-m-d H:i:s',$cur['end_time']);
    		$option = mdActivity::getOptions();
    		$option['cur'] = $cur;
    		return $this->fetch('common@activity/doActivity-admin',$option);
    	}
    }
    
    //处理活动事件
    public function doActivityEvent(){
    	$data = mdActivity::getEventData();
        switch ($data['event']){
            case 'on':
                $status = 1;
                break;
            case 'off':
                $status = 2;
                break;
            case 'delete':
                $status = 3;
                break;
            default:
                res_api('未指定该事件');
                break;
        }
        $flag = myDb::setField('Activity', [['id','=',$data['id']]], 'status',$status);
        if($flag){
        	myCache::rmActivity($data['id']);
        	res_api();
        }else{
        	res_api('操作失败');
        }
    }
    
}