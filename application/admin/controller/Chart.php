<?php
namespace app\admin\controller;

use site\mySearch;
use site\myValidate;
use app\common\model\mdChart;

class Chart extends Common{
    
    //充值统计
    public function charge(){
        if($this->request->isAjax()){
        	$rules = ['type' => ['require|between:1,2',['require'=>'请选择','between'=>'未指定该类型']]];
        	$type = myValidate::getData($rules,'get');
            $data = mdChart::getChargeList($type);
            $data = $data ? : [];
            res_table($data);
        }else{
        	$data = mdChart::getChargeData();
            return $this->fetch('common@chart/charge',['data'=>$data]);
        }
    }
    
    //投诉统计
    public function complaint(){
       if($this->request->isAjax()){
           $config = ['like'=>'keyword:b.name'];
           $where = mySearch::getWhere($config);
           $list = mdChart::getComplaintList($where);
           res_table($list);
       }else{
           
           return $this->fetch('common@chart/complaint');
       }
    }
    
    //用户统计
    public function member(){
    	
        $data = mdChart::getMemberCount();
        return $this->fetch('common@chart/member',$data);
    }
    
    //订单统计
    public function order(){
    	if($this->request->isAjax()){
    		$data = mdChart::getTodayOrderMsg();
    		res_table($data);
    	}else{
    		$data = mdChart::getOrderCount();
    		return $this->fetch('common@chart/order',$data);
    	}
    }
    
}