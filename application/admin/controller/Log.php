<?php
namespace app\admin\controller;

use think\Db;
use site\myHttp;
use site\mySearch;

class Log extends Common{
	
	//书籍同步日志
	public function book(){
		if($this->request->isAjax()){
			$config = [
				'like' => 'keyword:b.name',
				'between' => 'between_time:a.create_time'
			];
			$pages = myHttp::getPageParams();
			$where = mySearch::getWhere($config);
			$list = Db::name('BookCatLog a')
			->join('book b','a.book_id=b.id','left')
			->where($where)
			->field('a.*,b.name as book_name')
			->page($pages['page'],$pages['limit'])
			->order('a.id','desc')
			->select();
			$count = 0;
			if($list){
				foreach ($list as &$v){
					$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
				}
				$count = Db::name('BookCatLog a')
				->join('book b','a.book_id=b.id','left')
				->where($where)->count();
			}
			$key = 'area_update_ids11';
			cache($key,null);
			res_table($list,$count);
		}else{
			
			return $this->fetch('common@book');
		}
	}
	
}