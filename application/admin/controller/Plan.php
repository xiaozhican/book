<?php
namespace app\admin\controller;

use think\Db;
use weixin\wx;
use site\myCache;
use site\myBlock;
use think\Controller;
use function GuzzleHttp\json_decode;
use site\myDb;
use app\common\model\mdPlan;
use site\myPush;

class Plan extends Controller{
	
	/**
	#统计用户信息 凌晨5点
	0 5 * * * /usr/bin/curl localhost/admin/Plan/member.html
	#统计订单信息 凌晨5点10分
	10 5 * * * /usr/bin/curl localhost/admin/Plan/order.html
	#设置vip超时时间 每分钟
	* * * * * /usr/bin/curl localhost/admin/Plan/setViptime.html
	#发送客服消息 每分钟
	* * * * * /usr/bin/curl localhost/admin/Plan/sendCustomMessage.html
	#发送首冲提醒 每天 8点到11点 每分钟
	* 8-23 * * * /usr/bin/curl localhost/admin/Plan/pushFirstCharge.html
	#继续阅读提醒 每天8点到11点 每分钟
	* 8-23 * * * /usr/bin/curl localhost/admin/Plan/pushContinueRead.html
	#发送未支付提醒 每分钟
	* * * * * /usr/bin/curl localhost/admin/Plan/pushNotPay.html
	#发送猜你喜欢提醒 每天8点到11点 每分钟
	* 8-23 * * * /usr/bin/curl localhost/admin/Plan/pushLikes.html
	#更新站点缓存 每天5点20
	20 5 * * * /usr/bin/curl localhost/admin/Plan/refreshCache.html
	#自动结算 每日凌晨3点
	0 3 * * * /usr/bin/curl localhost/admin/Plan/doAutoWithdraw
	#统计系统数据 每小时59分
	59 * * * * /usr/bin/curl localhost/admin/Plan/doSysCount
	#延迟客服消息，每分钟
	* * * * * /usr/bin/curl localhost/admin/Plan/pushSubscribeAfter
	#推送热门书籍
	0 8 * * * /usr/bin/curl localhost/admin/Plan/bookPush
	* 
	 */
	
	public function __construct(){
		parent::__construct();
		$res = false;
		if('localhost' === $_SERVER['HTTP_HOST']){
			$res = true;
		}
		if(!$res){
			//echo '';exit;
		}
	}
	
	public function bookPush(){
		myPush::HotBookPush();
	}
	
    //统计用户信息，凌晨5点执行
    public function member(){
        set_time_limit(1800);
        $start_time = strtotime('yesterday');
        $end_time = $start_time + 86399;
        $create_date = date('Y-m-d',$start_time);
        $repeat = Db::name('TaskMember')->where('create_date','=',$create_date)->value('id');
        if($repeat){
        	exit;
        }
        $data = [];
        $list = Db::name('Member')->where('create_time','between',[$start_time,$end_time])->field('id,channel_id,agent_id,subscribe,subscribe_time,sex,is_charge')->select();
        if($list){
            foreach($list as $v){
                $sex = in_array($v['sex'], [1,2]) ? $v['sex'] : '0';
                $sex_key = 'sex'.$sex;
                $temp = ['create_date'=>$create_date,'channel_id'=>0,'add_num' => 1,'sub_num' => 0,'sex0' => 0,'sex1' => 0,'sex2' => 0,'charge_money' => 0,'charge_nums' => 0];
                if(!isset($data[0])){
                    $data[0] = $temp;
                }else{
                    $data[0]['add_num'] += 1;
                }
                $data[0][$sex_key] += 1;
                $channel_id = $v['channel_id'];
                $agent_id = $v['agent_id'];
                if ($channel_id){
                    if(!isset($data[$channel_id])){
                        $data[$channel_id] = $temp;
                        $data[$channel_id]['channel_id'] = $channel_id;
                    }else{
                        $data[$channel_id]['add_num'] += 1;
                    }
                    $data[$channel_id][$sex_key] += 1;
                }
                if ($agent_id){
                    if(!isset($data[$agent_id])){
                        $data[$agent_id] = $temp;
                        $data[$agent_id]['channel_id'] = $agent_id;
                    }else{
                        $data[$agent_id]['add_num'] += 1;
                    }
                    $data[$agent_id][$sex_key] += 1;
                }
                if($v['subscribe'] == 1 && ($start_time <= $v['subscribe_time'] && $end_time >= $v['subscribe_time'])){
                    $data[0]['sub_num'] += 1;
                    if($channel_id){
                        $data[$channel_id]['sub_num'] += 1;
                    }
                    if($agent_id){
                        $data[$agent_id]['sub_num'] += 1;
                    }
                }
                if($v['is_charge'] == 1){
                    $order = Db::name('Order')
                    ->where('uid','=',$v['id'])
                    ->where('status','=',2)
                    ->where('create_time','between',[$start_time,$end_time])
                    ->field('id,uid,channel_id,agent_id,money,is_count')
                    ->select();
                    if($order){
                        $channel_num = $agent_num = 0;
                        $data[0]['charge_nums'] += 1;
                        foreach ($order as $val){
                            $data[0]['charge_money'] += $val['money'];
                            if($channel_id > 0 && $channel_id == $val['channel_id'] && $val['is_count'] == 1){
                                $channel_num = 1;
                                $data[$channel_id]['charge_money'] += $val['money'];
                            }
                            if($agent_id > 0 && $agent_id == $val['agent_id'] && $val['is_count'] == 1){
                                $agent_num = 1;
                                $data[$agent_id]['charge_money'] += $val['money'];
                            }
                        }
                        if($channel_num){
                            $data[$channel_id]['charge_nums'] += $channel_num;
                        }
                        if($agent_num){
                            $data[$agent_id]['charge_nums'] += $agent_num;
                        }
                    }
                }
            }
        }
        if($data){
            Db::name('TaskMember')->insertAll($data);
        }
        
    }
    
    //统计订单信息，凌晨5点10分执行
    public function order(){
        set_time_limit(1800);
        $start_time = strtotime('yesterday');
        $end_time = $start_time + 86399;
        $create_date = date('Y-m-d',$start_time);
        $repeat = Db::name('TaskOrder')->where('create_date','=',$create_date)->value('id');
        if($repeat){
        	exit;
        }
        $data = [];
        $list = Db::name('Order')->where('create_time','between',[$start_time,$end_time])->field('id,type,channel_id,agent_id,package,uid,status,money,is_count')->select();
        if($list){
            foreach($list as $v){
                $temp = ['create_date'=>$create_date,'channel_id'=>0,'n_pay'=>0,'n_notpay'=>0,'n_money'=>0,'n_user'=>0,'n_rate'=>0,'p_pay'=>0,'p_notpay'=>0,'p_money'=>0,'p_user'=>0,'p_rate'=>0,'total_money'=>0,'type1_money'=>0,'type2_money'=>0,'puids'=>[],'nuids'=>[]];
                if(!isset($data[0])){
                    $data[0] = $temp;
                }
                $channel_id = $v['channel_id'];
                if($channel_id){
                    if(!isset($data[$channel_id])){
                        $data[$channel_id] = $temp;
                        $data[$channel_id]['channel_id'] = $channel_id; 
                    }
                }
                $agent_id = $v['agent_id'];
                if($agent_id){
                    if(!isset($data[$agent_id])){
                        $data[$agent_id] = $temp;
                        $data[$agent_id]['channel_id'] = $agent_id; 
                    }
                }
                if($v['status'] == 2){
                    if($v['package'] > 0){
                        $data[0]['p_pay'] += 1;
                        $data[0]['p_money'] += $v['money'];
                        if(!in_array($v['uid'], $data[0]['puids'])){
                            $data[0]['p_user'] += 1;
                            $data[0]['puids'][] = $v['uid'];
                        }
                        if($channel_id && $v['is_count'] == 1){
                            $data[$channel_id]['p_pay'] += 1;
                            $data[$channel_id]['p_money'] += $v['money'];
                            if(!in_array($v['uid'], $data[$channel_id]['puids'])){
                                $data[$channel_id]['p_user'] += 1;
                                $data[$channel_id]['puids'][] = $v['uid'];
                            }
                        }
                        if($agent_id && $v['is_count'] == 1){
                            $data[$agent_id]['p_pay'] += 1;
                            $data[$agent_id]['p_money'] += $v['money'];
                            if(!in_array($v['uid'], $data[$agent_id]['puids'])){
                                $data[$agent_id]['p_user'] += 1;
                                $data[$agent_id]['puids'][] = $v['uid'];
                            }
                        }
                    }else{
                        $data[0]['n_pay'] += 1;
                        $data[0]['n_money'] += $v['money'];
                        if(!in_array($v['uid'], $data[0]['nuids'])){
                            $data[0]['n_user'] += 1;
                            $data[0]['nuids'][] = $v['uid'];
                        }
                        if($channel_id && $v['is_count'] == 1){
                            $data[$channel_id]['n_pay'] += 1;
                            $data[$channel_id]['n_money'] += $v['money'];
                            if(!in_array($v['uid'], $data[$channel_id]['nuids'])){
                                $data[$channel_id]['n_user'] += 1;
                                $data[$channel_id]['nuids'][] = $v['uid'];
                            }
                        }
                        if($agent_id && $v['is_count'] == 1){
                            $data[$agent_id]['n_pay'] += 1;
                            $data[$agent_id]['n_money'] += $v['money'];
                            if(!in_array($v['uid'], $data[$agent_id]['nuids'])){
                                $data[$agent_id]['n_user'] += 1;
                                $data[$agent_id]['nuids'][] = $v['uid'];
                            }
                        }
                    }
                    $type_key = in_array($v['type'], [1,2]) ? 'type'.$v['type'].'_money' : 'type1_money';
                    $data[0][$type_key] += $v['money'];
                    $data[0]['total_money'] += $v['money'];
                    if($channel_id && $v['is_count'] == 1){
                        $data[$channel_id]['total_money'] += $v['money'];
                        $data[$channel_id][$type_key] += $v['money'];
                    }
                    if($agent_id && $v['is_count'] == 1){
                        $data[$agent_id]['total_money'] += $v['money'];
                        $data[$agent_id][$type_key] += $v['money'];
                    }
                }else{
                    if($v['package'] > 0){
                        $data[0]['p_notpay'] += 1;
                        if($channel_id){
                            $data[$channel_id]['p_notpay'] += 1;
                        }
                        if($agent_id){
                            $data[$agent_id]['p_notpay'] += 1;
                        }
                    }else{
                        $data[0]['n_notpay'] += 1;
                        if($channel_id){
                            $data[$channel_id]['n_notpay'] += 1;
                        }
                        if($agent_id){
                            $data[$agent_id]['n_notpay'] += 1;
                        }
                    }
                }
            }
        }
        if($data){
            foreach ($data as &$val){
                unset($val['puids']);
                unset($val['nuids']);
                if($val['n_notpay'] || $val['n_pay']){
                    $val['n_rate'] = round($val['n_pay']/($val['n_pay'] + $val['n_notpay']),2)*100;
                }
                if($val['p_notpay'] || $val['p_pay']){
                    $val['p_rate'] = round($val['p_pay']/($val['p_pay'] + $val['p_notpay']),2)*100;
                }
            }
            Db::name('TaskOrder')->insertAll($data);
        }
    }
    
    //vip超时设置
    public function setVipTime(){
    	$time = time();
    	$over_ids = Db::name('Member')->where('viptime','>',1)->where('viptime','<',$time)->limit(100)->column('id');
    	if(!empty($over_ids)){
    		Db::name('Member')->where('id','in',$over_ids)->setField('viptime',0);
    		foreach ($over_ids as $v){
    			$key = 'member_info_'.$v;
    			cache($key,null);
    		}
    	}
    }
    
    //发送客服消息,每分钟执行
    public function sendCustomMessage(){
    	set_time_limit(1800);
        $start = date('Y-m-d H:i').':00';
        $start_time = strtotime($start);
        $end_time = $start_time+59;
        $list = Db::name('Custom')->where('status','=',2)->where('send_time','between',[$start_time,$end_time])->field('id,type,channel_id,material,where')->select();
        $temp = [];
        if(!empty($list)){
            foreach ($list as $v){
                if($v['where'] && $v['material']){
                    $where = json_decode($v['where'],true);
                    $content = json_decode($v['material'],true);
                    if($where && $content){
                    	$config = null;
                        if(!isset($temp[$v['channel_id']])){
                            if($v['channel_id'] > 0){
                                $channel = myCache::getChannel($v['channel_id']);
                                if($channel && $channel['appid'] && $channel['appsecret']){
                                	$config = ['appid'=>$channel['appid'],'appsecret'=>$channel['appsecret']];
                                }
                            }else{
                            	$website = getMyConfig('weixin_param');
                                if($website){
                                	$config = [
                                		'appid' => $website['appid'],
                                		'appsecret' => $website['appsecret']
                                	];
                                }
                            }
                            $temp[$v['channel_id']] = $config;
                        }else{
                            $config = $temp[$v['channel_id']];
                        }
                        if($config){
                        	wx::$config = $config;
                        	Db::name('Custom')->where('id','=',$v['id'])->setField('status',1);
                        	$member = Db::name('Member')->where($where)->field('id,openid,nickname')->select();
                        	if(!empty($member)){
                        		self::sendToMember($member,$content,$v['id'],$v['type']);
                        	}
                        }
                    }
                }
            }
        }
    }
    
    //向用户发送消息
    private function sendToMember($member,$content,$custom_id,$type){
        foreach ($member as $v){
        	if($type == 1){
        		$re = wx::sendCustomMessage($v['openid'], $content);
        	}else{
        		$data = $content['data'];
        		foreach ($data as &$val){
        			if(strpos($val['value'], '{username}') !== false){
        				$val['value'] = str_replace('{username}', $v['nickname'], $val['value']);
        			}
        		}
        		$content['data'] = $data;
        		$re = wx::sendTemplateMessage($v['openid'], $content);
        	}
            $field = $re ? 'suc_num' : 'fail_num';
            Db::name('Custom')->where('id',$custom_id)->setInc($field);
        }
    }
    
    //首冲提醒
    public function pushFirstCharge(){
    	set_time_limit(60);
    	$task = Db::name('TaskMessage')->where('type','=',1)->where('status','=',1)->field('id,channel_id,hours,material')->select();
    	if($task){
    		$time = time();
    		foreach ($task as $v){
    			$config = [];
    			if($v['channel_id'] > 0){
    				$channel = myCache::getChannel($v['channel_id']);
    				if($channel && $channel['appid'] && $channel['appsecret']){
    					$config = ['appid'=>$channel['appid'],'appsecret'=>$channel['appsecret']];
    				}
    			}else{
    				$weixin = getMyConfig('weixin_param');
    				if($weixin){
    					$config = ['appid' => $weixin['appid'],'appsecret' => $weixin['appsecret']];
    				}
    			}
    			if($config){
    				wx::$config = $config;
    				$message = json_decode($v['material'],true);
    				$before_time = $time - 3600 * $v['hours'];
    				$where = [['a.subscribe','=',1],['a.subscribe_time','<',$before_time],['a.is_charge','=',2],['a.wx_id','=',$v['channel_id']]];
    				$ulist = Db::name('Member a')
    				->join('push_first_charge b','a.id=b.uid','left')
    				->where($where)
    				->field('a.id,a.wx_id,a.openid,IFNULL(b.id,0) as push_id')
    				->having('push_id=0')
    				->limit(100)
    				->select();
    				if($ulist){
    					foreach ($ulist as $uv){
    						wx::sendCustomMessage($uv['openid'], $message);
    						Db::name('PushFirstCharge')->insert(['uid'=>$uv['id'],'create_time'=>time()]);
    					}
    				}
    			}
    		}
    	}
    }
    
    //延迟客服消息
    public function pushSubscribeAfter(){
    	set_time_limit(60);
    	$task = Db::name('TaskMessage')->where('type','=',5)->where('status','=',1)->field('id,channel_id,hours,material')->select();
    	if($task){
    		$time = time();
    		foreach ($task as $v){
    			$config = [];
    			if($v['channel_id'] > 0){
    				$channel = myCache::getChannel($v['channel_id']);
    				if($channel && $channel['appid'] && $channel['appsecret']){
    					$config = ['appid'=>$channel['appid'],'appsecret'=>$channel['appsecret']];
    				}
    			}else{
    				$weixin = getMyConfig('weixin_param');
    				if($weixin){
    					$config = ['appid' => $weixin['appid'],'appsecret' => $weixin['appsecret']];
    				}
    			}
    			if($config){
    				wx::$config = $config;
    				$message = json_decode($v['material'],true);
    				$before_time = $time - 3600 * $v['hours'];
    				$where = [['a.subscribe','=',1],['a.subscribe_time','<',$before_time],['a.is_charge','=',2],['a.wx_id','=',$v['channel_id']]];
    				$ulist = Db::name('Member a')
    				->join('push_sub_after b','a.id=b.uid','left')
    				->where($where)
    				->field('a.id,a.wx_id,a.openid,IFNULL(b.id,0) as push_id')
    				->having('push_id=0')
    				->limit(100)
    				->select();
    				if($ulist){
    					foreach ($ulist as $uv){
    						wx::sendCustomMessage($uv['openid'], $message);
    						Db::name('PushSubAfter')->insert(['uid'=>$uv['id'],'create_time'=>time()]);
    					}
    				}
    			}
    		}
    	}
    }
    
    //继续阅读提醒
    public function pushContinueRead(){
    	set_time_limit(60);
    	$task = Db::name('TaskMessage')->where('type','=',2)->where('status','=',1)->field('id,channel_id')->select();
    	if(!empty($task)){
    		$time = time() - 28800;
    		$channel_ids = array_column($task,'channel_id');
    		$where = [
    			['a.channel_id','in',$channel_ids],
    			['a.is_end','=',1],
    			['a.create_time','<',$time],
    			['a.type','between',[1,2]]
    		];
    		$list = Db::name('ReadHistory a')
    		->join('PushRead b','a.id=b.rid','left')
    		->where($where)
    		->field('a.id,a.uid,IFNULL(b.id,0) as push_id')
    		->having('push_id=0')
    		->limit(100)
    		->select();
    		if(!empty($list)){
    			$temp = [];
    			foreach ($list as $v){
    				$cur = Db::name('ReadHistory a')
    				->join('member b','a.uid=b.id and b.subscribe=1')
    				->join('book c','a.book_id=c.id')
    				->where('a.id','=',$v['id'])
    				->field('a.id,a.book_id,a.number,a.channel_id,a.uid,b.openid,c.name,c.cover')
    				->find();
    				if($cur){
    					$config = null;
    					if(!isset($temp[$cur['channel_id']])){
    						if($cur['channel_id'] > 0){
    							$channel = myCache::getChannel($cur['channel_id']);
    							if($channel && $channel['appid'] && $channel['appsecret']){
    								$config = ['appid'=>$channel['appid'],'appsecret'=>$channel['appsecret'],'url'=>$channel['url']];
    							}
    						}else{
    							$weixin = getMyConfig('weixin_param');
    							if($weixin){
    								$website = getMyConfig('website');
    								if($website){
    									$config = [
    										'url' => $website['url'],
    										'appid' => $weixin['appid'],
    										'appsecret' => $weixin['appsecret']
    									];
    								}
    							}
    						}
    						$temp[$cur['channel_id']] = $config;
    					}else{
    						$config = $temp[$cur['channel_id']];
    					}
    					if($config){
    						$content = [
    							[
    								'title' => $cur['name'],
    								'description' => '您已经超过8小时未阅读该书籍了，点我继续阅读吧',
    								'picurl' => $cur['cover'],
    								'url' => 'http://'.$config['url'].'/index/Book/read.html?book_id='.$cur['book_id'].'&number='.$cur['number']
    							]
    						];
    						wx::$config = $config;
    						wx::sendCustomMessage($cur['openid'],$content);
    					}
    				}
    				Db::name('PushRead')->insert(['rid'=>$v['id'],'uid'=>$v['uid'],'create_time'=>time()]);
    			}
    		}
    	}
    }
    
    //未支付提醒
    public function pushNotPay(){
    	set_time_limit(60);
    	$task = Db::name('TaskMessage')->where('type','=',3)->where('status','=',1)->field('id,channel_id')->select();
    	if(!empty($task)){
    		$end_time = time() - 1800;
    		$start_time = $end_time - 360;
    		$channel_ids = array_column($task,'channel_id');
    		$where = [
    			['create_time','between',[$start_time,$end_time]],
    			['status','=',1],
    			['type','=',1],
    			['wx_id','in',$channel_ids]
    		];
    		$field = 'max(id) as id,uid';
    		$list = Db::name('Order')->where($where)->field($field)->group('uid')->limit(100)->order('id','desc')->select();
    		if($list){
    			$temp = [];
    			$website = getMyConfig('website');
    			if(!$website){
    				exit('');
    			}
    			foreach ($list as $v){
    				$repeat = Db::name('PushNotpay')->where('order_id','>=',$v['id'])->where('uid','=',$v['uid'])->value('id');
    				if(!$repeat){
    					$cur = Db::name('Order a')
    					->join('member b','a.uid=b.id and b.subscribe=1')
    					->where('a.id','=',$v['id'])
    					->field('a.id,a.pay_type,a.pay_url,a.wx_id,a.order_no,b.openid')
    					->find();
    					if($cur){
    						$config = null;
    						if(!isset($temp[$cur['wx_id']])){
    							if($cur['wx_id'] > 0){
    								$channel = myCache::getChannel($cur['wx_id']);
    								if($channel && $channel['appid'] && $channel['appsecret']){
    									$config = ['appid'=>$channel['appid'],'appsecret'=>$channel['appsecret'],'url'=>$channel['url']];
    								}
    							}else{
    								$weixin = getMyConfig('weixin_param');
    								if($weixin){
    									$config = [
    										'url' => $website['url'],
    										'appid' => $weixin['appid'],
    										'appsecret' => $weixin['appsecret']
    									];
    								}
    							}
    							$temp[$cur['wx_id']] = $config;
    						}else{
    							$config = $temp[$cur['wx_id']];
    						}
    						if($config){
    							$html = "书币订单未支付提醒；";
    							$html.= "\n";
    							$html.="亲，您的充值书币订单还未完成，点我立即支付该笔订单！";
    							$html.="\n\n";
    							$back_url = 'http://'.$config['url'].'/index/User/index.html';
    							$back_url = urlencode($back_url);
    							$url = '';
    							switch ($cur['pay_type']){
    								case 1:
    									$url = "http://".$website['url']."/index/Pay/jsPay.html?table=order&order_no=".$cur['order_no'].'&back_url='.$back_url;
    									break;
    								case 2:
    									$url = $cur['pay_url'];
    									break;
    							}
    							if($url){
    								$a = '<a href="'.$url.'">点击支付></a>';
    								$html.=$a;
    								wx::$config = $config;
    								$re = wx::sendCustomMessage($cur['openid'],$html,'text');
    							}
    						}
    					}
    					Db::name('PushNotpay')->insert(['order_id'=>$v['id'],'uid'=>$v['uid'],'create_time'=>time()]);
    				}
    			}
    		}
    	}
    }
    
    //猜你喜欢推送
    public function pushLikes(){
    	set_time_limit(60);
    	$task = Db::name('TaskMessage')->where('type','=',4)->where('status','=',1)->field('id,channel_id')->select();
    	if(!empty($task)){
    		$time = time() - 86400;
    		$channel_ids = array_column($task,'channel_id');
    		$where = [
    			['a.channel_id','in',$channel_ids],
    			['a.is_end','=',1],
    			['a.create_time','<',$time],
    		];
    		$list = Db::name('ReadHistory a')
    		->join('PushLikes b','a.id=b.rid','left')
    		->where($where)
    		->field('a.id,a.uid,IFNULL(b.id,0) as push_id')
    		->having('push_id=0')
    		->limit(100)
    		->select();
    		if(!empty($list)){
    			$temp = [];
    			foreach ($list as $v){
    				$cur = Db::name('ReadHistory a')
    				->join('member b','a.uid=b.id and b.subscribe=1')
    				->join('book c','a.book_id=c.id')
    				->where('a.id','=',$v['id'])
    				->field('a.id,a.book_id,a.channel_id,a.uid,b.openid,c.type,c.category')
    				->find();
    				if($cur){
    					$config = null;
    					if(!isset($temp[$cur['channel_id']])){
    						if($cur['channel_id'] > 0){
    							$channel = myCache::getChannel($cur['channel_id']);
    							if($channel && $channel['appid'] && $channel['appsecret']){
    								$config = ['appid'=>$channel['appid'],'appsecret'=>$channel['appsecret'],'url'=>$channel['url']];
    							}
    						}else{
    							$weixin = getMyConfig('weixin_param');
    							if($weixin){
    								$website = getMyConfig('website');
    								if($website){
    									$config = [
    										'url' => $website['url'],
    										'appid' => $weixin['appid'],
    										'appsecret' => $weixin['appsecret']
    									];
    								}
    							}
    						}
    						$temp[$cur['channel_id']] = $config;
    					}else{
    						$config = $temp[$cur['channel_id']];
    					}
    					if($config){
    						$books = Db::name('Book')
    						->where('status','=',1)
    						->where('category','=',$cur['category'])
    						->where('type','=',$cur['type'])
    						->field('id,name')
    						->limit(3)
    						->select();
    						if($books){
    							$html = "猜您喜欢；";
    							$html.= "\n";
    							$html.="根据您的喜好，我们为您推荐了您感兴趣的新书！";
    							$html.="\n\n";
    							foreach ($books as $lv){
    								$url = "http://".$config['url']."/index/Book/info.html?book_id=".$lv['id'];
    								$a = '➢  <a href="'.$url.'">'.$lv['name'].'></a>';
    								$html .= $a;
    								$html .= "\n\n";
    							}
    							wx::$config = $config;
    							$re = wx::sendCustomMessage($cur['openid'],$html,'text');
    						}
    					}
    				}
    				Db::name('PushLikes')->insert(['rid'=>$v['id'],'uid'=>$v['uid'],'create_time'=>time()]);
    			}
    		}
    	}
    }
    
    //刷新站点缓存
    public function refreshCache(){
    	myCache::clearAreaIds(1);
    	myBlock::createIndexHtml(1,1,'novel_content_man');
    	myBlock::createIndexHtml(1,2,'novel_content_woman');
    	myCache::clearAreaIds(2);
    	myBlock::createIndexHtml(2,1,'cartoon_content_man');
    	myBlock::createIndexHtml(2,2,'cartoon_content_woman');
    	myBlock::createBookRank(1,1);
    	myBlock::createBookRank(1,2);
    	myBlock::createBookRank(2,1);
    	myBlock::createBookRank(2,2);
    	myCache::rmBookPublishIds(1,1);
    	myCache::rmBookPublishIds(1,2);
    	myCache::rmBookPublishIds(2,1);
    	myCache::rmBookPublishIds(2,2);
    	myCache::rmBookOverType1Ids(1,1,1);
    	myCache::rmBookOverType1Ids(2,2,1);
    	myCache::rmBookOverType2Ids(1,1,2);
    	myCache::rmBookOverType2Ids(2,2,2);
    }
    
    //自动结算
    public function doAutoWithdraw(){
    	$date = date('Y-m-d');
    	$list = Db::name('ChannelChart')->where('create_date','<',$date)->where('status',2)->where('channel_id','>',0)->field('id,channel_id,charge_money,charge_num,income_money,create_date')->select();
    	if($list){
    		$accounts = [];
    		foreach ($list as $v){
    			$data = [];
    			if($v['income_money']){
    				$channel_id = $v['channel_id'];
    				$channel = myCache::getChannel($channel_id);
    				if($channel){
    					$data = [
    						'channel_id' => $channel_id,
    						'to_channel_id' => $channel['parent_id'],
    						'vip_id' => 0,
    						'money' => $v['income_money'],
    						'charge_money' => $v['charge_money'],
    						'charge_num' => $v['charge_num'],
    						'chart_id' => $v['id'],
    						'cur_date' => $v['create_date'],
    						'create_time' => time()
    					];
    					if($channel['is_wx'] == 1){
    						$vip_id = myCache::getVipIdByChannelId($channel_id);
    						if(!$vip_id){
    							continue;
    						}
    						$ak = 'vip_'.$vip_id;
    						$data['vip_id'] = $vip_id;
    						$sign_str = 'vip_'.$vip_id.'_'.$v['create_date'];
    						if(!isset($accounts[$ak])){
    							$vipinfo = myDb::getById('VipUser', $vip_id,'id,bank_name,bank_user,bank_no');
    							if($vipinfo && $vipinfo['bank_no']){
    								$accounts[$ak] = [
    									'bank_no' => $vipinfo['bank_no'],
    									'bank_name' => $vipinfo['bank_name'],
    									'bank_user' => $vipinfo['bank_user']
    								];
    							}
    						}
    					}else{
    						$ak = 'agent_'.$channel_id;
    						$sign_str = 'agent_'.$channel_id.'_'.$v['create_date'];
    						if(!isset($accounts[$ak])){
    							if($channel['bank_no']){
    								$accounts[$ak] = [
    									'bank_no' => $channel['bank_no'],
    									'bank_name' => $channel['bank_name'],
    									'bank_user' => $channel['bank_user']
    								];
    							}
    						}
    					}
    					if(!array_key_exists($ak, $accounts)){
    						continue;
    					}
    					$data['sign'] = md5($sign_str);
    					$data['bank_info'] = json_encode($accounts[$ak]);
    					Db::startTrans();
    					$flag = false;
    					$re = Db::name('ChannelChart')->where('id',$v['id'])->setField('status',1);
    					if($re){
    						$res = Db::name('Withdraw')->insert($data);
    						if($res){
    							$chartData = [
    								'withdraw_total' => Db::raw('withdraw_total+'.$data['money']),
    								'withdraw_wait' => Db::raw('withdraw_wait+'.$data['money'])
    							];
    							$chartRes = Db::name('ChannelMoney')->where('channel_id',$v['channel_id'])->update($chartData);
    							if($chartRes){
    								$flag = true;
    							}
    						}
    					}
    					if($flag){
    						myCache::rmChannelAmount($channel_id);
    						Db::commit();
    					}else{
    						Db::rollback();
    					}
    				}
    			}else{
    				Db::name('ChannelChart')->where('id',$v['id'])->setField('status',1);
    			}
    		}
    	}
    	die('ok');
    }
    

    //执行结算统计
    public function doSysCount(){
    	$time = time();
    	$hour = date('H',$time);
    	$date = date('Y-m-d',$time);
    	$tmpDate = $date.' '.$hour.':00:00';
    	$end_time = strtotime($tmpDate)-1;
    	$start_time = $end_time-3599;
    	mdPlan::doSysCount($start_time,$end_time);
    }
}
