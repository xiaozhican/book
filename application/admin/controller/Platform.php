<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use site\myValidate;
use app\common\model\mdPlatform;

class Platform extends Common{
    
    //渠道列表
    public function channel(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['a.status','between',[0,3]],['a.type','=',1],['a.is_wx','=',1],['a.parent_id','=',0]],
                'eq' => 'status:a.status',
                'like' => 'keyword:a.name'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdPlatform::getList($where, $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                	$v['charge_money'] = $v['charge_money'] ? : 0;
                	$v['money'] = myCache::getChannelAmount($v['id']);
                    $v['status_name'] = mdPlatform::getStatusName($v['status']);
                    $v['do_url'] = my_url('doChannel',['id'=>$v['id']]);
                    $v['child_url'] = my_url('child',['id'=>$v['id']]);
                    $v['ratio'] .= '%';
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            
            return $this->fetch('common@platform/channel');
        }
    }
    
    //新增渠道
    public function addChannel(){
        if($this->request->isAjax()){
            $rules = mdPlatform::getRules();
            $data = myValidate::getData($rules);
            mdPlatform::doneChannel($data);
        }else{
            $field = 'id,name,login_name,status,url,is_location:2,location_url,appid,appsecret,apptoken,qrcode,deduct_min:0,deduct_num:0,wefare_days:30,ratio:90';
            $cur = myDb::buildArr($field);
            $variable = mdPlatform::getChannelOptions();
            $variable['cur'] = $cur;
            return $this->fetch('common@platform/doChannel',$variable);
        }
    }
    
    //编辑渠道
    public function doChannel(){
        if($this->request->isAjax()){
        	$rules = mdPlatform::getRules(1,0);
        	$data = myValidate::getData($rules);
        	mdPlatform::doneChannel($data);
        }else{
            $id = myHttp::getId('渠道');
            $cur = myDb::getById('Channel', $id);
            if(!$cur){
                res_api('渠道不存在');
            }
            $variable = mdPlatform::getChannelOptions();
            $variable['cur'] = $cur;
            return $this->fetch('common@platform/doChannel',$variable);
        }
    }
    
    
    //处理渠道状态
    public function doChannelEvent(){
    	$rules = mdPlatform::getEventRules();
        $data = myValidate::getData($rules);
        $cur = myDb::getById('Channel',$data['id'],'id,url,location_url');
        if(!$cur){
            res_api('渠道信息异常');
        }
        $key = 'status';
        switch ($data['event']){
            case 'on':
                $value = 1;
                break;
            case 'off':
                $value = 2;
                break;
            case 'delete':
                $value = 4;
                break;
            case 'pass':
                $value = 1;
                break;
            case 'fail':
                $value = 3;
                break;
            case 'resetpwd':
                $key = 'password';
                $value = createPwd(123456);
                break;
            default:
                res_api('未指定该事件');
                break;
        }
        if($value === 4){
        	$saveData = ['status'=>4,'login_name'=>'','url'=>'','is_location'=>2,'location_url'=>''];
        	$re = myDb::save('Channel',[['id','=',$cur['id']]],$saveData);
        }else{
        	$re = myDb::setField('Channel', [['id','=',$cur['id']]],$key, $value);
        }
        if($re){
            if($key === 'status'){
            	myCache::rmChannel($data['id']);
                if($cur['url']){
                    myCache::rmUrlInfo($cur['url']);
                }
                if($cur['location_url']){
                    myCache::rmUrlInfo($cur['location_url']);
                }
            }
            res_api();
        }else{
            res_api('操作失败,请重试');
        }
    }
    
    //代理列表
    public function agent(){
        if($this->request->isAjax()){
            $config = [
                'eq' => 'status:a.status',
                'like' => 'keyword:a.name',
            	'default' => [['a.status','between',[0,3]],['a.type','=',1],['a.is_wx','=',2],['a.parent_id','=',0]]
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdPlatform::getList($where, $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                	$v['charge_money'] = $v['charge_money'] ? : 0;
                	$v['money'] = myCache::getChannelAmount($v['id']);
                    $v['status_name'] = mdPlatform::getStatusName($v['status']);
                    $v['do_url'] = my_url('doAgent',['id'=>$v['id']]);
                    $v['child_url'] = my_url('child',['id'=>$v['id']]);
                    $v['ratio'] .= '%';
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            
            return $this->fetch('common@agent/index',['js'=>getJs('platform.agent')]);
        }
    }
    
    //新增代理
    public function addAgent(){
        if($this->request->isAjax()){
            $rules = mdPlatform::getRules(0);
            $data = myValidate::getData($rules);
            $data['type'] = 1;
            $data['status'] = 1;
            mdPlatform::doneAgent($data);
        }else{
            $field = 'id,name,login_name,status,url,deduct_min:0,deduct_num:0,wefare_days:30,ratio:90,bank_user,bank_name,bank_no';
            $cur = myDb::buildArr($field);
            $variable = mdPlatform::getAgentOptions();
            $variable['ratio'] = 100;
            $variable['cur'] = $cur;
            $variable['backUrl'] = url('agent');
            return $this->fetch('common@platform/doAgent',$variable);
        }
    }
    
    //编辑代理
    public function doAgent(){
        if($this->request->isAjax()){
        	$rules = mdPlatform::getRules(0,0);
        	$data = myValidate::getData($rules);
        	mdPlatform::doneAgent($data);
        }else{
            $id = myHttp::getId('代理');
            $cur = myDb::getById('Channel', $id);
            if(!$cur){
                res_api('渠道不存在');
            }
            $variable = mdPlatform::getAgentOptions();
            $variable['ratio'] = 100;
            $variable['cur'] = $cur;
            $variable['backUrl'] = url('agent');
            return $this->fetch('common@platform/doAgent',$variable);
        }
    }
    
    //子代理列表
    public function child(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['a.status','between',[0,3]],['a.type','=',2]],
                'eq' => 'status:a.status,id:a.parent_id',
                'like' => 'keyword:a.name'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdPlatform::getList($where, $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                	$v['charge_money'] = $v['charge_money'] ? : 0;
                	$v['money'] = myCache::getChannelAmount($v['id']);
                    $v['status_name'] = mdPlatform::getStatusName($v['status']);
                    $v['do_url'] = my_url('doChild',['id'=>$v['id']]);
                    $v['ratio'] .= '%';
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            $parent_id = myHttp::getId('代理');
            $cur = myDb::getById('Channel',$parent_id,'id,is_wx');
            if(!$cur){
                res_api('渠道不存在');
            }
            $back_url = my_url('channel');
            if($cur['is_wx'] != 1){
                $back_url = my_url('agent');
            }
            return $this->fetch('common@platform/child',['back_url'=>$back_url]);
        }
    }
    
    //编辑子代理
    public function doChild(){
    	if($this->request->isAjax()){
    		$rules = mdPlatform::getRules(0,0);
    		$data = myValidate::getData($rules);
    		mdPlatform::doneAgent($data);
    	}else{
    		$id = myHttp::getId('代理');
    		$cur = myDb::getById('Channel', $id);
    		if(!$cur){
    			res_api('代理不存在');
    		}
    		if($cur['type'] != 2 || !$cur['parent_id']){
    			res_api('代理信息异常');
    		}
    		$variable = mdPlatform::getAgentOptions();
    		$parent = myDb::getById('Channle', $cur['parent_id'],'id,ratio');
    		$variable['cur'] = $cur;
    		$variable['ratio'] = $parent['ratio'];
    		$variable['backUrl'] = my_url('child',['id'=>$cur['parent_id']]);
    		return $this->fetch('common@platform/doAgent',$variable);
    	}
    }
    
    
    //处理代理状态
    public function doAgentEvent(){
    	$rules = mdPlatform::getEventRules();
    	$data = myValidate::getData($rules);
        $cur = myDb::getById('Channel',$data['id'],'id,url,location_url');
        if(!$cur){
            res_api('代理信息异常');
        }
        $key = 'status';
        switch ($data['event']){
            case 'on':
                $value = 1;
                break;
            case 'off':
                $value = 2;
                break;
            case 'delete':
                $value = 4;
                break;
            case 'pass':
                $value = 1;
                break;
            case 'fail':
                $value = 3;
                break;
            case 'resetpwd':
                $key = 'password';
                $value = createPwd(123456);
                break;
            default:
                res_api('未指定该事件');
                break;
        }
        if($value === 4){
        	$saveData = ['status'=>4,'login_name'=>'','url'=>'','location_url'=>''];
        	$re = myDb::save('Channel',[['id','=',$cur['id']]],$saveData);
        }else{
        	$re = myDb::setField('Channel', [['id','=',$cur['id']]],$key, $value);
        }
        if($re){
            if($key === 'status'){
            	myCache::rmChannel($cur['id']);
                if($cur['url']){
                    myCache::rmUrlInfo($cur['url']);
                }
            }
            res_api();
        }else{
            res_api('操作失败,请重试');
        }
    }
    
    //进入代理后台
    public function intoBackstage(){
        $id = myHttp::postId('代理');
        $url = mdPlatform::intoBackstage($id);
        res_api(['url'=>$url]);
    }
    
}