<?php


namespace app\admin\controller;


use app\common\model\mdActivity;
use app\common\model\mdNotice;
use site\myCache;
use site\myDb;
use site\myHttp;
use site\mySearch;
use site\myValidate;
use think\Db;

class Notice extends Common
{
    //广告列表
    public function index()
    {
        if($this->request->isAjax()){
            $config = [
                'default' => [['status','between',[1,2]]],
                'eq' => 'status:status,app_name:app_name,advertisers:advertisers',
                'like' => 'keyword:notice_name',
                'between' => 'between_time:create_time'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdNotice::getChargeOrder($where, $pages);//application/model/mdNotice.php
            if($res['data']){
                foreach($res['data'] as &$v){
                    $v['do_url'] = my_url('doNotice',['id'=>$v['id']]);
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            return $this->fetch('common@notice/index',['js'=>getJs('notice.index','admin',['date'=>20201018])]);
        }
    }
    //处理广告事件
    public function doNoticeEvent()
    {
        $data = mdNotice::getEventData();
        switch ($data['event']){
            case 'on':
                $status = 1;
                break;
            case 'off':
                $status = 2;
                break;
            case 'delete':
                $status = 3;
                break;
            default:
                res_api('未指定该事件');
                break;
        }
        $flag = myDb::setField('Notice', [['id','=',$data['id']]], 'status',$status);//extend/site/myDb.php
        if($flag){
            myCache::rmNotice($data['id']);
            res_api();
        }else{
            res_api('操作失败');
        }
    }
    //添加广告
    public function addNotice(){
        if($this->request->isAjax()){
            $rules = mdNotice::getRules();
            $data = myValidate::getData($rules);
            $flag = mdNotice::doneNotice($data);////application/common/model/mdNotice.php
            if($flag){
                myCache::rmLastNotice();
                if(cache('?app_start_img')){
                    cache("app_start_img",null);
                }
            }
            $str = $flag ? 'ok' : '添加失败';
            res_api($str);
        }else{
            $field = 'id,app_name,notice_name,status,notice_type,code,create_time,app_id,notice_id,advertisers,huawei_status,meizu_status,oppo_status,vivo_status,xiaomi_status';
            $option = mdNotice::getOptions();//application/common/model/mdNotice.php
            $option['cur'] = myDb::buildArr($field);
            return $this->fetch('common@notice/doNotice-admin',$option);
        }
    }
    //更新广告
    public function doNotice()
    {
        if($this->request->isAjax()){
            $rules = mdNotice::getRules(0);
            $data = myValidate::getData($rules);
            $flag = mdNotice::doneNotice($data);//application/common/model/mdNotice.php
            if($flag){
                myCache::rmLastNotice();
                myCache::rmNotice($data['id']);//extend/site/myCache.php
                if(cache('?app_start_img')){
                    cache("app_start_img",null);
                }
                res_api();
            }else{
                res_api('更新失败');
            }
        }else{
            $id = myHttp::getId('广告');
            $cur = myDb::getById('Notice', $id);
            if(!$cur){
                res_api('广告不存在');
            }
            //$cur['start_time'] = date('Y-m-d H:i:s',$cur['start_time']);
            //$cur['end_time'] = date('Y-m-d H:i:s',$cur['end_time']);
            $option = mdNotice::getOptions();
            $option['cur'] = $cur;
            return $this->fetch('common@notice/doNotice-admin',$option);
        }
    }

    //清除广告缓存
    public function delCache()
    {
        $key = 'app_start_img';
        if(cache('?'.$key)){
            cache($key,null);
        }
        res_api();
    }

    //一键关闭安卓广告状态
    public function closeAndroid()
    {
        $res = Db::name('Notice')->where(['app_name'=>'Android'])->update([
            'status'=>2,
            'huawei_status'=>2,
            'meizu_status'=>2,
            'oppo_status'=>2,
            'vivo_status'=>2,
            'xiaomi_status'=>2,
        ]);
        if($res){
            res_api();
        }else{
            res_api('操作失败');
        }
    }

    //一键关闭ios广告状态
    public function closeIos()
    {
        $res = Db::name('Notice')->where(['app_name'=>'IOS'])->update([
            'status'=>2
        ]);
        if($res){
            res_api();
        }else{
            res_api('操作失败');
        }
    }
}