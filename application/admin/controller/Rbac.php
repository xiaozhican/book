<?php
namespace app\admin\controller;


use site\myDb;
use site\myHttp;
use site\mySearch;
use site\myValidate;
use app\common\model\mdUser;
use app\common\model\mdNode;
use app\common\model\mdRole;
use app\common\model\mdLogin;

class Rbac extends Common{
    
    //节点管理
    public function node(){
        if($this->request->isAjax()){
            $list = mdNode::getNodeList(1);
            if($list){
                foreach ($list as &$v){
                    $v['menu_name'] = $v['is_menu'] == 1 ? '菜单' : '方法';
                    $v['add_url'] = my_url('addNode',['parent_id'=>$v['id']]);
                    $v['do_url'] = my_url('doNode',['id'=>$v['id']]);
                }
            }
            res_table($list);
        }else{
            return $this->fetch('common@rbac/node');
        }
    }
    
    //添加节点
    public function addNode(){
        if($this->request->isAjax()){
            $rules = mdNode::getRules();
            $rules['is_menu'] = ["require|in:1,2",["require"=>"请选择节点类型",'in'=>'未指定该节点类型']];
            $data = myValidate::getData($rules);
            $data['type'] = 1;
            $re = mdNode::doneNodes($data);
            if($re){
            	res_api();
            }else{
            	res_api('编辑节点失败');
            }
        }else{
            $field = 'id,pid,name,is_menu,icon,url,child_nodes';
            $cur = myDb::buildArr($field);
            $cur['parent_name'] = '作为一级节点';
            $parent_id = myValidate::getData(['parent_id'=>['number',['number'=>'参数错误']]],'get');
            $parent_id = $parent_id ? : 0;
            if($parent_id){
                $parent = myDb::getById('Nodes',$parent_id,'id,name');
                $cur['parent_name'] = $parent['name'];
            }
            $cur['pid'] = $parent_id;
            $variable = mdNode::getOptions();
            $variable['cur'] = $cur;
            $variable['backUrl'] = url('node');
            return $this->fetch('common@rbac/doNode',$variable);
        }
    }
    
    //编辑节点
    public function doNode(){
        if($this->request->isAjax()){
        	$rules = mdNode::getRules(0);
        	$rules['is_menu'] = ["require|in:1,2",["require"=>"请选择节点类型",'in'=>'未指定该节点类型']];
        	$data = myValidate::getData($rules);
        	$re = mdNode::doneNodes($data);
        	if($re){
        		mdLogin::clearNode();
        		res_api();
        	}else{
        		res_api('编辑节点失败');
        	}
        }else{
            $id = myHttp::getId('节点');
            $cur = myDb::getById('Nodes',$id);
            if(!$cur){
                res_api('节点不存在');
            }
            $cur['parent_name'] = '作为一级节点';
            if($cur['pid'] > 0){
                $parent = myDb::getById('Nodes',$cur['pid'],'id,name');
                $cur['parent_name'] = $parent['name'];
            }
            $variable = mdNode::getOptions();
            $variable['cur'] = $cur;
            $variable['backUrl'] = url('node');
            return $this->fetch('common@rbac/doNode',$variable);
        }
    }
    
    //处理节点排序及删除节点
    public function doNodeEvent(){
        $rules = mdNode::getEventRules();
        $data = myValidate::getData($rules);
        if($data['event'] === 'delete'){
            $re = mdNode::deleteNodes($data['id']);
        }else{
            $re = mdNode::doNodeSort($data);
        }
        if($re){
        	mdLogin::clearNode();
            res_api();
        }else{
            res_api('操作失败,请重试');
        }
    }
    
    //角色管理
    public function role(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['status','between',[1,2]]],
                'eq' => 'status',
                'like' => 'keyword:name'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = myDb::getPageList('Role',$where, '*', $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                    $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
                    $v['status_name'] = $v['status'] == 1 ? '启用' : '禁用'; 
                    $v['do_url'] = my_url('doRole',['id'=>$v['id']]);
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            
        	return $this->fetch('common@rbac/role');
        }
    }
    
    //新增角色
    public function addRole(){
        if($this->request->isAjax()){
            $rules = mdRole::getRules();
            $data = myValidate::getData($rules);
            mdRole::doneRole($data);
        }else{
            $field = 'id,name,status,summary';
            $cur = myDb::buildArr($field);
            $option = mdRole::getOptions();
            $option['cur'] = $cur;
            return $this->fetch('common@rbac/doRole',$option);
        }
    }
    
    //编辑角色
    public function doRole(){
        if($this->request->isAjax()){
        	$rules = mdRole::getRules(0);
        	$data = myValidate::getData($rules);
        	mdRole::doneRole($data);
        }else{
            $id = myHttp::getId('角色');
            $cur = myDb::getById('Role',$id);
            if(!$cur){
                res_api('角色不存在');
            }
            $option = mdRole::getOptions();
            $option['cur'] = $cur;
            return $this->fetch('common@rbac/doRole',$option);
        }
    }
    
    //处理角色事件
    public function doRoleEvent(){
        $rules = mdRole::getEventRules();
        $data = myValidate::getData($rules);
        switch ($data['event']){
            case 'on':
                $status = 1;
                break;
            case 'off':
                $status = 2;
                break;
            case 'delete':
                $status = 3;
                break;
        }
        $re = myDb::setField('Role', [['id','=',$data['id']]], 'status', $status);
        if($re){
            res_api();
        }else{
            res_api('操作失败,请重试');
        }
    }
    
    //用户管理
    public function user(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['status','between',[1,2]]],
                'eq' => 'status',
                'like' => 'keyword:name'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdUser::getUserList($where, $pages);
            res_table($res['data'],$res['count']);
        }else{
            
        	return $this->fetch('common@rbac/user');
        }
    }
    
    //新增用户
    public function addUser(){
        if($this->request->isAjax()){
        	$rules = mdUser::getRules();
        	$data = myValidate::getData($rules);
            mdUser::doneUser($data);
        }else{
            $field = 'id,name,role_id,login_name,password,status';
            $option = mdUser::getOptions();
            $option['cur'] = myDb::buildArr($field);
            return $this->fetch('common@rbac/doUser',$option);
        }
    }
    
    //编辑用户
    public function doUser(){
        if($this->request->isAjax()){
        	$rules = mdUser::getRules(0);
        	$data = myValidate::getData($rules);
        	mdUser::doneUser($data);
        }else{
            $id = myHttp::getId('用户');
            $cur = myDb::getById('Manage',$id);
            if(!$cur){
                res_api('用户不存在');
            }
            $option = mdUser::getOptions();
            $option['cur'] = $cur;
            return $this->fetch('common@rbac/doUser',$option);
        }
    }
    
    //处理用户事件
    public function doUserEvent(){
    	$rules = mdUser::getEventRules();
        $data = myValidate::getData($rules);
        $key = 'status';
        if($data['event'] === 'delete'){
        	$re = myDb::delById('Manage', $data['id']);
        }else{
        	switch ($data['event']){
        		case 'on':
        			$value = 1;
        			break;
        		case 'off':
        			$value = 2;
        			break;
        		case 'resetpwd':
        			$key = 'password';
        			$value = createPwd(123456);
        			break;
        	}
        	$re = myDb::setField('Manage', [['id','=',$data['id']]],$key, $value);
        }
        if($re){
            res_api();
        }else{
            res_api('操作失败,请重试');
        }
    }
    
    
}
