<?php


namespace app\admin\controller;


use app\common\model\mdCount;
use site\myHttp;
use site\mySearch;
use think\Db;

class Count extends Common
{
    //统计在线人数列表
    public function lists()
    {
        //$this->getData();
        if($this->request->isAjax()){
            $config = [
                //'default' => [['status','between',[1,2]]],
                //'eq' => 'status:status,app_name:app_name',
                //'like' => 'keyword:notice_name',
                'between' => 'between_time:date_time'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            if($where){
                $start_time = date('Y-m-d',$where[0][2][0]);
                $end_time = date('Y-m-d',$where[0][2][1]);
            }else{
                $start_time = date('2020-08-01 00:00:00');
                $end_time = date('Y-m-d 23:59:59');
            }
            $res = mdCount::getChargeOrder($start_time,$end_time, $pages);
            res_table($res['data'],$res['count']);
        }
        return $this->fetch('common@count/lists',['js'=>getJs('count.lists','admin',['date'=>20200813])]);
    }

    //获取用户登录日志信息，并写入统计在线人数表中
    public function getData()
    {
        $res = Db::name('Count')->order('id desc')->limit(1)->field('date_time')->find();
        if(!$res){
            $start_day = strtotime('2020-08-01 00:00:00');//如果表是空的，则从2020-08-01开始统计
        }else{
            $start_day = strtotime($res['date_time'].' 00:00:00');//从表中最后一条的时间开始统计
        }
        //$start_day = strtotime(date('Y-m-d',strtotime("-1 day")).' 00:00:00');//昨天的零点开始
        $end_day = strtotime(date('Y-m-d').' 23:59:59');//当天
        //计算昨天和今天的在线人数
        $sql = "SELECT count(id) renshu,login_type,FROM_UNIXTIME(create_time,'%Y-%m-%d') create_time  from sy_log where STATUS=1 and create_time BETWEEN {$start_day} and {$end_day} GROUP BY login_type,FROM_UNIXTIME(create_time,'%Y-%m-%d') ORDER BY create_time asc;";//统计安卓，ios，h5，pc端的在线人数
        $sql1 = "select count(t.renshu) day_renshu,t.create_time from (
SELECT count(id) renshu,username,FROM_UNIXTIME(create_time,'%Y-%m-%d') create_time  from sy_log where STATUS=1 and create_time BETWEEN {$start_day} and {$end_day} GROUP BY username,FROM_UNIXTIME(create_time,'%Y-%m-%d') ORDER BY create_time asc
)as t GROUP BY t.create_time;";//统计每天的日活人数
        $list = Db::query($sql);
        $list1 = Db::query($sql1);
        if($list){
            foreach ($list as $k=>$v){
                if($v['login_type']=='android'){
                    $android = $v['renshu'];
                }
                if($v['login_type']=='ios'){
                    $ios = $v['renshu'];
                }
                if($v['login_type']=='h5'){
                    $h5 = $v['renshu'];
                }
                if($v['login_type']=='pc'){
                    $pc = $v['renshu'];
                }
                $where = [
                    'date_time'=>$v['create_time']
                ];
                $query = Db::name('Count')->where($where)->find();
                //var_dump($query);
                if(!$query){
                    Db::name('Count')->insert([
                        'date_time'=>$v['create_time'],
                        'android'=>$android??0,
                        'ios'=>$ios??0,
                        'h5'=>$h5??0,
                        'pc'=>$pc??0,
                    ]);
                }else{
                    Db::name('Count')->where($where)->update([
                        //'date_time'=>$v['create_time'],
                        'android'=>$android??0,
                        'ios'=>$ios??0,
                        'h5'=>$h5??0,
                        'pc'=>$pc??0,
                    ]);
                }
            }
        }

        if($list1){
            foreach ($list1 as $val){
                $where1 = [
                    'date_time'=>$val['create_time']
                ];
                $query = Db::name('Count')->where($where1)->find();
                if(!$query){
                    Db::name('Count')->insert([
                        'date_time'=>$val['create_time'],
                        'day_activity'=>$val['day_renshu']
                    ]);
                }else{
                    Db::name('Count')->where($where1)->update([
                        'day_activity'=>$val['day_renshu']
                    ]);
                }
            }
        }
    }

    //历史登录记录
    public function historyLog()
    {
        if($this->request->isAjax()){
            $config = [
                'default' => [['status','between',[1,2]]],
                'eq' => 'status:status,login_type:login_type,is_online:is_online',
                'like' => 'username:username',
                'between' => 'between_time:create_time'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdCount::getloginLogOrder($where, $pages);
            res_table($res['data'],$res['count']);
        }else {
            return $this->fetch('common@count/historyLog', ['js' => getJs('count.historylog', 'admin', ['date' => 20200907])]);
        }
    }

    //书籍访问记录
    public function readLog()
    {
        if($this->request->isAjax()){
            $config = [
                //'default' => [['status','between',[1,2]]],
                //'eq' => 'status:status,login_type:login_type,is_online:is_online',
                'like' => 'book_name:book_name',
                'between' => 'between_time:create_time,update_time:update_time'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdCount::getReadLogOrder($where, $pages);
            if($res['data']){
                foreach($res['data'] as &$v){
                    $v['do_url'] = my_url('doReadDetail',['id'=>$v['id']]);
                }
            }
            res_table($res['data'],$res['count']);
        }else {
            return $this->fetch('common@count/readLog', ['js' => getJs('count.readlog', 'admin', ['date' => 20200813])]);
        }
    }

    //查看访问书籍的用户列表
    public function doReadDetail()
    {
        $read_id = $this->request->get('id');
        if($this->request->isAjax()){
            $config = [
                'like' => 'username:username',
                'between' => 'between_time:create_time'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdCount::getReadDetailOrder($read_id, $where, $pages);
            res_table($res['data'],$res['count']);
        }else{
            return $this->fetch('common@count/readDetail',['js'=>getJs('count.doreaddetail','admin',['date'=>20201105])]);
        }
    }
    
    //渠道下载统计
    public function appDownload()
    {
        if($this->request->isAjax()){
            $config = [
                //'between' => 'between_time:datetime'
                'like' => 'channel:channel',
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
//            if($where){
//                $start_time = date('Y-m-d',$where[0][2][0]);
//                $end_time = date('Y-m-d',$where[0][2][1]);
//            }else{
//                $start_time = date('2020-08-01');
//                $end_time = date('Y-m-d');
//            }
            //$res = mdCount::getAppDownloadOrder($start_time,$end_time, $pages);
            $res = mdCount::getAppDownloadOrder($where, $pages);
            if($res['data']){
                foreach($res['data'] as &$v){
                    $v['do_url'] = my_url('doAppDownloadDetail',['id'=>$v['id']]);
                    $v['do_app_start'] = my_url('doAppStart',['channel'=>$v['channel']]);
                }
            }
            res_table($res['data'],$res['count']);
        }
        return $this->fetch('common@count/appDownload',['js'=>getJs('count.appdownload','admin',['date'=>20201123])]);
    }
    //app渠道下载详情
    public function doAppDownloadDetail()
    {
        $id = $this->request->get('id');
        if($this->request->isAjax()){
            $pages = myHttp::getPageParams();
            //$where = mySearch::getWhere($config);
            $res = mdCount::getAppDownloadDetailOrder($id, $pages);
            res_table($res['data'],$res['count']);
        }else{
            return $this->fetch('common@count/appDownloadDetail',['js'=>getJs('count.doAppDownloadDetail','admin',['date'=>20201028])]);
        }
    }
    
    //回头客
    public function doAppStart()
    {
        $channel = $this->request->get('channel');
        $channel = $channel=='官网'?'default':$channel;
        if($this->request->isAjax()){
            $config = [
                'between' => 'update_time:update_time',
                'like' => 'phone:phone',
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdCount::getAppStartOrder($channel,$where,$pages);
            if($res['data']){
                foreach($res['data'] as &$v){
                    $v['do_url'] = my_url('doAppStartDetail',['phone'=>$v['phone']]);
                }
            }
            res_table($res['data'],$res['count']);
        }else{
            return $this->fetch('common@count/appStart',['js'=>getJs('count.appstart','admin',['date'=>20201116])]);
        }
    }
    
    //回头客详情
    public function doAppStartDetail()
    {
        $phone = $this->request->get('phone');
        if($this->request->isAjax()){
            $config = [
                'between' => 'update_time:update_time',
                //'like' => 'phone:phone',
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdCount::getAppStartDetailOrder($phone,$where,$pages);
            res_table($res['data'],$res['count']);
        }else{
            return $this->fetch('common@count/appStartDetail',['js'=>getJs('count.appstartdetail','admin',['date'=>20201027])]);
        }
    }
}