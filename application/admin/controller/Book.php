<?php
namespace app\admin\controller;

use think\Db;
use site\myHttp;
use site\myCache;
use OSS\OssClient;
use OSS\Core\OssException;
use site\myPush;

class Book{
	
	// 0 6 * * * /usr/bin/curl localhost/admin/Book/doStart
	
	private $channel = 'wktewan';
	
	private $ossClient;
	
	public function __construct(){
		if($_SERVER['HTTP_HOST'] !== 'localhost'){
			//exit();
		}
	}
	
	//开始更新书籍
	public function doStart(){
		$repeat_num = 0;
		set_time_limit(0);
		$bookIds = self::getBookIds();
		if($bookIds){
			$time = time();
			foreach ($bookIds as $v){
				$push_book_id = 0;
				$cur = Db::name('BookCat')->where('sync_id',$v['bookid'])->find();
				if($cur && $cur['is_end'] == 1){
					$cacheBook = myCache::getBook($cur['book_id']);
					if($cacheBook && $cacheBook['over_type'] == 1){
						$fre = Db::name('Book')->where('id',$cacheBook['id'])->setField('over_type',2);
						if($fre){
							myCache::doBook($cacheBook['id'], 'over_type', 2);
						}
					}
					continue;
				}
				$bookInfo = self::getBookInfo($v['bookid']);
				if($bookInfo){
					if(!$cur){
						$bookname = $bookInfo['bookname'];
						$repeat = Db::name('Book')->where('name',$bookname)->value('id');
						if($repeat){
							$bookname = $bookname.'/'.$bookInfo['authorname'];
							$repeat2 = Db::name('Book')->where('name',$bookname)->value('id');
							if($repeat2){
								$repeat_num++;
								continue;
							}
						}
						$data = [
							'type' => 1,
							'name' => $bookname,
							'author' => $bookInfo['authorname'],
							'cover' => self::getBookCover($bookInfo['bookpic']),
							'summary' => $bookInfo['intro'],
							'category' => ','.$bookInfo['bookType'].',',
							'gender_type' => $bookInfo['gender'],
							'over_type' => $bookInfo['state'] == 1 ? 2 : 1,
							'free_type' => 2,
							'free_chapter' => 15,
							'money' => 28,
							'word_number' => $bookInfo['words'],
							'share_title' => $bookInfo['bookname'],
							'share_desc' => $bookInfo['intro'],
							'create_time' => $time
						];
						Db::startTrans();
						$flag = false;
						$book_id = Db::name('Book')->insertGetId($data);
						if($book_id){
							$cur = [
								'book_id' => $book_id,
								'sync_id' => $v['bookid'],
								'last_num' => 0
							];
							$re = Db::name('BookCat')->insertGetId($cur);
							if($re){
								$cur['id'] = $re;
								$flag = true;
							}
						}
						if($flag){
							Db::commit();
						}else{
							Db::rollback();
							continue;
						}
					}
				}
				if($cur){
					if($cur['last_num'] < $bookInfo['chaptercount']){
						$chapter = self::getBookChapter($v['bookid']);
						if($chapter){
							$cur_num = $cur['last_num'] + 1;
							foreach ($chapter as $cv){
								if($cur_num == $cv['num']){
									$content = self::getChapterText($v['bookid'], $cv['chapterid']);
									if($content){
										$html = "<p>".$content;
										$html = preg_replace('/\n|\r\n/','</p><p>',$html);
										$chapterData = [
											'book_id' => $cur['book_id'],
											'name' => $cv['chaptername'],
											'number' => $cur_num,
											'create_time' => $time
										];
										Db::startTrans();
										$flag = false;
										$isOverType = false;
										$chapter_id = Db::name('BookChapter')->insertGetId($chapterData);
										if($chapter_id){
											$catData = ['last_num'=>$cur_num];
											if($cur_num == $bookInfo['chaptercount'] && $bookInfo['state'] == 1){
												$catData['is_end'] = 1;
											}
											$res = Db::name('BookCat')->where('id',$cur['id'])->update($catData);
											if($res){
												$doRes = true;
												if(isset($catData['is_end']) && $catData['is_end'] == 1){
													$doRes = false;
													$bres = Db::name('Book')->where('id',$cur['book_id'])->setField('over_type',2);
													if($bres){
														$isOverType = true;
														$doRes = true;
													}
												}
												if($doRes){
													$block_res = saveBlock($html,$cur_num,'book/'.$cur['book_id']);
													if($block_res){
														$flag = true;
													}
												}
											}
										}
										if($flag){
											Db::commit();
											$cur_num += 1;
											$push_book_id = $cur['book_id'];
											self::addLog($cur['book_id'], $chapterData['name'], $cur_num);
											self::clearCache($cur['book_id'],$isOverType);
										}else{
											Db::rollback();
											break 1;
										}
									}
								}
							}
						}
					}else{
						if($bookInfo['state'] == 1 && $cur['last_num'] == $bookInfo['chaptercount']){
							Db::startTrans();
							$flag = false;
							$re = Db::name('BookCat')->where('id',$cur['id'])->setField('is_end',1);
							if($re){
								$res = Db::name('Book')->where('id',$cur['book_id'])->setField('over_type',2);
								if($res){
									$flag = true;
								}
							}
							if($flag){
								Db::commit();
								myCache::doBook($cur['book_id'], 'over_type', 2);
							}else{
								Db::rollback();
							}
						}
					}
				}
				if($push_book_id){
					myPush::updateBookPush($push_book_id);
				}
			}
		}
		echo $repeat_num;
		exit;
	}
	
	//添加日志
	private function addLog($book_id,$chapter,$number){
		$data = [
			'book_id' => $book_id,
			'chapter' => $chapter,
			'chapter_num' => $number,
			'create_time' => time()
		];
		Db::name('BookCatLog')->insert($data);
	}
	
	//清除书籍缓存
	private function clearCache($book_id,$isOverType=false){
		myCache::rmBookChapterList($book_id);
		myCache::rmBookChapterNum($book_id);
		if($isOverType){
			myCache::doBook($book_id, 'over_type', 2);
		}
	}
	
	//获取书籍ID
	private function getBookIds(){
		$result = null;
		$url = 'http://www.01kanshu.com/Interface/CommonApi/books?channel='.$this->channel;
		$res = myHttp::doGet($url);
		if($res && $res['code'] == 0){
			$result = $res['result'];
		}
		return $result;
	}
	
	//获取书籍详情
	private function getBookInfo($bookid){
		$result = null;
		$url = 'http://www.01kanshu.com/Interface/CommonApi/Bookinfo';
		$param = [
			'channel' => $this->channel,
			'bookid' => $bookid
		];
		$url .= '?'.http_build_query($param);
		$res = myHttp::doGet($url);
		if($res && $res['code'] == 0){
			$result = $res['result'];
		}
		return $result;
	}
	
	//获取书籍章节列表
	private function getBookChapter($bookid){
		$result = null;
		$url = 'http://www.01kanshu.com/Interface/CommonApi/chapters';
		$param = [
			'channel' => $this->channel,
			'bookid' => $bookid
		];
		$url .= '?'.http_build_query($param);
		$res = myHttp::doGet($url);
		if($res && $res['code'] == 0){
			$list = $res['result'];
			$result = [];
			foreach ($list as $v){
				if(!empty($v['chapterlist'])){
					$result = array_merge($result,$v['chapterlist']);
				}
			}
		}
		return $result;
	}
	
	//获取章节内容
	private function getChapterText($bookid,$chapterid){
		$result = null;
		$url = 'http://www.01kanshu.com/Interface/CommonApi/ChapterInfo';
		$param = [
			'channel' => $this->channel,
			'bookid' => $bookid,
			'chapterid' => $chapterid
		];
		$url .= '?'.http_build_query($param);
		$res = myHttp::doGet($url);
		if($res && $res['code'] == 0){
			$result = $res['result']['content'];
		}
		return $result;
	}
	
	//获取小说封面
	private function getBookCover($img_url){
		$url = '';
		if($img_url){
			$config = getMyConfig('alioss');
			if($config){
				if(!$this->ossClient){
					$this->ossClient = new OssClient($config['accessKey'],$config['secretKey'],$config['url']);
				}
				if($this->ossClient){
					$pathInfo = pathinfo($img_url);
					if($pathInfo && is_array($pathInfo)){
						$imgContent = self::getImgContent($img_url);
						if($imgContent){
							$savename = 'images/'.date('Umd').'/'.md5($img_url).'.'.$pathInfo['extension'];
							try {
								$this->ossClient->putObject($config['bucket'],$savename,$imgContent);
								if(isset($config['cdn']) && $config['cdn']){
									$url = $config['cdn'].'/'.$savename;
								}else{
									$url = 'https://'.$config['bucket'].'.'.$config['url'].'/'.$savename;
								}
							}catch (OssException $e){
								
							}
						}
					}
				}
			}
		}
		return $url;
	}
	
	//获取图像内容
	private function getImgContent($img_url,$referer=''){
		$header = array('User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:45.0) Gecko/20100101 Firefox/45.0', 'Accept-Language: zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3', 'Accept-Encoding: gzip, deflate', );
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $img_url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		if($referer){
			curl_setopt($curl, CURLOPT_REFERER,$referer);
		}
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_ENCODING, 'gzip');
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		$data = curl_exec($curl);
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		return $data;
	}
	
	
}