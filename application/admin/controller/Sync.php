<?php
namespace app\admin\controller;

use think\Db;
use site\myHttp;
use site\myAliyunoss;
use think\Controller;

class Sync extends Controller{
	
	private $ip = 'www.bm819k.cn';
	
	public function __construct(){
		parent::__construct();
		if($_SERVER['HTTP_HOST'] !== 'localhost'){
			//die('非法访问');
		}
	}
	
	//同步小说
	public function novel(){
		set_time_limit(0);
		$bookList = self::getBookList();
		if(!empty($bookList)){
			foreach ($bookList as $v){
				$is_sync = false;
				$repeat = Db::name('BookSync')->where('sync_id','=',$v['id'])->find();
				if($repeat){
					$number = Db::name('BookChapter')->where('book_id','=',$repeat['book_id'])->max('number');
					if($number < $v['number']){
						$is_sync = true;
					}
				}else{
					$data = $v;
					unset($data['id']);
					unset($data['number']);
					$data['create_time'] = time();
					if($data['cover']){
						$data['cover'] = self::getImageUrl($data['cover']);
					}
					unset($data['detail_img']);
					Db::startTrans();
					$flag = false;
					$re = Db::name('Book')->insertGetId($data);
					if($re){
						$repeat = [
							'sync_id' => $v['id'],
							'book_id' => $re,
							'ip' => $this->ip
						];
						$res = Db::name('BookSync')->insert($repeat);
						if($res){
							$flag = true;
						}
					}
					if($flag){
						Db::commit();
						$is_sync = true;
					}else{
						Db::rollback();
					}
				}
				if($is_sync){
					$chapterList = self::getChapterList($repeat['sync_id']);
					if($chapterList){
						foreach ($chapterList as $val){
							$chapterId = Db::name('BookChapter')->where('book_id','=',$repeat['book_id'])->where('number','=',$val['number'])->value('id');
							if(!$chapterId){
								$one = [
									'name' => $val['name'],
									'number' => $val['number'],
									'book_id' => $repeat['book_id'],
									'create_time' => time()
								];
								$res = Db::name('BookChapter')->insert($one);
								if($res){
									$content = self::getChapterContent($repeat['sync_id'], $one['number']);
									if($content){
										saveBlock($content,$one['number'],'book/'.$one['book_id']);
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	//获取书籍列表
	private function getBookList(){
		$url = 'http://'.$this->ip.'/admin/Syncapi/getBookList';
		$result = myHttp::doGet($url);
		if(isset($result['data']) && $result['data']){
			return $result['data'];
		}
		return false;
	}
	
	//获取章节列表
	private function getChapterList($bookId){
		$url = 'http://'.$this->ip.'/admin/Syncapi/getChapterList?bookId='.$bookId;
		$result = myHttp::doGet($url);
		if(isset($result['data']) && $result['data']){
			return $result['data'];
		}
		return false;
	}
	
	//获取章节内容
	private function getChapterContent($bookId,$number){
		$url = 'http://'.$this->ip.'/admin/Syncapi/getChapterContent?bookId='.$bookId.'&number='.$number;
		$result = myHttp::doGet($url);
		if(isset($result['data']['content']) && $result['data']['content']){
			return $result['data']['content'];
		}
		return false;
	}
	
	//抓取图片到阿里云
	private function getImageUrl($url){
		$content = @file_get_contents($url);
		if($content){
			$config =
			$config = getMyConfig('alioss');
			if(!$config){
				res_error('阿里云参数未配置');
			}
			$info = pathinfo($url);
			myAliyunoss::$config = $config;
			$name = md5($url).'.'.$info['extension'];
			$savename = 'images/'.date('Ymd').'/'.$name;
			$url = myAliyunoss::putObject($savename, $content);
			if($url){
				return $url;
			}
		}
		return '';
	}
}
