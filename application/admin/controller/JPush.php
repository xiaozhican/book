<?php
namespace app\admin\controller;

use site\myValidate;
use site\myPush;
use site\myCache;

class JPush extends Common{
	
	//推送消息
	public function index(){
		if($this->request->isAjax()){
			$rules = [
				'title' => ['require|max:30',['require'=>'请输入推送标题','max'=>'推送标题不超过50个字符']],
				'content' => ['require|max:100',['require'=>'请输入推送内容','max'=>'推送内容不超过100个字符']],
				'page' => ['require|between:1,9',['require'=>'请选择打开页面','between'=>'未指定的点开页面']],
				'book_id' => ['number|gt:0',['number'=>'书籍参数错误','gt'=>'书籍参数不规范']],
				'book_number' => ['number|gt:0',['number'=>'书籍参数错误','gt'=>'书籍参数不规范']],
				'send_type' => ['require|in:1,2',['require|in:1,2',['require'=>'请选择发送范围','in'=>'未指定该发送范围']]],
				'uids' => ['requireIf:send_type,2',['requireIf'=>'请输入指定用户ID']]
			];
			$data = myValidate::getData($rules);
			$extras = ['page'=>$data['page']];
			switch ($data['page']){
				case 2:
					if(!$data['book_id']){
						res_api('您尚未选择指向书籍');
					}
					$book = myCache::getBook($data['book_id']);
					if(!$book){
						res_api('书籍已删除');
					}
					$extras['book_id'] = $book['id'];
					$extras['book_type'] = $book['type'];
					break;
				case 1:
					if(!$data['book_id']){
						res_api('您尚未选择指向书籍');
					}
					if(!$data['book_number']){
						res_api('您尚未选择指向书籍章节');
					}
					$book = myCache::getBook($data['book_id']);
					if(!$book){
						res_api('书籍已删除');
					}
					$extras['book_id'] = $book['id'];
					$extras['book_type'] = $book['type'];
					$extras['book_number'] = $data['book_number'];
					break;
			}
			$msg = ['title'=>$data['title'],'content'=>$data['content'],'extras'=>$extras];
			switch ($data['send_type']){
				case 2:
					$uids = explode(',', $data['uids']);
					$jpush_reg_ids = myPush::getJpushRegIds($uids);
					if(!$jpush_reg_ids){
						res_api('未检测到对应的APP用户');
					}
					break;
				case 1:
					$jpush_reg_ids = 'all';
					break;
			}
			$res = myPush::doPush($jpush_reg_ids, $msg);
			if($res){
				res_api();
			}else{
				res_api('推送失败');
			}
		}else{
			$option = ['name' => 'send_type','option' => [['val'=>1,'text'=>'全部用户','default'=>1],['val'=>2,'text'=>'指定用户','default'=>0]]];
			return $this->fetch('common@jpush/index',['option'=>$option]);
		}
	}
}