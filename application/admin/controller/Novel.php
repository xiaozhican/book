<?php
namespace app\admin\controller;

use site\myDb;
use site\myHttp;
use site\myBlock;
use site\myCache;
use site\mySearch;
use site\myValidate;
use app\common\model\mdBook;
use app\common\model\mdConfig;
use app\common\model\mdSpread;
use think\Db;

class Novel extends Common{
	
    //书籍列表
    public function index(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['type','=',1],['status','between',[1,3]]],
                'eq' => 'type,status,gender_type,over_type,free_type,is_god',
                'like' => 'keyword:name,category'
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdBook::getBookPageList($where, $pages);
            if($res['data']){
                foreach ($res['data'] as &$v){
                	$free_time = '';
                	if(myCache::checkBookFree($v['id'])){
                		$free_time = '未知';
                		$freeItem = myDb::getCur('BookFree', [['book_id','=',$v['id']]],'start_date,end_date');
                		if($freeItem){
                			$free_time = $freeItem['start_date'].'~'.$freeItem['end_date'];
                		}
                	}
                	$share_info = myCache::getBookShareInfo($v['id']);
                	$v['is_share'] = $share_info ? 1 : 2;
                	$share_info = null;
                	$v['free_time'] = $free_time;
                	$v['total_chapter'] = myCache::getBookChapterNum($v['id']);
                	$v['is_free'] = myCache::checkBookFree($v['id']);
                    $v['do_url'] = my_url('doBook',['id'=>$v['id']]);
                    $v['chapter_url'] = my_url('chapter',['book_id'=>$v['id']]);
                    $v['spread_url'] = my_url('Spread/createLink',['book_id'=>$v['id']]);
                    $v['guide_url'] = my_url('Guide/index',['book_id'=>$v['id']]);
                    $v['copy_url'] = my_url('copyLink',['id'=>$v['id']]);
                    $v['customer_url'] = my_url('Custom/addCustom',['book_id'=>$v['id']]);
                    $v['share_url'] = my_url('setShareData',['id'=>$v['id']]);
                }
            }
            res_table($res['data'],$res['count']);
        }else{
        	$category = getMyConfig('novel_category');
        	$keyword = $this->request->get('keyword');
        	$variable = ['js'=>getJs('book.index','admin',['date'=>'20200609']),'category'=>$category,'keyword'=>$keyword,'type'=>1];
        	return $this->fetch('common@book/index-admin',$variable);
        }
    }
    
    //复制链接
    public function copyLink(){
        $id = myHttp::getId('小说');
        $book = myDb::getById('Book',$id,'id,name');
        if(!$book){
            res_api('书籍参数错误');
        }
        $url = mdSpread::getSpreadUrl();
        $short_url = '/Index/Book/info.html?book_id='.$id;
        $data = [
            'notice' => '温馨提示 : 相对链接只能应用到页面跳转链接中，如轮播图链接等，渠道用户点击后不会跳转到总站',
            'links' => [
                ['title'=>'相对链接','val'=>$short_url],
                ['title'=>'绝对链接','val'=>$url.$short_url]
            ]
        ];
        return $this->fetch('common@public/copyLink',$data);
    }
    
    //新增小说
    public function addBook(){
        if($this->request->isAjax()){
        	$data = mdBook::getData(1);
        	$data['type'] = 1;
            mdBook::doneBook($data);
        }else{
            $field = 'id,name,cover,author,summary,money:28,status,long_type,free_type:2,new_type,gender_type,over_type,free_chapter:15';
            $field .= ',hot_num:0,share_title,share_desc,category,word_number:0,is_recommend:2';
            $option = mdBook::getOptions();
            $option['cur'] = myDb::buildArr($field);
            $option['category'] = getMyConfig('novel_category');
            $option['title'] = '新增小说';
            return $this->fetch('common@book/doBook',$option);
        }
    }
    
    //批量新增小说
    public function addMoreBook(){
        if($this->request->isAjax()){
            set_time_limit(7200);
            $rules = [
            	'status' => ["require|in:1,2,3",["require"=>"请选择书籍状态","in"=>"未指定该书籍状态"]],
            	'category' => ["array",["array"=>"小说分类参数异常"]],
            	'free_type' => ["require|in:1,2",["require"=>"请选择书籍是否免费","in"=>"未指定该书籍是否免费状态"]],
            	'gender_type' => ["require|in:1,2",["require"=>"请选择书籍频道","in"=>"未指定该书籍频道"]],
           		'over_type' => ["require|in:1,2",["require"=>"请选择书籍连载状态","in"=>"未指定该书籍连载状态"]],
            	'free_chapter' => ["number",["number"=>"免费章节必须为正整数"]],
            	'money' => ["require|number",["require"=>"请输入书籍章节收费书币数量","in"=>"书币数量必须为正整数"]],
            	'zip_title' => ['require|array',["require"=>"未检测到上传书籍","array"=>"书籍参数格式不规范"]],
            	'zip_filename' => ['require|array',["require"=>"未检测到上传书籍","array"=>"书籍参数格式不规范"]],
            ];
            $data = myValidate::getData($rules);
            mdBook::addMore($data);
        }else{
            $field = 'money:28,status:1,long_type,free_type:2,new_type,gender_type,over_type,free_chapter:15,area,category,is_recommend:2';
            $option = mdBook::getOptions();
            $option['cur'] = myDb::buildArr($field);
            $option['category'] = getMyConfig('novel_category');
            $option['title'] = '批量新增小说';
            return $this->fetch('common@book/addMoreBook',$option);
        }
    }
    
    //编辑小说
    public function doBook(){
        if($this->request->isAjax()){
        	$data = mdBook::getData(0);
            mdBook::doneBook($data);
        }else{
            $id = myHttp::getId('小说');
            $cur = myDb::getById('Book',$id);
            if(!$cur){
                res_api('书籍不存在');
            }
            $option = mdBook::getOptions();
            $option['category'] = getMyConfig('novel_category');
            $option['cur'] = $cur;
            $option['title'] = '更新小说信息';
            return $this->fetch('common@book/doBook',$option);
        }
    }
    
    //处理小说事件
    public function doBookEvent(){
       $flag = mdBook::doBookEvent();
       if($flag){
       		res_api();
       }else{
       		res_api('操作失败,请重试');
       }
    }
    
    //分集列表
    public function chapter(){
        $book_id = myHttp::getId('小说','book_id');
        if($this->request->isAjax()){
            $config = [
                'eq' => 'number',
                'like' => 'keyword:name',
            	'default' => [['book_id','=',$book_id]]
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $res = mdBook::getChapterPageList($where,$pages);
            if($res['data']){
            	$book = myCache::getBook($book_id);
            	if($book){
            		foreach ($res['data'] as &$v){
            			$v['is_free'] = 1;
            			if($book['free_type'] == 2){
            				if($v['number'] > $book['free_chapter']){
            					$v['is_free'] = 2;
            					if(!$v['money']){
            						$v['money'] = $book['money'];
            					}
            				}
            			}
            		}
            	}
            }
            res_table($res['data'],$res['count']);
        }else{
            
        	return $this->fetch('common@book/novel/chapter',['book_id'=>$book_id]);
        }
    }
    
    //解析zip文件
    public function doDecodeZip(){
        set_time_limit(1800);
        mdBook::doBookFile();
        res_api();
    }
    
    //新增章节
    public function addChapter(){
        if($this->request->isAjax()){
        	$data = mdBook::getChapterData();
            mdBook::doneChapter($data);
        }else{
        	$book_id = myHttp::getId('小说','book_id');
        	$number = myCache::getBookChapterNum($book_id);
            $field = 'id,name,number:'.($number+1).',content,money:0';
            $cur = myDb::buildArr($field);
            $cur['book_id'] = $book_id;
            return $this->fetch('common@book/novel/doChapter',['cur'=>$cur]);
        }
    }
    
    //设置广告出现页数
    public function setPageNoticeAll()
    {
        $book_id = $this->request->get('book_id');
        if($this->request->isAjax()){
            $data = mdConfig::getPageNoticeData();
            $res = Db::name('BookChapter')->where('book_id',$book_id)->update(['page_notice'=>$data]);
            if($res){
                res_api();
            }else{
                res_api('设置失败');
            }
        }else{
            return $this->fetch('common@book/novel/setPageNoticeAll');
        }
    }
    
    //编辑章节
    public function doChapter(){
        if($this->request->isAjax()){
        	$data = mdBook::getChapterData(0);
            mdBook::doneChapter($data);
        }else{
            $id = myHttp::getId('章节');
            $cur = myDb::getById('BookChapter', $id,'id,name,book_id,number,money');
            if(!$cur){
                res_api('章节不存在');
            }
            $cur['content'] = getBlockContent($cur['number'],'book/'.$cur['book_id']);
            return $this->fetch('common@book/novel/doChapter',['cur'=>$cur]);
        }
    }
    
    //查看章节
    public function showInfo(){
        $id = myHttp::getId('小说章节');
        $cur = myDb::getById('BookChapter', $id,'id,book_id,number');
        if(!$cur){
            res_api('章节不存在');
        }
        return $this->fetch('common@book/showChapter',['cur'=>$cur]);
    }
    
    //章节检测
    public function checkChapter(){
        $book_id = myHttp::postId('小说','book_id');
        $error = mdBook::checkChapter($book_id);
        if($error){
            res_api($error);
        }else{
            res_api('ok',1,0);
        }
    }
    
    //设置书币数量
    public function setChapterMoney(){
    	$data = mdBook::getChapterMoneyData();
    	$table = 'BookChapter';
    	$cur = myDb::getById($table,$data['id'],'id,book_id,money');
    	if(!$cur){
    		res_api('章节信息异常');
    	}
    	if($cur['money'] == $data['money']){
    		res_api();
    	}
    	$res = myDb::saveIdData($table, $data);
    	if($res){
    		myCache::rmBookChapterList($cur['book_id']);
    		res_api();
    	}else{
    		res_api('设置失败');
    	}
    }
    
    //设置章节广告出现页数
    public function setPageNotice()
    {
        $data = mdBook::getPageNoticeData();
        $table = 'BookChapter';
        $cur = myDb::getById($table,$data['id'],'id,book_id,page_notice');
        if(!$cur){
            res_api('章节信息异常');
        }
//    	if($cur['money'] == $data['money']){
//    		res_api();
//    	}
        $res = myDb::saveIdData($table, $data);
        if($res){
            myCache::rmBookChapterList($cur['book_id']);
            res_api();
        }else{
            res_api('设置失败');
        }
    }
    
    //删除章节
    public function delChapter(){
        $re = mdBook::delChapter();
        if($re){
            res_api();
        }else{
            res_api('删除失败');
        }
    }
    
    //清空所有章节
    public function delAllChapter(){
        $book_id = myHttp::postId('小说','book_id');
        $re = mdBook::delAllChapter($book_id);
        if($re){
            res_api();
        }else{
            res_api('删除失败');
        }
    }
    
    //发布区域配置
    public function area(){
        $key = 'novel_area';
        if($this->request->isAjax()){
        	$data = mdConfig::getAreaData();
            $res = mdConfig::saveConfig($key,$data);
            if($res){
                cache($key,$data);
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if($cur === false){
                $cur = [];
                $re = mdConfig::addConfig($key, $cur);
                if(!$re){
                    res_api('初始化数据失败，请重试');
                }
            }
            $variable = [
            	'cur' => $cur,
            	'backUrl' => url('index'),
                'title' => '小说发布区域配置'
                
            ];
            return $this->fetch('common@public/area',$variable);
        }
    }
    
    //轮播图片配置
    public function banners(){
    	$cate = myValidate::getData(['cate'=>['in:man,woman',['in'=>'访问参数异常']]],'get');
    	$cate = $cate ? : 'man';
        $key = 'novel_banner_'.$cate;
        if($this->request->isAjax()){
        	$data = mdConfig::getBannerData();
            $res = mdConfig::saveConfig($key,$data);
            if($res){
                $html = $this->fetch('common@block/banners',['list'=>$data]);
                saveBlock($html,$key,'other');
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if($cur === false){
                $cur = [];
                $re = mdConfig::addConfig($key, $cur);
                if(!$re){
                    res_api('初始化数据失败，请重试');
                }
            }
            $variable = [
            	'cur' => $cur,
                'cate' => $cate,
                'backUrl' => url('index'),
            	'title' => '小说轮播图片配置'
            ];
            return $this->fetch('common@book/banners',$variable);
        }
    }
    
    //类型配置
    public function category(){
        $key = 'novel_category';
        if($this->request->isAjax()){
        	$data = mdConfig::getCategoryData();
            $res = mdConfig::saveConfig($key,$data);
            if($res){
                cache($key,$data);
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if($cur === false){
                $cur = [];
                $re = mdConfig::addConfig($key, $cur);
                if(!$re){
                    res_api('初始化数据失败，请重试');
                }
            }
            $variable = [
            	'cur' => $cur,
                'title' => '小说类型配置',
                'backUrl' => url('index')
            ];
            return $this->fetch('common@public/category',$variable);
        }
    }

    //批量扣除金币
    public function delMoney()
    {
        if($this->request->isAjax()){
            $data = mdConfig::getMoneyData();
            $res = Db::name('Book')->update(['money'=>abs($data)]);
            if($res){
                res_api();
            }else{
                res_api('扣除失败');
            }
        }else{
            return $this->fetch('common@book/delMoney');
        }
    }

    //批量设置免费书
    public function setBook()
    {
        if($this->request->isAjax()){
            $data = mdConfig::getMoneyData();
            $res = Db::name('Book')->update(['free_chapter'=>abs($data)]);
            if($res){
                res_api();
            }else{
                res_api('设置失败');
            }
        }else{
            return $this->fetch('common@book/setBook');
        }
    }

    //批量设置广告页数
    public function noticeAll()
    {
        if($this->request->isAjax()){
            $data = mdConfig::getPageNoticeData();
            $res = Db::name('BookChapter')->where('id','>=',1)->update(['page_notice'=>$data]);
            if($res){
                res_api();
            }else{
                res_api('设置失败');
            }
        }else{
            $page_notice = Db::name('BookChapter')->where('id',1)->field('page_notice')->find();
            return $this->fetch('common@book/noticeAll',['page_notice'=>$page_notice]);
        }
    }
    
    //菜单导航
    public function nav(){
    	$cate = myValidate::getData(['cate'=>['in:man,woman',['in'=>'访问参数异常']]],'get');
    	$cate = $cate ? : 'man';
    	$key = 'novel_nav_'.$cate;
        if($this->request->isAjax()){
        	$data = mdConfig::getNavData();
            $res = mdConfig::saveConfig($key,$data);
            if($res){
                $html = $this->fetch('common@block/navMenu',['list'=>$data]);
                saveBlock($html,$key,'other');
                res_api();
            }else{
                res_api('保存失败');
            }
        }else{
            $cur = mdConfig::getConfig($key);
            if($cur === false){
                $cur = [];
                $re = mdConfig::addConfig($key,$cur);
                if(!$re){
                    res_api('初始化数据失败，请重试');
                }
            }
            $variable = [
            	'cur' => $cur,
            	'cate' => $cate,
                'backUrl' => url('index'),
            	'title' => '小说导航菜单配置'
            ];
            return $this->fetch('common@book/nav',$variable);
        }
    }
    
    //设置分享话术
    public function setShareData(){
    	if($this->request->isAjax()){
    		mdBook::doShareData();
    	}else{
    		$book_id = myHttp::getId('小说');
    		$cur = myDb::getCur('BookShare',[['book_id','=',$book_id]]);
    		if(!$cur){
    			$field = 'book_id:'.$book_id.',title,content';
    			$cur = myDb::buildArr($field);
    		}
    		$variable = ['cur'=>$cur,'backUrl'=>url('index')];
    		return $this->fetch('common@public/setShareData',$variable);
    	}
    }
    
    //刷新缓存
    public function refreshCache(){
    	myCache::clearAreaIds(1);
        myBlock::createIndexHtml(1,1,'novel_content_man');
        myBlock::createIndexHtml(1,2,'novel_content_woman');
        myBlock::createBookRank(1,1);
        myBlock::createBookRank(1,2);
        myCache::rmBookPublishIds(1,1);
        myCache::rmBookPublishIds(1,2);
        res_api();
    }
    
}