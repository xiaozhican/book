<?php


namespace app\common\model;


use site\myCache;
use site\myDb;
use site\myValidate;
use think\Db;

class mdNotice
{
    //广告列表
    public static function getChargeOrder($where,$pages){
        $field = '*';
        $count = 0;
        $data = [];
        $list = Db::name('Notice')->where($where)->field($field)->page($pages['page'],$pages['limit'])->order('id','desc')->select();
        if($list){
            $data = self::makeCleanData($list);
            $count = myDb::getCount('Notice', $where);
        }
        return ['data'=>$data,'count'=>$count];
    }
    //整理订单数据
    public static function makeCleanData($list){
        foreach ($list as $k=>$v){
            $one = [
                'id'=>$v['id'],
                'app_name' => $v['app_name'],
                'notice_name' => $v['notice_name'],
                'notice_type' => $v['notice_type'],
                'status' => $v['status'],
                'advertisers' => $v['advertisers'],
                'app_id' => $v['app_id'],
                'notice_id' => $v['notice_id'],
                'create_time' => date('Y-m-d H:i:s',$v['create_time'])
            ];
            $data[] = $one;
            $list[$k] = null;
        }
        return $data;
    }
    //获取活动事件值
    public static function getEventData(){
        $rules = [
            'id' =>  ["require|number|gt:0",['require'=>'主键参数错误','number'=>'主键参数不规范',"gt"=>"主键参数不规范"]],
            'event' => ["require|in:on,off,delete",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']]
        ];
        $data = myValidate::getData($rules);
        return $data;
    }
    //获取广告属性选项
    public static function getOptions(){
        $option = [
            'status' => [
                'name' => 'status',
                'option' => [['val'=>1,'text'=>'开启','default'=>1],['val'=>2,'text'=>'关闭','default'=>0]]
            ],
            'app_name' => [
                'name' => 'app_name',
                'option' => [['val'=>'Android','text'=>'Android','default'=>1],['val'=>'IOS','text'=>'IOS','default'=>0]]
            ],
            'notice_type'=>[
                'name'=>'notice_type',
                'option'=> [['val'=>'Banner广告','text'=>'Banner广告','default'=>1],['val'=>'开屏广告','text'=>'开屏广告','default'=>0],['val'=>'插屏广告','text'=>'插屏广告','default'=>0]]
            ],
            'advertisers'=>[
                'name'=>'advertisers',
                'option'=>[['val'=>'穿山甲','text'=>'穿山甲','default'=>1],['val'=>'广点通','text'=>'广点通','default'=>0]]
            ],
            'huawei_status' => [
                'name' => 'huawei_status',
                'option' => [['val'=>1,'text'=>'开启','default'=>1],['val'=>2,'text'=>'关闭','default'=>0]]
            ],
            'meizu_status' => [
                'name' => 'meizu_status',
                'option' => [['val'=>1,'text'=>'开启','default'=>1],['val'=>2,'text'=>'关闭','default'=>0]]
            ],
            'oppo_status' => [
                'name' => 'oppo_status',
                'option' => [['val'=>1,'text'=>'开启','default'=>1],['val'=>2,'text'=>'关闭','default'=>0]]
            ],
            'vivo_status' => [
                'name' => 'vivo_status',
                'option' => [['val'=>1,'text'=>'开启','default'=>1],['val'=>2,'text'=>'关闭','default'=>0]]
            ],
            'xiaomi_status' => [
                'name' => 'xiaomi_status',
                'option' => [['val'=>1,'text'=>'开启','default'=>1],['val'=>2,'text'=>'关闭','default'=>0]]
            ],
        ];
        return $option;
    }
    //获取规则
    public static function getRules($isAdd=1){
        $rules = [
            'notice_name' =>  ["require|max:200",["require"=>"请输入广告标题",'max'=>'广告标题最多支持200个字符']],
            'app_name'=>["require",["require"=>"请输入应用名称"]],
            'notice_type'=>["require",["require"=>"请选择广告类型"]],
            'status'=>["require|in:1,2",["require"=>"请选择广告状态","in"=>"未指定该广告状态"]],
            //'code'=>["require",["require"=>"请输入代码块"]],
            'app_id' => ["require|number",["require"=>"请输入应用名称ID","number"=>"参数格式不规范"]],
            'notice_id' => ["require|number",["require"=>"请输入广告名称ID","number"=>"参数格式不规范"]],
            'advertisers' => ["require",["require"=>"请输入广告商"]],
            'huawei_status'=>["require|in:1,2",["require"=>"请选择华为商店状态","in"=>"未指定该华为商店状态"]],
            'meizu_status'=>["require|in:1,2",["require"=>"请选择魅族商店状态","in"=>"未指定该魅族商店状态"]],
            'oppo_status'=>["require|in:1,2",["require"=>"请选择oppo商店状态","in"=>"未指定该oppo商店状态"]],
            'vivo_status'=>["require|in:1,2",["require"=>"请选择vivo商店状态","in"=>"未指定该vivo商店状态"]],
            'xiaomi_status'=>["require|in:1,2",["require"=>"请选择小米商店状态","in"=>"未指定该小米商店状态"]],
        ];
        if(!$isAdd){
            $rules['id'] = ["require|number|gt:0",['require'=>'主键参数错误','number'=>'主键参数不规范',"gt"=>"主键参数不规范"]];
        }
        return $rules;
    }
    //处理更新活动
    public static function doneNotice($data){
        if(isset($data['id'])){
            $flag = myDb::saveIdData('Notice', $data);
        }else {
            $data['create_time'] = time();
            $data['huawei'] = 'huawei';
            $data['meizu'] = 'meizu';
            $data['oppo'] = 'oppo';
            $data['vivo'] = 'vivo';
            $data['xiaomi'] = 'xiaomi';
            $flag = myDb::add('Notice', $data);
        }
        return $flag;
    }
}