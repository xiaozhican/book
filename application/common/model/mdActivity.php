<?php
namespace app\common\model;

use think\Db;
use site\myDb;
use site\myValidate;

class mdActivity{
	
    //获取总站活动列表
    public static function getListInSite($where,$pages){
        $field = 'a.*,count(b.id) as charge_nums,IFNULL(sum(b.money),0) as charge_total';
        $list = Db::name('Activity a')
        ->join('order b','a.id=b.pid and b.type=2 and b.status=2','left')
        ->where($where)
        ->field($field)
        ->group('a.id')
        ->page($pages['page'],$pages['limit'])
        ->order('a.id','DESC')
        ->select();
        $count = 0;
        if($list){
            $count = Db::name('Activity a')->where($where)->count();
        }
        return ['count'=>$count,'data'=>$list];
    }
    
    //获取渠道活动列表
    public static function getListInChannel($where,$pages,$key='channel_id'){
    	global $loginId;
    	$field = 'a.id,a.name,a.start_time,a.end_time,a.money,a.send_money,a.is_first,count(b.id) as charge_nums,IFNULL(sum(b.money),0) as charge_total';
    	$list = Db::name('Activity a')
    	->join('order b','a.id=b.pid and b.'.$key.'='.$loginId.' and b.type=2 and b.status=2 and b.is_count=1','left')
    	->where($where)
    	->field($field)
    	->group('a.id')
    	->page($pages['page'],$pages['limit'])
    	->order('a.id','DESC')
    	->select();
    	$count = 0;
    	if($list){
    		$count = Db::name('Activity a')->where($where)->count();
    	}
    	return ['count'=>$count,'data'=>$list];
    }
    
    //处理更新活动
    public static function doneActivity($data){
    	$data['start_time'] = strtotime($data['start_time']);
    	$data['end_time'] = strtotime($data['end_time']);
    	if($data['start_time'] >= $data['end_time']){
    		res_api('开始时间不能大于结束时间');
    	}
    	if(isset($data['id'])){
    		$flag = myDb::saveIdData('Activity', $data);
    	}else{
    		$data['create_time'] = time();
    		$flag = myDb::add('Activity', $data);
    	}
    	return $flag;
    }
    
    /**
     * 获取活动编码
     * @param number $actId 活动ID
     * @param number $channelId 渠道ID
     * @return number|unknown|mixed|string|bool
     */
    public static function getActivityCode($actId,$channelId=0){
    	$table = 'Code';
    	$code = myDb::getValue($table,[['cid','=',$channelId],['aid','=',$actId]],'code');
    	if(!$code){
    		$info = mdPlatform::getLevelInfo($channelId);
    		if(!$info){
    			res_api('数据异常，请联系管理员');
    		}
    		$data = [
    			'code' => myDb::createCode('Code'),
    			'cid' => $channelId,
    			'aid' => $actId,
    			'type' => 2,
    			'info' => json_encode($info)
    		];
    		$re = myDb::add($table,$data);
    		if($re){
    			$code = $data['code'];
    		}
    	}
    	return $code;
    }
    
    //获取规则
    public static function getRules($isAdd=1){
    	$rules = [
    		'name' =>  ["require|max:200",["require"=>"请输入活动标题",'max'=>'活动标题最多支持200个字符']],
    		'start_time' =>  ["require|date",['require'=>'请选择活动开始时间','date'=>'活动开始时间参数错误']],
    		'end_time' =>  ["require|date",['require'=>'请选择活动结束时间','date'=>'活动结束时间参数错误']],
    		'status' => ["require|in:1,2",["require"=>"请选择活动状态","in"=>"未指定该活动状态"]],
    		'is_first' => ["require|in:1,2",["require"=>"请选择充值限制次数","in"=>"未指定该限制次数类型"]],
    		'money' => ["require|float",["require"=>'请输入充值金额',"float"=>"充值金额格式不规范"]],
    		'send_money' => ["require|number",["require"=>'请输入赠送书币',"number"=>"赠送书币格式不规范"]]
    	];
    	if(!$isAdd){
    		$rules['id'] = ["require|number|gt:0",['require'=>'主键参数错误','number'=>'主键参数不规范',"gt"=>"主键参数不规范"]];
    	}
    	return $rules;
    }
    
    //获取活动属性选项
    public static function getOptions(){
        $option = [
            'status' => [
                'name' => 'status',
                'option' => [['val'=>1,'text'=>'启用','default'=>0],['val'=>2,'text'=>'禁用','default'=>1]]
            ],
            'is_first' => [
                'name' => 'is_first',
                'option' => [['val'=>1,'text'=>'仅限一次','default'=>1],['val'=>2,'text'=>'不限次数','default'=>0]]
            ]
        ];
        return $option;
    }
    
    //获取活动事件值
    public static function getEventData(){
    	$rules = [
    		'id' =>  ["require|number|gt:0",['require'=>'主键参数错误','number'=>'主键参数不规范',"gt"=>"主键参数不规范"]],
    		'event' => ["require|in:on,off,delete",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']]
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
}