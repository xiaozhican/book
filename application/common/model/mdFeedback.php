<?php
namespace app\common\model;

use think\Db;
use site\myDb;
use site\myCache;

class mdFeedback{
    
    
    //获取投诉列表
    public static function getComplaintList($where,$pages){
        $field = 'a.*,b.nickname,c.name as book_name';
        $list = Db::name('Complaint a')
        ->join('member b','a.uid=b.id','left')
        ->join('book c','a.book_id=c.id','left')
        ->where($where)
        ->field($field)
        ->page($pages['page'],$pages['limit'])
        ->order('a.id','DESC')
        ->select();
        $count = 0;
        if($list){
            foreach ($list as &$v){
                $v['type_name'] = self::getTypeName($v['type']);
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
            }
            $count = Db::name('Complaint a')->where($where)->count();
        }
        return ['data'=>$list,'count'=>$count];
    }
    
    //获取反馈列表
    public static function getFeedbackList($where,$pages){
        $field = 'a.*,b.nickname';
        $list = Db::name('Feedback a')
        ->join('member b','a.uid=b.id','left')
        ->where($where)
        ->field($field)
        ->page($pages['page'],$pages['limit'])
        ->order('a.id','DESC')
        ->select();
        $count = 0;
        if($list){
            foreach ($list as &$v){
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
            }
            $count = Db::name('Feedback a')->where($where)->count();
        }
        return ['data'=>$list,'count'=>$count];
    }
    
    //采纳意见
    public static function doAdopt($data){
    	$cur = myDb::getById('Feedback', $data['id'],'id,uid,is_adopt');
    	if(!$cur){
    		res_api('意见不存在');
    	}
    	if($cur['is_adopt'] == 1){
    		res_api('该意见已被采纳');
    	}
    	unset($data['id']);
    	$data['is_adopt'] = 1;
    	$data['adopt_time'] = time();
    	Db::startTrans();
    	$flag = false;
    	$re = Db::name('Feedback')->where('id',$cur['id'])->update($data);
    	if($re){
    		$flag = true;
    		if($data['adopt_coin']){
    			$flag = false;
    			$mData = [
    				'money' => Db::raw('money+'.$data['adopt_coin']),
    				'total_money' => Db::raw('total_money+'.$data['adopt_coin'])
    			];
    			$res = Db::name('Member')->where('id',$cur['uid'])->update($mData);
    			if($res){
    				$flag = true;
    			}
    		}
    	}
    	if($flag){
    		Db::commit();
    		if($data['adopt_coin']){
    			myCache::doMemberMoney($cur['uid'], $data['adopt_coin']);
    		}
    		myCache::rmMemberIsAdopt($cur['uid']);
    		mdMember::addChargeLog($cur['uid'], '+'.$data['adopt_coin'].' 书币', '采纳意见');
    	}else{
    		Db::rollback();
    	}
    	return $flag;
    }
    
    
    
    //获取类型名称
    private static function getTypeName($type){
        $name = '未知';
        switch ($type){
            case 1: $name = '色情低俗';break;
            case 2: $name = '政治敏感';break;
            case 3: $name = '暴力';break;
            case 4: $name = '违法';break;
            case 5: $name = '盗版';break;
            case 6: $name = '其他';break;
        }
        return $name;
    }
}