<?php
namespace app\common\model;

use think\Db;

class mdComments{

    //获取评论列表
    public static function getCommentsList($where,$pages){
        $field = 'a.*,b.nickname';
        $list = Db::name('Comments a')
            ->join('member b','a.uid=b.id','left')
            ->where($where)
            ->field($field)
            ->page($pages['page'],$pages['limit'])
            ->order('a.id','DESC')
            ->select();
        $count = 0;
        if($list){
            $count = Db::name('Comments a')->where($where)->count();
        }
        return ['count'=>$count,'data'=>$list];
    }

    //获取pc首页栏目列表(暂时不用)
    public static function getpcNavList_old($where,$pages){
        $list = Db::name('pc_nav')
            ->page($pages['page'],$pages['limit'])
            ->order('id asc')
            ->select();

        if ($list){
            foreach ($list as $k=>$v){
                $list[$k]['do_url'] = my_url('Comments/doPcnav',['id'=>$v['id']]);
            }
        }

        $count = 0;
        if($list){
            $count = Db::name('pc_nav')->where($where)->count();
        }
        return ['count'=>$count,'data'=>$list];
    }

    //获取pc首页栏目列表
    public static function getpcNavList($where,$pages){
        $list = self::cateTree();

        if ($list){
            foreach ($list as $k=>$v){
                $list[$k]['do_url'] = my_url('Comments/doPcnav',['id'=>$v['id']]);
                $list[$k]['do_add_child_url'] = my_url('Comments/addChildNav',['id'=>$v['id']]);
            }
        }

        $count = 0;
        return ['count'=>$count,'data'=>$list];
    }


    //无限极分类
    public static function cateTree(){
        $cateres=Db::name('pc_nav')
            ->order('id asc')
            ->select();//取出所有的栏目
        return self::sorts($cateres);
    }

    public static function sorts($data,$pid=0,$level=0){
        static $arr=[];//静态数组：多次调用sorts函数时候 前几次调用的$arr的值会被保留下来
        foreach ($data as $k=>$v){
            if ($v["pid"]==$pid){
                $v["level"]=$level;
                $arr[]=$v;
                self::sorts($data,$v["id"],$level+1);
            }
        }
        return $arr;
    }



    //点赞
    public static function doZan($pid,$uid){
        $data = ['uid'=>$uid,'pid'=>$pid];
        Db::startTrans();
        $flag = false;
        $re = Db::name('CommentsZan')->insert($data);
        if($re){
            $res = Db::name('Comments')->where('id',$pid)->setInc('zan_num');
            if($res){
                $flag = true;
            }
        }
        if($flag){
            Db::commit();
        }else{
            Db::rollback();
        }
        return $flag;
    }
}
