<?php
namespace app\common\model;

use site\myDb;

class mdRole{
    
    //处理更新角色
    public static function doneRole($data){
        $data['content'] = json_encode($data['content']);
        if(array_key_exists('id', $data)){
            $re = myDb::saveIdData('Role',$data);
        }else{
            $data['create_time'] = time();
            $re = myDb::add('Role', $data);
        }
        if($re){
            res_api();
        }else{
            res_api('保存失败，请重试');
        }
    }
    
    //获取更新角色选项
    public static function getOptions(){
        $option = [
            'status' => [
                'name' => 'status',
                'option' => [
                    ['val'=>1,'text'=>'启用','default'=>1],
                    ['val'=>2,'text'=>'禁用','default'=>0]
                ]
            ],
            'backUrl' => my_url('role')
        ];
        return $option;
    }
    
    //获取更新规则
    public static function getRules($isAdd=1){
    	$rules = [
    		'name' =>  ["require|max:20",["require"=>"请输入角色名称",'max'=>'角色名称最多支持20个字符']],
    		'status' => ["require|in:1,2",["require"=>"请选择角色状态","in"=>"未指定该角色状态"]],
    		'summary' => ["max:500",["max"=>"角色描述字数超出限制"]],
    		'content' => ["require|array",["require"=>"请选择该角色权限",'array'=>'权限集格式错误']],
    	];
    	if(!$isAdd){
    		$rules['id'] = ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]];
    	}
    	return $rules;
    }
    
    //获取事件规则
    public static function getEventRules(){
    	$rules = [
    		'id' =>  ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
    		'event' => ["require|in:on,off,delete",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']]
    	];
    	return $rules;
    }
}