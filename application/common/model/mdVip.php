<?php
namespace app\common\model;

use think\Db;
use site\myValidate;

class mdVip{
	
	//获取渠道列表
	public static function getChannelList($where,$pages){
		$count = 0;
		$field = 'a.id,a.name,a.login_name,a.status,b. charge_money as total_charge,c.sub_num as sub_today,c.charge_money as charge_today,d.sub_num as sub_yesterday,d.charge_money as charge_yesterday';
		$yesterday = date('Y-m-d',strtotime('yesterday'));
		$today = date('Y-m-d');
		$list = Db::name('Channel a')
		->join('channel_money b','a.id=b.channel_id','left')
		->join('channel_chart c','c.create_date="'.$today.'" and a.id=c.channel_id','left')
		->join('channel_chart d','d.create_date="'.$yesterday.'" and a.id=d.channel_id','left')
		->where($where)
		->field($field)
		->page($pages['page'],$pages['limit'])
		->select();
		if($list){
			$count = Db::name('Channel a')->where($where)->count();
		}
		return ['data'=>$list,'count'=>$count];
	}
	
	public static function getIncomeList($where,$pages){
		$list = Db::name('ChannelChart')->where($where)->page($pages['page'],$pages['limit'])->order('create_date','desc')->order('channel_id','desc')->select();
		$count = 0;
		if($list){
			$count = Db::name('ChannelChart')->where($where)->count();
		}
		return ['data'=>$list,'count'=>$count];
	}
	
	//获取账号更新数据
	public static function getData($isAdd=true){
		$rules = [
			'status' => ["require|in:1,2",["require"=>"请选择用户状态","in"=>"未指定该用户状态"]],
			'remark' => ["max:255",['max'=>'备注最多不超过255个字符']]
		];
		if($isAdd){
			$rules['login_name'] = ["require|alphaDash|length:5,12",["require"=>"请输入登陆账户名","alphaDash"=>'登陆账户名必须是英文、数字、下划线和破折号',"length"=>"请输入5至12位符合规范的登陆账户名"]];
			$rules['password'] = ["require|length:6,16",["require"=>"请输入登陆密码","length"=>"请输入6-16位登陆密码"]];
		}else{
			$rules['id'] = ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]];
		}
		$data = myValidate::getData($rules);
		return $data;
	}
	
	//获取用户事件数据
	public static function getEventData(){
		$rules = [
			'id' => ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
			'event' => ["require|in:on,off,delete,resetpwd",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']]
		];
		$data = myValidate::getData($rules);
		return $data;
	}
	
	//获取更新用户选项
	public static function getOptions(){
		$option = [
			'status' => [
				'name' => 'status',
				'option' => [
					['val'=>1,'text'=>'启用','default'=>1],
					['val'=>2,'text'=>'禁用','default'=>0]
				]
			],
			'backUrl' => url('index')
		];
		return $option;
	}
}