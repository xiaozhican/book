<?php
namespace app\common\model;

use think\Db;
use site\myDb;

class mdMessage{
	
	//获取总站消息列表
	public static function getAdminMsgList($where,$pages){
		$list = Db::name('Message a')
		->join('message_read b','a.id=b.message_id','left')
		->where($where)
		->field('a.*,count(b.id) as channel_num')
		->group('a.id')
		->page($pages['page'],$pages['limit'])
		->order('a.id','desc')
		->select();
		$count = 0;
		if(!empty($list)){
			$count = Db::name('Message a')->where($where)->count();
		}
		return ['data'=>$list,'count'=>$count];
	}
	
	//获取渠道消息列表
	public static function getChannelMsgList($where,$pages){
		$list = Db::name('MessageRead a')
		->join('message b','a.message_id=b.id')
		->where($where)
		->field('b.id,b.title,b.create_time,a.is_read')
		->page($pages['page'],$pages['limit'])
		->order('a.id','desc')
		->select();
		$count = 0;
		if(!empty($list)){
			$count = Db::name('MessageRead a')->where($where)->count();
		}
		return ['data'=>$list,'count'=>$count];
	}
    
    //处理更新公告消息
    public static function doneMessage($data){
        $content = $data['content'];
        unset($data['content']);
        $message_id = 0;
        if(array_key_exists('id', $data)){
            $message_id = $data['id'];
            $flag = myDb::saveIdData('Message',$data);
        }else{
        	Db::startTrans();
        	$flag = false;
            $data['create_time'] = time();
            $message_id = Db::name('Message')->insertGetId($data);
            if($message_id){
            	$flag = true;
            	$list = Db::name('Channel')->where('status',1)->column('id');
            	if(!empty($list)){
            		$flag = false;
            		$checkRes = true;
            		foreach ($list as $v){
            			$re = Db::name('MessageRead')->insert(['message_id'=>$message_id,'channel_id'=>$v]);
            			if(!$re){
            				$checkRes = false;
            				break;
            			}
            		}
            		$flag = $checkRes;
            	}
            }
            if($flag){
            	Db::commit();
            }else{
            	Db::rollback();
            }
        }
        if($flag){
            saveBlock($content,$message_id,'message');
            res_api();
        }else{
            res_api('保存失败，请重试');
        }
    }
    
    //删除消息
    public static function delMessage($id){
    	Db::startTrans();
    	$flag = false;
    	$re = Db::name('Message')->where('id',$id)->delete();
    	if($re !== false){
    		$res = Db::name('MessageRead')->where('message_id',$id)->delete();
    		if($res !== false){
    			$flag = true;
    		}
    	}
    	if($flag){
    		Db::commit();
    	}else{
    		Db::rollback();
    	}
    	return $flag;
    }
    
    //获取接收渠道列表
    public static function getChannelList($message_id,$pages){
    	$list = Db::name('MessageRead a')
    		->join('channel b','a.channel_id=b.id','left')
    		->where('a.message_id',$message_id)
    		->field('a.*,b.name as channel_name,b.is_wx')
    		->page($pages['page'],$pages['limit'])
    		->select();
    	$count = 0;
    	if(!empty($list)){
    		$count = Db::name('MessageRead')->where('message_id',$message_id)->count();
    	}
    	return ['data'=>$list,'count'=>$count];
    }
    
    //检查是否有未读公告
    public static function checkMessage(){
    	global $loginId;
    	$res = ['id'=>0];
    	$cur = Db::name('MessageRead a')
    		->join('message b','a.message_id=b.id')
    		->where('a.channel_id',$loginId)
    		->where('a.is_read',2)
    		->field('b.id,b.title')
    		->order('b.id','asc')
    		->find();
    	if($cur){
    		$res = ['id'=>$cur['id'],'title'=>$cur['title']];
    	}
    	return $res;
    }
    
    //获取更新消息规则
    public static function getRules($isAdd=1){
    	$rules = [
    		'title' =>  ["require|max:100",["require"=>"请输入公告标题",'max'=>'公告标题最多支持100个字符']],
    		'content' => ["require",["require"=>"请输入公告内容"]]
    	];
    	if(!$isAdd){
    		$rules['id'] = ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]];
    	}
    	return $rules;
    }
    
}