<?php


namespace app\common\model;


use site\myDb;
use site\myValidate;
use think\Db;

class mdCount
{
    //统计列表
    public static function getChargeOrder($start_time,$end_time,$pages){
        $field = '*';
        $count = 0;
        $data = [];
        $list = Db::name('Count')->whereTime('date_time','between',[$start_time,$end_time])->field($field)->page($pages['page'],$pages['limit'])->order('id','desc')->select();
        if($list){
            $data = self::makeCleanData($list);
            $count = myDb::getCountOnline('Count', $start_time,$end_time);
        }
        return ['data'=>$data,'count'=>$count];
    }
    //整理订单数据
    public static function makeCleanData($list){
        foreach ($list as $k=>$v){
            $one = [
                'id'=>$v['id'],
                'date_time' => $v['date_time'],
                'android' => $v['android'],
                'ios' => $v['ios'],
                'h5' => $v['h5'],
                'pc' => $v['pc'],
                'day_activity' => $v['day_activity'],
                'day_login' => $v['day_login'],
                //'create_time' => date('Y-m-d H:i:s',$v['create_time'])
            ];
            $data[] = $one;
            $list[$k] = null;
        }
        return $data;
    }
    //历史登录记录
    public static function getloginLogOrder($where,$pages){
        $field = '*';
        $count = 0;
        $data = [];
        $list = Db::name('Log')->where($where)->field($field)->page($pages['page'],$pages['limit'])->order('id','desc')->select();
        if($list){
            $data = self::makeLoginData($list);
            $count = myDb::getCount('Log', $where);
        }
        return ['data'=>$data,'count'=>$count];
    }
    //整理订单数据
    public static function makeLoginData($list){
        foreach ($list as $k=>$v){
            $one = [
                'id'=>$v['id'],
                'username' => $v['username'],
                //'login_ip' => $v['login_ip'],
                'login_type' => $v['login_type'],
                'status' => $v['status'],
                'remark' => $v['remark'],
                'is_online' => $v['is_online'],
                'create_time' => date('Y-m-d H:i:s',$v['create_time'])
            ];
            $data[] = $one;
            $list[$k] = null;
        }
        return $data;
    }
    //数据阅读记录
    public static function getReadLogOrder($where,$pages){
        $field = '*';
        $count = 0;
        $data = [];
        $list = Db::name('read_log')->where($where)->field($field)->page($pages['page'],$pages['limit'])->order('update_time','desc')->select();
        if($list){
            $data = self::makeReadLoginData($list);
            $count = myDb::getCount('read_log', $where);
        }
        return ['data'=>$data,'count'=>$count];
    }
    //整理订单数据
    public static function makeReadLoginData($list){
        foreach ($list as $k=>$v){
            $one = [
                'id'=>$v['id'],
                'book_id' => $v['book_id'],
                'book_name' => $v['book_name'],
                'create_time' => date('Y-m-d H:i:s',$v['create_time']),
                'update_time' => date('Y-m-d H:i:s',$v['update_time']),
            ];
            $data[] = $one;
            $list[$k] = null;
        }
        return $data;
    }

    public static function getReadDetailOrder($read_id,$where,$pages){
        $field = '*';
        $count = 0;
        $data = [];
        $list = Db::name('read_detail')->where(['read_id'=>$read_id])->where($where)->field($field)->page($pages['page'],$pages['limit'])->order('create_time','desc')->select();
        if($list){
            $data = self::makeReadDetailData($list);
            $count = myDb::getCountRead('read_detail', $where,$read_id);
        }
        return ['data'=>$data,'count'=>$count];
    }
    //整理订单数据
    public static function makeReadDetailData($list){
        foreach ($list as $k=>$v){
            $one = [
                'id'=>$v['id'],
                'username' => $v['username']==null?'游客':$v['username'],
                'book_name' => $v['book_name'],
                'book_zhangjie' => $v['book_zhangjie'],
                'create_time' => date('Y-m-d H:i:s',$v['create_time'])
            ];
            $data[] = $one;
            $list[$k] = null;
        }
        return $data;
    }

    //app渠道下载
    public static function getAppDownloadOrder($where,$pages){
        $field = "*";
        $count = 0;
        $data = [];
        //$list = Db::name('AppDownload')->whereTime('datetime','between',[$start_time,$end_time])->field($field)->page($pages['page'],$pages['limit'])->order('id','desc')->select();
        $starts = Db::name('AppStart')->field('count(id) c,channel')->where('type','android')->group('channel')->select();//统计相同渠道的数量
        foreach ($starts as $start){
            $key = 'channel_'.$start['channel'];
            if(cache('?'.$key)){
                $val = cache($key);
                if($val != $start['c']){
                    Db::name('AppDownload')->where('channel',$start['channel'])->update(['app_start'=>$start['c']]);
                    cache($key,$start['c']);
                }
            }else{
                Db::name('AppDownload')->where('channel',$start['channel'])->update(['app_start'=>$start['c']]);
                cache($key,$start['c']);
            }
        }
        self::countReg();
        self::countHuitou();
        //var_dump(3633);die;
        $zuotian_date = date('Y-m-d',strtotime('-1 day'));
        $day_date = date('Y-m-d');
        self::dayData();
        $app_download_flag = Db::name('AppDownloadFlag')->order('id desc')->limit(1)->find();
        if(!$app_download_flag){
            //如果表空的，则访问昨天数据，并写入今日数据
            self::zuotianData();
            Db::name('AppDownloadFlag')->insert([
                'date' => $day_date,
                'flag' => 1
            ]);
        }else{
            if($app_download_flag['date'] == $day_date){
                if($app_download_flag['flag']==0){
                    //说明今日还未访问，获取昨天数据，并把标记改为1（已访问）
                    self::zuotianData();
                    Db::name('AppDownloadFlag')->where('date',$day_date)->update(['flag'=>1]);
                }
            }elseif($app_download_flag['date'] == $zuotian_date){
                //如果没有今日数据，获取昨天数据，并把标记改为1（已访问）
                self::zuotianData();
                Db::name('AppDownloadFlag')->insert([
                    'date' => $day_date,
                    'flag' => 1
                ]);
            }else{
                //$begin = $app_download_flag['date'];
                $begintime = strtotime($app_download_flag['date']);$endtime = strtotime($day_date);
                for ($start = $begintime; $start <= $endtime; $start += 24 * 3600) {
                    $date_time = date("Y-m-d", $start);
                    Db::name('AppDownloadFlag')->insert([
                        'date' => $date_time,
                        'flag' => 0
                    ]);
                }
                self::zuotianData();
                Db::name('AppDownloadFlag')->where('date',$day_date)->update(['flag'=>1]);
            }
        }

        $list = Db::name('AppDownload')->where($where)->field($field)->page($pages['page'],$pages['limit'])->order('id','desc')->select();
        if($list){
            $data = self::makeAppDownloadData($list);
            $count = myDb::getCountAppDownloadOnline('AppDownload', $where);
        }
        return ['data'=>$data,'count'=>$count];
    }

    //昨天的数据
    public static function zuotianData()
    {
        $zuotian_date = date('Y-m-d',strtotime('-1 day'));
        $zt_regs = Db::name('ChannelReg')->where('date',$zuotian_date)->find();//昨天注册数据
        if(!$zt_regs){
            //如果昨天没有数据，则添加昨天数据
            $channels = ['default','xiaomi','oppo','meizu','huawei','vivo'];
            foreach ($channels as $channel){
                $day_count = Db::name('Member')->where('channel',$channel)->whereBetween('create_time',[strtotime($zuotian_date.' 00:00:00'),strtotime($zuotian_date.' 23:59:59')])->count();//统计今日渠道注册人数
                $order = Db::name('ChannelReg')->where('date',$zuotian_date)->find();//查找今日是否有数据
                if(!$order){
                    Db::name('ChannelReg')->insert([
                        'date' => $zuotian_date,
                        $channel => $day_count
                    ]);
                }else{
                    Db::name('ChannelReg')->where('date',$zuotian_date)->update([$channel=>$day_count]);
                }
            }
        }
        $zt_regs = !$zt_regs?Db::name('ChannelReg')->where('date',$zuotian_date)->find():$zt_regs;
        foreach ($zt_regs as $key=>$v){
            Db::name('AppDownload')->where('channel',$key)->update(['zuotian_reg'=>$v]);
        }

        $zt_huitou = Db::name('Huitou')->where('date',$zuotian_date)->find();//昨天回头
        foreach ($zt_huitou as $key=>$item){
            Db::name('AppDownload')->where('channel',$key)->update(['zuotian_huitou'=>$item]);
        }
    }

    //今日数据
    public static function dayData()
    {
        $day_date = date('Y-m-d');
        $regs = Db::name('ChannelReg')->where('date',$day_date)->find();//今日注册数据
        foreach ($regs as $k=>$reg){
            Db::name('AppDownload')->where('channel',$k)->update(['day_reg'=>$reg]);
        }

        $day_huitou = Db::name('Huitou')->where('date',$day_date)->find();//今日回头
        foreach ($day_huitou as $m=>$n){
            Db::name('AppDownload')->where('channel',$m)->update(['day_huitou'=>$n]);
        }
    }

    public static function makeAppDownloadData($list){
        foreach ($list as $k=>$v){
            $one = [
                'id'=>$v['id'],
                'datetime' => $v['datetime'],
                'channel' => $v['channel']=='default'?'官网':$v['channel'],
                'num' => $v['num'],
                'app_start' => $v['app_start'],
                'zuotian_reg' => $v['zuotian_reg'],
                'day_reg' => $v['day_reg'],
                'zuotian_huitou' => $v['zuotian_huitou'],
                'day_huitou' => $v['day_huitou'],
            ];
            $data[] = $one;
            $list[$k] = null;
        }
        return $data;
    }

    //app渠道下载详情
    public static function getAppDownloadDetailOrder($app_download_id,$pages){
        $field = "*";
        $count = 0;
        $data = [];
        $list = Db::name('AppDownloadDetail')->where('app_download_id',$app_download_id)->field($field)->page($pages['page'],$pages['limit'])->order('id','desc')->select();
        if($list){
            $data = self::makeAppDownloadDetailData($list);
            $count = myDb::getCountAppDownloadDetail('AppDownloadDetail', $app_download_id);
        }
        return ['data'=>$data,'count'=>$count];
    }
    public static function makeAppDownloadDetailData($list){
        foreach ($list as $k=>$v){
            $one = [
                'id'=>$v['id'],
                'date_time' => $v['date_time'],
                'channel' => $v['channel']=='default'?'官网':$v['channel'],
                'uuid' => $v['uuid'],
                'datetime' => $v['datetime']
            ];
            $data[] = $one;
            $list[$k] = null;
        }
        return $data;
    }

    //回头客
    public static function getAppStartOrder($channel,$where,$pages){
        $field = "*";
        $count = 0;
        $data = [];
        $list = Db::name('AppStart')->where('channel',$channel)->where('type','android')->where($where)->field($field)->page($pages['page'],$pages['limit'])->order('id','desc')->select();
        if($list){
            $data = self::makeAppStartData($list);
            $count = myDb::getCountAppStart('AppStart', $channel);
        }
        return ['data'=>$data,'count'=>$count];
    }
    public static function makeAppStartData($list){
        foreach ($list as $k=>$v){
            $one = [
                'id'=>$v['id'],
                'phone' => $v['phone'],
                'channel' => $v['channel']=='default'?'官网':$v['channel'],
                'date_time' => date('Y-m-d H:i:s',$v['date_time']),
                'phone_register' => date('Y-m-d H:i:s',$v['phone_register'])
            ];
            $data[] = $one;
            $list[$k] = null;
        }
        return $data;
    }

    //回头客详情
    public static function getAppStartDetailOrder($phone,$where,$pages){
        $field = "*";
        $count = 0;
        $data = [];
        $list = Db::name('AppStartDetail')->where('phone',$phone)->where('type','android')->where($where)->field($field)->page($pages['page'],$pages['limit'])->order('id','desc')->select();
        if($list){
            $data = self::makeAppStartDetailData($list);
            $count = myDb::getCountAppStartDetail('AppStartDetail', $phone);
        }
        return ['data'=>$data,'count'=>$count];
    }
    public static function makeAppStartDetailData($list){
        foreach ($list as $k=>$v){
            $one = [
                'id'=>$v['id'],
                'phone' => $v['phone'],
                'channel' => $v['channel']=='default'?'官网':$v['channel'],
                'date_time' => date('Y-m-d H:i:s',$v['date_time']),
                'phone_register' => date('Y-m-d H:i:s',$v['phone_register'])
            ];
            $data[] = $one;
            $list[$k] = null;
        }
        return $data;
    }

    //统计注册人数
    public static function countReg()
    {
        $channels = ['default','xiaomi','oppo','meizu','huawei','vivo'];
        $date = date('Y-m-d');
        $channel_reg = Db::name('ChannelReg')->order('id desc')->limit(1)->find();
        if(!$channel_reg){
            //如果没有数据，则添加昨天和今日数据
            foreach ($channels as $channel){
                $zuotian_count = Db::name('Member')->where('channel',$channel)->whereBetween('create_time',[strtotime(date('Y-m-d 00:00:00',strtotime('-1 day'))),strtotime(date('Y-m-d 23:59:59',strtotime('-1 day')))])->count();//统计昨天的渠道注册人数
                $zuotian = date('Y-m-d',strtotime('-1 day'));//昨天日期
                $z_order = Db::name('ChannelReg')->where('date',$zuotian)->find();//查找昨天是否有数据
                if(!$z_order){
                    Db::name('ChannelReg')->insert([
                        'date' => $zuotian,
                        $channel => $zuotian_count
                    ]);
                }else{
                    Db::name('ChannelReg')->where('date',$date)->update([$channel=>$zuotian_count]);
                }

                $day_count = Db::name('Member')->where('channel',$channel)->whereBetween('create_time',[strtotime($date.' 00:00:00'),strtotime($date.' 23:59:59')])->count();//统计今日渠道注册人数
                $order = Db::name('ChannelReg')->where('date',$date)->find();//查找今日是否有数据
                if(!$order){
                    Db::name('ChannelReg')->insert([
                        'date' => $date,
                        $channel => $day_count
                    ]);
                }else{
                    Db::name('ChannelReg')->where('date',$date)->update([$channel=>$day_count]);
                }
            }
        }else{
            if($channel_reg['date'] == $date){
                //如果表中最近的日期是今天，则修改数据
                foreach ($channels as $channel){
                    $day_count = Db::name('Member')->where('channel',$channel)->whereBetween('create_time',[strtotime($date.' 00:00:00'),strtotime($date.' 23:59:59')])->count();//统计今日渠道注册人数
                    Db::name('ChannelReg')->where('date',$date)->update([$channel=>$day_count]);
                }
            }elseif ($channel_reg['date'] == date('Y-m-d',strtotime('-1 day'))){
                //如果表中最近的日期是昨天，说明今天还未有数据，则先插入
                foreach ($channels as $channel){
                    $day_count = Db::name('Member')->where('channel',$channel)->whereBetween('create_time',[strtotime($date.' 00:00:00'),strtotime($date.' 23:59:59')])->count();//统计今日渠道注册人数
                    $order = Db::name('ChannelReg')->where('date',$date)->find();//查找今日是否有数据
                    if(!$order){
                        Db::name('ChannelReg')->insert([
                            'date' => $date,
                            $channel => $day_count
                        ]);
                    }else{
                        Db::name('ChannelReg')->where('date',$date)->update([$channel=>$day_count]);
                    }
                }
            }else{
                //如果是几天前，则循环添加到当天
                $begin = $channel_reg['date'];
                $begintime = strtotime($begin);$endtime = strtotime($date);
                for ($start = $begintime; $start <= $endtime; $start += 24 * 3600) {
                    $date_time = date("Y-m-d", $start);
                    //var_dump($date_time);
                    foreach ($channels as $channel){
                        $day_start1 = strtotime($date_time.' 00:00:00');//今日开始时间
                        $day_end1 = strtotime($date_time.' 23:59:59');//今日结束时间
                        //$day_reg_count = Db::name('Member')->whereBetween('create_time',[$day_start,$day_end])->where('channel',$channel)->count();//统计今日各渠道注册人数
                        $day_count = Db::name('Member')->whereBetween('create_time',[$day_start1,$day_end1])->where('channel',$channel)->count();//统计今日回头访问人数
                        $order = Db::name('ChannelReg')->where('date',$date)->find();//查找今日是否有数据
                        if(!$order){
                            Db::name('ChannelReg')->insert([
                                'date' => $date,
                                $channel => $day_count
                            ]);
                        }else{
                            Db::name('ChannelReg')->where('date',$date)->update([$channel=>$day_count]);
                        }
                    }
                }
            }
        }
    }
    //统计回头率
    public static function countHuitou()
    {
        $channels = ['default','xiaomi','oppo','meizu','huawei','vivo'];
        $huitou = Db::name('Huitou')->order('id desc')->limit(1)->find();
        //$huitou_rate = Db::name('HuitouRate')->order('id desc')->limit(1)->find();
        $zt_start = strtotime(date('Y-m-d 00:00:00',strtotime('-1 day')));//昨天开始时间
        $zt_end = strtotime(date('Y-m-d 23:59:59',strtotime('-1 day')));//昨天结束时间
        $day_start = strtotime(date('Y-m-d 00:00:00'));//今日开始时间
        $day_end = strtotime(date('Y-m-d 23:59:59'));//今日结束时间
        $zt_date = date('Y-m-d',strtotime('-1 day'));//昨天日期
        $day_date = date('Y-m-d');//今天日期
        if(!$huitou){
            //如果数据空的，则统计昨天和今日的
            foreach ($channels as $channel){
                //var_dump($channel);
                //$zt_reg_count = Db::name('Member')->whereBetween('create_time',[$zt_start,$zt_end])->where('channel',$channel)->count();//统计昨天各渠道注册人数
                $zt_huitou = Db::name('AppStart')->whereBetween('date_time',[$zt_start,$zt_end])->where(['channel'=>$channel,'type'=>'android'])->count();//统计昨天回头访问人数
                //$day_reg_count = Db::name('Member')->whereBetween('create_time',[$day_start,$day_end])->where('channel',$channel)->count();//统计今日各渠道注册人数
                $day_huitou = Db::name('AppStart')->whereBetween('date_time',[$day_start,$day_end])->where(['channel'=>$channel,'type'=>'android'])->count();//统计今日回头访问人数
                //var_dump($zt_reg_count);
                $order = Db::name('Huitou')->where('date',$zt_date)->find();//查找是否有昨天的数据
                if(!$order){
                    Db::name('Huitou')->insert([
                        'date' => $zt_date,
                        $channel => $zt_huitou,
                    ]);
                }else{
                    Db::name('Huitou')->where('date',$zt_date)->update([
                        $channel => $zt_huitou
                    ]);
                }
                $day_order = Db::name('Huitou')->where('date',$day_date)->find();//查找是否有今日的数据
                if(!$day_order){
                    Db::name('Huitou')->insert([
                        'date' => $day_date,
                        $channel => $day_huitou
                    ]);
                }else{
                    Db::name('Huitou')->where('date',$day_date)->update([
                        $channel => $day_huitou
                    ]);
                }
            }
        }else{
            if($huitou['date'] == $day_date){
                //var_dump(11);
                foreach ($channels as $channel){
                    //$day_reg_count = Db::name('Member')->whereBetween('create_time',[$day_start,$day_end])->where('channel',$channel)->count();//统计今日各渠道注册人数
                    $day_huitou = Db::name('AppStart')->whereBetween('date_time',[$day_start,$day_end])->where(['channel'=>$channel,'type'=>'android'])->count();//统计今日回头访问人数
                    Db::name('Huitou')->where('date',$day_date)->update([
                        $channel => $day_huitou
                    ]);
//                    Db::name('HuitouRate')->where('date',$day_date)->update([
//                        $channel => $day_huitou==0?0:($day_huitou/$day_reg_count)*100
//                    ]);
                }
            }elseif ($huitou['date'] == $zt_date){
                //如果最后一个数据是昨天的，说明今天还未有数据
                foreach ($channels as $channel){
                    //$day_reg_count = Db::name('Member')->whereBetween('create_time',[$day_start,$day_end])->where('channel',$channel)->count();//统计今日各渠道注册人数
                    $day_huitou = Db::name('AppStart')->whereBetween('date_time',[$day_start,$day_end])->where(['channel'=>$channel,'type'=>'android'])->count();//统计今日回头访问人数
                    $day_order = Db::name('Huitou')->where('date',$day_date)->find();//查找是否有今日的数据
                    if(!$day_order){
                        Db::name('Huitou')->insert([
                            'date' => $day_date,
                            $channel => $day_huitou
                        ]);
                    }else{
                        Db::name('Huitou')->where('date',$day_date)->update([
                            $channel => $day_huitou
                        ]);
                    }
                }
            }else{
                //如果是几天前，则循环添加到当天
                $begin = $huitou['date'];
                $begintime = strtotime($begin);$endtime = strtotime($day_date);
                for ($start = $begintime; $start <= $endtime; $start += 24 * 3600) {
                    $date_time = date("Y-m-d", $start);
                    foreach ($channels as $channel){
                        $day_start1 = strtotime($date_time.' 00:00:00');//今日开始时间
                        $day_end1 = strtotime($date_time.' 23:59:59');//今日结束时间
                        //$day_reg_count = Db::name('Member')->whereBetween('create_time',[$day_start,$day_end])->where('channel',$channel)->count();//统计今日各渠道注册人数
                        $day_huitou = Db::name('AppStart')->whereBetween('date_time',[$day_start1,$day_end1])->where(['channel'=>$channel,'type'=>'android'])->count();//统计今日回头访问人数
                        $day_order = Db::name('Huitou')->where('date',$date_time)->find();//查找是否有今日的数据
                        if(!$day_order){
                            Db::name('Huitou')->insert([
                                'date' => $date_time,
                                $channel => $day_huitou
                            ]);
                        }else{
                            Db::name('Huitou')->where('date',$date_time)->update([
                                $channel => $day_huitou
                            ]);
                        }
                    }
                }
            }
        }
    }
}