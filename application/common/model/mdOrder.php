<?php
namespace app\common\model;

use think\Db;
use site\myDb;
use site\myCache;

class mdOrder{
	
    
	//获取充值订单分页列表
	public static function getChargeOrder($where,$pages){
		$field = '*';
		$count = 0;
		$data = [];
		$list = Db::name('Order')->where($where)->field($field)->page($pages['page'],$pages['limit'])->order('id','desc')->select();
		if($list){
			$data = self::makeCleanData($list);
			$count = myDb::getCount('Order', $where);
		}
		return ['data'=>$data,'count'=>$count];
	}
	
	//整理订单数据
	public static function makeCleanData($list){
		foreach ($list as $k=>$v){
			$one = [
				'order_no' => $v['order_no'],
				'is_count' => $v['is_count'],
				'charge_num' => $v['charge_num'],
				'charge_money' => $v['charge_money'],
				'money' => $v['money'],
				'status' => $v['status'],
				'pay_type' => $v['pay_type'],
				'create_time' => date('Y-m-d H:i',$v['create_time'])
			];
			$count_info = json_decode($v['count_info']);
			$one['count_info'] = $count_info ? : '';
			$one['userinfo'] = 0;
			$one['userinfo_url'] = my_url('Member/info',['id'=>$v['uid']]);
			$member = myCache::getMember($v['uid']);
			if($member){
				$one['userinfo'] = ['id'=>$member['id'],'openid'=>$member['openid'],'nickname'=>$member['nickname'],'headimgurl'=>$member['headimgurl'],'create_time'=>date('Y-m-d H:i:s',$member['create_time'])];
				$member = null;
			}
			$one['channel_name'] = '';
			if($v['channel_id']){
				$channel = myCache::getChannel($v['channel_id']);
				$one['channel_name'] = $channel ? '渠道：'.$channel['name'] : '渠道：<font class="text-read">未知</font>';
				$channel = null;
				if($v['agent_id']){
					$agent = myCache::getChannel($v['agent_id']);
					$one['channel_name'] .= $agent ? '<br />代理：'.$agent['name'] : '<br />代理：<font class="text-read">未知</font>';
					$agent = null;
				}else{
					$one['channel_name'] .= '<br />代理:--';
				}
			}
			if($v['spread_id']){
				$spread = myCache::getSpread($v['spread_id']);
				$one['spread'] = $spread ? $spread['name'] : '<font class="text-read">未知</font>';
				$spread = null;
			}else{
				$one['spread'] = '直接充值';
			}
			if($v['type'] == 1){
				$one['from_name'] = '--';
				if($v['pid']){
					$book = myCache::getBook($v['pid']);
					$one['from_name'] = $book ? '书籍：'.$book['name'] : '<font class="text-read">未知</font>';
					$book = null;
				}
			}else{
				$activity = myCache::getActivity($v['pid']);
				$one['from_name'] = $activity ? '活动：'.$activity['name'] : '<font class="text-read">未知</font>';
			}
			$one['is_info'] = ($v['channel_id'] > 0 && $v['is_count'] == 1 && $one['count_info'] && $v['status'] == 2) ? 1 : 2;
			$data[] = $one;
			$list[$k] = null;
		}
		return $data;
	}
	
	//获取打赏订单
	public static function getRewardOrder($where,$pages){
		$field = '*';
		$count = 0;
		$data = [];
		$list = Db::name('OrderReward')->where($where)->field($field)->page($pages['page'],$pages['limit'])->order('id','desc')->select();
		if($list){
			foreach ($list as &$v){
				$one = [
					'count' => $v['count'],
					'money' => $v['money'],
					'gift' => json_decode($v['gift'],true),
					'create_time' => date('Y-m-d H:i',$v['create_time']),
					'userinfo' => '',
					'channel_name' => '',
					'book_name' => ''
				];
				$one['userinfo_url'] = my_url('Member/info',['id'=>$v['uid']]);
				$member = myCache::getMember($v['uid']);
				if($member){
					$one['userinfo'] = ['id'=>$member['id'],'openid'=>$member['openid'],'nickname'=>$member['nickname'],'headimgurl'=>$member['headimgurl'],'create_time'=>date('Y-m-d H:i:s',$member['create_time'])];
					$member = null;
				}
				$one['channel_name'] = '';
				if($v['channel_id']){
					$channel = myCache::getChannel($v['channel_id']);
					$one['channel_name'] = $channel ? '渠道：'.$channel['name'] : '渠道：<font class="text-read">未知</font>';
					$channel = null;
					if($v['agent_id']){
						$agent = myCache::getChannel($v['agent_id']);
						$one['channel_name'] .= $agent ? '<br />代理：'.$agent['name'] : '<br />代理：<font class="text-read">未知</font>';
						$agent = null;
					}else{
						$one['channel_name'] .= '<br />代理:--';
					}
				}
				$book = myCache::getBook($v['pid']);
				$one['book_name'] = $book ? '书籍：'.$book['name'] : '<font class="text-read">未知</font>';
				$book = null;
				$data[] = $one;
			}
			$count = myDb::getCount('Order', $where);
		}
		return ['data'=>$data,'count'=>$count];
	}
	
    //创建订单号
    public static function createOrderNo($table='Order'){
    	$orderno = date('YmdHis').mt_rand(100000,999999);
    	$repeat = Db::name($table)->where('order_no','=',$orderno)->value('id');
    	if($repeat){
    		self::createOrderNo($table);
    	}
    	return $orderno;
    }
    
    
    //获取扣量代理福利时长相关信息
    public static function createOrderInfo($member){
    	$channel_id = $agent_id = 0;
    	$is_count_temp = 1;
    	if($member['channel_id'] > 0){
    		$channel_id = $member['channel_id'];
    		$channel = myCache::getChannel($channel_id);
    		if(!$channel){
    			res_api('渠道信息异常');
    		}
    		if($member['agent_id'] > 0){
    			$agent_id = $member['agent_id'];
    			//开启代理福利时长
    			if($channel['wefare_days'] > 0){
    				$now_time = time();
    				$end_time = $member['create_time'] + 86400*$channel['wefare_days'];
    				if($now_time > $end_time){
    					$agent_id = 0;
    				}
    			}
    		}
    	}
    	return ['uid'=>$member['id'],'channel_id'=>$channel_id,'agent_id'=>$agent_id,'wx_id'=>$member['wx_id']];
    }
    
    //获取分成信息
    public static function getCountInfo($channel_id,$money){
    	$data = [];
    	$channel = myCache::getChannel($channel_id);
    	if($channel){
    		$data = [
    			'id' => $channel['id'],
    			'name' => $channel['name'],
    			'ratio' => '0%',
    			'money' => 0
    		];
    		if($channel['ratio']){
    			$ratio = $channel['ratio']/100;
    			$data['ratio'] = $channel['ratio'].'%';
    			$data['money'] = round($money * $ratio,2);
    		}
    	}
    	return $data;
    }
    
    //处理打赏
    public static function doReward($data,$book_name){
    	Db::startTrans();
    	$flag = false;
    	$re = Db::name('OrderReward')->insert($data);
    	if($re){
    		$res  = Db::name('Member')->where('id',$data['uid'])->setDec('money',$data['money']);
    		if($res){
    			$log = [
    				'uid' => $data['uid'],
    				'book_id' => $data['pid'],
    				'money' => $data['money'],
    				'summary' => '打赏书籍《'.$book_name.'》',
    				'create_time' => time()
    			];
    			$log_res = Db::name('MemberConsume')->insert($log);
    			if($log_res){
    				$flag = true;
    			}
    		}
    	}
    	if($flag){
    		Db::commit();
    	}else{
    		Db::rollback();
    	}
    	return $flag;
    }
    
    //创建支付url
    public static function createPayUrl($order_no,$pay_type,$back_url=''){
    	$pay_url = '';
    	$site = getMyConfig('website');
    	switch ($pay_type){
    		case 1:
    			$pay_url = 'http://'.$site['url'].'/index/Pay/jsPay.html?order_no='.$order_no;
    			break;
    		case 2:
    			$pay_url = 'http://'.$site['url'].'/index/Pay/mihuaPay.html?order_no='.$order_no;
    			break;
            case 3:
                $pay_url = 'http://'.$site['url'].'/index/Pay/payjq.html?order_no='.$order_no;
                break;
            case 4:
                $pay_url = 'http://'.$site['url'].'/index/Pay/aliScanPay.html?order_no='.$order_no;
                break;
            case 5:
                $pay_url = 'http://'.$site['url'].'/index/Pay/wxScanPay.html?order_no='.$order_no;
                break;
    	}
    	if($back_url){
    		$pay_url .= '&back_url='.urlencode($back_url);
    	}
    	return $pay_url;
    }

    //获取签名
    public static function getSign($arr){
        //去除数组中的空值、去除数组中键名为sign的值
        $arr=array_filter($arr);
        unset($arr['sign']);
        //排序
        ksort($arr);
        //组装字符串
        $str=self::arrToUrl($arr).'&key=2k2hgyu2g3u2hj3i2j3i2j3i2h32h32g';
        //使用md5 加密
        $str2=md5($str);
        //转换成大写
        return strtoupper($str2);
    }

    //获取带签名的数组
    public static function setSign($arr){
        $arr['sign']= self::getSign($arr);
        return $arr;
    }

    //数组转url字符串  不带key
    public static function arrToUrl($arr){
        $str=urldecode(http_build_query($arr));
        return $str;
    }

}