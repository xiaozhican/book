<?php
namespace app\common\model;

use think\Db;
use site\myDb;

class mdPush{
    
    //获取推送消息详情
    public static function getPushInfo($channel_id=0){
        $temp = [
            'type1' => ['status'=>2,'hours'=>0,'content'=>'','dourl'=>my_url('doPush',['type'=>1])],
            'type2' => ['status'=>2,'content'=>''],
            'type3' => ['status'=>2,'content'=>''],
            'type4' => ['status'=>2,'content'=>''],
        	'type5' => ['status'=>2,'hours'=>0,'content'=>'','dourl'=>my_url('doPush',['type'=>5])],
        	'type6' => ['status'=>2,'content'=>'','dourl'=>my_url('doNovelPush',['type'=>6])],
        ];
        $list = Db::name('TaskMessage')->where('channel_id','=',$channel_id)->select();
        if($list){
            foreach ($list as $v){
                $key = 'type'.$v['type'];
                if(!isset($temp[$key])){
                    continue;
                }
                if($temp[$key]['status'] == 6){
                    $temp[$key]['status'] = '';
                }
                $temp[$key]['status'] = $v['status'];
                if(in_array($v['type'], [1,5,6])){
                	$temp[$key]['hours'] = $v['hours'];
                	if($v['material']){
                		$material = json_decode($v['material'],true);
                		$temp[$key]['content'] = $material[0];
                	}
                }
            }
        }
        return $temp;
    }
    
    //处理推送事件
    public static function donePushEvent($data,$channel_id=0){
        $where = [['type','=',$data['type']],['channel_id','=',$channel_id]];
        $cur = Db::name('TaskMessage')->where($where)->field('id,status')->find();
        if($cur){
            $status = $data['event'] === 'on' ? 1 : 2;
            $flag = myDb::setField('TaskMessage',[['id','=',$cur['id']]],'status', $status);
        }else{
            if($data['event'] === 'on'){
                if(in_array($data['type'],[1,5,6])){
                    res_api('您尚未配置该消息内容');
                }
                $insert = [
                    'type' => $data['type'],
                    'status' => 1,
                    'channel_id' => $channel_id
                ];
                $flag = myDb::add('TaskMessage', $insert);
            }else{
                $flag = true;
            }
        }
        return $flag;
    }
    
    //获取事件规则
    public static function getEventRules(){
    	$rules = [
    		'type' => ["require|between:1,6",["require"=>"请选择推送消息类型","between"=>"未指定该推送消息类型"]],
    		'event' => ["require|in:on,off",["require"=>"请选择推送消息状态","in"=>"未指定该推送消息状态"]],
    	];
    	return $rules;
    }
    
}