<?php
namespace app\common\model;

use think\Db;
use site\myDb;
use site\myValidate;

class mdMaterial{
    
    //处理更新文案
    public static function doneMaterial($data){
        if(array_key_exists('id', $data)){
            $re = myDb::saveIdData('Material',$data);
        }else{
            $re = myDb::add('Material', $data);
        }
        if($re){
            res_api();
        }else{
            res_api('保存失败，请重试');
        }
    }
    
    //获取文案分组信息
    public static function getMaterialGroup(){
        $material = Db::name('Material')->field('title,cover')->select();
        $title = $cover = $res = [];
        if($material){
            $max = count($material);
            foreach ($material as $v){
                $title[] = $v['title'];
                $cover[] = $v['cover'];
            }
            $res = [
                'count' => $max,
                'title' => $title,
                'cover' => $cover
            ];
        }
        return $res;
    }
    
    //获取提交的图文消息
    public static function getGraphicMessageData(){
    	$rules = [
    		'message_title' => ['require|array',['require'=>'请填写消息标题','array'=>'消息标题格式错误']],
    		'message_cover' => ['require|array',['require'=>'请选择消息封面','array'=>'消息封面格式错误']],
    		'message_link' => ['require|array',['require'=>'请填写消息链接','array'=>'消息链接格式错误']],
    		'message_desc' => ['max:255',['max'=>'首条消息描述字数超出限制']]
    	];
    	$data = myValidate::getData($rules);
    	$main = $child = [];
    	$max = count($data['message_title']);
    	if($max == 0){
    		res_api('消息内容数据异常');
    	}
    	$key = 1;
    	$news = [];
    	foreach ($data['message_title'] as $k=>$v){
    		$title = $data['message_title'][$k];
    		if(!$title){
    			res_api('第'.$key.'条消息未输入标题');
    		}
    		$link = $data['message_link'][$k];
    		if(!$link){
    			res_api('第'.$key.'条消息未输入链接');
    		}
    		$cover = $data['message_cover'][$k];
    		if(!$cover){
    			res_api('第'.$key.'条消息未上传封面');
    		}
    		$one = ['title'=>$title,'url'=>$link,'picurl'=>$cover];
    		if($key === 1){
    			$one['description'] = $data['message_desc'];
    		}else{
    			$one['description'] = '';
    		}
    		$news[] = $one;
    		$key++;
    	}
    	return $news;
    }
    
    //获取提交的客服消息内容
    public static function getCustomMsg(){
    	$rules = [
    		'message_title' => ['require|max:30',['require'=>'请填写消息标题','max'=>'消息标题字数超出限制']],
    		'message_cover' => ['require|max:255',['require'=>'请选择消息封面','max'=>'消息封面异常']],
    		'message_link' => ['require|max:255',['require'=>'请填写消息链接','max'=>'消息链接字数超出限制']],
    		'message_desc' => ['max:255',['max'=>'消息描述字数超出限制']]
    	];
    	$data = myValidate::getData($rules);
    	$data = [
    		[
    			'title' => $data['message_title'],
    			'picurl' => $data['message_cover'],
   				'url' => $data['message_link'],
   				'description' => $data['message_desc']
   			]
    	];
    	return $data;
    }

    //获取提交的小说名称内容
    public static function getNovelMsg(){
        $rules = [
            'message_title' => ['require|max:30',['require'=>'请填写消息标题','max'=>'消息标题字数超出限制']],
//            'message_cover' => ['require|max:255',['require'=>'请选择消息封面','max'=>'消息封面异常']],
//            'message_link' => ['require|max:255',['require'=>'请填写消息链接','max'=>'消息链接字数超出限制']],
//            'message_desc' => ['max:255',['max'=>'消息描述字数超出限制']]
        ];
        $data = myValidate::getData($rules);
        $data = [
            [
                'title' => $data['message_title'],
//                'picurl' => $data['message_cover'],
//                'url' => $data['message_link'],
//                'description' => $data['message_desc']
            ]
        ];
        return $data;
    }
    
    //获取更新规则
    public static function getRules($isAdd=1){
    	$rules = [
    		'title' =>  ["require|max:200",["require"=>"请输入文案标题",'max'=>'文案标题最多支持200个字符']],
    		'cover' =>  ['require|max:255',["require"=>"请上传文案封面",'max'=>'文案封面异常']],
    	];
    	if(!$isAdd){
    		$rules['id'] = ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]];
    	}
    	return $rules;
    }
}