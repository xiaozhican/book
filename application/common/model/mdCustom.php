<?php
namespace app\common\model;

use think\Db;
use site\myDb;
use site\myValidate;

class mdCustom{
    
	private static $table = 'Custom';
	
    //更新客服消息
    public static function doneCustom($data){
        if(array_key_exists('id', $data)){
            $cur = myDb::getById(self::$table, $data['id'],'id,status');
            if(!$cur){
                res_api('客服消息异常');
            }
            if($cur['status'] != 2){
                res_api('已发送消息禁止编辑');
            }
        }
        $material = mdMaterial::getCustomMsg();
        $save = [
            'name' => $data['name'],
            'material' => json_encode($material,JSON_UNESCAPED_UNICODE),
            'send_time' => strtotime($data['send_time']),
            'is_all' => $data['is_all']
        ];
        $channel_id = isset($data['channel_id']) ? $data['channel_id'] : 0;
        if($data['is_all'] == 2){
            $info = self::getSendWhere($channel_id);
            $where = $info['where'];
            $condition = $info['data'];
        }else{
            $where = [
                ['wx_id','=',$channel_id],
                ['subscribe','=',1]
            ];
            $condition = myDb::buildArr('sex:-1,is_charge:-1,money:-1,subscribe_time:-1');
        }
        $save['where'] = json_encode($where);
        $save['condition'] = json_encode($condition);
        if(array_key_exists('id', $data)){
        	$re = myDb::save(self::$table,[['id','=',$data['id']]], $save);
        }else{
        	$save['channel_id'] = $channel_id;
        	$save['create_time'] = time();
        	$re = myDb::add(self::$table, $save);
        }
        if($re){
            res_api();
        }else{
            res_api('保存失败');
        }
    }
    
    public static function getSendWhere($channel_id=0){
    	$rules = [
    		'sex' => ["require|in:-1,0,1,2",["require"=>"请选择用户性别","in"=>"用户性别参数不规范"]],
    		'is_charge' => ["require|in:-1,1,2",["require"=>"请选择用户充值情况","in"=>"用户充值情况参数不规范"]],
    		'money' => ["require|in:-1,1,2,3",["require"=>"请选择用户书币余额","in"=>"用户书币余额参数不规范"]],
    		'subscribe_time' => ["require|in:-1,1,2,3,4,5,6",["require"=>"请选择用户关注时间","in"=>"用户关注时间参数不规范"]],
    	];
        $data = myValidate::getData($rules);
        $where = [
            ['wx_id','=',$channel_id],
            ['subscribe','=',1]
        ];
        if($data['sex'] != -1){
            $where[] = ['sex','=',$data['sex']];
        }
        if($data['is_charge'] != -1){
            $where[] = ['is_charge','=',$data['is_charge']];
        }
        if($data['money'] != -1){
            switch ($data['money']){
                case 1:
                    $where[] = ['money','<',500];
                    break;
                case 2:
                    $where[] = ['money','<',2000];
                    break;
                case 3:
                    $where[] = ['money','<',5000];
                    break;
            }
        }
        if($data['subscribe_time'] != -1){
            $daylong = 86400;
            $cur_time = time();
            switch ($data['subscribe_time']){
                case 1:
                    $where[] = ['subscribe_time','>',($cur_time-$daylong)];
                    break;
                case 2:
                    $where[] = ['subscribe_time','>',($cur_time-$daylong*7)];
                    break;
                case 3:
                    $where[] = ['subscribe_time','>',($cur_time-$daylong*15)];
                    break;
                case 4:
                    $where[] = ['subscribe_time','>',($cur_time-$daylong*30)];
                    break;
                case 5:
                    $where[] = ['subscribe_time','>',($cur_time-$daylong*90)];
                    break;
                case 6:
                    $where[] = ['subscribe_time','<',($cur_time-$daylong*90)];
                    break;
            }
        }
        return ['data'=>$data,'where'=>$where];
    }
    
    //获取筛选条件
    public static function getWhereOption(){
        $option = [
            'is_all' =>  [
                'name' => 'is_all',
                'option' => [
                    ['val'=>1,'text'=>'全部用户','default'=>1],
                    ['val'=>2,'text'=>'条件筛选','default'=>0]
                ]
            ],
            'sex' => [
                'name' => 'sex',
                'option' => [
                    ['val'=>-1,'text'=>'不限','default'=>1],
                    ['val'=>1,'text'=>'男','default'=>0],
                    ['val'=>2,'text'=>'女','default'=>0],
                    ['val'=>0,'text'=>'未知','default'=>0]
                ]
            ],
            'is_charge' => [
                'name' => 'is_charge',
                'option' => [
                    ['val'=>-1,'text'=>'不限','default'=>1],
                    ['val'=>1,'text'=>'已充值用户','default'=>0],
                    ['val'=>2,'text'=>'未充值用户','default'=>0]
                ]
            ],
            'money' => [
                'name' => 'money',
                'option' => [
                    ['val'=>-1,'text'=>'不限','default'=>1],
                    ['val'=>1,'text'=>'低于500','default'=>0],
                    ['val'=>2,'text'=>'低于2000','default'=>0],
                    ['val'=>3,'text'=>'低于5000','default'=>0]
                ]
            ],
            'subscribe_time' => [
                'name' => 'subscribe_time',
                'option' => [
                    ['val'=>-1,'text'=>'不限','default'=>1],
                    ['val'=>1,'text'=>'一天内','default'=>0],
                    ['val'=>2,'text'=>'一周内','default'=>0],
                    ['val'=>3,'text'=>'半月内','default'=>0],
                    ['val'=>4,'text'=>'一个月内','default'=>0],
                    ['val'=>5,'text'=>'三个月内','default'=>0],
                    ['val'=>6,'text'=>'更早','default'=>0]
                ]
            ]
        ];
        return $option;
    }
    
    //同步模版
    public static function doSyncTemplate($datas,$channel_id=0){
    	$insert = $delete = [];
    	$list = Db::name('ChannelTemplate')->where('channel_id',$channel_id)->field('id,template_id')->select();
    	if($list){
    		$tmp = array_column($list,'template_id');
    		foreach ($datas as $lv){
    			if(in_array($lv['template_id'], $tmp)){
    				$key = array_search($lv['template_id'], $tmp);
    				unset($tmp[$key]);
    				continue;
    			}else{
    				$data = [
    					'title' => $lv['title'],
    					'channel_id' => $channel_id,
    					'template_id' => $lv['template_id'],
    					'content' => json_encode($lv['content'])
    				];
    				$insert[] = $data;
    			}
    		}
    		if(!empty($tmp)){
    			$delete = array_values($tmp);
    		}
    	}else{
    		if(!empty($datas)){
    			foreach ($datas as $lv){
    				$data = [
    					'title' => $lv['title'],
    					'channel_id' => $channel_id,
    					'template_id' => $lv['template_id'],
    					'content' => json_encode($lv['content'])
    				];
    				$insert[] = $data;
    			}
    		}
    	}
    	if($insert || $delete){
    		Db::startTrans();
    		$flag = false;
    		$inRes = true;
    		if($insert){
    			$inRes = Db::name('ChannelTemplate')->insertAll($insert);
    		}
    		if($inRes){
    			if($delete){
    				$res = Db::name('ChannelTemplate')->where('template_id','in',$delete)->delete();
    				if($res){
    					$flag = true;
    				}
    			}else{
    				$flag = true;
    			}
    		}
    		if($flag){
    			Db::commit();
    		}else{
    			Db::rollback();
    		}
    	}
    	$list = Db::name('ChannelTemplate')->where('channel_id',$channel_id)->field('template_id as id,title as name')->select();
    	$list = $list ? : 0;
    	return $list;
    }
    
    public static function getCustomData($isAdd=1){
    	$rules = [
    		'name' => ['require|max:100',['require'=>'请输入任务名称','max'=>'任务名称字数超出限制']],
    		'is_all' => ["require|in:1,2",["require"=>"请选择接收用户群体","in"=>"接收用户群体参数不规范"]],
    		'send_time' => ["require|date",["require"=>"请选择消息发送时间","in"=>"消息发送时间不规范"]],
    	];
    	if(!$isAdd){
    		$rules['id'] = ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]];
    	}
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    public static function getTemplateData($isAdd=1){
    	$rules = [
    		'name' => ['require|max:100',['require'=>'请输入任务名称','max'=>'任务名称字数超出限制']],
    		'is_all' => ["require|in:1,2",["require"=>"请选择接收用户群体","in"=>"接收用户群体参数不规范"]],
    		'url' => ['url',['url'=>'原文链接格式不规范']],
    		'template_id' => ['require',['require'=>'未选择发送模版']],
    		'content' => ['require|array',['require'=>'模版信息未配置','array'=>'模版信息格式不规范']],
    		'send_time' => ["require|date",["require"=>"请选择消息发送时间","in"=>"消息发送时间不规范"]],
    	];
    	if(!$isAdd){
    		$rules['id'] = ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]];
    	}
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //更新模版消息
    public static function doneTemplate($data){
    	if(array_key_exists('id', $data)){
    		$cur = myDb::getById(self::$table, $data['id'],'id,status');
    		if(!$cur){
    			res_api('模版消息异常');
    		}
    		if($cur['status'] != 2){
    			res_api('已发送消息禁止编辑');
    		}
    	}
    	$material = [
    		'url' => $data['url'],
    		'template_id' => $data['template_id'],
    		'data' => $data['content']
    	];
    	$save = [
    		'type' => 2,
    		'name' => $data['name'],
    		'material' => json_encode($material,JSON_UNESCAPED_UNICODE),
    		'send_time' => strtotime($data['send_time']),
    		'is_all' => $data['is_all']
    	];
    	$channel_id = isset($data['channel_id']) ? $data['channel_id'] : 0;
    	if($data['is_all'] == 2){
    		$info = self::getSendWhere($channel_id);
    		$where = $info['where'];
    		$condition = $info['data'];
    	}else{
    		
    		$where = [
    			['wx_id','=',$channel_id],
    			['subscribe','=',1]
    		];
    		$condition = myDb::buildArr('sex:-1,is_charge:-1,money:-1,subscribe_time:-1');
    	}
    	$save['where'] = json_encode($where);
    	$save['condition'] = json_encode($condition);
    	if(array_key_exists('id', $data)){
    		$re = myDb::save(self::$table,[['id','=',$data['id']]], $save);
    	}else{
    		$save['channel_id'] = $channel_id;
    		$save['create_time'] = time();
    		$re = myDb::add(self::$table, $save);
    	}
    	if($re){
    		res_api();
    	}else{
    		res_api('保存失败');
    	}
    }
    
    //构建模版html
    public static function createTemplateHtml($template_id,$data=[]){
    	$str = '';
    	$cur = myDb::getCur('ChannelTemplate',[['template_id','=',$template_id]]);
    	if($cur){
    		$content = json_decode($cur['content'],true);
    		$content = explode("\n", $content);
    		if(!$content){
    			res_api('模版信息异常');
    		}
    		$reg = '/\{\{([a-zA-Z0-9]+)\.DATA\}\}/';
    		$str .= '<h3>'.$cur['title'].'</h3>';
    		$str .= '<p class="create_date">'.date('m月d日').'</p>';
    		$str .= '<div class="field-content">';
    		foreach ($content as $v){
    			if(strlen($v) > 0){
    				$line_str = $v;
    				preg_match_all($reg, $v,$match);
    				if($match[0]){
    					foreach ($match[0] as $mk=>$mv){
    						$keyfield = $match[1][$mk];
    						$replace = '<span class="keyfield" data-key="'.$keyfield.'">';
    						if(isset($data[$keyfield])){
    							$value = $data[$keyfield];
    							$replace .= '<span class="keyvalue" data-color="'.$value['color'].'" style="color:'.$value['color'].';">'.$value['value'].'</span>';
    						}else{
    							$replace .= '<span class="keyvalue" data-color=""></span>';
    						}
    						$replace .= '&nbsp;<a href="javascript:void(0);" class="layui-icon layui-icon-edit edit-btn"></a>';
    						$replace .= '</span>';
    						$line_str = str_replace($mv, $replace, $line_str);
    					}
    				}
    				$str .= $line_str;
    				$str .= '<br />';
    			}
    		}
    		$str .= '</div>';
    	}
    	return $str;
    }
    
}