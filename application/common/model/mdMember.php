<?php
namespace app\common\model;

use think\Db;
use weixin\wx;
use site\myDb;
use site\myCache;
use site\myValidate;

class mdMember{
    
    /**
     * 获取用户列表
     * @param array $where 查询条件
     * @param array $pages 分页参数
     * @return array
     */
    public static function getMemberList($where,$pages){
        $field = 'a.*,b.name as channel_name,c.name as agent_name';
        $list = Db::name('Member a')
        ->join('Channel b','a.channel_id=b.id','left')
        ->join('Channel c','a.agent_id=c.id','left')
        ->where($where)
        ->field($field)
        ->page($pages['page'],$pages['limit'])
        ->order('a.id','desc')
        ->select();
        $count = 0;
        if($list){
            foreach ($list as &$v){
                $v['info_url'] = my_url('Member/info',['id'=>$v['id']]);
                $v['status_name'] = ($v['status'] == 1) ? '正常' : '禁用';
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
                $v['is_subscribe'] = $v['subscribe'] > 0 ? '已关注' : '未关注';
                if($v['channel_id']){
                    if(!$v['channel_name']){
                        $v['channel_name'] = '<font class="text-red">未知</font>';
                    }
                    if($v['agent_id']){
                    	if(!$v['agent_name']){
                    		$v['agent_name'] = '<font class="text-red">未知</font>';
                    	}
                    }else{
                    	$v['agent_name'] = '/';
                    }
                }else{
                    $v['channel_name'] = '总站';
                    $v['agent_name'] = '/';
                }
                $v['vip_str'] = 'N/A';
                if($v['viptime'] > 0){
                	if($v['viptime'] == 1){
                		$v['vip_str'] = '终身';
                	}else{
                		$v['vip_str'] = '到期时间:'.date('Y-m-d',$v['viptime']);
                	}
                }
            }
            $count = Db::name('Member a')->where($where)->count();
        }
        return ['data'=>$list,'count'=>$count];
    }
    
    //获取用户累计信息
    public static function getMemberCountMsg($uid,$channel_id=0){
    	$where = [['uid','=',$uid],['status','=',2]];
    	if($channel_id){
    		$where[] = ['is_count','=',1];
    		$channel = myCache::getChannel($channel_id);
    		$key = $channel['type'] == 1 ? 'channel_id' : 'agent_id';
    		$where[] = [$key,'=',$channel_id];
    	}
        $chargeMoney = Db::name('Order')->where($where)->sum('money');
        $consumeMoney = Db::name('MemberConsume')->where('uid','=',$uid)->sum('money');
        return ['charge'=>$chargeMoney,'consume'=>$consumeMoney];
    }
    
    //获取用户订单列表
    public static function getChargeOrder($where,$pages){
    	$field = 'id,type,order_no,is_count,money,status,pid,create_time';
    	$list = Db::name('Order')->where($where)->field($field)->page($pages['page'],$pages['limit'])->order('id','desc')->select();
    	$count = 0;
    	if($list){
    		foreach ($list as $k=>&$v){
    			$one['from_name'] = '--';
    			if($v['type'] == 1){
    				if($v['pid']){
    					$book = myCache::getBook($v['pid']);
    					$one['from_name'] = $book ? '书籍：'.$book['name'] : '<font class="text-read">未知</font>';
    					$book = null;
    				}
    			}else{
    				$activity = myCache::getActivity($v['pid']);
    				$one['from_name'] = $activity ? '活动：'.$activity['name'] : '<font class="text-read">未知</font>';
    			}
    			$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
    		}
    		$count = myDb::getCount('Order', $where);
    	}
    	return ['count'=>$count,'data'=>$list];
    }
    
    //打赏订单
    public static function getRewardOrder($where,$pages){
    	$field = '*';
    	$count = 0;
    	$data = Db::name('OrderReward')->where($where)->field($field)->page($pages['page'],$pages['limit'])->order('id','desc')->select();
    	if($data){
    		foreach ($data as &$v){
    			$v['gift'] = json_decode($v['gift'],true);
    			$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
    			$book = myCache::getBook($v['pid']);
    			$v['from_name'] = $book ? '书籍：'.$book['name'] : '<font class="text-read">未知</font>';
    			$book = null;
    		}
    		$count = myDb::getCount('OrderReward', $where);
    	}
    	return ['data'=>$data,'count'=>$count];
    }
    
    //获取阅读历史记录
    public static function getReadHistory($uid){
    	$list = [];
    	if($uid){
    		$cache = myCache::getReadCache($uid);
    		if($cache){
    			foreach ($cache as $v){
    				$book = myCache::getBook($v['book_id']);
    				if(!$book || !in_array($book['status'], [1,2])){
    					unset($list[$k]);
    					continue;
    				}
    				$one = [
    					'book_id' => $v['book_id'],
    					'name' => $book['name'],
    					'cover' => $book['cover'],
    					'number' => $v['last_number'],
    					'type' => intval($book['type']),
    					'create_time' => date('Y-m-d H:i',$v['last_time'])
    				];
    				$chapter = myCache::getBookChapterList($book['id']);
    				$one['chapter_name'] = $chapter && isset($chapter[$one['number']]) ? $chapter[$one['number']]['name'] : '第'.$one['number'].'章';
    				$chapter = null;
    				if(!$one['cover']){
    					$one['cover'] = '/static/templet/default/cover.png';
    				}
    				$list[] = $one;
    			}
    			$create_times = array_column($list,'create_time');
    			array_multisort($create_times,SORT_DESC,$list);
    		}
    	}
    	return $list;
    }
    
    //获取用户签到列表
    public static function getSignList($where,$pages){
        $list = Db::name('MemberSign')
        ->where($where)
        ->page($pages['page'],$pages['limit'])
        ->order('id','desc')
        ->select();
        $count = 0;
        if($list){
            foreach ($list as &$v){
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
            }
            $count = Db::name('MemberSign')
            ->where($where)
            ->count();
        }
        return ['count'=>$count,'data'=>$list];
    }
    
    //获取用户消费记录
    public static function getConsumeList($where,$pages){
        $list = Db::name('MemberConsume')
        ->where($where)
        ->page($pages['page'],$pages['limit'])
        ->order('id','desc')
        ->select();
        $count = 0;
        if($list){
            foreach ($list as &$v){
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
            }
            $count = Db::name('MemberConsume')
            ->where($where)
            ->count();
        }
        return ['count'=>$count,'data'=>$list];
    }
    
    //获取邀请记录
    public static function getInviteList($where,$pages){
    	$list = Db::name('Share a')
    	->join('member b','a.uid=b.id','left')
    	->where($where)
    	->field('a.id,a.money,a.create_date,b.nickname,b.phone')
    	->page($pages['page'],$pages['limit'])
    	->order('a.id','desc')
    	->select();
    	$count = 0;
    	if($list){
    		$count = Db::name('Share a')
    		->where($where)
    		->count();
    	}
    	return ['count'=>$count,'data'=>$list];
    }
    
    //更新用户书币余额
    public static function setMemberMoney($id,$money){
        if($money > 0){
            $data = [
                'money' => Db::raw('money+'.$money),
                'total_money' => Db::raw('total_money+'.$money)
            ];
        }else{
            $data = [
                'money' => Db::raw('money'.$money)
            ];
        }
        $re = Db::name('Member')->where('id','=',$id)->update($data);
        if($re){
        	myCache::doMemberMoney($id,$money,'inc');
        	if($money > 0){
        		self::addChargeLog($id, '+'.$money.' 书币', '平台赠送');
        	}
            return true;
        }else{
            return false;
        }
    }
    
    //获取事件规则
    public static function getEventRules(){
    	$rules = [
    		'id' =>  ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
    		'event' => ["require|in:charge,vipon,vipoff,statuson,statusoff",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']],
    		'money' =>  ["requireIf:event,charge|integer",["requireIf"=>"请输入要调整的书币数",'integer'=>'书币格式错误']],
    		'month' =>  ["requireIf:event,vipon|number|gt:0",["requireIf"=>"请输入vip充值月数",'number'=>'vip月数必须为大于0的数值',"gt"=>"vip月数必须为大于0的数值"]]
    	];
    	return $rules;
    }
    
    
    
    
    /****************************以下代码为前端所用*****************************/
    
    //增加书币充值记录
    public static function addChargeLog($uid,$money,$summary){
    	$data = [
    		'uid' => $uid,
    		'money' => $money,
    		'summary' => $summary,
    		'create_time' => time()
    	];
    	Db::name('MemberChargeLog')->insert($data);
    }
    
    public static function getReadStr($uid,$url){
    	$str = '';
    	if($uid){
    		$list = myCache::getReadCache($uid);
    		if($list){
    			$last_times = array_column($list,'last_time');
    			array_multisort($last_times,SORT_DESC,$list);
    			$list = array_values($list);
    			$num = 1;
    			$str .= "\n\n";
    			$href = '➢ <a href="http://'.$url.'/index/Book/read.html?book_id='.$list[0]['book_id'].'&number='.$list[0]['last_number'].'">点我继续上次阅读</a>';
    			$str .= $href;
    			$str .= "\n\n";
    			$str .= '历史阅读记录';
    			foreach ($list as $v){
    				if($num >= 7){
    					break;
    				}
    				$book = myCache::getBook($v['book_id']);
    				if(!$book || !in_array($book['status'], [1,2])){
    					continue;
    				}
    				$str .= "\n\n";
    				$str .= '➢ <a href="http://'.$url.'/index/Book/read.html?book_id='.$v['book_id'].'&number='.$v['last_number'].'">'.$book['name'].'</a>';
    				$num++;
    			}
    		}
    	}
    	return $str;
    }
    
    /**
     * 获取用户默认信息
     * @return string[]|unknown[]
     */
    public static function createInfo(){
    	$data = [
    		'nickname' => '游客',
    		'headimgurl' => '/static/templet/default/headimg.jpeg'
    	];
    	return $data;
    }
    
    /**
     * 创建用户渠道代理信息
     * @return 
     */
    public static function createAgentInfo($kc_code=''){
    	$urlInfo = myCache::getUrlInfo();
    	$channel_id = $urlInfo['channel_id'];
    	$agent_id = $urlInfo['agent_id'];
    	$wx_id = $urlInfo['wx_id'];
    	$spread_id = 0;
    	if(!$kc_code && session('?kc_code')){
    		$kc_code = session('kc_code');
    	}
    	if($kc_code){
    		$codeInfo = myCache::getKcCodeInfo($kc_code);
    		if($codeInfo){
    			$agentInfo = $codeInfo['info'];
    			if($wx_id > 0){
    				if($wx_id == $agentInfo['wx_id']){
    					$channel_id = $agentInfo['channel_id'];
    					$agent_id = $agentInfo['agent_id'];
    					$wx_id = $agentInfo['wx_id'];
    					$spread_id = $codeInfo['sid'];
    				}
    			}else{
    				$channel_id = $agentInfo['channel_id'];
    				$agent_id = $agentInfo['agent_id'];
    				$wx_id = $agentInfo['wx_id'];
    				$spread_id = $codeInfo['sid'];
    			}
    		}
    	}
    	$res = [
    		'channel_id' => $channel_id,
    		'agent_id' => $agent_id,
    		'wx_id' => $wx_id,
    		'spread_id' => $spread_id
    	];
    	return $res;
    }
    
    /**
     * 微信自动创建用户
     * @param string $openid 微信openid
     * @param array $get get参数
     */
    public static function doWxRegister($openid){
    	$rules = [
    		'kc_code'=>['alphaDash|length:6',['alphaDash'=>'非法访问','length'=>'非法访问']],
    		'invite_code'=>['alphaDash|length:6',['alphaDash'=>'非法访问','length'=>'非法访问']]
    	];
    	$get = myValidate::getData($rules,'get');
    	$data = [
    		'sex' => 0,
    		'openid' => $openid,
    		'create_time' => time()
    	];
    	$data['invite_code'] = myDb::createCode('Member','invite_code');
    	$userInfo = self::createInfo();
    	$data = array_merge($data,$userInfo);
    	$agentInfo = [];
    	$inviteUid = 0;
    	if($get['invite_code']){
    		$inviteUid = myCache::getUidByCode($get['invite_code']);
    		if($inviteUid){
    			$inviteUser = myCache::getMember($inviteUid);
    			if($inviteUser){
    				$agentInfo = [
    					'channel_id' => $inviteUser['channel_id'],
    					'agent_id' => $inviteUser['agent_id'],
    					'wx_id' => $inviteUser['wx_id'],
    					'spread_id' => 0
    				];
    			}
    		}
    	}
    	if(empty($agentInfo)){
    		$agentInfo = self::createAgentInfo($get['kc_code']);
    	}
    	$data = array_merge($data,$agentInfo);
    	Db::startTrans();
    	$flag = $is_money = false;
    	$uid = Db::name('Member')->insertGetId($data);
    	if($uid){
    		$res = Db::name('MemberCharge')->insert(['uid'=>$uid]);
    		if($res){
    			$mRes = true;
    			if($inviteUid){
    				$shareConfig = getMyConfig('site_share');
    				if($shareConfig && $shareConfig['reg_money']){
    					$mRes = false;
    					$inviteData = [
    						'uid' => $uid,
    						'from_uid' => $inviteUid,
    						'money' => $shareConfig['reg_money'],
    						'create_date' => date('Y-m-d')
    					];
    					$inviteRes = Db::name('Share')->insert($inviteData);
    					if($inviteRes){
    						$mData = [
    							'money' => Db::raw('money+'.$inviteData['money']),
    							'total_money' => Db::raw('total_money+'.$inviteData['money'])
    						];
    						$meRes = Db::name('Member')->where('id',$inviteUid)->update($mData);
    						if($meRes){
    							$log = [
    								'uid' => $inviteUid,
    								'money' => '+'.$inviteData['money'].'书币',
    								'summary' => '邀请用户赠送',
    								'create_time' => time()
    							];
    							$ls = Db::name('MemberChargeLog')->insert($log);
    							if($ls){
    								$mRes = true;
    								$is_money = true;
    							}
    						}
    					}
    				}
    			}
    			if($mRes){
    				$flag = true;
    			}
    		}
    	}
    	if($flag){
    		Db::commit();
    		if($is_money){
    			myCache::doMemberMoney($inviteUid, $inviteData['money']);
    		}
    		return $uid;
    	}else{
    		Db::rollback();
    		return 0;
    	}
    }
    
    /**
     * 手机号注册用户
     * @param string $openid 微信openid
     * @param array $get get参数
     */
    public static function doPhoneRegister($phone,$password=''){
    	if(!$password){
    		$password = 123456;
    	}
    	$data = [
    		'sex' => 0,
    		'openid' => '',
    		'phone' => $phone,
    		'password' => createPwd($password),
    		'create_time' => time()
    	];
    	$data['invite_code'] = myDb::createCode('Member','invite_code');
    	$userInfo = self::createInfo();
    	$data = array_merge($data,$userInfo);
    	$agentInfo = [];
    	$inviteUid = 0;
    	if(session('?invite_code')){
    		$inviteUid = myCache::getUidByCode(session('invite_code'));
    		if($inviteUid){
    			$inviteUser = myCache::getMember($inviteUid);
    			if($inviteUser){
    				$agentInfo = [
    					'channel_id' => $inviteUser['channel_id'],
    					'agent_id' => $inviteUser['agent_id'],
    					'wx_id' => $inviteUser['wx_id'],
    					'spread_id' => 0
    				];
    			}
    		}
    	}
    	if(empty($agentInfo)){
    		$agentInfo = self::createAgentInfo();
    	}
    	$data = array_merge($data,$agentInfo);
    	Db::startTrans();
    	$flag = $is_money = false;
    	$uid = Db::name('Member')->insertGetId($data);
    	if($uid){
    		$res = Db::name('MemberCharge')->insert(['uid'=>$uid]);
    		if($res){
    			$mRes = true;
    			if($inviteUid){
    				$shareConfig = getMyConfig('site_share');
    				if($shareConfig && $shareConfig['reg_money']){
    					$mRes = false;
    					$inviteData = [
    						'uid' => $uid,
    						'from_uid' => $inviteUid,
    						'money' => $shareConfig['reg_money'],
    						'create_date' => date('Y-m-d')
    					];
    					$inviteRes = Db::name('Share')->insert($inviteData);
    					if($inviteRes){
    						$mData = [
    							'money' => Db::raw('money+'.$inviteData['money']),
    							'total_money' => Db::raw('total_money+'.$inviteData['money'])
    						];
    						$meRes = Db::name('Member')->where('id',$inviteUid)->update($mData);
    						if($meRes){
    							$log = [
    								'uid' => $inviteUid,
    								'money' => '+'.$inviteData['money'].'书币',
    								'summary' => '邀请用户赠送',
    								'create_time' => time()
    							];
    							$ls = Db::name('MemberChargeLog')->insert($log);
    							if($ls){
    								$mRes = true;
    								$is_money = true;
    							}
    						}
    					}
    				}
    			}
    			if($mRes){
    				$flag = true;
    			}
    		}
    	}
    	if($flag){
    		Db::commit();
    		if($is_money){
    			myCache::doMemberMoney($inviteUid, $inviteData['money']);
    		}
    		$data['id'] = $uid;
    		return $data;
    	}else{
    		Db::rollback();
    		return false;
    	}
    }
    
    //获取我的收藏列表
    public static function getMyCollect($uid){
    	$list = [];
    	$cache = myCache::getCollectBookIds($uid);
    	//$arr = [];
    	if($cache){
    		foreach ($cache as $book_id){
    			$book = myCache::getBook($book_id);
    			if(!$book || !in_array($book['status'], [1,2])){
    				unset($list[$k]);
    				continue;
    			}
    			$one = [
    				'book_id' => $book['id'],
    				'name' => $book['name'],
    				'cover' => $book['cover'],
    				'type' => intval($book['type']),
    				'hot_num' => 0
    			];
    			$hot_num = myCache::getBookHotNum($book['id']);
    			$one['hot_num'] = formatNumber($hot_num);
    			if(!$one['cover']){
    				$one['cover'] = '/static/templet/default/cover.png';
    			}
    			$list[] = $one;
    		}
    	}
        return $list;
    }
    public static function getMyCollect1($uid,$type=""){
        $list = [];
        $cache = myCache::getCollectBookIds($uid);
        //$arr = [];
        if($cache){
            //如果是api接口那边调用的就读取广告信息
            if($type){
                $notice_list = Db::name("Notice")->where(['status'=>1,'notice_type'=>'插屏广告'])->select();
            }
            foreach ($cache as $book_id){
                $book = myCache::getBook($book_id);
                if(!$book || !in_array($book['status'], [1,2])){
                    unset($list[$k]);
                    continue;
                }
                $one = [
                    'book_id' => $book['id'],
                    'name' => $book['name'],
                    'cover' => $book['cover'],
                    'type' => intval($book['type']),
                    'hot_num' => 0
                ];
                $hot_num = myCache::getBookHotNum($book['id']);
                $one['hot_num'] = formatNumber($hot_num);
                if(!$one['cover']){
                    $one['cover'] = '/static/templet/default/cover.png';
                }
                $list[] = $one;
                if($type){
                    $list['notice_list'] = $notice_list;
                    //$arr[] = $list;
                }
            }
        }
        return $list;
    }
    
    //获取我的消费记录
    public static function getMyConsumeList($uid,$page){
    	$pages = ['page'=>$page,'limit'=>20];
    	$list = myDb::getOnlyPageList('MemberConsume', [['uid','=',$uid]],'*',$pages);
    	if($list){
    		$temp = [];
    		foreach ($list as &$v){
    			if(isset($temp[$v['book_id']])){
    				$one = $temp[$v['book_id']];
    			}else{
    				$book = myCache::getBook($v['book_id']);
    				if($book){
    					$one = ['cover'=>$book['cover'],'name'=>$book['name']];
    				}else{
    					$one = ['cover'=>'/static/templet/default/cover.png','name'=>'未知'];
    				}
    				$temp[$v['book_id']] = $one;
    			}
    			$v['name'] = $one['name'];
    			$v['cover'] = $one['cover'];
    			$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
    		}
    	}
    	return $list;
    }
    
    //获取我的充值记录
    public static function getMyChargeList($uid,$page){
    	$pages = ['page'=>$page,'limit'=>20];
    	$list = myDb::getOnlyPageList('MemberChargeLog', [['uid','=',$uid]],'id,money,summary,create_time',$pages);
    	if($list){
    		foreach ($list as &$v){
    			$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
    		}
    	}
    	return $list;
    }
    
    
    //获取我的打赏记录
    public static function getMyRewardList($uid,$page){
    	$pages = ['page'=>$page,'limit'=>20];
    	$list = myDb::getOnlyPageList('OrderReward', [['uid','=',$uid]],'id,money,count,gift,pid,create_time',$pages);
    	if($list){
    		foreach ($list as &$v){
    			$book = myCache::getBook($v['pid']);
    			$v['book_name'] = $book ? $book['name'] : '未知';
    			$v['gift'] = json_decode($v['gift'],true);
    			$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
    		}
    	}
    	return $list;
    }
    
    
    
    //签到
    public static function doSign($data,$uid){
    	Db::startTrans();
    	$flag = false;
    	$re = Db::name('MemberSign')->insert($data);
    	if($re){
    		$mdata = [
    			'money' => Db::raw('money+'.$data['money']),
    			'total_money' => Db::raw('total_money+'.$data['money'])
    		];
    		$res = Db::name('Member')->where('id','=',$uid)->update($mdata);
    		if($res){
    			$flag = true;
    		}
    	}
    	if($flag){
    		Db::commit();
    		myCache::doMemberMoney($uid, $data['money']);
    	}else{
    		Db::rollback();
    	}
    	return $flag;
    }
    
    /**
     * 用户关注更新用户信息
     * @param array $info 微信用户公开信息
     */
    public static function subscribeMember($info){
    	$result = 0;
    	$uid = myCache::getUidByOpenId($info['openid']);
    	$data = [
    		'nickname' => $info['nickname'] ? wx::removeEmoji($info['nickname']) : '',
    		'sex' => $info['sex'],
    		'city' => $info['city'],
    		'province' => $info['province'],
    		'country' => $info['country'],
    		'headimgurl' => $info['headimgurl'],
    		'subscribe' => 1
    	];
    	if($uid){
    		$member = myCache::getMember($uid);
    		if(!$member['subscribe_time']){
    			$data['subscribe_time'] = time();
    		}
    		$re = myDb::save('Member', [['id','=',$member['id']]], $data);
    		if($re){
    			$result = $uid;
    			myCache::rmMember($member['id']);
    		}
    	}else{
    		$time = time();
    		$config = myCache::getUrlInfo();
    		$data['openid'] = $info['openid'];
    		$data['wx_id'] = $config['wx_id'];
    		$data['channel_id'] = $config['channel_id'];
    		$data['agent_id'] = $config['agent_id'];
    		$data['invite_code'] = myDb::createCode('Member','invite_code');
    		$data['subscribe_time'] = $time;
    		$data['create_time'] = $time;
    		Db::startTrans();
    		$flag = false;
    		$uid = Db::name('Member')->insertGetId($data);
    		if($uid){
    			$res = Db::name('MemberCharge')->insert(['uid'=>$uid]);
    			if($res){
    				$flag = true;
    			}
    		}
    		if($flag){
    			$result = $uid;
    			Db::commit();
    		}else{
    			Db::rollback();
    		}
    	}
    	return $result;
    }
    
    /**
     * 取消关注
     * @param string $openid
     * @return number
     */
    public static function unsubscribeMember($openid){
    	$re = true;
    	$uid = myCache::getUidByOpenId($openid);
    	if($uid){
    		$re = Db::name('Member')->where('id','=',$uid)->setField('subscribe',0);
    		if($re){
    			myCache::updateMember($uid,'subscribe', 0);
    		}
    	}
    	return $re;
    }
    
    public static function bindPhone($uid,$phone,$password){
    	$data = [
    		
    	];
    }
    
    /**
     * 处理用户菜单签到
     * @param string $openid
     * @param array $config
     * @return string
     */
    public static function doMenuSign($openid,$config){
    	$uid = myCache::getUidByOpenId($openid);
    	if($uid){
    		$signInfo = myCache::getUserSignInfo($uid);
    		if(is_string($signInfo)){
    			$str = "签到失败";
    			$str .= "\n\n";
    			$str .= $signInfo;
    			return ['flag'=>3,'str'=>$str];
    		}
    		if($signInfo['cur_sign'] === 'yes'){
    			$next_day = 1;
    			foreach ($signInfo['list'] as $kv){
    				if($kv['is_sign'] === 'no'){
    					$next_day = $kv['day'];
    					break;
    				}
    			}
    			$next_money = $signInfo['list']['day'.$next_day]['money'];
    			$str = "今日已签到,明日签到将获得".$next_money."书币";
    			return ['flag'=>2,'str'=>$str];
    		}
    		$data = [
    			'uid' => $uid,
    			'date' => date('Ymd'),
    			'create_time' => time()
    		];
    		$num = 0;
    		foreach ($signInfo['list'] as &$v){
    			$num++;
    			if($v['is_sign'] === 'no'){
    				$data['money'] = $v['money'];
    				$data['days'] = $v['day'];
    				$v['is_sign'] = 'yes';
    				break;
    			}
    		}
    		if(!$data['money']){
    			$str = "签到失败";
    			$str .= "\n\n";
    			$str .= "签到参数配置有误";
    			return ['flag'=>3,'str'=>$str];
    		}
    		Db::startTrans();
    		$flag = false;
    		$re = Db::name('MemberSign')->insert($data);
    		if($re){
    			$mdata = [
    				'money' => Db::raw('money+'.$data['money']),
    				'total_money' => Db::raw('total_money+'.$data['money'])
    			];
    			$res = Db::name('Member')->where('id','=',$uid)->update($mdata);
    			if($res){
    				$flag = true;
    			}
    		}
    		if($flag){
    			Db::commit();
    			myCache::doMemberMoney($uid,$data['money']);
    			self::addChargeLog($uid, '+'.$data['money'].' 书币', '签到送书币');
    			$signInfo['cur_sign'] = 'yes';
    			$key = md5($data['date'].'_signuser_'.$uid);
    			cache($key,$signInfo,86400);
    			$str = '本日签到成功，赠送'.$data['money'].'书币,您已连续签到'.$num.'天,多签多送,最高赠送'.$signInfo['list']['day7']['money'].'书币';
    			return ['flag'=>1,'str'=>$str];
    		}else{
    			$str = "签到失败";
    			$str .= "\n\n";
    			$str .= "当前签到人数较多，请稍后再试";
    			return ['flag'=>3,'str'=>$str];
    		}
    	}else{
    		$str = "签到失败";
    		$str .= "\n\n";
    		$str .= "您的账户异常，请联系客服";
    		return ['flag'=>3,'str'=>$str];
    	}
    }
    
}