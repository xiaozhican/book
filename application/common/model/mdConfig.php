<?php
namespace app\common\model;

use think\Db;
use site\myValidate;

class mdConfig{
    
    //保存配置
    public static function saveConfig($key,$value){
        $flag = false;
        $json = json_encode($value,JSON_UNESCAPED_UNICODE);
        $data = [
        	'value' => $json,
        	'status' => 1
        ];
        $re = Db::name('Config')->where('key','=',$key)->update($data);
        if($re !== false){
        	cache($key,$value);
            $flag = true;
        }
        return $flag;
    }
    
    //新增配置
    public static function addConfig($key,$data){
        $flag = false;
        $json = json_encode($data,JSON_UNESCAPED_UNICODE);
        $save = [
            'key' => $key,
            'value' => $json,
        	'status' => 2
        ];
        $re = Db::name('Config')->insert($save);
        if($re){
            $flag = true;
        }
        return $flag;
    }
    
    //获取配置
    public static function getConfig($key){
        $config = Db::name('Config')->where('key','=',$key)->field('key,value')->find();
        if($config){
            $cur = json_decode($config['value'],true);
        }else{
            $cur = false;
        }
        return $cur;
    }
    
    //获取导航图片参数
    public static function getBannerData(){
    	$rules = [
            'link' => ["array",['array'=>'链接不规范']],
            'bname' => ["array",['array'=>'名称不规范']],
    		'src' => ["array",['array'=>'图片格式格式不规范']],
    	];
    	$data = myValidate::getData($rules);
    	$config = [];
    	if($data['src']){
    		$num = 0;
    		foreach ($data['src'] as $k=>$v){
    			$num++;
    			$one = ['src'=>$v];
    			if(!$one['src']){
    				res_api('第'.$num.'张轮播图片未上传');
    			}
                $one['link'] = $data['link'][$k];
                $one['bname'] = $data['bname'][$k];
    			$config[] = $one;
    		}
    	}
    	return $config;
    }
    
    //获取分类参数
    public static function getCategoryData(){
    	$rules = ['category' => ["require|array",['require'=>'请输入小说类型','array'=>'类型参数格式不规范']]];
    	$data = myValidate::getData($rules);
    	if($data){
    		$data = array_values($data);
    	}else{
    		$data = [];
    	}
    	return $data;
    }

    //获取广告页数
    public static function getPageNoticeData(){
        $rules = ["page_notice" => ['require',['require'=>'请输入广告页数']]];
        $data = myValidate::getData($rules);
        $data = $data ? : [];
        return $data;
    }

    //获取金币参数
    public static function getMoneyData(){
        $rules = ["money" => ['require|number',['require'=>'请输入付费金币数','number'=>'参数格式不规范，必须为正整数']]];
        $data = myValidate::getData($rules);
        $data = $data ? : [];
        return $data;
    }

    //获取免费书
    public static function getBookData(){
        $rules = ["free_chapter" => ['require|number',['require'=>'请输入免费书数','number'=>'参数格式不规范，必须为正整数']]];
        $data = myValidate::getData($rules);
        $data = $data ? : [];
        return $data;
    }
    
    //获取发布区域参数
    public static function getAreaData(){
    	$rules = ['area' => ["require|array",['require'=>'请输入小说发布区域','array'=>'发布区域参数格式不规范']]];
    	$data = myValidate::getData($rules);
    	$data = $data ? : [];
    	return $data;
    }
    
    //获取导航菜单
    public static function getNavData(){
    	$rules = [
    		'src' => ["array",['array'=>'图片格式格式不规范']],
    		'text' => ["array",['array'=>'标题格式不规范']],
    		'link' => ["array",['array'=>'链接不规范']],
    	];
    	$data = myValidate::getData($rules);
    	$config = [];
    	if($data['src']){
    		$num = 0;
    		foreach ($data['src'] as $k=>$v){
    			$num++;
    			$one = ['src'=>$v];
    			if(!$one['src']){
    				res_api('第'.$num.'张图标未上传');
    			}
    			$one['link'] = $data['link'][$k];
    			$one['text'] = $data['text'][$k];
    			$config[] = $one;
    		}
    		if($num > 5){
    			res_api('最多可配置5个菜单导航图标');
    		}
    	}
    	return $config;
    }
    
    //获取视频导航参数
    public static function getVideoNavData(){
    	$rules = [
    		'text' => ["array",['array'=>'标题格式不规范']],
    		'link' => ["array",['array'=>'链接不规范']]
    	];
    	$data = myValidate::getData($rules);
    	$config = [];
    	if($data['text']){
    		$num = 0;
    		foreach ($data['text'] as $k=>$v){
    			$num++;
    			$one = ['text'=>$v];
    			if(!$one['text']){
    				res_api('第'.$num.'条标题未填写');
    			}
    			$one['link'] = $data['link'][$k];
    			$config[] = $one;
    		}
    		if($num > 5){
    			res_api('最多上传5个菜单导航');
    		}
    	}
    	return $config;
    }
    
    //获取打赏提交参数
    public static function getRewardData(){
    	$rules = [
    		'name' => ['array',['array'=>'礼物名称格式不规范']],
    		'src' => ['array',['array'=>'礼物图标格式不规范']],
    		'money' => ['array',['array'=>'礼物价格格式不规范']],
    		'gift_key' => ['array',['array'=>['礼物key值异常']]]
    	];
    	$data = myValidate::getData($rules);
    	$config = [];
    	$gk = 0;
    	if($data['src']){
    		$num = 0;
    		foreach ($data['src'] as $k=>$v){
    			$num++;
    			$gift_key = $data['gift_key'][$k];
    			if(!$gift_key){
    				res_api('礼物key值异常');
    			}
    			$one = ['src'=>$v,'gift_key'=> $gift_key];
    			if(!$one['src']){
    				res_api('第'.$num.'张图标未上传');
    			}
    			if(!$data['name'][$k]){
    				res_api('未填写第'.$num.'个礼物名称');
    			}
    			$one['name'] = $data['name'][$k];
    			if(!$data['money'][$k]){
    				res_api('未填写第'.$num.'个礼物价格');
    			}
    			if(!is_numeric($data['money'][$k]) || $data['money'][$k] < 0){
    				res_api('第'.$num.'项礼物价格格式不规范');
    			}
    			$one['money'] = $data['money'][$k];
    			$config[$gift_key] = $one;
    		}
    	}
    	return $config;
    }
    
    //获取站点设置选项
    public static function getWebsiteOptions(){
    	$variable = [
            'is_location' => ['name' => 'is_location','option' => [['val'=>1,'text'=>'开启','default'=>1],['val'=>2,'text'=>'关闭','default'=>0]]],
            'is_https' => ['name' => 'is_https','option' => [['val'=>1,'text'=>'开启','default'=>1],['val'=>2,'text'=>'关闭','default'=>0]]],
    		'is_cartoon' => ['name' => 'is_cartoon','option' => [['val'=>1,'text'=>'开启','default'=>1],['val'=>2,'text'=>'关闭','default'=>0]]],
    		'pay_type' => ['name' => 'pay_type','option' => [['val'=>1,'text'=>'微信支付','default'=>1],['val'=>2,'text'=>'米花支付','default'=>0],['val'=>3,'text'=>'payjq支付','default'=>0]]],
            'app_pay_type' => ['name' => 'app_pay_type','option' => [['val'=>21,'text'=>'微信支付','default'=>1],['val'=>22,'text'=>'支付宝支付','default'=>0]]],
            'sms_type' => ['name' => 'sms_type','option' => [['val'=>1,'text'=>'赛迪短信','default'=>1],['val'=>2,'text'=>'dxton短信','default'=>0]]],
    	];
    	return $variable;
    }
    
    //获取站点配置提交数据
    public static function getWebsiteData(){
    	$rules = [
    		'logo' => ['url',['url'=>'客服微信图片地址异常']],
    		'name' =>  ["require|max:20",["require"=>"请输入站点名称",'max'=>'站点名称最多支持20个字符']],
    		'url' =>  ["require|max:50",["require"=>"请输入安全域名",'url'=>'安全域名长度超出限制']],
    		'ip' =>  ["require|max:30",["require"=>"请输入服务器ip",'url'=>'服务器ip长度超出限制']],
            'is_location' => ["require|in:1,2",["require"=>"请选择是否开启跳转域名","in"=>"未指定该跳转状态"]],
            'is_https' => ["require|in:1,2",["require"=>"请选择是否开启HTTPS","in"=>"未指定该状态"]],
    		'is_cartoon' => ["require|in:1,2",["require"=>"请选择是否开启漫画板块","in"=>"未指定该板块状态"]],
            'pay_type' => ["require|in:1,2,3",["require"=>"请选择公众号支付方式","in"=>"未指定该支付方式"]],
            'app_pay_type' => ["require|in:21,22",["require"=>"请选择APP默认支付方式","in"=>"未指定该支付方式"]],
            'sms_type' => ["require|in:1,2",["require"=>"请选择短信平台","in"=>"未指定该短信平台"]],
    		'location_url' => ["max:50|requireIf:is_location,1",["requireIf"=>"请输入跳转域名","max"=>"跳转域名长度超出限制"]],
    		'download_url' => ['url',['url'=>'APP下载地址异常']],
    		
    	];
    	$data = myValidate::getData($rules);
    	if($data['url'] == $data['location_url']){
    		res_api('绑定域名不能和跳转域名一致');
    	}
    	mdPlatform::checkUrlRepeat($data['url'],0,true);
    	if($data['location_url']){
    		mdPlatform::checkUrlRepeat($data['location_url'],0,true);
    	}
    	return $data;
    }
    
    //获取联系方式配置
    public static function getContactData(){
    	$rules = [
    		'qrcode' => ['url',['url'=>'客服微信图片地址异常']],
    		'tel' => ['regex:[-\d]{8,20}',['regex'=>'联系电话格式不规范']],
    		'email' => ['email',['email'=>'邮箱格式不规范']]
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //获取联系方式配置
    public static function getShareData(){
    	$rules = [
    		'bg' => ['url',['url'=>'客服微信图片地址异常']],
    		'title' => ['require',['require'=>'请输入分享标题']],
    		'desc' => ['require',['require'=>'请输入分享描述']],
    		'reg_money' => ["number",["number"=>"请输入数值类型的奖励"]]
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //获取签到配置
    public static function getSignData(){
    	$rules = [
    		'is_sign' => ["require|in:1,2",["require"=>"请选择是否开启签到","in"=>"未指定该签到状态"]],
    		'sign_day1' => ["number|requireIf:is_sign,1",["number"=>"第一天签到奖励格式不规范",'requireIf'=>'请输入第一天签到奖励','gt'=>'第一天签到奖励格式不规范']],
    		'sign_day2' => ["number|requireIf:is_sign,1",["number"=>"第二天签到奖励格式不规范"],'requireIf'=>'请输入第二天签到奖励','gt'=>'第二天签到奖励格式不规范'],
    		'sign_day3' => ["number|requireIf:is_sign,1",["number"=>"第三天签到奖励格式不规范"],'requireIf'=>'请输入第三天签到奖励','gt'=>'第三天签到奖励格式不规范'],
    		'sign_day4' => ["number|requireIf:is_sign,1",["number"=>"第四天签到奖励格式不规范"],'requireIf'=>'请输入第四天签到奖励','gt'=>'第四天签到奖励格式不规范'],
    		'sign_day5' => ["number|requireIf:is_sign,1",["number"=>"第五天签到奖励格式不规范"],'requireIf'=>'请输入第五天签到奖励','gt'=>'第五天签到奖励格式不规范'],
    		'sign_day6' => ["number|requireIf:is_sign,1",["number"=>"第六天签到奖励格式不规范"],'requireIf'=>'请输入第六天签到奖励','gt'=>'第六天签到奖励格式不规范'],
    		'sign_day7' => ["number|requireIf:is_sign,1",["number"=>"第七天签到奖励格式不规范"],'requireIf'=>'请输入第七天签到奖励','gt'=>'第七天签到奖励格式不规范']
    	];
    	$data = myValidate::getData($rules);
    	$data['sign_config'] = [];
    	for($i=1;$i<=7;$i++){
    		$dkey = 'sign_day'.$i;
    		$skey = 'day'.$i;
    		$data['sign_config'][$skey] = $data[$dkey];
    		unset($data[$dkey]);
    	}
    	return $data;
    }
    
    //获取微信支付配置数据
    public static function getWxPayData(){
    	$rules = [
    		'APPID' => ['require',['require'=>'请输入微信支付APPID']],
    		'MCHID' => ['require',['require'=>'请输入微信支付商户ID']],
    		'APIKEY' => ['require',['require'=>'请输入微信支付KEY']]
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //获取微信支付配置数据
    public static function getWxAppPayData(){
    	$rules = [
    		'APPID' => ['require',['require'=>'请输入微信支付APPID']],
    		'MCHID' => ['require',['require'=>'请输入微信支付商户ID']],
    		'APIKEY' => ['require',['require'=>'请输入微信支付KEY']],
    		'is_on' => ['require|in:1,2',['require'=>'请选择是否开启该支付方式','in'=>'未指定该配置']]
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //获取微信支付配置数据
    public static function getAliAppPayData(){
    	$rules = [
    		'appId' => ['require',['require'=>'请输入商户ID']],
    		'rsaPrivateKey' => ['require',['require'=>'请输入您的私钥']],
    		'alipayrsaPublicKey' => ['require',['require'=>'请输入支付宝公钥']],
    		'is_on' => ['require|in:1,2',['require'=>'请选择是否开启该支付方式','in'=>'未指定该配置']]
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //获取米花支付配置数据
    public static function getMihuaPayData(){
    	$rules = [
    		'merAccount' => ['require',['require'=>'请输入米花支付商户标识']],
    		'merNo' => ['require',['require'=>'请输入米花支付商户编号']],
    		'privateKey' => ['require',['require'=>'请输入米花支付私钥']],
    		'publicKey' => ['require',['require'=>'请输入米花支付公钥']],
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }

    //获取赛邮短信配置数据
    public static function getSaiyouData(){
        $rules = [
            'appid' => ['require',['require'=>'请输入短信appid']],
            'appkey' => ['require',['require'=>'请输入短信appkey']],
            'sign' => ['require',['require'=>'请输入短信签名']],
            'content' => ['require',['require'=>'请输入内容']]
        ];
        $data = myValidate::getData($rules);
        if(strpos($data['content'],'CODE') === false){
            res_api('短信内容CODE变量不存在');
        }
        return $data;
    }

    //获取dxton短信配置数据
    public static function getDxtonData(){
        $rules = [
            'account' => ['require',['require'=>'请输入用户账号']],
            'password' => ['require',['require'=>'请输入短信接口密码']],
            'content' => ['require',['require'=>'请输入内容']]
        ];
        $data = myValidate::getData($rules);
        if(strpos($data['content'],'CODE') === false){
            res_api('短信内容CODE变量不存在');
        }
        return $data;
    }
    
    //获取aliyunOSS配置数据
    public static function getAliossData(){
    	$rules = [
    		'accessKey' => ['require',['require'=>'请输入阿里云accessKey']],
    		'secretKey' => ['require',['require'=>'请输入阿里云secretKey']],
    		'bucket' => ['require',['require'=>'请输入阿里云空间名称']],
    		'url' => ['require',['require'=>'请输入阿里云绑定域名']],
    		'cdn' => ['url',['url'=>'绑定域名不规范']]
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //获取版本信息
    public static function getVersionData(){
    	$rules = [
    		'android_code' => ['require',['require'=>'请输入安卓端最新版本号']],
    		'ios_code' => ['require',['require'=>'请输入ios端最新版本号']],
    		'android_download_url' => ['require|url',['require'=>'请输入下载安卓地址','url'=>'请输入有效的下载地址']],
    		'ios_download_url' => ['url',['url'=>'请输入有效的下载地址']],
    		'summary' => ['require',['require'=>'请输入更新描述']],
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //获取充值配置数据
    public static function getChargeData(){
    	$rules = [
    		'money' => ['require|array',['require'=>'充值金额列数据异常','array'=>'充值金额列数据异常']],
    		'reward' => ['require|array',['require'=>'赠送金额列数据异常','array'=>'赠送金额列数据异常']],
    		'coin' => ['require|array',['require'=>'充值书币列数据异常','array'=>'充值书币列数据异常']],
    		'is_hot' => ['require|array',['require'=>'是否热门列数据异常','array'=>'是否热门列数据异常']],
    		'package' => ['require|array',['require'=>'套餐列数据异常','array'=>'套餐列数据异常']],
    		'is_on' => ['require|array',['require'=>'是否开启列数据异常','array'=>'是否开启列数据异常']],
    		'is_checked' => ['require|array',['require'=>'是否选中列数据异常','array'=>'是否选中列数据异常']]
    	];
    	$data = myValidate::getData($rules);
    	$config = [];
    	$is_check = 0;
    	foreach ($data['money'] as $k=>$v){
    		$type = 0;
    		$one = ['money'=>$v];
    		$one['package'] = $data['package'][$k];
    		if(!in_array($one['package'],[0,1,2,3,4,5,6])){
    			res_api('套餐数据选择错误');
    		}
    		$one['reward'] = $data['reward'][$k];
    		if($one['reward'] && (!is_numeric($one['reward']) || $one['reward'] < 0)){
    			res_api('赠送书币必须大于等于0的数字');
    		}
    		$one['is_hot'] = $data['is_hot'][$k];
    		if(!in_array($one['is_hot'], [0,1,2])){
    			res_api('是否热门参数错误');
    		}
    		if($one['package'] > 0){
    			$one['coin'] = 0;
    		}else{
    			$one['coin'] = floor($data['coin'][$k]);
    			if(!is_numeric($one['coin']) || $one['coin'] < 0){
    				res_api('充值书币必须为数值且必须大于0');
    			}
    		}
    		$one['is_on'] = $data['is_on'][$k];
    		if(!in_array($one['is_on'], [0,1,2])){
    			res_api('是否启用参数错误');
    		}
    		$one['is_checked'] = $data['is_checked'][$k];
    		if($one['is_checked'] == 1){
    			if($is_check == 0){
    				$is_check = 1;
    			}else{
    				res_api('是否选中只能选择一行');
    			}
    		}
    		$config[] = $one;
    	}
    	return $config;
    }
}