<?php
namespace app\common\model;

use think\Db;
use site\myDb;
use site\myCache;
use site\myValidate;
use OSS\OssClient;
use OSS\Core\OssException;
use site\myPush;

class mdBook{

    //获取书籍列表
    public static function getBookPageList($where,$pages){
        $list = Db::name('Book')
            ->where($where)
            ->page($pages['page'],$pages['limit'])
            ->order('top_num','DESC')
            ->order('id','DESC')
            ->select();
        $count = 0;
        if($list){
            $count = Db::name('Book')->where($where)->count();
        }
        return ['count'=>$count,'data'=>$list];
    }

    //获取书籍列表
    public static function getFreeBookList($where,$pages){
        $field = 'a.*,b.name,b.cover';
        $list = Db::name('BookFree a')
            ->join('book b','a.book_id=b.id')
            ->where($where)
            ->field($field)
            ->group('a.id')
            ->page($pages['page'],$pages['limit'])
            ->order('a.id','DESC')
            ->select();
        $count = 0;
        if($list){
            $count = Db::name('BookFree a')->join('book b','a.book_id=b.id')->where($where)->count();
        }
        return ['count'=>$count,'data'=>$list];
    }

    //获公告资讯列表
    public static function getNewsList($where,$pages){
        $list = Db::name('news')
            ->where($where)
            ->page($pages['page'],$pages['limit'])
            ->order('create_time desc')
            ->select();

        if ($list){
            foreach ($list as $k=>$v){
                $list[$k]['create_time'] = date('Y-m-d H:i:s',$v['create_time']);
                $list[$k]['do_url'] = my_url('Free/doNews',['id'=>$v['id']]);
            }
        }

        $count = 0;
        if($list){
            $count = Db::name('news')->where($where)->count();
        }
        return ['count'=>$count,'data'=>$list];
    }

    //处理更新书籍
    public static function doneBook($data){
        if($data['category']){
            $data['category'] = ','.implode(',', $data['category']).',';
        }
        if(isset($data['id'])){
            $flag = myDb::saveIdData('Book', $data);
        }else{
            $data['create_time'] = time();
            $flag = myDb::add('Book', $data);
        }
        if($flag){
            if(isset($data['id'])){
                myCache::rmBook($data['id']);
                myCache::rmBookHotNum($data['id']);
            }
            res_api();
        }else{
            res_api('保存失败，请重试');
        }
    }

    //批量新增小说
    public static function addMore($data){
        $zip_title = $data['zip_title'];
        $zip_filename = $data['zip_filename'];
        unset($data['zip_title']);
        unset($data['zip_filename']);
        if($data['category']){
            $data['category'] = ','.implode(',', $data['category']).',';
        }
        $data['create_time'] = time();
        $zipPath = env('root_path').'static/temp/zip/';
        $table = 'BookChapter';
        $logs = [];
        foreach ($zip_title as $k=>$v){
            $book_name = rtrim($v,'.zip');
            if(!$book_name){
                $logs[] = '小说名称异常';
                continue;
            }
            $data['name'] = $book_name;
            $repeat = Db::name('Book')->where('name','=',$book_name)->value('id');
            if($repeat){
                $logs[] = '小说【'.$book_name.'】已存在';
                continue;
            }
            $zipFile = $zipPath.$zip_filename[$k];
            if(!@is_file($zipFile)){
                $logs[] = '小说【'.$book_name.'】压缩包不存在';
                continue;
            }
            $zip = new \ZipArchive();
            $rs = $zip->open($zipFile);
            if(!$rs){
                $zip->close();
                @unlink($zipFile);
                $logs[] = '小说【'.$book_name.'】解压失败';
                continue;
            }
            $book_id = Db::name('Book')->insertGetId($data);
            if(!$book_id){
                $logs[] = '小说【'.$book_name.'】新增失败';
                $zip->close();
                @unlink($zipFile);
                continue;
            }
            $docnum = $zip->numFiles;
            for($i = 0; $i < $docnum; $i++) {
                $stateInfo = $zip->statIndex($i);
                if($stateInfo['crc'] != 0 && $stateInfo['size'] > 0){
                    $name = $zip->getNameIndex($i, \ZipArchive::FL_ENC_RAW);
                    $encode = mb_detect_encoding($name,['ASCII','GB2312','GBK','UTF-8']);
                    $encode = $encode ? $encode : 'GBK';
                    $thisName = mb_convert_encoding($name,'UTF-8',$encode);
                    $pathInfo = pathinfo($thisName);
                    if($pathInfo && is_array($pathInfo)){
                        if($pathInfo['extension'] === 'txt'){
                            $title = $pathInfo['filename'];
                            $number = self::getFileNumber($title);
                            if($number > 0){
                                $chapter_repeat = Db::name($table)->where('book_id','=',$book_id)->where('number','=',$number)->value('id');
                                if(!$chapter_repeat){
                                    $content = file_get_contents('zip://'.$zipFile.'#'.$stateInfo['name']);
                                    $encode = mb_detect_encoding($content,['ASCII','GB2312','GBK','UTF-8']);
                                    $encode = $encode ? $encode : 'GBK';
                                    $content = mb_convert_encoding($content, 'UTF-8', $encode);
                                    $html = "<p>".$content;
                                    $html = preg_replace('/\n|\r\n/','</p><p>',$html);
                                    $chapter_data = [
                                        'book_id'=>$book_id,
                                        'name'=>$title,
                                        'number'=>$number,
                                        'create_time'=>time()
                                    ];
                                    $chapter_id = Db::name($table)->insertGetId($chapter_data);
                                    if($chapter_id){
                                        $block_res = saveBlock($html,$number,'book/'.$book_id);
                                        if(!$block_res){
                                            Db::name($table)->where('id','=',$chapter_id)->delete();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $logs[] = '小说【'.$book_name.'】新增成功';
            $zip->close();
            @unlink($zipFile);
        }
        res_api($logs);
    }

    //处理书籍事件
    public static function doBookEvent(){
        $flag = false;
        $rules = [
            'id' =>  ["require|number|gt:0",['require'=>'主键参数错误','number'=>'主键参数不规范',"gt"=>"主键参数不规范"]],
            'event' => ["require|in:on,off,offread,delete,godon,godoff,topon,topoff",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']]
        ];
        $data = myValidate::getData($rules);
        if(in_array($data['event'], ['on','off','offread'])){
            $status = 1;
            switch ($data['event']){
                case 'on':$status=1;break;
                case 'offread':$status=2;break;
                case 'off':$status=3;break;
            }
            $flag = myDb::setField('Book', [['id','=',$data['id']]], 'status', $status);
        }else{
            switch ($data['event']){
                case 'delete':
                    $re = self::delAllChapter($data['id']);
                    if($re){
                        $res = Db::name('Book')->where('id','=',$data['id'])->delete();
                        if($res){
                            $flag = true;
                        }
                    }
                    break;
                case 'topon':
                    $top_num = Db::name('Book')->max('top_num');
                    $top_num = $top_num ? $top_num+1 : 1;
                    $flag = myDb::save('Book', [['id','=',$data['id']]],['top_num'=>$top_num,'is_top'=>1]);
                    break;
                case 'topoff':
                    $flag = myDb::save('Book', [['id','=',$data['id']]],['top_num'=>0,'is_top'=>2]);
                    break;
                case 'godon':
                    $flag = myDb::setField('Book', [['id','=',$data['id']]], 'is_god', 1);
                    break;
                case 'godoff':
                    $flag = myDb::setField('Book', [['id','=',$data['id']]], 'is_god', 2);
                    break;
            }
        }
        if($flag){
            myCache::rmBook($data['id']);
        }
        return $flag;
    }

    //处理书籍压缩包文件
    public static function doBookFile(){
        $rules = [
            'book_id' =>  ["require|number|gt:0",["require"=>"书籍参数错误",'number'=>'书籍参数错误',"gt"=>"书籍参数错误"]],
            'filename' =>  ["require|length:36",["require"=>"文件名异常",'max'=>'文件名错误']],
        ];
        $data = myValidate::getData($rules);
        $cur = myDb::getById('Book', $data['book_id'],'id,type,name');
        if(empty($cur)){
            res_api('书籍不存在');
        }
        switch ($cur['type']){
            case 1:
                self::readAndSaveNovel($data['filename'],$data['book_id']);
                break;
            case 2:
                self::readAndSaveCartoon($data['filename'],$data['book_id']);
                break;

        }
    }

    //获取书籍属性选项
    public static function getOptions(){
        $option = [
            'status' => [
                'name' => 'status',
                'option' => [['val'=>1,'text'=>'上架','default'=>1],['val'=>2,'text'=>'下架可阅','default'=>0],['val'=>3,'text'=>'下架','default'=>0]]
            ],
            'free_type' => [
                'name' => 'free_type',
                'option' => [['val'=>1,'text'=>'免费','default'=>0],['val'=>2,'text'=>'收费','default'=>1]]
            ],
            'gender_type' => [
                'name' => 'gender_type',
                'option' => [['val'=>1,'text'=>'男频','default'=>1],['val'=>2,'text'=>'女频','default'=>0]]
            ],
            'over_type' => [
                'name' => 'over_type',
                'option' => [['val'=>1,'text'=>'连载中','default'=>1],['val'=>2,'text'=>'已完结','default'=>0]]
            ],
            'is_recommend' => [
                'name' => 'is_recommend',
                'option' => [['val'=>1,'text'=>'是','default'=>1],['val'=>2,'text'=>'否','default'=>0]]
            ],
        ];
        return $option;
    }

    //获取分集列表
    public static function getChapterPageList($where,$pages){
        $list = Db::name('BookChapter')->where($where)->page($pages['page'],$pages['limit'])->order('number','desc')->select();
        $count = 0;
        if($list){
            foreach ($list as &$v){
                $v['do_url'] = my_url('doChapter',['id'=>$v['id']]);
                $v['show_url'] = my_url('showInfo',['id'=>$v['id']]);
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
            }
            $count = Db::name('BookChapter')->where($where)->count();
        }
        return ['data'=>$list,'count'=>$count];
    }

    //获取前10章内容
    public static function getTenChapter($book_id){
        $list = Db::name('BookChapter')->where('book_id','=',$book_id)->where('number','<=',10)->field('number as id,name')->order('number','asc')->limit(10)->select();
        return $list;
    }

    //获取前10章内容
    public static function getSpreadChapter($book_id){
        $list = Db::name('BookChapter')->where('book_id','=',$book_id)->where('number','<=',30)->field('number as id,name')->order('number','asc')->limit(30)->select();
        return $list;
    }

    //获取文案书籍信息
    public static function getGuideChapter(){
        $rules = [
            'number' =>  ["require|number|between:1,10",["require"=>"请求章节异常",'number'=>'请求章节异常',"between"=>"请求章节异常"]],
            'book_id' =>  ["require|number|gt:0",["require"=>"书籍参数错误",'number'=>'书籍参数错误',"gt"=>"书籍参数错误"]],
        ];
        $data = myValidate::getData($rules);
        $list = Db::name('BookChapter')->where('book_id','=',$data['book_id'])->where('number','<=',$data['number'])->field('id,book_id,name,number')->order('number','asc')->select();
        if($list){
            foreach ($list as &$v){
                $v['content'] = self::getChapterContent($v['book_id'],$v['number']);
            }
        }
        return $list;
    }

    //设置书籍分享内容
    public static function doShareData(){
        $rules = [
            'book_id' => ['require|number|gt:0',['require'=>'书籍信息异常','number'=>'书籍信息异常','gt'=>'书籍信息异常']],
            'title' => ['max:100',['max'=>'分享标题最多输入100个字符']],
            'content' => ['max:500',['max'=>'分享内容最多输入500个字符']]
        ];
        $data = myValidate::getData($rules);
        $cur = myDb::getCur('BookShare',[['book_id','=',$data['book_id']]]);
        if($cur){
            $re = myDb::save('BookShare',[['book_id','=',$data['book_id']]], $data);
        }else{
            $re = myDb::add('BookShare', $data);
        }
        if($re){
            myCache::rmBookShareInfo($data['book_id']);
            res_api();
        }else{
            res_api('配置失败,请重试');
        }
    }

    //检查书籍章节连贯性
    public static function checkChapter($book_id){
        $field = 'id,chapter';
        $list = Db::name('BookChapter')->where('book_id','=',$book_id)->field('id,number')->select();
        $max = 0;
        $chapter = $error = [];
        if($list){
            foreach ($list as $v){
                if(in_array($v['number'], $chapter)){
                    $error[] = '第'.$v['number'].'章:重复';
                }else{
                    $chapter[] = $v['number'];
                }
                if($v['number'] > $max){
                    $max = $v['number'];
                }
            }
        }
        if($max > 0){
            $path = env('root_path').'static/block/book/'.$book_id;
            for ($i=1;$i<=$max;$i++){
                if(!in_array($i,$chapter)){
                    $error[] = '第'.$i.'章:缺失';
                }else{
                    $file = $path.'/'.$i.'.html';
                    if(@is_file($file)){
                        continue;
                    }else{
                        $error[] = '第'.$i.'章:未检测到内容';
                    }
                }
            }
        }
        return $error;
    }

    //更新章节
    public static function doneChapter($data){
        $content = '';
        if(array_key_exists('content', $data)){
            $content = $data['content'];
            unset($data['content']);
        }
        $res = false;
        if(array_key_exists('id', $data)){
            $re = Db::name('BookChapter')->where('id','=',$data['id'])->update($data);
        }else{
            $repeat = Db::name('BookChapter')->where('book_id','=',$data['book_id'])->where('number','=',$data['number'])->value('id');
            if($repeat){
                res_api('该章节已存在');
            }
            $data['create_time'] = time();
            $re = Db::name('BookChapter')->insert($data);
        }
        if($re !== false){
            if($content){
                $dir = 'book/'.$data['book_id'];
                saveBlock($content,$data['number'],$dir);
            }
            if(!isset($data['id'])){
                myCache::rmBookChapterNum($data['book_id']);
            }
            myCache::rmBookChapterList($data['book_id']);
            myPush::updateBookPush($data['book_id']);
            res_api('ok');
        }else{
            res_api('操作失败，请重试');
        }
    }

    //获取章节设置书币参数
    public static function getChapterMoneyData(){
        $rules = [
            'id'=>["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
            'money'=>["require|number|egt:0",["require"=>"金额参数错误",'number'=>'金额参数错误',"egt"=>"金额参数错误"]],
        ];
        $data = myValidate::getData($rules);
        return $data;
    }

    //获取章节设置广告出现页数
    public static function getPageNoticeData()
    {
        $rules = [
            'id'=>["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
            'page_notice'=>["require",["require"=>"金额参数错误"]],
        ];
        $data = myValidate::getData($rules);
        return $data;
    }

    //删除章节
    public static function delChapter(){
        $rules = [
            'id' =>  ["require|number|gt:0",["require"=>"分集主键参数错误",'number'=>'分集主键参数错误',"gt"=>"分集主键参数错误"]],
            'book_id' =>  ["require|number|gt:0",["require"=>"书籍参数错误",'number'=>'书籍参数错误',"gt"=>"书籍参数错误"]],
        ];
        $data = myValidate::getData($rules);
        $book = myDb::getById('Book',$data['book_id'],'id,name');
        if(empty($book)){
            res_api('书籍信息异常');
        }
        $cur = myDb::getById('BookChapter',$data['id'],'id,book_id,number,src,files');
        if(empty($cur)){
            res_api('章节信息异常');
        }
        $res = false;
        $re = Db::name('BookChapter')->where('id','=',$cur['id'])->delete();
        if($re){
            $res = true;
            $filename = env('root_path').'static/block/book/'.$book['id'].'/'.$cur['number'].'.html';
            if(@is_file($filename)){
                @unlink($filename);
            }
            myCache::rmBookChapterNum($book['id']);
            myCache::rmBookChapterList($book['id']);
        }
        return $res;
    }

    //删除所有章节
    public static function delAllChapter($book_id){
        $res = false;
        $book = myDb::getById('Book',$book_id,'id,name');
        if(empty($book)){
            res_api('书籍信息异常');
        }
        $list = $config = [];
        $re = Db::name('BookChapter')->where('book_id','=',$book_id)->delete();
        if($re !== false){
            $dir = env('root_path').'static/block/book/'.$book_id;
            if(@is_dir($dir)){
                self::delDirAndFile($dir);
            }
            $res = true;
            myCache::rmBookChapterNum($book_id);
            myCache::rmBookChapterList($book_id);
        }
        return $res;
    }

    //获取小说章节内容
    public static function getChapterContent($book_id,$number){
        $filename = env('root_path').'static/block/book/'.$book_id.'/'.$number.'.html';
        $content = '';
        if(@is_file($filename)){
            $content = file_get_contents($filename);
        }
        return $content;
    }

    //读取zip并保存小说章节
    public static function readAndSaveNovel($filename,$book_id){
        $zipFile = env('root_path').'static/temp/zip/'.$filename;
        if(@is_file($zipFile)){
            $zip = new \ZipArchive();
            $rs = $zip -> open($zipFile);
            if(!$rs){
                @unlink($zipFile);
                res_api('读取压缩文件失败');
            }
            $docnum = $zip->numFiles;
            $table = 'BookChapter';
            for($i = 0; $i < $docnum; $i++) {
                $stateInfo = $zip->statIndex($i);
                if($stateInfo['crc'] != 0 && $stateInfo['size'] > 0){
                    $name = $zip->getNameIndex($i, \ZipArchive::FL_ENC_RAW);
                    $encode = mb_detect_encoding($name,['ASCII','GB2312','GBK','UTF-8']);
                    $encode = $encode ? $encode : 'GBK';
                    $thisName = mb_convert_encoding($name,'UTF-8',$encode);
                    $pathInfo = pathinfo($thisName);
                    if($pathInfo && is_array($pathInfo)){
                        if($pathInfo['extension'] === 'txt'){
                            $title = $pathInfo['filename'];
                            $number = self::getFileNumber($title);
                            if($number > 0){
                                $repeat = Db::name($table)->where('book_id','=',$book_id)->where('number','=',$number)->value('id');
                                if(!$repeat){
                                    $content = file_get_contents('zip://'.$zipFile.'#'.$stateInfo['name']);
                                    $encode = mb_detect_encoding($content,['ASCII','GB2312','GBK','UTF-8']);
                                    $encode = $encode ? $encode : 'GBK';
                                    $content = mb_convert_encoding($content, 'UTF-8', $encode);
                                    $html = "<p>".$content;
                                    $html = preg_replace('/\n|\r\n/','</p><p>',$html);
                                    $data = [
                                        'book_id'=>$book_id,
                                        'name'=>$title,
                                        'number'=>$number,
                                        'create_time'=>time()
                                    ];
                                    $chapter_id = Db::name($table)->insertGetId($data);
                                    if($chapter_id){
                                        $block_res = saveBlock($html,$number,'book/'.$book_id);
                                        if(!$block_res){
                                            Db::name($table)->where('id','=',$chapter_id)->delete();
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            $zip -> close();
            @unlink($zipFile);
            myCache::rmBookChapterNum($book_id);
            myCache::rmBookChapterList($book_id);
        }else{
            res_api('zip文件不存在');
        }
    }

    //读取并解析上传漫画章节
    public static function readAndSaveCartoon($filename,$book_id){
        $zipFile = env('root_path').'static/temp/zip/'.$filename;
        if(@is_file($zipFile)){
            $zip = new \ZipArchive();
            $rs = $zip -> open($zipFile);
            if(!$rs){
                @unlink($zipFile);
                res_api('读取压缩文件失败');
            }
            $config = getMyConfig('alioss');
            if(!$config){
                $zip->close();
                @unlink($zipFile);
                res_api('您尚未配置阿里云参数');
            }
            $table = 'BookChapter';
            $docnum = $zip->numFiles;
            $res = $temp = $chapter = [];
            for ($i=0;$i<$docnum;$i++){
                $stateInfo = $zip->statIndex($i);
                if($stateInfo['crc'] != 0 && $stateInfo['size'] > 0){
                    $name = $zip->getNameIndex($i, \ZipArchive::FL_ENC_RAW);
                    $encode = mb_detect_encoding($name,['ASCII','GB2312','GBK','UTF-8']);
                    $encode = $encode ? $encode : 'GBK';
                    $thisName = mb_convert_encoding($name,'UTF-8',$encode);
                    $pathInfo = pathinfo($thisName);
                    if($pathInfo && is_array($pathInfo)){
                        if(is_numeric($pathInfo['filename'])){
                            if(isset($pathInfo['dirname']) && $pathInfo['dirname']){
                                if(in_array($pathInfo['dirname'],$temp)){
                                    $number = array_search($pathInfo['dirname'],$temp);
                                }else{
                                    $dirInfo = self::getDirNumber($pathInfo['dirname']);
                                    $number = $dirInfo['number'];
                                }
                                if($number > 0){
                                    $fileKey = $pathInfo['filename'];
                                    if(!isset($chapter[$number])){
                                        $chapter[$number] = [
                                            'title'=>$dirInfo['title'],
                                            'number'=>$number,
                                            'child'=>[
                                                $fileKey => [
                                                    'name' => $stateInfo['name'],
                                                    'savename' => 'book/'.$book_id.'/'.$number.'/'.md5($fileKey).'.'.$pathInfo['extension']
                                                ]
                                            ]
                                        ];
                                    }else{
                                        $chapter[$number]['child'][$fileKey] = [
                                            'name' => $stateInfo['name'],
                                            'savename' => 'book/'.$book_id.'/'.$number.'/'.md5($fileKey).'.'.$pathInfo['extension']
                                        ];
                                    }
                                }
                            }
                        }
                    }
                }
            }
            if(!empty($chapter)){
                $ossClient = new OssClient($config['accessKey'],$config['secretKey'],$config['url']);
                ksort($chapter);
                foreach ($chapter as $v){
                    if($v['child']){
                        $child = $v['child'];
                        ksort($child);
                        $repeat = Db::name($table)->where('book_id','=',$book_id)->where('number','=',$v['number'])->value('id');
                        if(!$repeat){
                            $data = [
                                'book_id'=>$book_id,
                                'name'=> $v['title'],
                                'src' => '',
                                'number'=> $v['number'],
                                'files' => [],
                                'create_time'=>time()
                            ];
                            $html = '';
                            foreach ($child as $val){
                                $local_file = 'zip://'.$zipFile.'#'.$val['name'];
                                $content = file_get_contents($local_file);
                                if($content){
                                    try {
                                        $ossClient->putObject($config['bucket'],$val['savename'],$content);
                                        if(isset($config['cdn']) && $config['cdn']){
                                            $url = $config['cdn'].'/'.$val['savename'];
                                        }else{
                                            $url = 'https://'.$config['bucket'].'.'.$config['url'].'/'.$val['savename'];
                                        }
                                        if(!$data['src']){
                                            $data['src'] = $url;
                                        }
                                        $data['files'][] = $val['savename'];
                                        $html .= '<img src="'.$url.'" />';
                                    }catch (OssException $e){
                                        try {
                                            $ossClient->putObject($config['bucket'],$val['savename'],$content);
                                            if(isset($config['cdn']) && $config['cdn']){
                                                $url = $config['cdn'].'/'.$val['savename'];
                                            }else{
                                                $url = 'https://'.$config['bucket'].'.'.$config['url'].'/'.$val['savename'];
                                            }
                                            if(!$data['src']){
                                                $data['src'] = $url;
                                            }
                                            $data['files'][] = $val['savename'];
                                            $html .= '<img src="'.$url.'" />';
                                        }catch (OssException $e){
                                            $zip->close();
                                            @unlink($zipFile);
                                            res_api('章节：'.$v['title'].'=>上传到阿里云OSS失败，原因：'.$e->getMessage());
                                        }
                                    }
                                }else{
                                    $zip->close();
                                    @unlink($zipFile);
                                    res_api('章节：'.$v['title'].'=>读取失败');
                                }
                            }
                            $data['files'] = json_encode($data['files']);
                            $chapter_id = Db::name($table)->insertGetId($data);
                            if($chapter_id){
                                $block_res = saveBlock($html,$v['number'],'book/'.$book_id);
                                if(!$block_res){
                                    Db::name($table)->where('id','=',$chapter_id)->delete();
                                }
                            }
                        }
                    }
                }
            }
            $zip -> close();
            @unlink($zipFile);
            myCache::rmBookChapterNum($book_id);
            myCache::rmBookChapterList($book_id);
        }else{
            res_api('zip文件不存在');
        }
    }

    //解析路径中文件章节
    private static function getDirNumber($dir){
        $arr = explode('/', $dir);
        $title = end($arr);
        $number = self::getFileNumber($title);
        return ['number'=>$number,'title'=>$title];
    }

    //获取章节
    private static function getFileNumber($title){
        $number = 0;
        $pattern = '/[(\d)|(零一壹二贰三叁四肆五伍六陆七柒八捌九玖十拾百佰千仟万两)]+/u';
        $data = preg_match($pattern, $title,$match);
        if(!empty($match)){
            $number = $match[0];
            if(is_numeric($number)){
                $number = intval($number);
            }else{
                $number = self::chrtonum($number);
            }
        }
        return $number;
    }

    //删除文件夹及子目录和文件
    private static function delDirAndFile( $dirName){
        if ( @$handle = opendir( "$dirName" ) ) {
            while ( false !== ( $item = readdir( $handle ) ) ) {
                if ( $item !== "." && $item !== ".." ) {
                    if ( is_dir( "$dirName/$item" ) ) {
                        self::delDirAndFile( "$dirName/$item" );
                    } else {
                        @unlink( "$dirName/$item" );
                    }
                }
            }
            closedir( $handle );
            rmdir( "$dirName/$item" );
        }
    }

    //中文转阿拉伯
    private static function chrtonum($string){
        if(is_numeric($string)){
            return $string;
        }
        $string = str_replace('仟', '千', $string);
        $string = str_replace('佰', '百', $string);
        $string = str_replace('拾', '十', $string);
        $num = 0;
        $wan = explode('万', $string);
        if (count($wan) > 1) {
            $num += self::chrtonum($wan[0]) * 10000;
            $string = $wan[1];
        }
        $qian = explode('千', $string);
        if (count($qian) > 1) {
            $num += self::chrtonum($qian[0]) * 1000;
            $string = $qian[1];
        }
        $bai = explode('百', $string);
        if (count($bai) > 1) {
            $num += self::chrtonum($bai[0]) * 100;
            $string = $bai[1];
        }
        $shi = explode('十', $string);
        if (count($shi) > 1) {
            $num += self::chrtonum($shi[0] ? $shi[0] : '一') * 10;
            $string = $shi[1] ? $shi[1] : '零';
        }
        $ling = explode('零', $string);
        if (count($ling) > 1) {
            $string = $ling[1];
        }
        $d = [
            '一' => '1','二' => '2','三' => '3','四' => '4','五' => '5','六' => '6','七' => '7','八' => '8','九' => '9',
            '壹' => '1','贰' => '2','叁' => '3','肆' => '4','伍' => '5','陆' => '6','柒' => '7','捌' => '8','玖' => '9',
            '零' => 0, '0' => 0, 'O' => 0, 'o' => 0,
            '两' => 2
        ];
        return $num + @$d[$string];
    }

    //获取书籍规则
    public static function getData($isAdd){
        $rules = [
            'name' =>  ["require|max:200",["require"=>"请输入书籍名称",'max'=>'书籍名称最多支持200个字符']],
            'author' =>  ["max:50",['max'=>'作者名称最多支持50个字符']],
            'cover' =>  ['max:255',['max'=>'书籍封面异常']],
            'summary' =>  ["max:500",['max'=>'书籍简介最多支持500个字符']],
            'status' => ["require|in:1,2,3",["require"=>"请选择书籍状态","in"=>"未指定该书籍状态"]],
            'category' => ["array",["array"=>"小说分类参数异常"]],
            'free_type' => ["require|in:1,2",["require"=>"请选择书籍是否免费","in"=>"未指定该书籍是否免费状态"]],
            'gender_type' => ["require|in:1,2",["require"=>"请选择书籍频道","in"=>"未指定该书籍频道"]],
            'is_recommend' => ["require|in:1,2",["require"=>"请选择是否推荐精选","in"=>"未指定该推荐精选状态值"]],
            'over_type' => ["require|in:1,2",["require"=>"请选择书籍连载状态","in"=>"未指定该书籍连载状态"]],
            'free_chapter' => ["number",["number"=>"免费章节必须为正整数"]],
            'money' => ["require|number",["require"=>"请输入书籍章节收费书币数量","in"=>"书币数量必须为正整数"]],
            'hot_num' => ["number",["require"=>"人气值必须为正整数"]],
            'share_title' =>  ["max:100",['max'=>'分享标题最多支持100个字符']],
            'share_desc' =>  ["max:500",['max'=>'分享描述最多支持500个字符']],
            'word_number' => ["number",["number"=>"小说字数格式不规范"]],
        ];
        if(!$isAdd){
            $rules['id'] =  ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]];
        }
        $data = myValidate::getData($rules);
        return $data;
    }

    //获取章节规则
    public static function getChapterData($isAdd=1){
        $rules = [
            'book_id' =>  ["require|number|gt:0",["require"=>"书籍参数错误",'number'=>'书籍参数错误',"gt"=>"书籍参数错误"]],
            'content' =>  ["require",['require'=>'请输入分集内容']],
            'number' => ["require|number|gt:0",["require"=>"章节参数错误",'number'=>'章节参数错误',"gt"=>"章节参数错误"]],
            'money' => ["require|number|egt:0",["require"=>"请输入章节书币数量",'number'=>'章节书币格式不规范',"egt"=>"章节书币必须大于等于0"]],
            'name' =>  ["require|max:50",["require"=>"请输入章节名称",'max'=>'章节名称最多支持50个字符']],
        ];
        if(!$isAdd){
            $rules['id'] =  ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]];
        }
        $data = myValidate::getData($rules);
        return $data;
    }


    /***********************以下为前端调用方法*******************************/

    //获取发布区域书籍列表
    public static function getAreaBookList($type,$gender_type){
        $res = [
            ['area'=>'精品推荐','area_id'=>1,'book_list'=>[]],
            ['area'=>'本周热搜','area_id'=>2,'book_list'=>[]],
            ['area'=>'热门推荐','area_id'=>3,'book_list'=>[]],
            ['area'=>'新书推荐','area_id'=>4,'book_list'=>[]],
        ];
        foreach ($res as $k=>&$v){
            $result = self::getAreaBookInfo($type, $gender_type, $v['area_id'], 1, 6);
            if(!$result){
                unset($res[$k]);
                continue;
            }
            if($result['list']){
                $v['book_list'] = $result['list'];
            }
        }
        $res = $res ? array_values($res) : [];
        return $res;
    }

    //获取指定区域书籍
    public static function getAreaBookInfo($type,$gender_type,$area_id,$page,$limit){
        $ids = null;
        switch ($area_id){
            case 1:
                $ids = myCache::getRecommendIds($type, $gender_type);
                break;
            case 2:
                $ids = myCache::getReadTopIds($type, $gender_type);
                break;
            case 3:
                $ids = myCache::getHotIds($type, $gender_type);
                break;
            case 4:
                $ids = myCache::getUpdateIds($type, $gender_type);//extend/site/myCache
                break;
        }
        if(!$ids){
            return false;
        }
        $page--;
        $arr = array_chunk($ids, $limit);
        if(!isset($arr[$page])){
            return false;
        }
        $list = $arr[$page];
        $count = count($arr);
        $res = [];
        foreach ($list as $book_id){
            $book = myCache::getBook($book_id);
            if($book && $book['status'] == 1){
                $chapter = Db::name('book_chapter')->where(['book_id'=>$book_id])
                    ->order('number desc')->find();
                $res[] = [
                    'id' => $book['id'],
                    'name' => $book['name'],
                    'cover' => $book['cover'],
                    'author' => $book['author'],
                    'summary' => $book['summary'],
                    'category' => $book['category'],
                    'hot_num' => myCache::getBookHotNum($book_id),
                    'type' => $book['type'],
                    'word_number'=>$book['word_number'],
                    'over_type'=>$book['over_type']== 1 ? '连载中':'已完结',
                    'best_new_chapter'=>$chapter['name'],
                    'edit_time'=>date('Y-m',$chapter['create_time']),
                ];
            }
        }
        return ['list'=>$res,'count'=>$count];
    }

    //获取列表书籍详情
    public static function getListBookInfo($book_ids,$is_url=true){
        $data = [];
        if($book_ids){
            foreach ($book_ids as $book_id){
                $book = myCache::getBook($book_id);
                if($book && $book['status'] == 1){
                    $one = [
                        'id' => $book['id'],
                        'name' => $book['name'],
                        'author' => $book['author'],
                        'cover' => $book['cover'],
                        'summary' => $book['summary'],
                        'category' => $book['category']
                    ];
                    if($is_url){
                        $one['info_url'] = my_url('info',['book_id'=>$book_id]);
                    }
                    $data[] = $one;
                }
            }
        }
        return $data;
    }

    //新增阅读量
    public static function addHot($book_id){
        $re = Db::name('Book')->where('id','=',$book_id)->setInc('hot_num');
        if($re){
            myCache::incBookHotNum($book_id);
        }
    }

    //获取书籍分类列表
    public static function getCategoryList($data,$is_url=true){
        $field = 'id,name,cover,summary,category';
        $where = [['status','=',1]];
        $where[] = ['type','=',$data['type']];
        if(isset($data['gender_type']) && $data['gender_type']){
            $where[] = ['gender_type','=',$data['gender_type']];
        }
        if(isset($data['category']) && $data['category']){
            $where[] = ['category','like','%'.$data['category'].'%'];
        }
        if($data['orderby'] == 3){
            $where[] = ['over_type','=',1];
        }
        $obj = Db::name('book')
            ->where($where)
            ->field($field)
            ->page($data['page'],10);
        if($data['orderby'] == 2){
            $obj->order('hot_num','desc')->order('id','desc');
        }else{
            $obj->order('id','desc');
        }
        $list = $obj->select();
        if($list){
            foreach ($list as &$v){
                $v['category'] = $v['category'] ? explode(',', trim($v['category'],',')) : [];
                if($is_url){
                    $v['info_url'] = my_url('info',['book_id'=>$v['id']]);
                }
            }
        }
        return $list;
    }

    //获取分类选项
    public static function getCategoryOption($key,$get){
        $cateConfig = getMyConfig($key);
        $temp = ['title'=>['name'=>'全部','val'=>'','is_check'=>1],'child'=>[]];
        if($cateConfig){
            foreach ($cateConfig as $v){
                $temp['child'][] = ['name'=>$v,'val'=>$v,'is_check'=>0];
            }
        }
        $gender_type = [
            'title' => ['name'=>'全部','val'=>'','is_check'=>1],
            'child' => [
                ['name'=>'男生频道','val'=>'1','is_check'=>0],
                ['name'=>'女生频道','val'=>'2','is_check'=>0]
            ]
        ];
        if($get['gender_type']){
            $gender_type['title']['is_check'] = 0;
            foreach ($gender_type['child'] as &$gv){
                $gv['is_check'] = 0;
                if($gv['val'] == $get['gender_type']){
                    $gv['is_check'] = 1;
                }
            }
        }
        $orderby = [
            'title' => ['name'=>'最新','val'=>1,'is_check'=>1],
            'child' => [
                ['name' => '最热','val'=>2,'is_check'=>0],
                ['name' => '完结','val'=>3,'is_check'=>0],
            ]
        ];
        return ['gender_type'=>$gender_type,'category'=>$temp,'orderby'=>$orderby,'type'=>$get['type']];
    }

    /**
     * 添加阅读历史
     * @param array $book 书籍信息
     * @param number $number 章节数
     * @param array $member 用户信息
     * @param boolean $is_money 是否需要书币购买 1是2否
     * @return boolean
     */
    public static function addReadhistory($book,$money,$chapter,$member,$is_money=false){
        Db::startTrans();
        $flag = false;
        $time = time();
        $fres = Db::name('ReadHistory')->where('uid','=',$member['id'])->where('is_end','=',1)->setField('is_end',2);
        if($fres !== false){
            $data = [
                'book_id' => $book['id'],
                'type' => 1,
                'number' => $chapter['number'],
                'uid' => $member['id'],
                'is_end' => 1,
                'channel_id' => $member['wx_id'],
                'create_time' => $time
            ];
            $re = Db::name('ReadHistory')->insert($data);
            if($re){
                $flag = true;
                if($is_money){
                    $flag = false;
                    $res = Db::name('Member')->where('id','=',$member['id'])->setDec('money',$money);
                    if($res){
                        $log = [
                            'uid' => $member['id'],
                            'book_id' => $book['id'],
                            'money' => $money,
                            'chapter_title' => $chapter['name'],
                            'summary' => '阅读书籍《'.$book['name'].'》',
                            'create_time' => $time
                        ];
                        $log_res = Db::name('MemberConsume')->insert($log);
                        if($log_res){
                            $flag = true;
                        }
                    }
                }
            }
        }
        if($flag){
            Db::commit();
            myCache::addRead($member['id'], $book['id'], $chapter['number'],$time);
            if($is_money){
                myCache::doMemberMoney($member['id'],$money,'dec');
            }
        }else{
            Db::rollback();
        }
        return $flag;
    }

    public static function addReadhistory1($book,$money,$chapter,$member){
        Db::startTrans();
        $flag = false;
        $time = time();
        $fres = Db::name('ReadHistory')->where('uid','=',$member['id'])->where('is_end','=',1)->setField('is_end',2);
        if($fres !== false){
            $data = [
                'book_id' => $book['id'],
                'type' => 1,
                'number' => $chapter['number'],
                'uid' => $member['id'],
                'is_end' => 1,
                'channel_id' => $member['wx_id'],
                'create_time' => $time
            ];
            $re = Db::name('ReadHistory')->insert($data);
            if($re){
                $log = [
                    'uid' => $member['id'],
                    'book_id' => $book['id'],
                    'money' => $money,
                    'chapter_title' => $chapter['name'],
                    'summary' => '阅读书籍《'.$book['name'].'》',
                    'create_time' => $time
                ];
                $log_res = Db::name('MemberConsume')->insert($log);
                if($log_res){
                    $flag = true;
                }
            }
        }
        if($flag){
            Db::commit();
            myCache::addRead($member['id'], $book['id'], $chapter['number'],$time);
//            if($is_money){
//                myCache::doMemberMoney($member['id'],$money,'dec');
//            }
        }else{
            Db::rollback();
        }
        return $flag;
    }

    /**
     * 增加章节阅读数
     * @param unknown $chapter_id
     */
    public static function addChapterRead($chapter_id){
        Db::name('BookChapter')->where('id',$chapter_id)->setInc('read_num');
    }

    //获取限免书籍数据
    public static function getFreeBookData($type,$gender_type,$page,$is_url = true){
        $list = [];
        $cache = myCache::getBookFreeIds($type, $gender_type);
        if($cache){
            $page -= 1;
            $data = array_chunk($cache, 10);
            if(isset($data[$page])){
                foreach ($data[$page] as $v){
                    $book = myCache::getBook($v['id']);
                    if($book && $book['status'] == 1){
                        if($book['type'] == $type && $book['gender_type'] == $gender_type){
                            $one = [
                                'id' => $book['id'],
                                'name' => $book['name'],
                                'cover' => $book['cover'],
                                'summary' => $book['summary'],
                                'start_date' => $v['start_date'],
                                'end_date' => $v['end_date']
                            ];
                            if($is_url){
                                $one['info_url'] = my_url('info',['book_id'=>$book['id']]);
                            }
                            $list[] = $one;
                        }
                    }
                }
            }
        }
        return $list;
    }

    //获取限免小说
    public static function getIndexFreeBook($type,$gender_type,$page){
        $date = date('Y-m-d');
        $field = 'a.*,b.name,b.cover,b.summary';
        $list = Db::name('BookFree a')
            ->join('book b','a.book_id=b.id')
            ->where('a.start_date','<=',$date)
            ->where('a.end_date','>=',$date)
            ->where('b.type',$type)
            ->where('b.status',1)
            ->where('b.gender_type',$gender_type)
            ->field($field)
            ->group('a.id')
            ->page($page,10)
            ->order('a.id','DESC')
            ->select();
        if($list){
            foreach ($list as &$v){
                $v['info_url'] = my_url('info',['book_id'=>$v['book_id']]);
            }
        }
        return $list;
    }

}