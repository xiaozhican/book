<?php
namespace app\common\model;

use think\Db;
use site\myDb;
use site\myCache;
use site\myValidate;

class mdPlatform{
	
	public static function getList($where,$pages){
		$field = 'a.id,a.name,a.login_name,a.url,a.ratio,a.status,b.charge_money';
		$list = Db::name('Channel a')
		->join('channel_money b','a.id=b.channel_id','left')
		->where($where)
		->field($field)
		->page($pages['page'],$pages['limit'])
		->order('a.id','desc')
		->select();
		$count = 0;
		if($list){
			$count = Db::name('Channel a')->where($where)->count();
		}
		return ['data'=>$list,'count'=>$count];
	}
    
    //获取渠道选项
    public static function getChannelOptions(){
        $option = [
            'status' => [
                'name' => 'status',
                'option' => [
                    ['val'=>1,'text'=>'启用','default'=>1],
                    ['val'=>2,'text'=>'禁用','default'=>0]
                ]
            ],
            'is_location' => [
                'name' => 'is_location',
                'option' => [
                    ['val'=>1,'text'=>'开启','default'=>0],
                    ['val'=>2,'text'=>'关闭','default'=>1]
                ]
            ],
        ];
        return $option;
    }
    
    //保存渠道信息
    public static function doneChannel($data){
    	$flag = false;
        if($data['url'] && $data['url'] == $data['location_url']){
            res_api('绑定域名和跳转域名不能一致');
        }
        if(array_key_exists('id',$data)){
        	if($data['url']){
        		self::checkUrlRepeat($data['url'],$data['id']);
        	}
            if($data['location_url']){
                self::checkUrlRepeat($data['location_url'],$data['id']);
            }
            $cur = myDb::getById('Channel',$data['id'],'id,url,location_url');
            if(!$cur){
                res_api('渠道信息异常');
            }
            $flag = myDb::saveIdData('Channel', $data);
            if($flag){
            	myCache::rmChannel($cur['id']);
            	if($cur['url']){
            		myCache::rmUrlInfo($cur['url']);
            	}
            	if($cur['location_url']){
            		myCache::rmUrlInfo($cur['location_url']);
            	}
            }
        }else{
        	if(isset($data['login_name'])){
        		$repeat = Db::name('Channel')->where('login_name','=',$data['login_name'])->value('id');
        		if($repeat){
        			res_api('该账号已存在');
        		}
        	}
        	if($data['url']){
        		self::checkUrlRepeat($data['url']);
        	}
            if($data['location_url']){
                self::checkUrlRepeat($data['location_url']);
            }
            Db::startTrans();
            $flag = false;
            $data['type'] = 1;
            $data['is_wx'] = 1;
            $data['password'] = createPwd($data['password']);
            $data['create_time'] = time();
            $channelId = Db::name('Channel')->insertGetId($data);
            if($channelId){
            	$res = Db::name('ChannelMoney')->insert(['channel_id'=>$channelId]);
            	if($res){
            		$flag = true;
            	}
            }
            if($flag){
            	Db::commit();
            }else{
            	Db::rollback();
            }
        }
        if($flag){
            res_api();
        }else{
            res_api('保存失败，请重试');
        }
    }
    
    
    
    //检查域名是否重复
    public static function checkUrlRepeat($url,$id=0,$is_site=false){
        $where = [['url|location_url','=',$url]];
        if($id){
            $where[] = ['id','<>',$id];
        }
        $repeat = myDb::getCur('Channel', $where,'id,name');
        if($repeat){
            res_api('该域名已占用，请更换');
        }
        if(!$is_site){
            $site = getMyConfig('website');
            if($url == $site['location_url'] || $url == $site['url']){
                res_api('该域名已占用，请更换');
            }
        }
    }
    
    public static function getLevelInfo($channel_id=0){
    	$data = [
    		'channel_id' => 0,
    		'agent_id' => 0,
    		'wx_id' => 0
    	];
    	if($channel_id){
    		$channel = myCache::getChannel($channel_id);
    		if(!$channel){
    			return false;
    		}
    		if($channel['type'] == 1){
    			$data['channel_id'] = $channel['id'];
    			if($channel['is_wx'] == 1){
    				$data['wx_id'] = $channel['id'];
    			}
    		}else{
    			$parent = myCache::getChannel($channel['parent_id']);
    			if(!$channel){
    				return false;
    			}
    			$data['channel_id'] = $parent['id'];
    			$data['agent_id'] = $channel['id'];
    			if($parent['is_wx'] == 1){
    				$data['wx_id'] = $parent['id'];
    			}
    		}
    	}
    	return $data;
    }
    
    //获取更新代理选项
    public static function getAgentOptions(){
        $option = [
            'status' => [
                'name' => 'status',
                'option' => [
                    ['val'=>1,'text'=>'启用','default'=>1],
                    ['val'=>2,'text'=>'禁用','default'=>0]
                ]
            ]
        ];
        return $option;
    }
    
    //保存代理信息
    public static function doneAgent($data){
        $data['location_url'] = '';
        if(array_key_exists('id', $data)){
        	if($data['url']){
        		self::checkUrlRepeat($data['url'],$data['id']);
        	}
            $cur = myDb::getById('Channel',$data['id'],'id,url');
            if(!$cur){
                res_api('代理信息异常');
            }
            $key = 'channel_info_'.$data['id'];
            $flag = myDb::saveIdData('Channel', $data);
            if($flag){
            	myCache::rmChannel($cur['id']);
            	if($cur['url']){
            		myCache::rmUrlInfo($cur['url']);
            	}
            }
        }else{
        	if(isset($data['login_name'])){
        		$repeat = Db::name('Channel')->where('login_name','=',$data['login_name'])->value('id');
        		if($repeat){
        			res_api('该账号已存在');
        		}
        	}
        	if($data['url']){
        		self::checkUrlRepeat($data['url']);
        	}
        	Db::startTrans();
        	$flag = false;
            $data['is_wx'] = 2;
            $data['password'] = createPwd($data['password']);
            $data['create_time'] = time();
            $cid = Db::name('Channel')->insertGetId($data);
            if($cid){
            	$moneyData = ['channel_id'=>$cid];
            	if(isset($data['parent_id']) && $data['parent_id']){
            		$moneyData['pid'] = $data['parent_id'];
            	}
            	$res = Db::name('ChannelMoney')->insert($moneyData);
            	if($res){
            		$info = self::getLevelInfo($cid);
	            	if($info){
	            		$codeInfo = [
	            			'type' => 3,
	            			'cid' => $cid,
	            			'code' => myDb::createCode('Code'),
	            			'info' => json_encode($info)
	            		];
	            		$codeRes = Db::name('Code')->insert($codeInfo);
	            		if($codeRes){
	            			$flag = true;
	            		}
	            	}
            	}
            }
            if($flag){
            	Db::commit();
            }else{
            	Db::rollback();
            }
        }
        if($flag){
            res_api();
        }else{
            res_api('保存失败，请重试');
        }
    }
    
    //跳转代理后台
    public static function intoBackstage($channel_id){
        $cur = myDb::getById('Channel', $channel_id);
        if(!$cur){
            res_api('代理参数异常');
        }
        $url = '/channel';
        $key = 'CHANNEL_LOGIN_ID';
        if($cur['is_wx'] == 2){
            $key = 'AGENT_LOGIN_ID';
            $url = '/agent';
        }
        session($key,$cur['id']);
        return $url;
    }
    
    /**
     * 获取代理状态名称
     * @param number $status 状态值
     * @return string
     */
    public static function getStatusName($status){
        $name = '未知';
        switch ($status){
            case 0:
                $name = '待审核';
                break;
            case 1:
                $name = '正常';
                break;
            case 2:
                $name = '禁用';
                break;
            case 3:
                $name = '审核不通过';
                break;
        }
        return $name;
    }
    
    //获取渠道更新规则
    public static function getRules($is_channel=1,$isAdd=1){
    	$rules = [
    		'name' =>  ["require|max:20",["require"=>"请输入渠道名称",'max'=>'渠道名称最多支持20个字符']],
    		'url' =>  ["max:50",['url'=>'绑定域名长度超出限制']],
    		'deduct_min' => ["number",["number"=>"请输入正确格式的距扣量数"]],
    		'deduct_num' => ["number",["number"=>"请输入正确格式的扣量数"]],
    		'ratio' => ["number|between:0,100",["number"=>"请输入正确格式的返额比例",'between'=>'请输入0-100区间的返额比例']],
    		'wefare_days' => ["number",["number"=>"请输入正确格式的代理福利时长"]],
    	];
    	if($is_channel){
    		$rules['status'] = ["require|in:1,2",["require"=>"请选择渠道状态","in"=>"未指定渠道状态"]];
    		$rules['is_location'] = ["require|in:1,2",["require"=>"请选择是否开启域名跳转","in"=>"未指定该域名跳转状态"]];
    		$rules['location_url'] = ["max:50|requireIf:is_location,1",["requireIf"=>"请输入跳转域名","max"=>"跳转域名长度超出限制"]];
    		$rules['appid'] = ['alphaNum|max:32',['alphaNum'=>'公众号appid格式不规范','max'=>'公众号appid长度超出限制']];
    		$rules['appsecret'] = ['alphaNum|max:64',['alphaNum'=>'公众号secret格式不规范','max'=>'公众号secret长度超出限制']];
    		$rules['apptoken'] = ['alphaNum|max:32',['alphaNum'=>'公众号token格式不规范','max'=>'公众号token长度超出限制']];
    		$rules['qrcode'] = ['url',['url'=>'公众号二维码图片格式不规范']];
    	}else{
    		$rules['bank_user'] = ["max:20",['max'=>'开户人姓名长度超出限制']];
    		$rules['bank_name'] = ["max:20",['max'=>'开户网点长度超出限制']];
    		$rules['bank_no'] = ["max:20",['max'=>'账号长度超出限制']];
    	}
    	if(!$isAdd){
    		$rules['id'] = ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]];
    	}else{
    		$rules['login_name'] = ["require|alphaDash|length:5,12",["require"=>"请输入登陆账户名","alphaDash"=>'登陆账户名必须是英文、数字、下划线和破折号',"length"=>"请输入5至12位符合规范的登陆账户名"]];
    		$rules['password'] = ["require|length:6,16",["require"=>"请输入登陆密码","length"=>"请输入6-16位登陆密码"]];
    	}
    	return $rules;
    }
    
    //获取提现信息规则
    public static function getAccountData(){
    	$rules = [
    		'bank_user' => ["require|max:10",['require'=>'请输入开户人姓名','max'=>'开户人姓名长度超出限制']],
    		'bank_name' => ["require|max:20",['require'=>'请输入开户银行名称','max'=>'开户银行长度超出限制']],
    		'bank_no' => ["max:30",['require'=>'请输入银行卡号','max'=>'银行卡号长度超出限制']],
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //获取事件规则
    public static function getEventRules(){
    	$rules = [
    		'id' => ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
    		'event' => ["require|in:on,off,delete,resetpwd,pass,fail",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']]
    	];
    	return $rules;
    }
    
    
}