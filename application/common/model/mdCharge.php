<?php
namespace app\common\model;

use think\Db;
use site\myDb;
use site\myValidate;
use site\myCache;

class mdCharge{
	
	//获取充值列表
	public static function getChargeList($where){
		$list = Db::name('Charge')->where($where)->order('sort_num','desc')->select();
		return $list;
	}
	
	
    
    //更新套餐
    public static function doneCharge($data){
    	$coin = $send_coin = 0;
    	if($data['type'] == 1){
    		$coin = $data['coin'] ? : 0;
    		if($coin <= 0){
    			res_api('充值书币必须大于0');
    		}
    		$send_coin = $data['send_coin'] ? : 0;
    	}
    	$content = [
    		'coin' => $coin,
    		'send_coin' => $send_coin,
    		'vip_type' => $data['vip_type'] ? : 0
    	];
    	unset($data['coin']);
    	unset($data['send_coin']);
    	unset($data['vip_type']);
        $data['content'] = json_encode($content);
        if(array_key_exists('id', $data)){
            $flag = myDb::saveIdData('Charge', $data);
        }else{
        	Db::startTrans();
        	$flag = false;
        	$charge_id = Db::name('Charge')->insertGetId($data);
        	if($charge_id){
        		$re = Db::name('Charge')->where('id',$charge_id)->setField('sort_num',$charge_id);
        		if($re){
        			$flag = true;
        		}
        	}
        	if($flag){
        		Db::commit();
        	}else{
        		Db::rollback();
        	}
        }
        if($flag){
        	myCache::rmCharge(0);
        	myCache::rmSiteCharge();
        }
        return $flag;
    }
    
    //设置字段
    public static function setField($id,$field,$channel_id=0){
    	Db::startTrans();
    	$flag = false;
    	$re = Db::name('Charge')->where('channel_id',$channel_id)->setField($field,2);
    	if($re !== false){
    		$res = Db::name('Charge')->where('id',$id)->setField($field,1);
    		if($res){
    			$flag = true;
    		}
    	}
    	if($flag){
    		Db::commit();
    	}else{
    		Db::rollback();
    	}
    	return $flag;
    }
    
    //删除套餐
    public static function deleteCharge($id){
    	Db::startTrans();
        $flag = false;
        $re = Db::name('Charge')->where('id','=',$id)->delete();
        if($re !== false){
        	$res = Db::name('Charge')->where('pid',$id)->delete();
        	if($res !== false){
        		$flag = true;
        	}
        }
        if($flag){
        	Db::commit();
        	myCache::rmCharge(0);
        	myCache::rmSiteCharge();
        }else{
        	Db::rollback();
        }
        return $flag;
    }
    
    //获取节点选项
    public static function getOptions(){
        $option = [
            'status' => [
                'name' => 'status',
                'option' => [
                    ['val'=>1,'text'=>'显示','default'=>1],
                    ['val'=>2,'text'=>'隐藏','default'=>0]
                ]
            ],
            'type' => [
                'name' => 'type',
                'option' => [
                    ['val'=>1,'text'=>'书币充值','default'=>1],
                    ['val'=>2,'text'=>'VIP套餐充值','default'=>0]
                ]
            ],
        	'vip_list' => [
        		['id'=>1,'name'=>'日会员'],
        		['id'=>2,'name'=>'月会员'],
        		['id'=>3,'name'=>'季度会员'],
        		['id'=>4,'name'=>'半年度会员'],
        		['id'=>5,'name'=>'年度会员'],
        		['id'=>6,'name'=>'终身会员'],
        	],
        ];
        return $option;
    }
    
    //处理排序
    public static function doSort($data){
    	$flag = false;
    	$table = 'Charge';
    	$cur = Db::name($table)->where('id','=',$data['id'])->field('id,channel_id,sort_num')->find();
    	if($cur){
    		switch ($data['event']){
    			case 'sortUp':
    				$where = [['sort_num','>',$cur['sort_num']],['channel_id','=',$cur['channel_id']]];
    				$other = Db::name($table)->where($where)->field('id,sort_num')->order('sort_num','ASC')->find();
    				break;
    			case 'sortDown':
    				$where = [['sort_num','<',$cur['sort_num']],['channel_id','=',$cur['channel_id']]];
    				$other = Db::name($table)->where($where)->field('id,sort_num')->order('sort_num','DESC')->find();
    				break;
    		}
    		if($other){
    			Db::startTrans();
    			$re = Db::name($table)->where('id','=',$cur['id'])->setField('sort_num',$other['sort_num']);
    			if($re){
    				$res = Db::name($table)->where('id','=',$other['id'])->setField('sort_num',$cur['sort_num']);
    				if($res){
    					$flag = true;
    				}
    			}
    			if($flag){
    				Db::commit();
    			}else{
    				Db::rollback();
    			}
    		}
    	}
    	return $flag;
    }
    
    
    //获取更新规则
    public static function getData($isAdd=1){
    	$rules = [
    		'money' => ["require|float",["require"=>"请输入充值金额",'float'=>'充值金额格式错误']],
    		'type' =>  ["require|in:1,2",["require"=>"请选择充值类型",'in'=>'未指定该充值类型']],
    		'coin' => ["requireIf:type,1|number",["requireIf"=>"请输入充值书币",'number'=>'书币必须为整数类型']],
    		'send_coin' => ["requireIf:type,1|number",["requireIf"=>"请输入充值书币",'number'=>'赠送必须为整数类型']],
    		'vip_type' => ["requireIf:type,2|between:1,6",["requireIf"=>"请选择vip套餐类型",'between'=>'未指定该套餐类型']],
    		'status' =>  ["require|in:1,2",["require"=>"请选择状态",'in'=>'未指定该状态']],
    		'pay_times' => ["require|number",["require"=>"请输入限制充值次数",'number'=>'充值次数必须为数值类型']],
    	];
    	if(!$isAdd){
    		$rules['id'] = ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]];
    	}
    	$data = myValidate::getData($rules);
    	return $data;
    }
    
    //获取事件规则
    public static function getEventData(){
    	$rules = [
    		'id' =>  ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
    		'event' => ["require|in:sethot,setcheck,delete,on,off,sortUp,sortDown",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']]
    	];
    	$data = myValidate::getData($rules);
    	return $data;
    }
}