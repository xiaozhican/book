<?php


namespace app\common\model;


use think\Db;

class mdOnline
{
    /**
     * 统计在线人数
     * @param $username   登录用户
     * @throws \think\Exception
     * @throws \think\db\exception\DataNotFoundException
     * @throws \think\db\exception\ModelNotFoundException
     * @throws \think\exception\DbException
     * @throws \think\exception\PDOException
     */
    public static function countOnline($username)
    {
        $redis = new \Redis();
        $redis->connect('127.0.0.1',6379);
        $date = date('Y-m-d');
        $res = Db::name('count')->where('date_time',$date)->find();
        $redis->pfadd('login.'.$date,[$username]);//用到了redis的HyperLogLog
        $day_login = $redis->pfcount('login.'.$date);//统计日登人数
        //var_dump($res);die;
        if(!$res){
            Db::name('count')->insert([
                'date_time' => $date,
                'day_login' => 1
            ]);
        }else{
            Db::name('count')->where('date_time',$date)->update(['day_login'=>$day_login]);
        }
    }
}