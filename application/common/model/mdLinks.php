<?php
namespace app\common\model;

use site\myDb;

class mdLinks{
    
    //获取友情链接列表
    public static function getLinksList($where,$pages){
        $res = myDb::getPageList('Links',$where, '*', $pages,['sort'=>'asc']);
        if($res['data']){
            foreach ($res['data'] as &$v){
                $v['do_url'] = my_url('doLinks',['id'=>$v['id']]);
            }
        }
        return $res;
    }
    
    
    //处理更新友情链接
    public static function doneLinks($data){
        if(array_key_exists('id', $data)){
            $re = myDb::saveIdData('Links',$data);
        }else{
            $data['create_time'] = time();
            $re = myDb::add('Links', $data);
        }
        if($re){
            \site\myCache::rmLinksList();
            res_api();
        }else{
            res_api('保存失败，请重试');
        }
    }
    
    //获取更新友情链接选项
    public static function getOptions(){
        $option = [
            'backUrl' => url('links')
        ];
        return $option;
    }
    
    //获取更新友情链接规则
    public static function getRules($isAdd=1){
    	$rules = [
            'title' =>  ["require|max:20",["require"=>"请输入友情连接标题",'max'=>'友情连接标题最多支持20个字符']],
            'desc' =>  ["max:50",["max"=>"描述不超过50个字符"]],
    		'url' => ["require|url",["require"=>"请输入友情连接地址","url"=>'请输入正确的url地址']],
    	];
    	if(!$isAdd){
            $rules['sort'] = ["require|number|gt:0",["require"=>"排序序号必填",'number'=>'排序序号错误',"gt"=>"排序序号错误"]];
            $rules['id'] = ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]];
    	}
    	return $rules;
    }
    
    //获取用户事件规则
    public static function getEventRules(){
    	$rules = [
    		'id' => ["require|number|gt:0",["require"=>"主键参数错误",'number'=>'主键参数错误',"gt"=>"主键参数错误"]],
    		'event' => ["require|in:on,off,delete,resetpwd",["require"=>'请选择按钮绑定事件',"in"=>'按钮绑定事件错误']]
    	];
    	return $rules;
    }
}