<?php
namespace app\vip\controller;


use site\myDb;
use site\myHttp;
use site\myCache;
use site\mySearch;
use site\myValidate;
use think\Controller;
use app\common\model\mdVip;
use app\common\model\mdLogin;
use app\common\model\mdPlatform;
use app\common\model\mdWithdraw;

class Index extends Controller{
	
	private $loginId;
	
	//初始化
	public function __construct(){
		parent::__construct();
		$loginId = session('VIP_LOGIN_ID');
		if(!$loginId){
			if($this->request->isAjax()){
				res_api('登录已失效');
			}else{
				exit("<script language=\"javascript\">window.open('".url('Login/index')."','_top');</script>");
			}
		}
		$this->loginId = $loginId;
	}
	
	//首页
	public function index(){
		$cur = myDb::getById('VipUser',$this->loginId);
		return $this->fetch('index',['cur'=>$cur]);
	}
	
	//渠道列表
	public function channel(){
		if($this->request->isAjax()){
			$ids = myCache::getVipChannelIds($this->loginId);
			if($ids){
				$config = [
					'like'=>'keyword:a.name',
					'default' => [['a.id','in',$ids]]
				];
				$pages = myHttp::getPageParams();
				$where = mySearch::getWhere($config);
				$res = mdVip::getChannelList($where,$pages);
				if($res['data']){
					$cur_date = date('Y-m-d');
					$date = date('Y-m-d',strtotime('yesterday'));
					foreach ($res['data'] as &$v){
						$v['sub_yesterday'] = $v['sub_yesterday'] ? : 0;
						$v['charge_yesterday'] = $v['charge_yesterday'] ? : 0;
						$v['sub_today'] = $v['sub_today'] ? : 0;
						$v['charge_today'] = $v['charge_today'] ? : 0;
						$v['money'] = myCache::getChannelAmount($v['id']);
					}
					$add = ['id'=>0,'money'=>0,'sub_today'=>0,'sub_yesterday'=>0,'charge_today'=>0,'charge_yesterday'=>0,'total_charge'=>0];
					foreach ($res['data'] as $tv){
						$add['money'] += $tv['money'];
						$add['sub_today'] += $tv['sub_today'];
						$add['sub_yesterday'] += $tv['sub_yesterday'];
						$add['charge_today'] += $tv['charge_today'];
						$add['charge_yesterday'] += $tv['charge_yesterday'];
						$add['total_charge'] += $tv['total_charge'];
					}
					$res['data'][] = $add;
				}
				res_table($res['data'],$res['count']);
			}
			res_table([]);
		}else{
			
			return $this->fetch();
		}
	}
	
	//绑定渠道
	public function doBind(){
		if($this->request->isAjax()){
			$rules = [
				'login_name' => ["require|alphaDash|length:5,12",["require"=>"请输入登陆账户名","alphaDash"=>'登陆账户名必须是英文、数字、下划线和破折号',"length"=>"请输入5至12位符合规范的登陆账户名"]],
				'password' => ["require|length:6,16",["require"=>"请输入登陆密码","length"=>"请输入6-16位登陆密码"]],
			];
			$post = myValidate::getData($rules);
			$channel = myDb::getCur('Channel', [['login_name','=',$post['login_name']]],'id,is_wx,password,status,relation_code');
			if(!$channel){
				res_api('账号或密码输入错误');
			}
			$pwd = createPwd($post['password']);
			if($pwd != $channel['password']){
				res_api('账号或密码输入错误');
			}
			if($channel['status'] != 1){
				res_api('该渠道尚未启用');
			}
			if($channel['is_wx'] != 1){
				res_api('不支持绑定代理账号');
			}
			$ids = myCache::getVipChannelIds($this->loginId);
			if($ids && in_array($channel['id'], $ids)){
				res_api('您已绑定该渠道');
			}
			$data = [
				'channel_id' => $channel['id'],
				'vip_id' => $this->loginId
			];
			$re = myDb::add('VipChannel', $data);
			if($re){
				myCache::rmVipChannelIds($this->loginId);
				myCache::saveVipIdByChannelId($channel['id'], $this->loginId);
				res_api();
			}else{
				res_api('绑定失败，请重试');
			}
		}else{
			
			return $this->fetch('common@public/doBind');
		}
	}
	
	//收入明细
	public function income(){
		if($this->request->isAjax()){
			$ids = myCache::getVipChannelIds($this->loginId);
			if($ids){
				$config = [
					'eq' => 'channel_id',
					'between' => 'between_time:create_date%date',
					'default' => [['channel_id','in',$ids]],
					'rules' => ['channel_id'=>'in:'.implode(',', $ids)]
				];
				$where = mySearch::getWhere($config);
				$pages = myHttp::getPageParams();
				$res = mdVip::getIncomeList($where,$pages);
				if($res['data']){
					foreach ($res['data'] as &$v){
						$channel = myCache::getChannel($v['channel_id']);
						$v['name'] = $channel['name'];
						$v['ratio'] = $channel['ratio'].'%';
						$channel = null;
					}
				}
				res_table($res['data'],$res['count']);
			}
			res_table([]);
		}else{
			$ids = myCache::getVipChannelIds($this->loginId);
			$money_field = 'charge_money:0,income_money:0,cost_money:0,charge_num:0,withdraw_total:0,withdraw_pay:0,withdraw_wait:0,amount:0';
			$money = myDb::buildArr($money_field);
			$channels = [];
			if($ids){
				foreach ($ids as $v){
					$channel = myCache::getChannel($v);
					if($channel){
						$channels[] = ['id'=>$channel['id'],'name'=>$channel['name']];
					}
					$channel = null;
					$money['amount'] += myCache::getChannelAmount($v);
					$one = myDb::getCur('ChannelMoney',[['channel_id','=',$v]]);
					if($one){
						foreach ($money as $key=>&$val){
							if($key !== 'amount'){
								$val += $one[$key];
							}
						}
					}
				}
			}
			$get = myHttp::getData('channel_id,between_time','get');
			return $this->fetch('income',['option'=>$channels,'get'=>$get,'money'=>$money]);
		}
	}
	
	//提现明细
	public function withdraw(){
		if($this->request->isAjax()){
			$config = [
				'between' => 'between_time:cur_date%date',
				'default' => [['status','=',0],['vip_id','=',$this->loginId]]
			];
			$where = mySearch::getWhere($config);
			$list = mdWithdraw::getWaitList($where);
			res_table($list);
		}else{
			$ids = myCache::getVipChannelIds($this->loginId);
			$channels = [];
			if($ids){
				foreach ($ids as $v){
					$channel = myCache::getChannel($v);
					if($channel){
						$channels[] = ['id'=>$channel['id'],'name'=>$channel['name']];
					}
					$channel = null;
				}
			}
			return $this->fetch('withdraw',['option'=>$channels]);
		}
	}
	
	//导出数据
	public function exportData(){
		$where = [['status','=',0],['vip_id','=',$this->loginId]];
		$data = mdWithdraw::getExportData($where);
		res_table($data);
	}
	
	//已支付
	public function paylist(){
		if($this->request->isAjax()){
			$config = [
				'between' => 'between_time:cur_date%date',
				'default' => [['vip_id','=',$this->loginId],['status','=',1]]
			];
			$where = mySearch::getWhere($config);
			$pages = myHttp::getPageParams();
			$res = mdWithdraw::getPassList($where, $pages);
			res_table($res['data'],$res['count']);
		}else{
			
			return $this->fetch();
		}
	}
	
	//提现明细
	public function detail(){
		$rules = ['sign' => ['require|alphaNum|length:32',['require'=>'明细参数异常','alphaNum'=>'明细参数格式不规范','length'=>'明细参数格式不规范']]];
		$sign = myValidate::getData($rules,'get');
		$field = 'id,channel_id,money,charge_money,charge_num,create_time';
		$list = myDb::getList('Withdraw', [['sign','=',$sign]],'id,channel_id,money,charge_money,charge_num,create_time');
		if($list){
			foreach ($list as &$v){
				$v['name'] = '未知';
				$channel = myCache::getChannel($v['channel_id']);
				if($channel){
					$v['name'] = $channel['name'];
				}
				$v['create_time'] = date('Y-m-d H:i:s',$v['create_time']);
			}
		}
		return $this->fetch('common@withdraw/detail',['list'=>$list]);
	}
	
	//设置账号
	public function setAccount(){
		if($this->request->isAjax()){
			$data = mdPlatform::getAccountData();
			$re = myDb::save('VipUser',[['id','=',$this->loginId]], $data);
			if($re){
				res_api();
			}else{
				res_api('设置失败');
			}
		}else{
			$cur = myDb::getById('VipUser', $this->loginId);
			return $this->fetch('common@public/setAccount',['cur'=>$cur]);
		}
	}
	
	//修改密码
	public function password(){
		if($this->request->isAjax()){
			$re = mdLogin::changePwd($this->loginId,3);
			if($re){
				res_api();
			}else{
				res_api('保存失败');
			}
		}else{
			
			return $this->fetch('common@index/password');
		}
	}
	
	public function logOut(){
		session('VIP_LOGIN_ID',null);
		$this->redirect('Login/index');
	}
	
}