<?php
namespace app\lib\exception;

use Exception;
use think\exception\HttpException;

class ExceptionHandler extends \think\exception\Handle
{
    public function render(Exception $e)
    {
        if($e instanceof HttpException){
            //redirect('/');
            redirect('www.baidu.com');
        }
        //dump($e);
    }
}