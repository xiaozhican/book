<?php
namespace app\index\controller;

use think\Controller;

class Doc extends Controller{
	
	//隐私政策
	public function privacyPolicy(){
		$title = '隐私声明';
		$is_back = $this->request->get('is_back');
		$is_back = in_array($is_back, ['yes','no']) ? $is_back : 'no';
		return $this->fetch('index',['is_back'=>$is_back,'title'=>$title,'key'=>'privacypolicy']);
	}
	
	//用户协议
	public function agreement(){
		$title = '用户协议';
		$is_back = $this->request->get('is_back');
		$is_back = in_array($is_back, ['yes','no']) ? $is_back : 'no';
		return $this->fetch('index',['is_back'=>$is_back,'title'=>$title,'key'=>'agreement']);
	}
	
	//版权声明
	public function statement(){
		$title = '版权声明';
		return $this->fetch('index',['is_back'=>'no','title'=>$title,'key'=>'statement']);
	}
}

