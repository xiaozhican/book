<?php
namespace app\index\controller;

use app\common\model\mdConfig;
use think\Db;
use site\myDb;
use site\myMsg;
use site\myHttp;
use site\myCache;
use site\myValidate;
use app\common\model\mdOrder;

class Com extends Common{
	
	//书籍收藏
	public function doCollect(){
		global $loginId;
		if(!$loginId){
			res_api('您尚未登陆,是否立即登录？',3);
		}
		$rules = [
			'book_id'=>['require|number|gt:0',['require'=>'书籍信息异常','number'=>'书籍信息异常','gt'=>'书籍信息异常']],
			'event'=>['require|in:join,remove',['require'=>'操作参数异常','in'=>'操作参数异常']]
		];
		$post = myValidate::getData($rules);
		$ids = myCache::getCollectBookIds($loginId);
		switch ($post['event']){
			case 'join':
				if($ids && in_array($post['book_id'], $ids)){
					res_api();
				}
				$data = [
					'uid' => $loginId,
					'book_id' => $post['book_id'],
					'create_time' => time()
				];
				$re = myDb::add('MemberCollect', $data);
				break;
			case 'remove':
				if($ids && !in_array($post['book_id'], $ids)){
					res_api();
				}
				$re = myDb::delByWhere('MemberCollect', [['uid','=',$loginId],['book_id','=',$post['book_id']]]);
				break;
		}
		if($re){
			myCache::rmCollectBookIds($loginId);
			res_api();
		}else{
			res_api('收藏失败');
		}
	}
	
	//打赏 
	public function doReward(){
		global $loginId;
		if(!$loginId){
			res_api('您尚未登陆,是否立即登录？',3);
		}
		$member = myCache::getMember($loginId);
		if(!$member){
			session('INDEX_LOGIN_ID',null);
			res_api('您尚未登陆,是否立即登录？',3);
		}
		$rules = [
			'pid'=>['require|number|gt:0',['require'=>'书籍信息异常','number'=>'书籍信息异常','gt'=>'书籍信息异常']],
			'money'=>['require|number|gt:0',['require'=>'礼物信息异常','number'=>'礼物信息异常','gt'=>'礼物信息异常']],
			'count'=>['require|number|gt:0',['require'=>'礼物数量异常','number'=>'礼物数量异常','gt'=>'礼物数量异常']],
			'gift_key'=>['require|alphaNum',['require'=>'礼物信息异常','alphaNum'=>'礼物信息异常']]
		];
		$data = myValidate::getData($rules);
		$gifts = getMyConfig('reward');
		if($gifts && isset($gifts[$data['gift_key']])){
			$gift = $gifts[$data['gift_key']];
			unset($data['gift_key']);
			$money = $gift['money'] * $data['count'];
			if($money != $data['money']){
				res_api('礼物信息异常');
			}
			$userMoney = myCache::getMemberMoney($loginId);
			if($userMoney < $money){
				res_api('您的金币余额不足');
			}
			$data['channel_id'] = $member['channel_id'];
			$data['agent_id'] = $member['agent_id'];
			$data['uid'] = $loginId;
			$data['type'] = 1;
			$data['gift'] = json_encode($gift);
			$data['create_time'] = time();
			$book = myCache::getBook($data['pid']);
			$book_name = $book ? $book['name'] : '未知';
			$re = mdOrder::doReward($data,$book_name);
			if($re){
				myCache::doMemberMoney($loginId, $money,'dec');
				myCache::rmRewardCache($data['pid']);
				res_api();
			}else{
				res_api('打赏失败，请重试');
			}
		}else{
			res_api('礼物已下架');
		}
	}
	
	//删除阅读历史记录
	public function delReadHistory(){
		global $loginId;
		$post = myHttp::getData('book_ids');
		$book_ids = is_array($post['book_ids']) ? $post['book_ids'] : '';
		if(!$book_ids){
			res_api('请选择要移除的书籍');
		}
		$where = [['uid','=',$loginId],['book_id','in',$book_ids],['status','=',1]];
		$res = myDb::setField('ReadHistory', $where, 'status', 2);
		if($res){
			myCache::rmReadCache($loginId);
			res_api();
		}else{
			res_api('删除失败');
		}
	}
	
	//删除收藏
	public function delCollect(){
		global $loginId;
		$post = myHttp::getData('book_ids');
		$book_ids = is_array($post['book_ids']) ? $post['book_ids'] : '';
		if(!$book_ids){
			res_api('请选择要移除的书籍');
		}
		$where = [['uid','=',$loginId],['book_id','in',$book_ids]];
		$res = myDb::delByWhere('MemberCollect', $where);
		if($res){
			myCache::rmCollectBookIds($loginId);
			res_api();
		}else{
			res_api('删除失败');
		}
	}
	
	//获取热门书籍
	public function getHotBook(){
		$page = myHttp::postId('分页','page');
		$pages = ['page'=>$page,'limit'=>3];
		$list = myDb::getOnlyPageList('Book', [['status','=',1]],'id,name,cover,hot_num',$pages,['hot_num'=>'desc']);
		if($list){
			foreach ($list as &$v){
				$v['hot_num'] = formatNumber($v['hot_num']);
			}
		}else{
			$list = '';
		}
		res_table($list);
	}
	
	
	//投诉
	public function complaint(){
		global $loginId;
		$rules = [
			'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
			'number'=>['require|number|gt:0',['require'=>'章节参数错误','number'=>'章节参数错误','gt'=>'章节参数错误']],
		];
		if($this->request->isAjax()){
			if(!$loginId){
				res_api('您尚未登陆',3);
			}
			$user = myCache::getMember($loginId);
			if(!$user){
				session('INDEX_LOGIN_ID',null);
				res_api('登陆信息异常');
			}
			$rules['type'] = ['require|between:1,7',['require'=>'请选择反馈类型','between'=>'未指定该反馈类型']];
			$rules['content'] = ['max:100',['max'=>'反馈内容最大支持100个字符']];
			$rules['phone'] = ['mobile',['mobile'=>'手机号格式不规范']];
			$data = myValidate::getData($rules);
			$book = myCache::getBook($data['book_id']);
			if(!$book){
				res_api('书籍信息异常');
			}
			$data['channel_id'] = $user['channel_id'];
			$data['agent_id'] = $user['agent_id'];
			$data['uid'] = $loginId;
			$data['book_name'] = $book['name'];
			$data['create_time'] = time();
			$re = Db::name('Complaint')->insert($data);
			if($re){
				res_api();
			}else{
				res_api('投诉失败，请重试');
			}
		}else{
			if(!$loginId){
				$this->redirect('Login/index');
			}
			$get = myValidate::getData($rules,'get');
			$variable = [
				'book_id' => $get['book_id'],
				'number' => $get['number'],
			];
			return $this->fetch('complaint',$variable);
		}
	}
	
	//发送短信验证码
    public function sendMessageCode(){
        $rules = ['phone' =>  ["require|mobile",['require'=>'请输入手机号','mobile'=>'请输入正确格式的手机号']]];
        $phone = myValidate::getData($rules);

        $repeat_uid = myDb::getValue('Member', [['phone','=',$phone]], 'id');
        if($repeat_uid){
            res_api('该手机号已被注册');
        }

        $website = mdConfig::getConfig('website');
        if (!isset($website['sms_type'])){
            $website['sms_type'] = 1;
        }

        if ($website['sms_type'] == 1){
            $re = myMsg::saiyouSend($phone);
        }elseif($website['sms_type'] == 2){
            $re = myMsg::dxtonSend($phone);
        }else{
            $re = myMsg::saiyouSend($phone);
        }
        if($re){
            res_api();
        }else{
            res_api('发送失败');
        }
    }
}