<?php
namespace app\index\controller;

use site\myHttp;
use site\myCache;
use site\myValidate;
use think\Controller;
use app\common\model\mdMember;
use think\Db;


class Common extends Controller{
    
    //站点名称
    protected $site_title;
    
    //初始化程序
    public function __construct(){
        parent::__construct();
        $server_url = $_SERVER['HTTP_HOST'];
        if(!$server_url){
        	res_error('非法访问');
        }
        $urlData = myCache::getUrlInfo($server_url);
        //app上注销用户------------------------20201103
        if(session('INDEX_LOGIN_ID')){
            $m = Db::name('Member')->where('id',session('INDEX_LOGIN_ID'))->field('phone,old_phone')->find();
            if($m['old_phone']!=null && $m['phone'] != $m['old_phone']){
                session('INDEX_LOGIN_ID',null);//清除登录缓存
            }
        }
        //---------------------------------
        //微信环境下如果没有用户信息
        if(checkIsWeixin()){
        	$rules = ['location_key'=>['alphaDash|length:32',['alphaDash'=>'非法访问','length'=>'非法访问']]];
        	$locationKey = myValidate::getData($rules,'get');
        	if(!session('?INDEX_LOGIN_ID')){
        		if($server_url == $urlData['url']){
        			self::autoLogin($urlData,$locationKey);
        		}else{
        			if($locationKey){
        				$cache = self::getLocationCache($locationKey);
        				if(!$cache['uid']){
        					res_error('请求超时');
        				}
        				session('INDEX_LOGIN_ID',$cache['uid']);
        			}else{
        				$locationKey = self::createLocationkey($server_url);
        				$url = self::createBackUrl($urlData['url'], $locationKey);
        				$this->redirect($url);
        			}
        		}
        	}else{
        		if($server_url === $urlData['url']){
        			if($urlData['is_location'] == 1){
        				if($locationKey){
        					$cache = self::getLocationCache($locationKey);
        					if($cache){
        						my_print($cache);
        						$uid = session('INDEX_LOGIN_ID');
        						session('INDEX_LOGIN_ID',null);
        						self::saveLocationUid($locationKey, $uid);
        						$this->redirect($cache['back_url']);
        					}
        				}else{
        					$uid = session('INDEX_LOGIN_ID');
        					session('INDEX_LOGIN_ID',null);
        					$cache = self::createLocationkey($urlData['location_url'],$uid,'data');
        					$this->redirect($cache['back_url']);
        				}
        			}
        		}
        	}
        }
        if(!$this->request->isAjax()){
        	$kc_code = myValidate::getData(['kc_code'=>['alphaDash|length:6',['alphaDash'=>'非法访问','length'=>'非法访问']]],'get');
        	if($kc_code){
        		session('kc_code',$kc_code);
        	}
        }
        global $loginId;
        $loginId = session('INDEX_LOGIN_ID');
        $loginId = $loginId ? : 0;
        if(!$this->request->isAjax()){
        	$this->assign('page_title',$urlData['name']);
        }
    }
    
    //获取跳转缓存key
    private function createLocationkey($host,$uid=0,$back='key'){
    	$sid = session_id();
    	if(!$sid){
    		session_start();
    		$sid = session_id();
    	}
    	if(!$sid){
    		res_error('非法访问');
    	}
    	$str  = $sid.'#'.mt_rand(10000,999999);
    	$key = md5($str);
    	$backUrl = self::createBackUrl($host, $key);
    	$data = [
    		'uid' => $uid,
    		'back_url' => $backUrl
    	];
    	cache($key,$data,30);
    	if($back === 'key'){
    		return $key;
    	}else{
    		return $data;
    	}
    }
    
    //保存用户跳转缓存
    private function saveLocationUid($key,$value){
    	$data = ['uid' => $value];
    	cache($key,$data,30);
    }
    
    //添加跳转参数
    private function createBackUrl($host,$vaue){
    	$url = 'http://'.$host;
    	$request_uri = $_SERVER['REQUEST_URI'];
    	if(strpos($request_uri,'?') !== false){
    		$request_uri .= '&location_key='.$vaue;
    	}else{
    		$request_uri .= '?location_key='.$vaue;
    	}
    	$url .= $request_uri;
    	return $url;
    }
    
    //获取跳转缓存
    private function getLocationCache($key){
    	if(cache('?'.$key)){
    		return cache($key);
    	}
    	return '';
    }
    
    // 尝试自动登录
    private function autoLogin($urlData,$locationKey=''){
        $wx_param = myHttp::getData('code,state','get');
        $config = myCache::getWeixinInfo();
        if($wx_param['code'] && $wx_param['state'] === 'getnow'){
        	$url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$config['appid']."&secret=".$config['appsecret']."&code=".$wx_param['code']."&grant_type=authorization_code";
            $res = myHttp::doGet($url);
            if($res){
                if(isset($res['openid']) && $res['openid']){
                	$cur_member_id = myCache::getUidByOpenId($res['openid']);
                	if(!$cur_member_id){
                		$cur_member_id = mdMember::doWxRegister($res['openid']);
                        if(!$cur_member_id){
                        	res_api('用户注册失败');
                        }
                    }
                    if($locationKey){
                    	$cache = self::getLocationCache($locationKey);
                    	self::saveLocationUid($locationKey, $cur_member_id);
                    	$this->redirect($cache['back_url']);
                    }else{
                    	if($urlData['is_location'] == 1){
                    		$cache = self::createLocationkey($urlData['location_url'],$cur_member_id,'data');
                    		$this->redirect($cache['back_url']);
                    	}else{
                    		session('INDEX_LOGIN_ID',$cur_member_id);
                    	}
                    }
                }else{
                    $wxerror = isset($res['errmsg']) ? $res['errmsg'] : '';
                    $error = '授权失败';
                    if($wxerror){
                        $error .= ':'.$wxerror;
                    }
                    res_api($error);
                }
            }else{
                res_api('获取openid失败');
            }
        }else{
            $redirect_url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $redirect_url = urlencode($redirect_url);
            $url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$config['appid']."&redirect_uri=".$redirect_url."&response_type=code&scope=snsapi_base&state=getnow#wechat_redirect";
            $this->redirect($url);
        }
    }
    
    //检查该板块是否有权限
    protected function checkBlock($type){
    	$web_block = getMyConfig('web_block');
    	$view = false;
    	$title = '';
    	if($web_block){
    		foreach ($web_block as $v){
    			if($v['type'] == $type && $v['is_on'] == 1){
    				$view = true;
    				$title = $v['name'];
    			}
    		}
    	}
    	if(!$view){
    		$error = $title.'板块已关闭';
    		res_error($error);
    	}
    }
    
    //登陆跳转
    protected function checkLogin(){
    	global $loginId;
    	if($this->request->isAjax()){
    		if(!$loginId){
    			res_api('您尚未登陆',3,['url'=>url('Login/index')]);
    		}
    	}else{
    		if(!$loginId){
    			$this->redirect('Login/index');
    			exit;
    		}
    		$member = myCache::getMember($loginId);
    		if(!$member){
    			session('INDEX_LOGIN_ID',null);
    			if(checkIsWeixin()){
    				$this->redirect('User/index');
    			}else{
    				$this->redirect('Login/index');
    			}
    			exit;
    		}
    	}
    }
    
    public function _empty(){
        
        res_error('页面不存在');
    }
    
}