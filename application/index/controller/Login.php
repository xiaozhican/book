<?php
namespace app\index\controller;

use app\common\model\mdOnline;
use site\myDb;
use site\myMsg;
use site\myValidate;
use think\Controller;
use app\common\model\mdMember;

class Login extends Common{
    
    //登录
    public function index(){
    	if($this->request->isAjax()){
    		$rules = [
    			'phone' => ["require|mobile",['require'=>'请输入手机号','mobile'=>'请输入正确格式的手机号']],
    			'password' => ['require|length:6,16',['require'=>'请输入密码','length'=>'密码不少于6-16位']],
    		];
    		$post = myValidate::getData($rules);
    		$user = myDb::getCur('Member', [['phone','=',$post['phone']]], 'id,password,status');
            $serv = 'h5';
    		if(!$user){
                \site\mdLogin::saveLog($post['phone'],$serv,'账号不存在');
    			res_api('账号不存在');
    		}
    		$pwd = createPwd($post['password']);
    		if($pwd != $user['password']){
                \site\mdLogin::saveLog($post['phone'],$serv,'您的密码输入有误');
    			res_api('您的密码输入有误');
    		}
    		if($user['status'] != 1){
                \site\mdLogin::saveLog($post['phone'],$serv,'您已被禁止登录');
    			res_api('您已被禁止登录');
    		}
    		session('INDEX_LOGIN_ID',$user['id']);
            \site\mdLogin::saveLog($post['phone'],$serv);
            mdOnline::countOnline($post['phone']);//统计日登人数
    		res_api(['url'=>url('user/index')]);
    	}else{
    		if(checkIsWeixin()){
    			$this->redirect('User/index');
    		}else{
    			if($this->request->isMobile()){
    				return $this->fetch('phoneLogin',['page_title'=>'登录','title'=>'用户登录','btn'=>'立即登录']);
    			}else{
    				$this->redirect('home/index/index');
    			}
    		}
    	}
    }
    
    public function register(){
    	if($this->request->isAjax()){
    		$rules = [
    			'phone' => ["require|mobile",['require'=>'请输入手机号','mobile'=>'请输入正确格式的手机号']],
    			'code' => ["require|number|length:4",['require'=>'请输入短信验证码','number'=>'验证码为4位纯数字','length'=>'验证码为4位纯数字']],
    			'password' => ['require|length:6,16',['require'=>'请输入密码','length'=>'密码不少于6-16位']],
    		];
    		$post = myValidate::getData($rules);
    		$flag = myMsg::check($post['phone'], $post['code']);
    		if(is_string($flag)){
    			res_api($flag);
    		}
    		$user = myDb::getCur('Member', [['phone','=',$post['phone']]], 'id,status');
    		if($user){
    			res_api('该手机号已注册');
    		}
    		$res = mdMember::doPhoneRegister($post['phone'],$post['password']);
    		if($res){
    			session('INDEX_LOGIN_ID',$res['id']);
    			res_api(['url'=>url('user/index')]);
    		}else{
    			res_api('注册失败，请重试');
    		}
    		
    	}else{
    		
    		return $this->fetch('phoneReg',['page_title'=>'注册','title'=>'用户注册','btn'=>'立即注册']);
    	}
    }

    //退出
    public function logout(){
        session('INDEX_LOGIN_ID',null);
        $this->redirect('/');
    }
    
    
    //手机号登录
    private function phoneLogin(){
    	$rules = [
    		'phone' => ["require|mobile",['require'=>'请输入手机号','mobile'=>'请输入正确格式的手机号']],
    		'code' => ["require|number|length:4",['require'=>'请输入短信验证码','number'=>'验证码为4位纯数字','length'=>'验证码为4位纯数字']],
    	];
    	$post = myValidate::getData($rules);
    	$flag = myMsg::check($post['phone'], $post['code']);
    	if(is_string($flag)){
    		res_api($flag);
    	}
    	$user = myDb::getCur('Member', [['phone','=',$post['phone']]], 'id,status');
    	if($user){
    		if($user['status'] != 1){
    			res_api('您已被禁止登录');
    		}
    		$uid = $user['id'];
    	}else{
    		$uid = 0;
    		$res = mdMember::doPhoneRegister($post['phone']);
    		if($res){
    			$uid = $res['id'];
    		}
    	}
    	if($uid){
    		session('INDEX_LOGIN_ID',$user['id']);
    		res_api(['type'=>2,'url'=>url('user/index')]);
    	}else{
    		res_api('登录失败');
    	}
    }
}