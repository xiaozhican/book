<?php
namespace app\index\controller;

use weixin\wx;
use site\myDb;
use site\myHttp;
use site\myCache;
use site\myValidate;
use app\common\model\mdBook;

class Book extends Common{
	
	//书籍分类
	public function category(){
		$rules = [
			'type' => ['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'gender_type' => ['in:1,2',['in'=>'参数错误']]
		];
		if($this->request->isAjax()){
			$rules['category'] = ['max:20',['max'=>'参数错误']];
			$rules['orderby'] = ['require|between:1,3',['require'=>'参数错误','between'=>'参数错误']];
			$rules['page'] = ['require|number|gt:0',['require'=>'参数错误','number'=>'参数错误','gt'=>'参数错误']];
			$data = myValidate::getData($rules);
			$list = mdBook::getCategoryList($data);
			$list = $list ? : '';
			res_table($list);
		}else{
			if(!request()->isMobile()){
				$this->redirect('/home/book/category');
			}
			$get = myValidate::getData($rules,'get');
			$key = $get['type'] == 1 ? 'novel_category' : 'cartoon_category';
			$variable = ['option' => mdBook::getCategoryOption($key,$get)];
			return $this->fetch('category',$variable);
		}
	}
	
	//发布区域更多书籍
	public function more(){
		$rules = [
			'type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'gender_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'area_id' => ['require|between:1,4',['require'=>'参数错误','between'=>'参数错误']]
		];
		$get = myValidate::getData($rules,'get');
		if($this->request->isAjax()){
			$page = myHttp::postId('分页','page');
			$res = mdBook::getAreaBookInfo($get['type'], $get['gender_type'], $get['area_id'], $page, 10);
			$list = $res['list'] ? : '';
			res_table($list);
		}else{
			if(!request()->isMobile()){
				$uri = str_replace('index','home', $_SERVER['REQUEST_URI']);
				$this->redirect($uri);
			}
			$name = '';
			switch ($get['area_id']){
				case 1:$name='精品推荐';break;
				case 2:$name='本周热搜';break;
				case 3:$name='热门推荐';break;
				case 4:$name='新书推荐';break;
			}
			return $this->fetch('more',['book_title' => $name]);
		}
	}
	
	//精选更多书籍
	public function genderMore(){
		$rules = [
			'type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'gender_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
		];
		$get = myValidate::getData($rules,'get');
		if($this->request->isAjax()){
			$page = myHttp::postId('分页','page');
			$list = [];
			$cache = myCache::getHotIds($get['type'], $get['gender_type']);
			if($cache){
				$page -= 1;
				$arr = array_chunk($cache, 10);
				if(isset($arr[$page])){
					$list = mdBook::getListBookInfo($arr[$page]);
				}
			}
			$list = $list ? : '';
			res_table($list);
		}else{
			if(!request()->isMobile()){
				$uri = '/home/book/more.html?type='.$get['type'].'&gender_type='.$get['gender_type'].'&area_id=3';
				$this->redirect($uri);
			}
			$title = $get['gender_type'] == 1 ? '男生精选' : '女生精选';
			$variable = ['book_title' => $title];
			return $this->fetch('more',$variable);
		}
	}
	
	//出版书籍
	public function publish(){
		$rules = [
			'type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'gender_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']]
		];
		$get = myValidate::getData($rules,'get');
		if($this->request->isAjax()){
			$page = myHttp::postId('分页','page');
			$list = [];
			$cache = myCache::getBookPublishIds($get['type'], $get['gender_type']);
			if($cache){
				$page -= 1;
				$arr = array_chunk($cache,10);
				if(isset($arr[$page])){
					$list = mdBook::getListBookInfo($arr[$page]);
				}
			}
			$list = $list ? : '';
			res_table($list);
		}else{
			$variable = ['book_title' => '出版'];
			return $this->fetch('more',$variable);
		}
	}
	
	//限免书籍
	public function free(){
		$rules = [
			'type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'gender_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']]
		];
		$get = myValidate::getData($rules,'get');
		if($this->request->isAjax()){
			$page = myHttp::postId('分页','page');
			$list = mdBook::getFreeBookData($get['type'], $get['gender_type'],$page);
			$list = $list ? : 0;
			res_table($list);
		}else{
			
			return $this->fetch('free');
		}
	}

    //完结
    public function overtype2(){
        $rules = [
            'type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
            'gender_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
            'over_type'=>['require|in:2',['require'=>'非法访问','in'=>'非法访问']]
        ];
        $get = myValidate::getData($rules,'get');
        if($this->request->isAjax()){
            $page = myHttp::postId('分页','page');
            $list = [];
            $cache = myCache::getBookOverType2Ids($get['type'], $get['gender_type'],$get['over_type']);
            if($cache){
                $page -= 1;
                $arr = array_chunk($cache,10);
                if(isset($arr[$page])){
                    $list = mdBook::getListBookInfo($arr[$page]);
                }
            }
            $list = $list ? : '';
            res_table($list);
        }else{
            $variable = ['book_title' => '完结'];
            return $this->fetch('more',$variable);
        }
    }

    //连载
    public function overtype1(){
        $rules = [
            'type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
            'gender_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
            'over_type'=>['require|in:1',['require'=>'非法访问','in'=>'非法访问']]
        ];
        $get = myValidate::getData($rules,'get');
        if($this->request->isAjax()){
            $page = myHttp::postId('分页','page');
            $list = [];
            $cache = myCache::getBookOverType1Ids($get['type'], $get['gender_type'],$get['over_type']);
            if($cache){
                $page -= 1;
                $arr = array_chunk($cache,10);
                if(isset($arr[$page])){
                    $list = mdBook::getListBookInfo($arr[$page]);
                }
            }
            $list = $list ? : '';
            res_table($list);
        }else{
            $variable = ['book_title' => '连载'];
            return $this->fetch('more',$variable);
        }
    }
    
    //书籍详情
    public function info(){
    	global $loginId;
    	$rules = [
    		'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
    		'invite_code' => ['alphaDash|length:6',['alphaDash'=>'非法访问','length'=>'非法访问']]
    	];
    	$get = myValidate::getData($rules,'get');
    	if($get['invite_code']){
    		if(!checkIsWeixin() && !$loginId){
    			session('invite_code',$get['invite_code']);
    		}
    	}
        $book = myCache::getBook($get['book_id']);
        if(empty($book)){
            res_error('书籍不存在');
        }
        if(!in_array($book['status'], [1,2])){
        	res_error('书籍已下架');
        }
        $read_number = 1;
        if($loginId){
        	$readCache = myCache::getReadCache($loginId);
        	$rkey = 'book_'.$book['id'];
        	if($readCache && isset($readCache[$rkey])){
        		$read_number = $readCache[$rkey]['last_number'];
        	}
        }
        mdBook::addHot($book['id']);
        $book['word_number'] = formatNumber($book['word_number']);
        $book['hot_num'] = myCache::getBookHotNum($book['id']);
        $book['chapter_num'] = myCache::getBookChapterNum($book['id']);
        $book['hot_num'] = formatNumber($book['hot_num']);
        $chapter_list = myCache::getBookChapterList($book['id']);
        $chapter = [];
        if($chapter_list){
        	reset($chapter_list);
        	$first = array_shift($chapter_list);
        	$chapter['first']['name'] = $first['name'];
        	$chapter['first']['number'] = $first['number'];
        	if($chapter_list){
        		$end = end($chapter_list);
        		$chapter['end']['name'] = $end['name'];
        		$chapter['end']['number'] = $end['number'];
        	}
        }
        $book['chapter'] = $chapter;
        $is_collect = 'no';
        $money = 0;
        if($loginId){
        	$collectIds = myCache::getCollectBookIds($loginId);
        	if($collectIds && in_array($book['id'], $collectIds)){
        		$is_collect = 'yes';
        	}
        	$money = myCache::getMemberMoney($loginId);
        }
        $kefu = getMyConfig('site_contact');
        $variable = [
        	'book' => $book,
        	'comments' => ['count'=>0,'list'=>''],
        	'is_collect' => $is_collect,
        	'qrcode' => $kefu ? $kefu['qrcode'] : '',
        	'read_number' => $read_number,
        	'user' => ['id'=>$loginId,'money'=>$money],
        	'reward' => myCache::getRewardCache($book['id']),
        ];
        $comments = myCache::getBookComments($book['id']);
        if($comments){
        	$variable['comments']['count'] = count($comments);
        	$variable['comments']['list'] = array_slice($comments, 0,3);
        	$zanIds = myCache::getZanIds($loginId);
        	foreach ($variable['comments']['list'] as &$cv){
        		$cv['is_zan'] = $zanIds && in_array($cv['id'], $zanIds) ? 1 : 2;
        		$cv['zan_num'] = myCache::getCommentsZannum($cv['id']);
        	}
        }
        $is_share = 'no';
        if(checkIsWeixin()){
        	$member = '';
        	if($loginId){
        		$member = myCache::getMember($loginId);
        	}
        	$share_info = self::getWxShareInfo($book,$member);
        	if($share_info){
        		$is_share = 'yes';
        		$variable['share_info'] = $share_info;
        	}
        }
        $variable['is_share'] = $is_share;
        return $this->fetch('info',$variable);
    }
    
    //章节信息
    public function chapter(){
    	$book_id = myHttp::getId('书籍','book_id');
    	$book = myCache::getBook($book_id);
    	if(!$book){
    		res_error('书籍不存在');
    	}
    	if(!in_array($book['status'], [1,2])){
    		res_error('书籍已下架');
    	}
    	$num = 0;
    	$temp = [];
    	$is_free = myCache::checkBookFree($book['id']);
    	$cache = myCache::getBookChapterList($book['id']);
    	if($cache){
    		if(!$is_free && $book['free_type'] == 2){
    			foreach ($cache as &$cv){
    				if($cv['number'] > $book['free_chapter']){
    					if(!$cv['money']){
    						$cv['money'] = $book['money'];
    					}
    				}
    			}
    		}
    		$list = array_chunk($cache, 50);
    		foreach ($list as $k=>$v){
    			$length = count($v);
    			$title  = ($num+1).'~'.($num+$length);
    			$is_check = $num === 0 ? 'yes' : 'no';
    			$temp[] = ['title'=>$title,'is_check'=> $is_check,'id'=>'block_'.$k,'list'=>$v];
    			$num += $length;
    		}
    	}
    	$variable = ['book_id'=>$book['id'],'list'=>$temp,'total'=>$num];
    	return $this->fetch('chapter',$variable);
    	
    }
    
    //书籍正文
    public function read(){
    	global $loginId;
    	$kc_code = myValidate::getData(['kc_code'=>['alphaDash|length:6',['alphaDash'=>'非法访问','length'=>'非法访问']]],'get');
    	if($kc_code){
    		$codeInfo = myCache::getKcCodeInfo($kc_code);
    		if($codeInfo && $codeInfo['type'] == 1){
    			$spread = myCache::getSpread($codeInfo['sid']);
    			if(!$spread){
    				res_error('该链接已失效');
    			}
    			myDb::add('SpreadView',['sid'=>$spread['id'],'create_time'=>time()]);
    			$get = [
    				'book_id' => $spread['book_id'],
    				'number' => $spread['chapter_number']
    			];
    			$get['number'] = self::getReadNumber($get['book_id'],$get['number']);
    			session('spread_id',$spread['id']);
    			if(!checkIsWeixin()){
    				session('kc_code',$kc_code);
    			}
    		}else{
    			res_error('非法访问');
    		}
    	}else{
    		$rules = [
    			'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
    			'number'=>['number|gt:0',['require'=>'章节参数错误','number'=>'章节参数错误','gt'=>'章节参数错误']],
    		];
    		$get = myValidate::getData($rules,'get');
    		if(!$get['number']){
    			$get['number'] = self::getReadNumber($get['book_id']);
    		}
    	}
    	$book = myCache::getBook($get['book_id']);
    	if(!$book){
    		res_error('书籍不存在');
    	}
    	if(!in_array($book['status'], [1,2])){
    		res_error('书籍已下架');
    	}
    	$totalChapter = myCache::getBookChapterNum($book['id']);
    	if($totalChapter == 0){
    		res_error('该书籍尚无可读章节');
    	}
    	$chapterList = myCache::getBookChapterList($book['id']);
    	$number = ($get['number'] > $totalChapter) ? $totalChapter : $get['number'];
    	if(!isset($chapterList[$number])){
    		res_error('该章节不存在');
    	}
    	$next_number = (($number + 1) > $totalChapter) ? 0 : ($number + 1);
    	$chapter = $chapterList[$number];
    	$is_free = myCache::checkBookFree($book['id']);
    	$is_collect = 'no';
    	$member = '';
    	if($loginId){
    		$collectIds = myCache::getCollectBookIds($loginId);
    		if($collectIds && in_array($book['id'], $collectIds)){
    			$is_collect = 'yes';
    		}
    		$member = myCache::getMember($loginId);
    	}else{
    		if(!$is_free){
    			if($book['free_type'] == 2 && $get['number'] > $book['free_chapter']){
    				$this->redirect('Login/index');
    			}
    		}
    	}
    	$lineQrStr = $showQrStr = '';
    	if(session('?spread_id')){
    		$spread = myCache::getSpread(session('spread_id'));
    		if($spread){
    			if($get['number'] >= $spread['number']){
    				switch ($spread['click_type']){
    					case 1:
    						if(!$loginId){
    							$this->redirect('Login/index');
    						}
    						if($member['subscribe'] == 0){
    							$this->redirect('Qr/sub');
    						}
    						break;
    					case 2:
    						$config = getMyConfig('website');
    						$url = $config['download_url'];
    						$url .= strpos($config['download_url'], '?') !== false ? '&' : '?';
    						$url .= 'spread_id='.$spread['id'];
    						$this->redirect($url);
    						break;
    					default:
    						if(checkIsWeixin()){
    							if($member['subscribe'] == 0){
    								$urlData = myCache::getWeixinInfo();
    								if($get['number'] == $spread['number']){
    									$showQrStr = createNoAttentionHtml($urlData['qrcode']);
    								}else{
    									if($get['number'] > $spread['number']){
    										$lineQrStr = createAttentionHtml($urlData['qrcode']);
    									}
    								}
    							}
    						}
    						break;
    				}
    			}
    		}
    	}
    	if($member){
    		$is_read = myCache::checkIsRead($member['id'], $book['id'], $number);
    		if(!$is_read){
    			$isAdd = true;
    			if(!$is_free){
    				if($book['free_type'] == 2 && $number > $book['free_chapter']){
    					$is_money = true;
    					if($member['viptime']){
    						$is_money = false;
    						if($member['viptime'] != 1){
    							if(time() > $member['viptime']){
    								$is_money = true;
    							}
    						}
    					}
    					if($is_money){
    						$isAdd = false;
    						$money = myCache::getMemberMoney($loginId);
    						$book_money = $chapter['money'] ? $chapter['money'] : $book['money'];
    						if($money < $book_money){
    							res_error('您的书币余额不足');
    						}
    						$res = mdBook::addReadhistory($book,$book_money,$chapter, $member,true);
    						if(!$res){
    							res_error('章节购买失败，请重试');
    						}
    					}
    				}
    			}
    			if($isAdd){
    				mdBook::addReadhistory($book,0,$chapter,$member);
    			}
    		}
    	}
    	$variable = [
    		'book' => $book,
    		'showQrStr' => $showQrStr,
    		'lineQrStr' => $lineQrStr,
    		'is_collect' => $is_collect,
    		'dir' => 'book/'.$book['id'],
    		'chapter_title' => $chapter['name'],
    		'number' => ['current'=>$number,'prev'=>($number-1),'next'=>$next_number]
    	];
    	$is_weixin = $is_share = 'no';
    	if(checkIsWeixin()){
    		$is_weixin = 'yes';
    		$weixin = myCache::getWeixinInfo();
    		$variable['qrcode'] = $weixin['qrcode'];
    		$share_info = self::getWxShareInfo($book,$member);
    		if($share_info){
    			$is_share = 'yes';
    			$variable['share_info'] = $share_info;
    		}
    	}
    	$variable['is_share'] = $is_share;
    	$variable['is_weixin'] = $is_weixin;
    	mdBook::addChapterRead($chapter['id']);
    	$tpl = $book['type'] == 1 ? 'book/read/novel' : 'book/read/cartoon';
    	return $this->fetch($tpl,$variable);
    }
    
    //获取阅读章节
    private function getReadNumber($book_id,$number=0){
    	$number = $number ? : 1;
    	if(checkIsWeixin()){
    		global $loginId;
    		$cache = myCache::getReadCache($loginId);
    		$key = 'book_'.$book_id;
    		if(isset($cache[$key])){
    			$number = $cache[$key]['last_number'];
    		}
    	}
    	return $number;
    }
    
    //检查章节是否可读
    public function checkChapter(){
    	global $loginId;
    	$rules = [
    		'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
    		'number'=>['require|number|gt:0',['require'=>'章节参数错误','number'=>'章节参数错误','gt'=>'章节参数错误']],
    	];
    	$post = myValidate::getData($rules);
    	$book = myCache::getBook($post['book_id']);
    	if(!$book){
    		res_api('书籍不存在');
    	}
    	if(!in_array($book['status'], [1,2])){
    		res_api('书籍已下架');
    	}
    	$totalChapter = myCache::getBookChapterNum($book['id']);
    	if($totalChapter == 0){
    		res_api('该书籍尚无可读章节');
    	}
    	if($post['number'] > $totalChapter){
    		res_api('已经是最后一章了');
    	}
    	$chapterList = myCache::getBookChapterList($book['id']);
    	if(!isset($chapterList[$post['number']])){
    		res_api('该章节不存在');
    	}
    	$chapter = $chapterList[$post['number']];
    	$is_free = myCache::checkBookFree($book['id']);
    	$member = '';
    	if($loginId){
    		$member = myCache::getMember($loginId);
    	}else{
    		if(!$is_free){
    			if($book['free_type'] == 2 && $post['number'] > $book['free_chapter']){
    				res_api('您尚未登录，是否立即登录',3);
    			}
    		}
    	}
    	if(session('?spread_id')){
    		$spread = myCache::getSpread(session('spread_id'));
    		if($spread){
    			if($post['number'] >= $spread['number']){
    				switch ($spread['click_type']){
    					case 1:
    						if(!$loginId || !$member['subscribe']){
    							$urlData = myCache::getWeixinInfo();
    							$str = createAttentionHtml($urlData['qrcode']);
    							res_api($str,4);
    						}
    						break;
    					case 2:
    						$config = getMyConfig('website');
    						$url = $config['download_url'];
    						$url .= strpos($config['download_url'], '?') !== false ? '&' : '?';
    						$url .= 'spread_id='.$spread['id'];
    						res_api('阅读下章节需要下载APP，是否立即下载',3,['url'=>$url]);
    						break;
    				}
    			}
    		}
    	}
    	if($member){
    		if($book['free_type'] == 2 && $post['number'] > $book['free_chapter']){
    			$is_read = myCache::checkIsRead($member['id'], $book['id'], $post['number']);
    			if(!$is_read){
    				if(!$is_free){
    					$is_money = true;
    					if($member['viptime'] > 0){
    						$is_money = false;
    						if($member['viptime'] != 1){
    							if(time() > $member['viptime']){
    								$is_money = true;
    							}
    						}
    					}
    					if($is_money){
    						$money = myCache::getMemberMoney($loginId);
    						$book_money = $chapter['money'] ? $chapter['money'] : $book['money'];
    						if($money < $book_money){
    							res_api('您的书币余额不足，是否立即充值',3,['url'=>my_url('Charge/index',['book_id'=>$book['id']])]);
    						}
    					}
    				}
    			}
    		}
    	}
    	res_api(['url'=>my_url('read',['book_id'=>$book['id'],'number'=>$post['number']])]);
    }
    
    
    
    //排行
    public function rank(){
    	$rules = [
    		'type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
    		'gender_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
    		'cate'=>['between:1,4',['between'=>'非法访问']]
    	];
    	$get = myValidate::getData($rules,'get');
    	$cate = $get['cate'] ? : 1;
    	$block_name = 'book_rank_'.$get['type'].$get['gender_type'].$cate;
    	$variable = [
    		'cate' => $cate,
    		'type' => $get['type'],
    		'gender_type' => $get['gender_type'],
    		'block_name' => $block_name,
    	];
    	return $this->fetch('rank',$variable);
    }
    
    
    /**
     * 获取微信分享内容
     */
    private function getWxShareInfo($book,$member){
    	$res = 0;
    	$urlInfo = myCache::getUrlInfo();
    	if($_SERVER['HTTP_HOST'] === $urlInfo['url']){
    		if($book['cover'] && $book['share_title'] && $book['share_desc']){
    			$config = myCache::getWeixinInfo();
    			wx::$config = $config;
    			$jsConfig = wx::getJsConfig();
    			$share_url = 'http://'.$_SERVER['HTTP_HOST'].'/index/Book/info.html?book_id='.$book['id'];
    			if($member){
    				$share_url .= '&invite_code='.$member['invite_code'];
    			}
    			$res = [
    				'config' => $jsConfig,
    				'info' => [
    					'title' => $book['share_title'],
    					'url' => $share_url,
    					'img' => $book['cover'],
    					'desc' => $book['share_desc']
    				]
    			];
    			$res = json_encode($res,JSON_UNESCAPED_UNICODE);
    		}
    	}
    	return $res;
    }
    
}