<?php
namespace app\index\controller;

use site\myCache;
use site\myValidate;
use think\Db;

class Index extends Common{
    
    //首页
    public function index(){
        Db::name('request_url')->insert([
            'url'=>$this->request->url(true),
            'create_time'=>date('Y-m-d H:i:s'),
            'type'=>'h5',
            'web_host' => $this->request->host()
        ]);
        $this->countData();
    	$cate = myValidate::getData(['cate'=>['in:man,woman',['in'=>'访问参数异常']]],'get');
    	$cate = $cate ? : 'man';
    	$gender_type = $cate === 'man' ? 1 : 2;
    	if(!request()->isMobile()){
    		$this->redirect('/?type=1&gender_type='.$gender_type);
    	}
    	$weixin = myCache::getWeixinInfo();
    	$catlog = $this->getData(1,1);//新书推荐
    	$blocks = [
    		'cate' => $cate,
    		'nav' => 'novel_nav_'.$cate,
    		'banner' => 'novel_banner_'.$cate,
    		'content' => 'novel_content_'.$cate
    	];
    	$variable = [
    		'book_type' => 1,
    		'footer_type' => 2,
    		'blocks' => $blocks,
    		'gender_type' => $gender_type,
    		'qrcode' => $weixin['qrcode'],
            'catlog' => $catlog
    	];
    	//var_dump($catlog);
    	return $this->fetch('index',$variable);
    }

    public function getData($type,$gender_type)
    {
        $date = date('Y-m-d');
        $where = [
            ['a.type','=',$type],
            ['a.status','=',1],
            ['a.gender_type','=',$gender_type],
            ['b.create_time','between',[strtotime($date.' 00:00:00'),strtotime($date.' 23:59:59')]]
        ];
        $list = Db::name('Book a')
            ->join('book_cat_log b','a.id=b.book_id')
            ->where($where)
            ->field('a.id,a.name,a.cover,max(b.create_time) as create_time')
            ->group('a.id')
            ->order('create_time','desc')
            ->limit(6)
            ->select();
        //$data = $list ? array_column($list, 'id') : 0;
        //var_dump($list);
        return $list;
    }
    
    //漫画首页
    public function cartoon(){
    	$config = getMyConfig('website');
    	if($config['is_cartoon'] != 1){
    		$this->redirect('index');
    	}
    	$weixin = myCache::getWeixinInfo();
    	$cate = myValidate::getData(['cate'=>['in:man,woman',['in'=>'访问参数异常']]],'get');
    	$cate = $cate ? : 'man';
    	$gender_type = $cate === 'man' ? 1 : 2;
    	if(!request()->isMobile()){
    		$this->redirect('/?type=2&gender_type='.$gender_type);
    	}
    	$blocks = [
    		'cate' => $cate,
    		'nav' => 'cartoon_nav_'.$cate,
    		'banner' => 'cartoon_banner_'.$cate,
    		'content' => 'cartoon_content_'.$cate
    	];
    	$variable = [
    		'book_type' => 2,
    		'footer_type' => 3,
    		'blocks' => $blocks,
    		'qrcode' => $weixin['qrcode'],
    	];
    	return $this->fetch('index',$variable);
    }

    //统计在线人数和日活人数
    public function countData()
    {
        $index_login_id = session('INDEX_LOGIN_ID');
        $date = date('Y-m-d');
        //$table = Db::name('Count');
        $data = Db::name('Count')->where('date_time',$date)->find();//查找当天有无数据
        if(!$data){
            //如果没有，则插入一条
            Db::name('Count')->insert([
                'date_time' => $date,
                'h5' => 1
            ]);
        }else{
            //如果有数据，则相应的字段+1
            Db::name('Count')->where('date_time',$date)->setInc('h5');
        }
        //如果登录，则获取登录用户名；用户没有登录，则获取访问ip
        if($index_login_id == null){
            $username = $this->request->ip();
        }else{
            $username = $index_login_id;
        }
        //$username = $token?$user['phone']:$this->request->ip();
        //var_dump($this->request->host());
        $redis = new \Redis();
        $redis->connect('127.0.0.1',6379);//连接redis
        //$date = date('Y-m-d');
        $host = $this->request->host();//获取域名或ip
        $url = $this->request->url(true);
        //var_dump($url);
        if($url == "http://".$host."/index/index/index.html" || $url == "http://".$host."/Index"){
            //如果是首页，获取一个相同的值
            $url = "http://".$host."/Index";
        }
        $res = Db::name('count')->where('date_time',$date)->find();
        $val = $redis->pfadd($date.'.'.$url,[$username]);
        //返回值=1，说明添加成功；=0说明添加失败，不做操作
        if($val == 1){
            if(!$res){
                Db::name('count')->insert([
                    'date_time' => $date,
                    'day_activity' => 1
                ]);
            }else{
                Db::name('Count')->where('date_time',$date)->setInc('day_activity');//日活人数
            }
        }
    }

    public function downApp()
    {
        if(strpos($_SERVER['HTTP_USER_AGENT'], 'iPhone')||strpos($_SERVER['HTTP_USER_AGENT'], 'iPad')){
            echo "ios";
        }else if(strpos($_SERVER['HTTP_USER_AGENT'], 'Android')){
            header('Location: http://666.srongkj.cn');
        }else{
            header('Location: http://666.srongkj.cn');
        }
    }
    
}