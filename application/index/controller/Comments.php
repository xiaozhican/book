<?php
namespace app\index\controller;

use site\myDb;
use site\myHttp;
use site\myCache;
use site\myValidate;
use app\common\model\mdComments;


class Comments extends Common{
	
	//获取评论列表
	public function index(){
		if($this->request->isAjax()){
			global $loginId;
			$rules = [
				'book_id' => ['require|number|gt:0',['require'=>'评论对象参数错误','number'=>'评论对象参数不规范','gt'=>'评论参数对象不规范']],
				'page' => ['require|number|gt:0',['require'=>'分页参数异常','number'=>'分页参数异常','gt'=>'分页参数异常']]
			];
			$post = myValidate::getData($rules);
			$list = myCache::getBookComments($post['book_id']);
			if($list){
				$page = $post['page'] - 1;
				$temp = array_chunk($list, 20);
				if(isset($temp[$page])){
					$list = $temp[$page];
					$zanIds = myCache::getZanIds($loginId);
					foreach ($list as &$v){
						$v['is_zan'] = $zanIds && in_array($v['id'], $zanIds) ? 1 : 2;
						$v['zan_num'] = myCache::getCommentsZannum($v['id']);
					}
				}
			}else{
				$list = '';
			}
			res_table($list);
		}else{
			$book_id = myHttp::getId('书籍','book_id');
			return $this->fetch('index',['book_id'=>$book_id]);
		}
	}
	
	//提交评论
	public function doSubmit(){
		if($this->request->isAjax()){
			global $loginId;
			if(!$loginId){
				res_api('您尚未登陆,是否立即登录',3);
			}
			$rules = [
				'pid' => ['require|number|gt:0',['require'=>'评论对象参数错误','number'=>'评论对象参数不规范','gt'=>'评论参数对象不规范']],
				'content' => ['require|max:200',['require'=>'请输入评论内容','max'=>'评论字数超出限制']],
			];
			$data = myValidate::getData($rules);
			$data['type'] = 1;
			$data['status'] = 0;
			$data['pname'] = '未知';
			$data['uid'] = $loginId;
			$data['create_time'] = time();
			$data['content'] = htmlspecialchars($data['content']);
			if($data['type'] == 2){
				$video = myCache::getVideo($data['pid']);
				if($video){
					$data['pname'] = $video['name'];
				}
			}else{
				$book = myCache::getBook($data['pid']);
				if($book){
					$data['pname'] = $book['name'];
				}
			}
			$re = myDb::add('Comments', $data);
			if($re){
				res_api();
			}else{
				res_api('发表失败，请重试');
			}
		}else{
			$bookId = myHttp::getId('书籍','book_id');
			$book = myCache::getBook($bookId);
			if(!$book){
				res_error('书籍不存在');
			}
			return $this->fetch('doSubmit',['book'=>$book]);
		}
	}
	
	//评论点赞
	public function doZan(){
		global $loginId;
		if(!$loginId){
			res_api('您尚未登陆,是否立即登录',3);
		}
		$pid = myHttp::postId('评论','pid');
		$repeat = myDb::getValue('CommentsZan', [['pid','=',$pid],['uid','=',$loginId]],'id');
		if($repeat){
			res_api('您已经点过赞了');
		}
		$re = mdComments::doZan($pid, $loginId);
		if($re){
			myCache::incCommentsZan($pid);
			myCache::addZanIds($loginId, $pid);
			res_api();
		}else{
			res_api('点赞失败');
		}
	}
}