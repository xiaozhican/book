<?php
namespace app\index\controller;

use site\myCache;

class Qr extends Common{
	
	//联系客服
	public function contact(){
		global $loginId;
		$config = getMyConfig('site_contact');
		if(!$config || !$config['qrcode']){
			res_error('尚未配置客服二维码');
		}
		$variable = [
			'uid'=>$loginId,
			'qrcode'=>$config['qrcode'],
		];
		return $this->fetch('contact',$variable);
	}
	
	//关注
	public function sub(){
		$urlData = myCache::getWeixinInfo();
		if(!$urlData){
			res_error('未配置公众号信息');
		}
		$variable = ['qrcode'=>$urlData['qrcode']];
		return $this->fetch('sub',$variable);
	}
}