<?php
namespace app\index\controller;

use site\myCache;

class Activity extends Common{
	
	//充值活动
	public function index(){
		global $loginId;
		if(!checkIsWeixin()){
			res_error('请在微信中打开该链接');
		}
		$member = myCache::getMember($loginId);
		$member['money'] = myCache::getMemberMoney($loginId);
		if(!session('?kc_code')){
			res_error('非法访问');
		}
		$kc_code = session('kc_code');
		$codeInfo = myCache::getKcCodeInfo($kc_code);
		if(!$codeInfo){
			return self::errPage('对不起，您来晚了！','活动已下线');
		}
		if($codeInfo['type'] != 2){
			return self::errPage('对不起，您来晚了！','活动已下线');
		}
		$activity = myCache::getActivity($codeInfo['aid']);
		if($activity){
			if($activity['status'] != 1){
				return self::errPage('对不起，您来晚了！','活动已下线');
			}
			$urlInfo = myCache::getUrlInfo();
			if($urlInfo['wx_id'] > 0){
				$delIds = myCache::getActivityDelIds($urlInfo['wx_id']);
				if($delIds && in_array($activity['id'], $delIds)){
					return self::errPage('对不起，您来晚了！','活动已下线');
				}
			}
			$time = time();
			if($time < $activity['start_time']){
				return self::errPage('亲，您真是未卜先知啊！','活动未开始');
			}
			if($time > $activity['end_time']){
				return self::errPage('对不起，您来晚了！','活动已结束');
			}
			if($activity['is_first'] == 1){
				$uids = myCache::getActivityUids($activity['id']);
				if(in_array($loginId, $uids)){
					return self::errPage('对不起，您已经充值过了！','该活动仅限充值一次');
				}
			}
			$variable = ['cur' => $activity,'user'=>$member];
			return $this->fetch('index',$variable);
		}else{
			return self::errPage('您好像迷路了！','活动不存在');
		}
	}
	
	//活动错误页
	private function errPage($title,$sub_title){
		$msg = [
			'title' => $title,
			'sub_title' => $sub_title
		];
		return $this->fetch('errPage',$msg);
	}
	
	//最新活动
	public function cur(){
		global $loginId;
		if(!checkIsWeixin()){
			res_error('请在微信中打开该链接');
		}
		$member = myCache::getMember($loginId);
		$cur_id = myCache::getLastActivityId();
		if($cur_id){
			$channel_id = 0;
			if($member['channel_id']){
				if($member['agent_id']){
					$channel_id = $member['agent_id'];
				}else{
					$channel_id = $member['channel_id'];
				}
			}
			$code = myCache::getActivityCode($cur_id,$channel_id);
			$url = 'http://'.$_SERVER['HTTP_HOST'].'/index/activity/index.html?kc_code='.$code;
			$this->redirect($url);
		}else{
			return self::errPage('对不起', '当前暂未发布活动');
		}
	}
}