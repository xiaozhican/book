<?php
namespace app\index\controller;

use Endroid\QrCode\QrCode;
use site\myDb;
use site\myHttp;
use payjq\payJQ;
use site\myImg;
use weixin\wxpay;
use mihua\mihuaPay;
use think\Controller;

class Pay extends Controller{

    //jsapi支付
    public function jsPay(){
        if(!checkIsWeixin()){
            res_error('请在微信中完成支付');
        }
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_error('非法访问');
        }
        if(!session('PAY_OPEN_ID')){
            self::_auto_login();
        }
        $get = myHttp::getData('order_no,back_url','get');
        if(!$get['order_no']){
            res_error('订单参数异常');
        }
        $where = [['order_no','=',$get['order_no']]];
        $order = myDb::getCur('Order', $where,'id,order_no,money,status,pay_type');
        if(!$order){
            res_error('订单数据异常');
        }
        if($order['status'] != 1){
            res_error('该订单已支付');
        }
        if($order['pay_type'] != 1){
            res_error('支付方式有误');
        }
        $config = getMyConfig('wxjspay');
        if(!$config){
            res_error('未配置支付参数');
        }
        $notify_url = 'http://'.$website['url'].'/index/Notify/index.html';
        wxpay::$config = $config;
        $params = [
            'body' => '书币充值',
            'out_trade_no' => $order['order_no'],
            'total_fee' => $order['money']*100,
            'attach' => $order['id'],
            'trade_type' => 'JSAPI',
            'openid' => session('PAY_OPEN_ID'),
            'notify_url' => $notify_url
        ];
        $paymsg = wxpay::getPayMsg($params);
        if(!$paymsg){
            res_error('支付数据异常');
        }

        $variable = [
            'cur' => $order,
            'back_url' => $get['back_url'],
            'paymsg' => json_encode($paymsg,JSON_UNESCAPED_UNICODE),
        ];
        return $this->fetch('jsPay',$variable);
    }

    //微信扫码支付
    public function wxScanPay(){
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_error('非法访问');
        }
        $get = myHttp::getData('order_no,table,back_url','get');
        if(!$get['order_no']){
            res_error('订单参数异常');
        }
        $where = [['order_no','=',$get['order_no']]];
        $order = myDb::getCur('Order', $where,'id,order_no,money,status,pay_type');
        if(!$order){
            res_error('订单数据异常');
        }

        if($order['status'] != 1){
            res_error('该订单已支付');
        }
        if($order['pay_type'] != 5){
            res_error('支付方式有误');
        }
        $config = getMyConfig('wxjspay');
        if(!$config){
            res_error('未配置支付参数');
        }
        wxpay::$config = $config;
        $notify_url = 'http://'.$website['url'].'/index/Notify/wxpayScan.html';

        $params=[
            'body'=>'书币充值',//商品描述
            'out_trade_no'=>$order['order_no'],//内部订单号
            'attach' => $order['id'],
            'total_fee'=>$order['money']*100,//总价（单位：分）
            'notify_url'=>$notify_url,//支付通知URL
            'trade_type'=>'NATIVE',//支付类型
        ];//构建参数数组（不带签名sign）

        $paymsg = wxpay::getPayMsg($params);
        if(!$paymsg){
            //res_error('支付数据异常');
            res_error('该商户未开通扫码支付产品');
        }
        $path = './files/pc_wxpay/'.date('Ymd');
        if (!is_dir($path)){
            mkdir($path,0777,true);
        }
        $filename = $path.'/'.time().mt_rand(10000,99999).'.png';
        $variable = [
            'cur' => $order,
            'back_url' => $get['back_url'],
            'paymsg' =>myImg::createQrCode($paymsg,$filename),
            'pc_scan'=>1
        ];
        $variable['paymsg'] = substr($variable['paymsg'],1);
        return $this->fetch('jsPay',$variable);
    }

    //支付宝扫码支付
    public function aliScanPay(){
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_error('非法访问');
        }
        $get = myHttp::getData('order_no,table,back_url','get');
        if(!$get['order_no']){
            res_error('订单参数异常');
        }
        $where = [['order_no','=',$get['order_no']]];
        $order = myDb::getCur('Order', $where,'id,order_no,charge_id,money,status,pay_type');

        $order['order_name'] = '充值';
        if (isset($order['charge_id'])){
            $charge = myDb::getById('charge',$order['charge_id'],'type');
            if ($charge['type'] == 1){
                $order['order_name'] = '书币充值';
            }elseif($charge['type'] == 2){
                $order['order_name'] = 'VIP会员充值';
            }
        }

        if(!$order){
            res_error('订单数据异常');
        }

        if($order['status'] != 1){
            res_error('该订单已支付');
        }
        if($order['pay_type'] != 4){
            res_error('支付方式有误');
        }
        $config = getMyConfig('alismpay');
        if(!$config){
            res_error('未配置支付参数');
        }
        require_once '../extend/alismpay/config.php';
        require_once '../extend/alismpay/pagepay/service/AlipayTradeService.php';
        require_once '../extend/alismpay/pagepay/buildermodel/AlipayTradePagePayContentBuilder.php';

        //商户订单号，商户网站订单系统中唯一订单号，必填
        $out_trade_no = trim($order['order_no']);

        //订单名称，必填
        $subject = trim($order['order_name']);

        //付款金额，必填
        $total_amount = trim($order['money']);

        //商品描述，可空
        $body = "";

        //构造参数
        $payRequestBuilder = new \AlipayTradePagePayContentBuilder();
        $payRequestBuilder->setBody($body);
        $payRequestBuilder->setSubject($subject);
        $payRequestBuilder->setTotalAmount($total_amount);
        $payRequestBuilder->setOutTradeNo($out_trade_no);

        $aop = new \AlipayTradeService($config);

        /**
         * pagePay 电脑网站支付请求
         * @param $builder 业务参数，使用buildmodel中的对象生成。
         * @param $return_url 同步跳转地址，公网可以访问
         * @param $notify_url 异步通知地址，公网可以访问
         * @return $response 支付宝返回的信息
         */
        $response = $aop->pagePay($payRequestBuilder,$config['return_url'],$config['notify_url']);

        //输出表单
        var_dump($response);
    }

    //米花支付
    public function mihuaPay(){
        if(!checkIsWeixin()){
            res_error('请在微信中完成支付');
        }
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_error('非法访问');
        }
        if(!session('PAY_OPEN_ID')){
            self::_auto_login();
        }
        $get = myHttp::getData('order_no,table,back_url','get');
        if(!$get['order_no']){
            res_error('订单参数异常');
        }
        $where = [['order_no','=',$get['order_no']]];
        $order = myDb::getCur('Order', $where,'id,order_no,money,status,pay_type');
        if(!$order){
            res_error('订单数据异常');
        }
        if($order['status'] != 1){
            res_error('该订单已支付');
        }
        if($order['pay_type'] != 2){
            res_error('支付方式有误');
        }
        $config = getMyConfig('mihuaPay');
        if(!$config){
            res_error('未配置支付参数');
        }
        mihuaPay::$config = $config;
        $notify_url = 'http://'.$website['url'].'/index/Notify/mihua.html';
        $params = [
            'userIp' => request()->ip(),
            'product' => '书币充值',
            'orderId' => $order['order_no'],
            'amount' => $order['money']*100,
            'attach' => $order['id'],
            'openId' => session('PAY_OPEN_ID'),
            'returnUrl' => $get['back_url'],
            'notifyUrl' => $notify_url
        ];
        $payurl = mihuaPay::doPay($params);
        if($payurl){
            myDb::save('Order',[['id','=',$order['id']]],['pay_url'=>$payurl]);
            $this->redirect($payurl);
        }else{
            res_error('创建支付数据失败');
        }
    }

    //payjq支付
    public function payjq(){
        if(!checkIsWeixin()){
            res_error('请在微信中完成支付');
        }
        $website = getMyConfig('website');
        if($_SERVER['HTTP_HOST'] !== $website['url']){
            res_error('非法访问');
        }
        $get = myHttp::getData('order_no,back_url,openid','get');
        if(!$get['order_no']){
            res_error('订单参数异常');
        }
        $config = getMyConfig('site_payjq');
        if(!$config){
            res_error('未配置支付参数');
        }
        payJQ::$config = $config;
        if(!$get['openid']){
            $callbackurl = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $url = 'https://payjq.cn/api/openid?mchid='.payJQ::$config['mchid'].'&callback_url='.$callbackurl;
            $this->redirect($url);
        }
        $order = myDb::getCur('Order',[['order_no','=',$get['order_no']]],'id,order_no,money,status,pay_type');
        if(!$order){
            res_error('订单数据异常');
        }
        if($order['status'] != 1){
            res_error('该订单已支付');
        }
        if($order['pay_type'] != 3){
            res_error('支付方式有误');
        }
        $params = [
            'body' => '书币充值',
            'out_trade_no' => $order['order_no'],
            'total_fee' => $order['money']*100,
            'attach' => $order['id'],
            'openid' => $get['openid'],
            'notify_url' => 'http://'.$_SERVER['HTTP_HOST'].'/index/Notify/payjq.html'
        ];
        $paymsg = payJQ::getPayMsg($params);
        if(!$paymsg){
            res_error('支付数据异常');
        }
        $variable = [
            'paymsg' => json_encode($paymsg,JSON_UNESCAPED_UNICODE),
            'cur' => $order,
            'back_url' => $get['back_url']
        ];
        return $this->fetch('jsPay',$variable);
    }

    // 尝试自动登录
    private function _auto_login(){
        $config = getMyConfig('weixin_param');
        if(!$config){
            res_api('未配置微信公众号参数');
        }
        $get = myHttp::getData('code,state','get');
        if($get['code'] && $get['state'] === 'getnow'){
            $url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$config['appid']."&secret=".$config['appsecret']."&code=".$get['code']."&grant_type=authorization_code";
            $res = myHttp::doGet($url);
            if($res && isset($res['openid'])){
                if(isset($res['openid']) && $res['openid']){
                    session('PAY_OPEN_ID',$res['openid']);
                }else{
                    $wxerror = isset($res['errmsg']) ? $res['errmsg'] : '';
                    $error = '授权失败';
                    if($wxerror){
                        $error .= ':'.$wxerror;
                    }
                    res_api($error);
                }
            }else{
                res_api('获取openid失败');
            }
        }else{
            $redirect_url = 'http://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
            $redirect_url = urlencode($redirect_url);
            $url = "https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$config['appid']."&redirect_uri=".$redirect_url."&response_type=code&scope=snsapi_base&state=getnow#wechat_redirect";
            $this->redirect($url);
        }
    }

}