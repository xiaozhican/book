<?php
namespace app\index\controller;

use think\Db;
use weixin\wx;
use site\myDb;
use payjq\payJQ;
use site\myCache;
use weixin\wxpay;
use mihua\mihuaPay;
use think\Controller;
use app\common\model\mdMember;

class Notify extends Controller{

    private $pay_type = 1;

    //微信支付异步回调
    public function index(){
        $this->pay_type = 1;
        $postStr = file_get_contents('php://input');
        libxml_disable_entity_loader(true);
        $result = json_decode(json_encode(simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        if(isset($result['sign'])){
            $sign = $result['sign'];
            unset($result['sign']);
            wxpay::$config = getMyConfig('wxjspay');
            $create_sign = wxpay::getSign($result);
            if($create_sign == $sign){
                if($result['result_code'] == 'SUCCESS'){
                    $res = [
                        'order_id' => $result['attach'],
                        'total_fee' => $result['total_fee'],
                        'out_trade_no' => $result['out_trade_no']
                    ];
                    self::doChargeOrder($res);
                }
            }
            self::resFail();
        }
    }

    //微信APP支付异步回调
    public function wxpayApp(){
        $this->pay_type = 1;
        $postStr = file_get_contents('php://input');
        libxml_disable_entity_loader(true);
        $result = json_decode(json_encode(simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        if(isset($result['sign'])){
            $sign = $result['sign'];
            unset($result['sign']);
            wxpay::$config = getMyConfig('wxapppay');
            $create_sign = wxpay::getSign($result);
            if($create_sign == $sign){
                if($result['result_code'] == 'SUCCESS'){
                    $res = [
                        'order_id' => $result['attach'],
                        'total_fee' => $result['total_fee'],
                        'out_trade_no' => $result['out_trade_no']
                    ];
                    self::doChargeOrder($res);
                }
            }
            self::resFail();
        }
    }

    //微信扫码支付异步回调
    public function wxpayScan(){
        $this->pay_type = 1;
        $postStr = file_get_contents('php://input');
        libxml_disable_entity_loader(true);
        $result = json_decode(json_encode(simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA)), true);
        if(isset($result['sign'])){
            $sign = $result['sign'];
            unset($result['sign']);
            wxpay::$config = getMyConfig('wxapppay');
            $create_sign = wxpay::getSign($result);
            if($create_sign == $sign){
                if($result['result_code'] == 'SUCCESS'){
                    $res = [
                        'order_id' => $result['attach'],
                        'total_fee' => $result['total_fee'],
                        'out_trade_no' => $result['out_trade_no']
                    ];
                    self::doChargeOrder($res);
                }
            }
            self::resFail();
        }
    }

    //支付宝app支付异步通知
    public function alipayApp(){
        $this->pay_type = 3;
        $config = getMyConfig('aliapppay');
        if($config){
            $path = env('root_path').'extend/alipay/AopSdk.php';
            require_once $path;
            $post = $this->request->post();
            writeLogs($post,'支付宝异步通知');
            $aop = new \AopClient();
            $aop->alipayrsaPublicKey = $config['alipayrsaPublicKey'];
            $flag = $aop->rsaCheckV1($post, NULL, "RSA2");
            if($flag){
                $params = explode('|', $post['passback_params']);
                $res = [
                    'order_id' => $post['passback_params'],
                    'total_fee' => $post['receipt_amount'],
                    'out_trade_no' => $post['out_trade_no']
                ];
                self::doChargeOrder($res);
            }
        }
        exit('fail');
    }

    //支付宝扫码支付异步通知
    public function alipaySm(){
        $this->pay_type = 3;
        $config = getMyConfig('alismpay');
        if($config){
            $path = env('root_path').'extend/alismpay/AopSdk.php';
            require_once $path;
            $post = $this->request->post();
            writeLogs($post,'支付宝异步通知');
            $aop = new \AopClient();
            $aop->alipayrsaPublicKey = $config['alipayrsaPublicKey'];
            $flag = $aop->rsaCheckV1($post, NULL, "RSA2");
            if($flag){
                $params = explode('|', $post['passback_params']);
                $res = [
                    'order_id' => $post['passback_params'],
                    'total_fee' => $post['receipt_amount'],
                    'out_trade_no' => $post['out_trade_no']
                ];
                self::doChargeOrder($res);
            }
        }
        exit('fail');
    }

    //米花支付回调
    public function mihua(){
        $this->pay_type = 2;
        $get = $this->request->get('data');
        $config = getMyConfig('mihuaPay');
        if(!$config){
            self::resFail();
        }
        mihuaPay::$config = $config;
        $data = mihuaPay::decryptData($get);
        $data = json_decode($data,true);
        if($data['orderStatus'] != 'SUCCESS'){
            self::resFail();
        }
        $res = [
            'order_id' => $data['attach'],
            'total_fee' => $data['amount']*100,
            'out_trade_no' => $data['orderId']
        ];
        self::doChargeOrder($res);
    }

    //jq异步通知
    public function payjq(){
        $this->pay_type = 3;
        $post = $this->request->post();
        if($post['return_code'] == 1){
            $cur_sign = $post['sign'];
            unset($post['sign']);
            $config = getMyConfig('site_payjq');
            if($config){
                payJQ::$config = $config;
                $sys_sign = payJQ::getSign($post);
                if($sys_sign === $cur_sign){
                    self::doChargeOrder($post['attach'],$post['out_trade_no']);
                }
            }
        }
        die('fail');
    }

    //处理订单状态
    private function doChargeOrder($result){
        $order = Db::name('Order')->where('id','=',$result['order_id'])->field('id,type,channel_id,agent_id,order_no,uid,money,send_money,package,spread_id,status,count_info,charge_id,pid')->find();
        if(!$order || $order['order_no'] != $result['out_trade_no']){
            self::resFail();
        }
        if($order['status'] != 1){
            self::resOk();
        }
        $user = myCache::getMember($order['uid']);
        if(!$user){
            self::resFail();
        }
        $data = [
            'status' => 2,
            'is_count' => 1,
            'pay_time' => time()
        ];
        $member_charge_type = 1;
        if($order['agent_id']){
            $data['is_count'] = self::getIsCount($order['agent_id']);
        }else{
            if($order['channel_id']){
                $data['is_count'] = self::getIsCount($order['channel_id']);
            }
        }
        $mdata = ['is_charge'=>1];
        if($order['package']){
            $member_charge_type = 2;
            $mdata['viptime'] = self::getVipTime($order['package'],$user['viptime']);
        }elseif ($order['send_money'] > 0){
            $mdata['money'] = Db::raw('money+'.$order['send_money']);
            $mdata['total_money'] = Db::raw('total_money+'.$order['send_money']);
        }
        Db::startTrans();
        $flag = false;
        $re = true;
        if($data['is_count'] == 1){
            $mcInfo = myCache::getMemberChargeId($order['uid']);
            $mcData = ['charge_num'=>Db::raw('charge_num+1'),'charge_money'=>Db::raw('charge_money+'.$order['money'])];
            $re = Db::name('MemberCharge')->where('uid',$mcInfo['uid'])->update($mcData);
            $mcInfo['charge_num'] = $data['charge_num'] = $mcInfo['charge_num'] + 1;
            $mcInfo['charge_money'] = $data['charge_money'] = $mcInfo['charge_money'] + $order['money'];
        }
        if($re){
            $res = Db::name('Order')->where('id','=',$order['id'])->update($data);
            if($res){
                $ms = Db::name('Member')->where('id','=',$user['id'])->update($mdata);
                if($ms){
                    $flag = true;
                    $count_info = json_decode($order['count_info'],true);
                    if($count_info){
                        $moneyData = ['real_charge_num'=>Db::raw('real_charge_num+1')];
                        $flag = false;
                        $moneyRes = true;
                        foreach ($count_info as $cv){
                            if($data['is_count'] == 1){
                                $moneyData['charge_num'] = Db::raw('charge_num+1');
                                $moneyData['income_money'] = Db::raw('income_money+'.$cv['money']);
                                $moneyData['charge_money'] = Db::raw('charge_money+'.$order['money']);
                            }
                            $cres = Db::name('ChannelMoney')->where('channel_id',$cv['id'])->update($moneyData);
                            if(!$cres){
                                $moneyRes = false;
                                break;
                            }
                        }
                        if($moneyRes){
                            $flag = true;
                        }
                    }
                }
            }
        }
        if($flag){
            Db::commit();
            $logTitle = '书币充值';
            if($data['is_count'] == 1){
                myCache::getMemberChargeId($order['uid'],$mcInfo);
            }
            if($order['type'] == 2){
                $logTitle = '活动充值';
                myCache::addActivityUids($order['pid'], $order['uid']);
            }
            switch ($member_charge_type){
                case 1:
                    myCache::doMemberMoney($user['id'], $order['send_money']);
                    mdMember::addChargeLog($order['uid'], '+'.$order['send_money'].' 书币', $logTitle);
                    break;
                case 2:
                    myCache::updateMember($user['id'], 'viptime', $mdata['viptime']);
                    $log = self::getVipName($order['package']);
                    mdMember::addChargeLog($order['uid'],$log,'会员充值');
                    break;
            }
            myCache::incChargeNum($order['uid'], $order['charge_id']);
            self::sendNotice($order);
            self::resOk();
        }else{
            Db::rollback();
            self::resFail();
        }
    }

    //检测渠道是否应该扣量
    private function getIsCount($channel_id){
        $isCount = 1;
        $channel = myCache::getChannel($channel_id);
        if($channel && $channel['deduct_num'] > 0){
            $info = myDb::getCur('ChannelMoney',[['channel_id','=',$channel_id]],'real_charge_num,charge_num');
            if($info){
                $diff = $info['real_charge_num'] - $channel['deduct_min'];
                if($diff > 0){
                    if($channel['deduct_num'] == 1){
                        $isCount = 2;
                    }else{
                        $week = ceil($diff/$channel['deduct_num']);
                        $deductCount = $info['real_charge_num'] - $info['charge_num'];
                        if($week > $deductCount){
                            $last = $diff%$channel['deduct_num'];
                            if(($last + 1) == $channel['deduct_num']){
                                $isCount = 2;
                            }else{
                                $rand = mt_rand(0,100);
                                if($rand < 50){
                                    $isCount = 2;
                                }
                            }
                        }
                    }
                }
            }
        }
        return $isCount;
    }

    //发送
    private function sendNotice($order){
        $member = myCache::getMember($order['uid']);
        if($member){
            if($member['wx_id']){
                $channel = myCache::getChannel($member['wx_id']);
                $config = [];
                if($channel){
                    $config['appid'] = $channel['appid'];
                    $config['appsecret'] = $channel['appsecret'];
                    $read_url = $channel['url'];
                }
            }else{
                $config = getMyConfig('weixin_param');
                $read_url = $_SERVER['HTTP_HOST'];
            }
            if($config){
                wx::$config = $config;
                $str = '充值成功通知：';
                $str .= "\n\n";
                $str .= '充值金额 : '.$order['money'].'元';
                if($order['package']){
                    $str .= "\n";
                    $vipname = self::getVipName($order['package']);
                    $str .= '充值会员 : '.$vipname;
                    if($member['viptime'] == 1){
                        $viptime = '终身有效';
                    }else{
                        $viptime = date('Y-m-d',$member['viptime']);
                    }
                    $str .= "\n";
                    $str .= '会员有效期 ： '.$viptime;
                }else{
                    $str .= "\n";
                    $str .= '充值书币 : '.$order['send_money'];
                    $str .= "\n";
                    $str .= '剩余书币 : '.myCache::getMemberMoney($order['uid']);
                }
                $str .= mdMember::getReadStr($order['uid'],$read_url);
                $str .= "\n\n";
                $str .= '为方便下次阅读，请置顶公众号';
                wx::sendCustomMessage($member['openid'], $str,'text');
            }
        }
    }

    //返回失败状态
    private function resFail(){
        exit('fail');
    }

    //返回成功状态
    private function resOk(){
        switch ($this->pay_type){
            case 1:
                $str = '<xml><return_code><![CDATA[SUCCESS]]></return_code><return_msg><![CDATA[OK]]></return_msg></xml>';
                break;
            case 2:
                $str = 'SUCCESS';
                break;
            case 3:
                $str = 'success';
                break;
        }
        exit($str);
    }

    //获取充值vip名称
    private function getVipName($package){
        $str = '未知';
        switch ($package){
            case 1:
                $str = '日会员';
                break;
            case 2:
                $str = '月会员';
                break;
            case 3:
                $str = '季度会员';
                break;
            case 4:
                $str = '半年度会员';
                break;
            case 5:
                $str = '年度会员';
                break;
            case 6:
                $str = '终身会员';
                break;
        }
        return $str;
    }

    //获取vip包月时间
    private function getVipTime($package,$viptime){
        $time = time();
        if($viptime){
            if($viptime == 1){
                return 1;
            }else{
                if($viptime > $time){
                    $time = $viptime;
                }
            }
        }
        switch ($package){
            case 1:
                $time += 86400;
                break;
            case 2:
                $time += 86400*30;
                break;
            case 3:
                $time += 86400*90;
                break;
            case 4:
                $time += 86400*180;
                break;
            case 5:
                $time += 86400*365;
                break;
            case 6:
                $time = 1;
                break;
        }
        return $time;
    }

}