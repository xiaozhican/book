<?php
namespace app\index\controller;

use think\Db;
use site\myHttp;
use site\myCache;

class Search extends Common{
	
	public function index(){
		if($this->request->isAjax()){
			$post = myHttp::getData('keyword');
			$keyword = $post['keyword'] ? : '';
			if(!$keyword){
				res_api('未键入关键字');
			}
			$keyword = htmlspecialchars($keyword);
			$where = [['status','=',1],['name|author','like','%'.$keyword.'%']];
			$field = 'id,name,cover,summary,category';
			$list = Db::name('Book')->where($where)->field($field)->order('hot_num','desc')->select();
			if($list){
				foreach ($list as &$v){
					$category = $v['category'] ? explode(',', trim($v['category'],',')) : [];
					$v['category'] = !empty($category) ? $category : '';
					$v['info_url'] = my_url('Book/info',['book_id'=>$v['id']]);
				}
			}else{
				$list = '';
			}
			res_table($list);
		}else{
			$list = [];
			$cache = myCache::getBookRecommendIds();
			if($cache){
				$ids = array_slice($cache, 0,6);
				foreach ($ids as $book_id){
					$book = myCache::getBook($book_id);
					if($book && $book['status'] == 1){
						$list[] = $book['name'];
					}
				}
			}
			return $this->fetch('search/index',['hot_search' => $list]);
		}
	}
	
}