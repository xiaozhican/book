<?php
namespace app\index\controller;

use site\myDb;
use site\myMsg;
use site\myImg;
use site\myHttp;
use site\myCache;
use site\myValidate;
use app\common\model\mdMember;

class User extends Common{
	
	//构造函数
	public function __construct(){
		parent::__construct();
		parent::checkLogin();
		
	}
    
    //个人中心
    public function index(){
        global $loginId;
        myCache::changeMember($loginId);//修改金币缓存和删除用户信息缓存
        $cur = myCache::getMember($loginId);
        $cur['money'] = myCache::getMemberMoney($loginId);
        if($cur['phone']){
        	$cur['phone'] = substr_replace($cur['phone'], '****', 3, 4);
        }
        $cur['feedback_msg'] = '';
        $is_adopt = myCache::getMemberIsAdopt($loginId);
        if($is_adopt === 'yes'){
        	$cur['feedback_msg'] = '您的意见已被采纳，书币已到账';
        }
        $contact = getMyConfig('site_contact');
        $tel = ($contact && $contact['tel']) ? $contact['tel'] : '';
        $variable = ['cur' => $cur,'tel' => $tel,'date'=>date('Y-m-d')];
    	return $this->fetch('index',$variable);
    }
    
    //获取分享图片
    public function getShareImg(){
    	global $loginId;
    	$user = myCache::getMember($loginId);
    	$share_img = myImg::getShareImg($user['id'], $user['invite_code']);
    	if($share_img){
    		res_api(['url'=>$share_img]);
    	}else{
    		res_api('图片生成失败');
    	}
    }
    
    //签到
    public function sign(){
    	global $loginId;
    	$config = getMyConfig('site_sign');
    	if($config['is_sign'] != 1){
    		res_error('签到功能未启用');
    	}
    	$signInfo = myCache::getUserSignInfo($loginId);
    	if($this->request->isAjax()){
    		if($signInfo['cur_sign'] === 'yes'){
    			res_api();
    		}
    		$data = [
    			'uid' => $loginId,
    			'date' => date('Ymd'),
    			'create_time' => time()
    		];
    		foreach ($signInfo['list'] as &$v){
    			if($v['is_sign'] === 'no'){
    				$data['money'] = $v['money'];
    				$data['days'] = $v['day'];
    				$v['is_sign'] = 'yes';
    				break;
    			}
    		}
    		if(!isset($data['money'])){
    			res_api('签到参数有误');
    		}
    		$re = mdMember::doSign($data, $loginId);
    		if($re){
    			$signInfo['add_coin'] = intval($data['money']);
    			$signInfo['cur_sign'] = 'yes';
    			$key = md5($data['date'].'_signuser_'.$loginId);
    			cache($key,$signInfo,86400);
    			mdMember::addChargeLog($loginId, '+'.$data['money'].' 书币', '签到送书币');
    			res_api($signInfo);
    		}else{
    			res_api('签到失败');
    		}
    	}else{
    		$variable = ['cur'=>$signInfo];
    		return $this->fetch('sign',$variable);
    	}
    }
    
    //消费记录
    public function consume(){
    	if($this->request->isAjax()){
    		global $loginId;
    		$page = myHttp::postId('分页','page');
    		$list = mdMember::getMyConsumeList($loginId, $page);
    		$list = $list ? : '';
    		res_table($list);
    	}else{
    		
    		return $this->fetch();
    	}
    }
    
    //充值记录
    public function charge(){
    	if($this->request->isAjax()){
    		global $loginId;
    		$page = myHttp::postId('分页','page');
    		$list = mdMember::getMyChargeList($loginId, $page);
    		$list = $list ? : '';
    		res_table($list);
    	}else{
    		return $this->fetch();
    	}
    }
    
    //阅读历史
    public function readHistory(){
    	if($this->request->isAjax()){
    		global $loginId;
    		$list = mdMember::getReadHistory($loginId);
    		$list = $list ? : '';
    		res_table($list);
    	}else{
    		
    		return $this->fetch('readHistory');
    	}
    }
    
    //书架
    public function bookshelf(){
    	if($this->request->isAjax()){
    		global $loginId;
    		$list = mdMember::getMyCollect($loginId);
    		$list = $list ? : '';
    		res_table($list);
    	}else{
    		
    		return $this->fetch('bookshelf');
    	}
    }
    
    //绑定手机号
    public function bindPhone(){
    	global $loginId;
    	$user = myCache::getMember($loginId);
    	if($this->request->isAjax()){
    		$rules = [
    			'phone' => ["require|mobile",['require'=>'请输入手机号','mobile'=>'请输入正确格式的手机号']],
    			'code' => ["require|number|length:4",['require'=>'请输入短信验证码','number'=>'验证码为4位纯数字','length'=>'验证码为4位纯数字']],
    			'password' => ['require|length:6,16',['require'=>'请输入密码','length'=>'请输入6到16位任意字符密码']]
    		];
    		$post = myValidate::getData($rules);
    		$flag = myMsg::check($post['phone'], $post['code']);
    		if(is_string($flag)){
    			res_api($flag);
    		}
    		if($user['phone']){
    			res_api('您已绑定手机号');
    		}
    		$repeat = myDb::getValue('Member', [['phone','=',$post['phone']]], 'id');
    		if($repeat){
    			res_api('该手机号已被绑定');
    		}
    		$data = [
    			'phone' => $post['phone'],
    			'password' => createPwd($post['password'])
    		];
    		$re = myDb::save('Member', [['id','=',$user['id']]], $data);
    		if($re){
    			myCache::updateMember($user['id'], 'phone', $data['phone']);
    			res_api();
    		}else{
    			res_api('绑定失败');
    		}
    	}else{
    		if($user['phone']){
    			$this->redirect('index');
    		}
    		return $this->fetch('bindPhone');
    	}
    }
    
    //绑定手机号
    public function changePhone(){
    	global $loginId;
    	$user = myCache::getMember($loginId);
    	if($this->request->isAjax()){
    		$rules = [
    			'phone' => ["require|mobile",['require'=>'请输入手机号','mobile'=>'请输入正确格式的手机号']],
    			'code' => ["require|number|length:4",['require'=>'请输入短信验证码','number'=>'验证码为4位纯数字','length'=>'验证码为4位纯数字']],
    		];
    		$post = myValidate::getData($rules);
    		$flag = myMsg::check($post['phone'], $post['code']);
    		if(is_string($flag)){
    			res_api($flag);
    		}
    		if(!$user['phone']){
    			res_api('非法请求');
    		}
    		$repeat = myDb::getValue('Member', [['phone','=',$post['phone']]], 'id');
    		if($repeat){
    			res_api('该手机号已被绑定');
    		}
    		$re = myDb::setField('Member', [['id','=',$user['id']]], 'phone',$post['phone']);
    		if($re){
    			myCache::updateMember($user['id'], 'phone', $post['phone']);
    			res_api(['type'=>1]);
    		}else{
    			res_api('绑定失败');
    		}
    	}else{
    		if(!$user['phone']){
    			$this->redirect('index');
    		}
    		return $this->fetch('codePhone',['title'=>'更改绑定手机','btn'=>'立即绑定']);
    	}
    }
    
    
    //设置
    public function set(){
    	if($this->request->isAjax()){
    		
    	}else{
    		$config = getMyConfig('website');
    		return $this->fetch('set',['name'=>$config['name']]);
    	}
    }
    
    //关于我们
    public function about(){
    	$site = getMyConfig('website');
    	$contact = getMyConfig('site_contact');
    	if($contact){
    		$cur['email'] = $contact['email'];
    		$cur['tel'] = $contact['tel'];
    	}else{
    		$cur = myDb::buildArr('email,tel');
    	}
    	$cur['logo'] = $site['logo'];
    	$cur['name'] = $site['name'];
    	$cur['url'] = $site['url'];
    	return $this->fetch('about',['cur'=>$cur]);
    }
    
    
    //反馈意见
    public function feedback(){
    	global $loginId;
    	$user = myCache::getMember($loginId);
    	if($this->request->isAjax()){
    		$rules = [
    			'content' => ['require|max:100',['require'=>'请输入反馈内容','max'=>'反馈内容最大支持100个字符']],
    			'phone' => ['require|mobile',['require'=>'请输入手机号','mobile'=>'手机号格式不规范']]
    		];
    		$data = myValidate::getData($rules);
    		$data['content'] = htmlspecialchars($data['content']);
    		$data['channel_id'] = $user['channel_id'];
    		$data['agent_id'] = $user['agent_id'];
    		$data['uid'] = $user['id'];
    		$data['create_time'] = time();
    		$re = myDb::add('Feedback', $data);
    		if($re){
    			myCache::setMemberIsAdopt($loginId);
    			res_api();
    		}else{
    			res_api('提交失败');
    		}
    	}else{
    		
    		return $this->fetch('feedback',['phone'=>$user['phone']]);
    	}
    }
   
}