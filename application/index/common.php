<?php

//构建分类选项html
function createCategoryHtml($option){
	$str = '';
	foreach ($option as $k=>$v){
		if(is_array($v)){
			$str .= '<div class="type_box">';
			$str .= '<div class="left">';
			$titleCheck = $v['title']['is_check'] == 1 ? 'checked="checked"' : '';
			$str .= '<input type="radio" id="'.$k.'" name="'.$k.'" value="'.$v['title']['val'].'" '.$titleCheck.' hidden>';
			$str .= '<label for="'.$k.'">'.$v['title']['name'].'</label>';
			$str .= '</div>';
			$str .= '<div class="right">';
			foreach ($v['child'] as $key=>$val){
				$checkStr = $val['is_check'] == 1 ? 'checked="checked"' : '';
				$str .= '<input type="radio" id="'.$k.'_'.$key.'" name="'.$k.'" value="'.$val['val'].'" '.$checkStr.' hidden>';
				$str .= '<label for="'.$k.'_'.$key.'">'.$val['name'].'</label>';
			}
			$str .= '</div>';
			$str .= '</div>';
		}else{
			$str .= '<input type="hidden" name="'.$k.'" value="'.$v.'" />';
		}
	}
	return $str;
}

//创建底部导航
function createFooterHtml($id){
	$str = '';
	$list = [
		['id'=>1,'name'=>'书架','url'=>url('user/bookshelf')],
		['id'=>2,'name'=>'小说','url'=>'/Index'],
		['id'=>3,'name'=>'漫画','url'=>'/index/index/cartoon'],
		['id'=>4,'name'=>'充值','url'=>url('charge/index')],
		['id'=>5,'name'=>'我的','url'=>url('user/index')]
	];
	$img_path = '/static/resource/index/images/';
	$str .= '<div class="footer">';
	$config = getMyConfig('website');
	foreach ($list as $v){
		if($v['id'] == 3 && $config['is_cartoon'] != 1){
			continue;
		}
		$class = ($id == $v['id']) ? 'active' : '';
		$url = $class ? 'javascript:void(0);' : $v['url'];
		$str .= '<a href="'.$url.'" class="footer_item">';
		$img_name = $id == $v['id'] ? 'footer_'.$v['id'].'_a.png' : 'footer_'.$v['id'].'.png';
		$str .= '<img src="'.$img_path.$img_name.'">';
		$str .= '<span class="'.$class.'">'.$v['name'].'</span>';
		$str .= '</a>';
	}
	$str .= '</div>';
	return $str;
}

function createComplaintList($book_id){
	$list = [
		['id'=>1,'name'=>'色情'],
		['id'=>2,'name'=>'血腥'],
		['id'=>3,'name'=>'暴力'],
		['id'=>4,'name'=>'违法'],
		['id'=>5,'name'=>'盗版'],
		['id'=>6,'name'=>'其他'],
	];
	$str = '';
	foreach ($list as $v){
		$str .= '<li class="ripple" onclick="javascript:doComplaint('.$v['id'].','.$book_id.');"><a href="javascript:;">'.$v['name'].'</a></li>';
	}
	return $str;
}

function createAttentionHtml($qrcode){
	$str = '';
	$str .= '<div class="chaseBook">';
	$str .= '<div class="chaseBookTop">';
	$str .= '<p class="chaseORange">长按作者授权公众号继续阅读</p>';
	$str .= '<div class="greyEWM"><img src="'.$qrcode.'" alt=""> </div>';
	$str .= '<p class="chaseBlack" style="font-weight:bold;color:red;">长按识别上方二维码关注</p>';
	$str .= '<p class="chaseBlack" style="font-weight:bold;color:red;">继续免费阅读下一章</p>';
	$str .= '</div>';
	$str .= '<div class="today">';
	$str .= '<a href="javascript:;" s onclick="javascript:layer.closeAll();">确认</a>';
	$str .= '</div>';
	$str .= '</div>';
	return $str;
}

function createNoAttentionHtml($qrcode){
	$str = '';
	$str .= '<div class="chaseBook">';
	$str .= '<div class="chaseBookTop">';
	$str .= '<p class="chaseORange" style="font-size:.4rem;line-height:1rem;color:#ff6f00;">长按二维码，关注公众号<br />方便下次阅读</p>';
	$str .= '<div class="greyEWM"><img src="'.$qrcode.'" alt=""> </div>';
	$str .= '<p class="chaseBlack" style="font-size:.35rem;">如果您不喜欢,页面将在<font id="qrTime">10</font>s后自动关闭</p>';
	$str .= '</div>';
	$str .= '<div class="today">';
	$str .= '<a href="javascript:;" onclick="javascript:layer.closeAll();" style="font-size:.45rem;">关闭</a>';
	$str .= '</div>';
	$str .= '</div>';
	return $str;
}
	