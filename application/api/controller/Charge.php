<?php
namespace app\api\controller;

use think\Db;
use site\myHttp;
use weixin\wxpay;
use site\myCache;
use site\myValidate;
use app\common\model\mdOrder;

class Charge extends Common{
	
	//获取支付方式列表
	public function getPayTypeList(){
		checkToken();
		$list = [
			['key'=>'wxapppay','value'=>21,'name'=>'微信支付','is_check'=>2],
			['key'=>'aliapppay','value'=>22,'name'=>'支付宝支付','is_check'=>2]
		];
		$config = getMyConfig('website');
		$is_check = false;
		foreach ($list as $k=>&$v){
			$cur = getMyConfig($v['key']);
			if($cur && $cur['is_on'] == 1){
				if(!$is_check){
					if($v['value'] == $config['app_pay_type']){
						$v['is_check'] = 1;
						$is_check = true;
					}
				}
			}else{
				unset($list[$k]);
				continue;
			}
		}
		if($list){
			$list = array_values($list);
			if(!$is_check){
				$list[0]['is_check'] = 1;
			}
		}else{
			$list = [];
		}
		res_api($list);
	}
	
	//获取充值套餐列表
	public function getChargeList(){
		checkToken();
		global $loginId;
		$user = myCache::getMember($loginId);
		$charge = myCache::getCharge($user['wx_id']);
		if($charge){
			$content = myCache::getSiteCharge();
			foreach ($charge as $k=>&$v){
				$key = $v['pid'] ? : $v['id'];
				if(!isset($content[$key])){
					unset($charge[$k]);
					continue;
				}
				$v['money'] = $content[$key]['money'];
				if($content[$key]['pay_times']){
					$charge_num = myCache::getChargeNum($loginId, $v['id']);
					if($charge_num >= $content[$key]['pay_times']){
						unset($charge[$k]);
						continue;
					}
				}
				$v = array_merge($v,$content[$key]['content']);
			}
		}else{
			$charge = 'ok';
		}
		res_api($charge);
	}
	
	//书币充值
	public function doCharge(){
		checkToken();
		global $loginId;
		$rules = [
			'book_id' => ['number|gt:0',['number'=>'书籍参数异常','gt'=>'书籍参数异常']],
			'charge_id' => ['require|number',['require'=>'充值参数异常','number'=>'充值参数异常']],
			'charge_money' => ['require|float',['require'=>'充值金额异常','float'=>'充值金额异常']],
			'pay_type' => ['require|in:21,22',['require'=>'请选择支付方式','in'=>'未指定该支付方式']]
		];
		$post = myValidate::getData($rules);
		$chargePid = myCache::getChargePid($post['charge_id']);
		$content = myCache::getSiteCharge();
		if(!$content || !isset($content[$chargePid])){
			res_api('套餐已下架');
		}
		$time = time();
		$charge = $content[$chargePid];
		if($charge['pay_times']){
			$chargeNum = myCache::getChargeNum($loginId, $post['charge_id']);
			if($chargeNum >= $charge['pay_times']){
				res_api('该套餐仅限充值'.$charge['pay_times'].'次');
			}
		}
		$member = myCache::getMember($loginId);
		$data = mdOrder::createOrderInfo($member);
		$data['charge_id'] = $post['charge_id'];
		$data['money'] = $charge['money'];
		if($charge['type'] == 1){
			$data['send_money'] = $charge['content']['coin'];
			if($charge['content']['send_coin']){
				$data['send_money'] += $charge['content']['send_coin'];
			}
		}else{
			$data['package'] = $charge['content']['vip_type'];
		}
		$data['type'] = 1;
		$data['create_date'] = date('Ymd',$time);
		$data['create_time'] = $time;
		$data['order_no'] = mdOrder::createOrderNo();
		switch ($post['pay_type']){
			case 21:
				$pay_config = getMyConfig('wxapppay');
				break;
			case 22:
				$pay_config = getMyConfig('aliapppay');
				break;
		}
		if(!$pay_config){
			res_api('未配置支付参数');
		}
		$data['pay_type'] = $post['pay_type'];
		if($post['book_id']){
			$data['ptype'] = 1;
			$data['pid'] = $post['book_id'];
		}
		$count_info = [];
		if($data['channel_id']){
			$curInfo =  mdOrder::getCountInfo($data['channel_id'], $data['money']);
			if($curInfo){
				$count_info['channel'] = $curInfo;
			}
		}
		if($data['agent_id']){
			$curInfo = mdOrder::getCountInfo($data['agent_id'], $data['money']);
			if($curInfo){
				$count_info['agent'] = $curInfo;
			}
		}
		$data['count_info'] = json_encode($count_info);
		$order_id = Db::name('Order')->insertGetId($data);
		if($order_id){
			$order = ['id'=>$order_id,'order_no'=>$data['order_no'],'money'=>$data['money'],'pay_type'=>$data['pay_type']];
			switch ($data['pay_type']){
				case 21:
					self::doWxPay($pay_config,$order);
					break;
				case 22:
					self::doAlipay($pay_config,$order);
					break;
			}
		}else{
			res_api('创建订单失败');
		}
	}
	
	//活动充值
	public function doActivityCharge(){
		checkToken();
		global $loginId;
		$member = myCache::getMember($loginId);
		$activity_id = myHttp::postId('活动','activity_id');
		$activity = myCache::getActivity($activity_id);
		if(!$activity){
			res_api('该活动不存在');
		}
		if($activity['is_first'] == 1){
			$acUids = myCache::getActivityUids($activity_id);
			if(in_array($loginId, $acUids)){
				res_api('该活动仅限充值一次');
			}
		}
		$time = time();
		if($activity['start_time'] > $time){
			res_api('该活动尚未开始');
		}
		if($activity['end_time'] < $time){
			res_api('该活动已结束');
		}
		if(floatval($activity['money']) <= 0){
			res_api('充值金额异常');
		}
		$data = mdOrder::createOrderInfo($member);
		$data['type'] = 2;
		$data['create_time'] = $time;
		$data['order_no'] = mdOrder::createOrderNo();
		$data['ptype'] = 0;
		$data['pid'] = $activity['id'];
		$data['money'] = $activity['money'];
		$data['send_money'] = $activity['send_money'];
		if(floatval($data['money']) <= 0){
			res_api('充值金额异常');
		}
		$config = getMyConfig('website');
		$data['pay_type'] = $config['app_pay_type'];
		switch ($data['pay_type']){
			case 21:
				$pay_config = getMyConfig('wxapppay');
				break;
			case 22:
				$pay_config = getMyConfig('aliapppay');
				break;
		}
		if(!$pay_config){
			res_api('未配置支付参数');
		}
		$count_info = [];
		if($data['channel_id']){
			$curInfo =  mdOrder::getCountInfo($data['channel_id'], $data['money']);
			if($curInfo){
				$count_info['channel'] = $curInfo;
			}
		}
		if($data['agent_id']){
			$curInfo = mdOrder::getCountInfo($data['agent_id'], $data['money']);
			if($curInfo){
				$count_info['agent'] = $curInfo;
			}
		}
		$data['count_info'] = json_encode($count_info);
		$order_id = Db::name('Order')->insertGetId($data);
		if($order_id){
			$order = ['id'=>$order_id,'order_no'=>$data['order_no'],'money'=>$data['money'],'pay_type'=>$data['pay_type']];
			switch ($data['pay_type']){
				case 21:
					self::doWxPay($pay_config,$order);
					break;
				case 22:
					self::doAlipay($pay_config,$order);
					break;
			}
		}else{
			res_api('创建订单失败');
		}
	}
	
	//处理微信支付
	private function doWxPay($config,$order){
		wxpay::$config = $config;
		$params = [
			'body' => '购买商品',
			'out_trade_no' => $order['order_no'],
			'total_fee' => $order['money']*100,
			'attach' => $order['id'],
			'trade_type' => 'APP',
			'notify_url' => 'http://'.$_SERVER['HTTP_HOST'].'/index/Notify/wxpayApp.html'
		];
		$paymsg = wxpay::getPayMsg($params);
		if(!$paymsg){
			res_api('发起支付失败，请重试');
		}
		res_api(['pay_msg'=>$paymsg,'pay_type'=>intval($order['pay_type'])]);
	}
	
	//处理支付宝支付
	private function doAlipay($config,$order){
		$path = env('root_path').'extend/alipay/AopSdk.php';
		require_once $path;
		$aop = new \AopClient();
		$aop->gatewayUrl = "https://openapi.alipay.com/gateway.do";
		$aop->appId = $config['appId'];
		$aop->rsaPrivateKey = $config['rsaPrivateKey'];
		$aop->format = "json";
		$aop->charset = "UTF-8";
		$aop->signType = "RSA2";
		$aop->alipayrsaPublicKey = $config['alipayrsaPublicKey'];
		$request = new \AlipayTradeAppPayRequest();
		$bizconntent = [
			'subject' => '购买商品',
			'out_trade_no' => $order['order_no'],
			'total_amount' => $order['money'],
			'passback_params' => $order['id']
		];
		$notify_url = 'http://'.$_SERVER['HTTP_HOST'].'/index/Notify/alipayApp.html';
		$request->setNotifyUrl($notify_url);
		$request->setBizContent(json_encode($bizconntent,JSON_UNESCAPED_UNICODE));
		$response = $aop->sdkExecute($request);
		if($response){
			res_api(['pay_msg'=>$response,'pay_type'=>intval($order['pay_type'])]);
		}else{
			res_api('发起支付失败，请重试');
		}
	}
}