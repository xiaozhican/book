<?php
namespace app\api\controller;

use think\Db;
use think\Controller;
use think\facade\Cache;
use site\myCache;
use site\myDb;
use app\common\model\mdBook;

class Test extends Controller{
	
	public function cache(){
		$list = mdBook::getAreaBookList(1, 1);
		my_print($list);
	}
	
	public function bookList(){
		$list = Db::name('BookCat a')
		->join('book b','a.book_id=b.id','left')
		->field('a.sync_id as book_id,b.name')
		->select();
		echo json_encode($list,JSON_UNESCAPED_UNICODE);
		exit;
	}
	
	public function setHot(){
		$num = 0;
		$list = Db::name('Book')->where('hot_num','<',1000)->field('id,name')->select();
		foreach ($list as $v){
			$hot_num = mt_rand(40000,160000);
			$re = Db::name('Book')->where('id',$v['id'])->setField('hot_num',$hot_num);
			if($re){
				$num++;
				myCache::rmBookHotNum($v['id']);
			}
		}
		echo $num;
	}
	
	
	public function test(){
		
		$loginId = 2;
		$user = myCache::getMember($loginId);
		$user_money = myCache::getMemberMoney($loginId);
		$book_id = 1;
		$book = myCache::getBook($book_id);
		if(!$book){
			res_api('书籍不存在');
		}
		$money = 0;
		$list = myCache::getBookChapterList($book['id']);
		if(!$list){
			res_api('书籍章节不存在');
		}
		$temp = [];
		$filepath = env('root_path').'static/block/book/'.$book['id'];
		$is_free = myCache::checkBookFree($book['id']);
		$read_cache = myCache::getReadCache($loginId);
		$read_key = 'book_'.$book['id'];
		$read_numbers = isset($read_cache[$read_key]) ? $read_cache[$read_key]['numbers'] : [];
		$cur_time = time();
		$is_vip = false;
		if($user['viptime']){
			if($user['viptime'] == 1 || $user['viptime'] > $cur_time){
				$is_vip = true;
			}
		}
		$is_download = false;
		$date = date('Y-m-d');
		if($is_vip){
			$download = myDb::getList('BookDownload', [['uid','=',$user['id']],['create_date','=',$date]]);
			if($download){
				if(count($download) >= 2){
					$downloadBookIds = array_column($download, 'book_id');
					if(!in_array($book['id'], $downloadBookIds)){
						res_api('VIP用户每日最多下载两次');
					}
				}
			}
		}
		foreach ($list as $v){
			$name = $v['number'].'.html';
			$filename = $filepath.'/'.$name;
			if(@is_file($filename)){
				$one = ['filename'=>$filename,'name'=>$name];
				if(!in_array($v['number'], $read_numbers)){
					$one['is_read'] = 2;
					if(!$is_vip){
						if(!$is_free && $book['free_type'] == 2){
							if($book['free_chapter'] < $v['number']){
								$money += $v['money'] ? : $book['money'];
							}
						}
					}
					$temp[] = $one;
				}
			}
		}
		if(empty($temp)){
			res_api('该书籍暂无可下载章节');
		}
		if($user_money < $money){
			res_api(['flag'=>2,'msg'=>'您的书币余额不足，请及时充值','zip_file'=>'']);
		}
		$zip = new \ZipArchive();
		$path = './files/zip/'.date('Ymd');
		$zip_name = $path.'/'.md5(microtime().mt_rand(0,99999999)).'.zip';
		if($zip->open($zip_name,\ZipArchive::CREATE) === true){
			foreach ($temp as $val){
				$zip->addFile($val['filename'],$val['name']);
			}
		}
		$zip->close();
		$data = [
			'uid' => $user['id'],
			'book_id' => $book['id'],
			'create_date' => $date
		];
		Db::startTrans();
		$flag = false;
		$re = Db::name('BookDownload')->insert($data);
		if($re){
			if($money){
				$res = Db::name('Member')->where('id',$user['id'])->setDec('money',$money);
				if($res){
					$flag = true;
				}
			}else{
				$flag = true;
			}
		}
		if($flag){
			Db::commit();
			if($money){
				myCache::doMemberMoney($user['id'], $money,'dec');
			}
		}else{
			Db::rollback();
		}
		$re = myDb::add('BookDownload', $data);
		if($re){
			$zip_file = 'http://'.$_SERVER['HTTP_HOST'].ltrim($zip_name);
			res_api(['flag'=>1,'zip_file'=>$zip_file]);
		}else{
			@unlink($zip_name);
			res_api('生成下载文件失败');
		}
		
	}
	
}