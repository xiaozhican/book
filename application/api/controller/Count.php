<?php


namespace app\api\controller;


use site\myValidate;
use think\Db;
use think\facade\Log;

class Count extends Common
{
    //统计在线访问人数
    public function countOnLine()
    {
        //checkSign('get');
//        $rules = [
//            'field' => ['require',['require'=>'请输入字段名']],
//            //'activity' => ['require|number',['require'=>'请输入数字','number'=>'格式错误']],//activity默认为0，说明不统计，为1，说明需要统计
//        ];
//        $get = myValidate::getData($rules,'get');
        $field = $this->request->get('field');
        //var_dump($field);
        $date = date('Y-m-d');
        //$table = Db::name('Count');
        $data = Db::name('Count')->where('date_time',$date)->find();//查找当天有无数据
        if(!$data){
            //如果没有，则插入一条
            $rs = Db::name('Count')->insert([
                'date_time' => $date,
                $field => 1
            ]);
        }else{
            //如果有数据，则相应的字段+1
            $rs = Db::name('Count')->where('date_time',$date)->setInc($field);
        }
//        if($get['activity'] > 0){
//            Db::name('Count')->where('date_time',$date)->setInc('day_activity');//日活人数
//        }
        if($rs){
            res_api('ok',1,[]);
        }else{
            res_api('操作失败',-1,[]);
        }
    }
    //统计日活人数
    public function countActivity()
    {
        checkToken(false);
//        $rules = [
//            'activity' => ['require|number',['require'=>'请输入数字','number'=>'格式错误']],//activity默认为0，说明不统计，为1，说明需要统计
//        ];
//        $get = myValidate::getData($rules,'get');
        $date = date('Y-m-d');
        //$table = Db::name('Count');
        $data = Db::name('Count')->where('date_time',$date)->find();//查找当天有无数据
        if(!$data){
            //如果没有，则插入一条
            $res = Db::name('Count')->insert([
                'date_time' => $date,
                'day_activity' => 1
            ]);
        }else{
            //如果有数据，则相应的字段+1
            $res = Db::name('Count')->where('date_time',$date)->setInc('day_activity');//日活人数
        }
        if($res){
            res_api('ok',1,[]);
        }else{
            res_api('操作失败',-1,[]);
        }
    }
    //渠道下载app
    public function appDownload()
    {
        //$channel = $this->request->get('channel');//渠道名称
        //$uuid = $this->request->get('uuid');
        checkSign('get');
        checkToken(false);
        $rules = [
            'channel' => ['require',['require'=>'非法访问']],
            'uuid' => ['require',['require'=>'非法访问']],
        ];
        $get = myValidate::getData($rules,'get');
        $channel = $get['channel'];
        $uuid = $get['uuid'];
        Log::info($channel);
        Log::info($uuid);
        $flag = false;
        //$appChannel = Db::name('AppDownload')->where(['channel' => $channel, 'datetime' => date('Y-m-d')])->find();//查找当前日期的渠道是否存在
        $appChannel = Db::name('AppDownload')->where('channel',$channel)->find();//查找当前日期的渠道是否存在
        if (!$appChannel) {
            Db::startTrans();
            //如果不存在，则写入数据表
            $app_download_id = Db::name('AppDownload')->insertGetId([
                'channel' => $channel,
                'date_time' => time(),
                'datetime' => date('Y-m-d'),
                'num' => 1
            ]);
            if ($app_download_id) {
                //写入渠道下载详情表
                $res = Db::name('AppDownloadDetail')->insert([
                    'app_download_id' => $app_download_id,
                    'channel' => $channel,
                    'date_time' => date('Y-m-d H:i:s'),
                    'uuid' => $uuid,
                    'datetime' => date('Y-m-d')
                ]);
                if ($res) {
                    $flag = true;
                }
            }
            if ($flag) {
                Db::commit();
            } else {
                Db::rollback();
            }
        } else {
            Db::startTrans();
            $num = Db::name('AppDownload')->where('channel',$channel)->setInc('num');//下载次数+1
            if ($num) {
                //写入渠道下载详情表
                $res = Db::name('AppDownloadDetail')->insert([
                    'app_download_id' => $appChannel['id'],
                    'channel' => $channel,
                    'date_time' => date('Y-m-d H:i:s'),
                    'uuid' => $uuid,
                    'datetime' => date('Y-m-d')
                ]);
                if ($res) {
                    $flag = true;
                }
            }
            if ($flag) {
                Db::commit();
            } else {
                Db::rollback();
            }

        }
        if($flag){
            res_api('ok',1,[]);
        }else{
            res_api('操作失败',-1,[]);
        }
    }
    //统计注册人数
    public function countReg()
    {
        $channels = ['default','xiaomi','oppo','meizu','huawei','vivo'];
        /*

        $date = '2020-11-22';
        foreach ($channels as $channel){
            $day_count = Db::name('Member')->where('channel',$channel)->whereBetween('create_time',[strtotime($date.' 00:00:00'),strtotime($date.' 23:59:59')])->count();//统计今日渠道注册人数
            $order = Db::name('ChannelReg')->where('date',$date)->find();//查找今日是否有数据
            if(!$order){
                Db::name('ChannelReg')->insert([
                    'date' => $date,
                    $channel => $day_count
                ]);
            }else{
                Db::name('ChannelReg')->where('date',$date)->update([$channel=>$day_count]);
            }
        }
        die;
        */
        $date = date('Y-m-d');
        $channel_reg = Db::name('ChannelReg')->order('id desc')->limit(1)->find();
        if(!$channel_reg){
            foreach ($channels as $channel){
                $zuotian_count = Db::name('Member')->where('channel',$channel)->whereBetween('create_time',[strtotime(date('Y-m-d 00:00:00',strtotime('-1 day'))),strtotime(date('Y-m-d 23:59:59',strtotime('-1 day')))])->count();//统计昨天的渠道注册人数
                $zuotian = date('Y-m-d',strtotime('-1 day'));//昨天日期
                $z_order = Db::name('ChannelReg')->where('date',$zuotian)->find();//查找昨天是否有数据
                if(!$z_order){
                    Db::name('ChannelReg')->insert([
                        'date' => $zuotian,
                        $channel => $zuotian_count
                    ]);
                }else{
                    Db::name('ChannelReg')->where('date',$date)->update([$channel=>$zuotian_count]);
                }

                $day_count = Db::name('Member')->where('channel',$channel)->whereBetween('create_time',[strtotime($date.' 00:00:00'),strtotime($date.' 23:59:59')])->count();//统计今日渠道注册人数
                $order = Db::name('ChannelReg')->where('date',$date)->find();//查找今日是否有数据
                if(!$order){
                    Db::name('ChannelReg')->insert([
                        'date' => $date,
                        $channel => $day_count
                    ]);
                }else{
                    Db::name('ChannelReg')->where('date',$date)->update([$channel=>$day_count]);
                }
            }
        }else{
            if($channel_reg['date'] == $date){
                //如果表中最近的日期是今天，则修改数据
                foreach ($channels as $channel){
                    $day_count = Db::name('Member')->where('channel',$channel)->whereBetween('create_time',[strtotime($date.' 00:00:00'),strtotime($date.' 23:59:59')])->count();//统计今日渠道注册人数
                    Db::name('ChannelReg')->where('date',$date)->update([$channel=>$day_count]);
                }
            }elseif ($channel_reg['date'] == date('Y-m-d',strtotime('-1 day'))){
                //如果表中最近的日期是昨天，说明今天还未有数据，则先插入
                foreach ($channels as $channel){
                    $day_count = Db::name('Member')->where('channel',$channel)->whereBetween('create_time',[strtotime($date.' 00:00:00'),strtotime($date.' 23:59:59')])->count();//统计今日渠道注册人数
                    $order = Db::name('ChannelReg')->where('date',$date)->find();//查找今日是否有数据
                    if(!$order){
                        Db::name('ChannelReg')->insert([
                            'date' => $date,
                            $channel => $day_count
                        ]);
                    }else{
                        Db::name('ChannelReg')->where('date',$date)->update([$channel=>$day_count]);
                    }
                }
            }else{
                //如果是几天前，则循环添加到当天
                $begin = $channel_reg['date'];
                $begintime = strtotime($begin);$endtime = strtotime($date);
                for ($start = $begintime; $start <= $endtime; $start += 24 * 3600) {
                    $date_time = date("Y-m-d", $start);
                    //var_dump($date_time);
                    foreach ($channels as $channel){
                        $day_count = Db::name('Member')->where('channel',$channel)->whereBetween('create_time',[strtotime($date_time.' 00:00:00'),strtotime($date_time.' 23:59:59')])->count();//统计今日渠道注册人数
                        $order = Db::name('ChannelReg')->where('date',$date_time)->find();//查找今日是否有数据
                        if(!$order){
                            Db::name('ChannelReg')->insert([
                                'date' => $date_time,
                                $channel => $day_count
                            ]);
                        }else{
                            Db::name('ChannelReg')->where('date',$date_time)->update([$channel=>$day_count]);
                        }
                    }
                }
            }
        }
    }
    //统计回头率
    public function countHuitou()
    {
        /*
        $member = Db::name('Member')->where('phone','15880043714')->find();
        $channel = 'default';
        $type = 'android';
        $time = time();
        $is_start = Db::name('AppStart')->where(['channel'=>$channel,'phone'=>$member['phone'],'type'=>$type])->find();
        if($is_start){
            if(date('Y-m-d',$member['create_time']) == date('Y-m-d',strtotime('-1 day'))){
                //如果用户是昨天注册的
                Db::name('AppStart')->insert([
                    'channel' => $channel,
                    'phone' => $member['phone'],
                    'date_time' => $time,
                    'update_time' => $time,
                    'phone_register' => $member['create_time'],
                    'type' => $type
                ]);
            }
//                        else{
//                            Db::name('AppStart')->where(['channel'=>$channel,'phone'=>$member['phone'],'type'=>$type])->update([
//                                'update_time' => $time
//                            ]);
//                        }
        }else{
            Db::name('AppStart')->insert([
                'channel' => $channel,
                'phone' => $member['phone'],
                'date_time' => $time,
                'update_time' => $time,
                'phone_register' => $member['create_time'],
                'type' => $type
            ]);
        }

        Db::name('AppStartDetail')->insert([
            'channel' => $channel,
            'phone' => $member['phone'],
            'date_time' => $time,
            'phone_register' => $member['create_time'],
            'phone_channel' => $member['channel'],
            'type' => $type
        ]);
        die;
        */
        $channels = ['default','xiaomi','oppo','meizu','huawei','vivo'];
        $huitou = Db::name('Huitou')->order('id desc')->limit(1)->find();
        //$huitou_rate = Db::name('HuitouRate')->order('id desc')->limit(1)->find();
        $zt_start = strtotime(date('Y-m-d 00:00:00',strtotime('-1 day')));//昨天开始时间
        $zt_end = strtotime(date('Y-m-d 23:59:59',strtotime('-1 day')));//昨天结束时间
        $day_start = strtotime(date('Y-m-d 00:00:00'));//今日开始时间
        $day_end = strtotime(date('Y-m-d 23:59:59'));//今日结束时间
        $zt_date = date('Y-m-d',strtotime('-1 day'));//昨天日期
        $day_date = date('Y-m-d');//今天日期
        //$zt_huitou = Db::name('AppStart')->whereBetween('date_time',[$zt_start,$zt_end])->where(['channel'=>'default','type'=>'android'])->fetchSql(true)->count();
        //var_dump($zt_huitou);die;
        if(!$huitou){
            //如果数据空的，则统计昨天和今日的
            foreach ($channels as $channel){
                //var_dump($channel);
                //$zt_reg_count = Db::name('Member')->whereBetween('create_time',[$zt_start,$zt_end])->where('channel',$channel)->count();//统计昨天各渠道注册人数
                $zt_huitou = Db::name('AppStart')->whereBetween('date_time',[$zt_start,$zt_end])->where(['channel'=>$channel,'type'=>'android'])->count();//统计昨天回头访问人数
                //$day_reg_count = Db::name('Member')->whereBetween('create_time',[$day_start,$day_end])->where('channel',$channel)->count();//统计今日各渠道注册人数
                $day_huitou = Db::name('AppStart')->whereBetween('date_time',[$day_start,$day_end])->where(['channel'=>$channel,'type'=>'android'])->count();//统计今日回头访问人数
                //var_dump($zt_reg_count);
                $order = Db::name('Huitou')->where('date',$zt_date)->find();//查找是否有昨天的数据
                if(!$order){
                    Db::name('Huitou')->insert([
                        'date' => $zt_date,
                        $channel => $zt_huitou,
                    ]);
                }else{
                    Db::name('Huitou')->where('date',$zt_date)->update([
                        $channel => $zt_huitou
                    ]);
                }
                $day_order = Db::name('Huitou')->where('date',$day_date)->find();//查找是否有今日的数据
                if(!$day_order){
                    Db::name('Huitou')->insert([
                        'date' => $day_date,
                        $channel => $day_huitou
                    ]);
                }else{
                    Db::name('Huitou')->where('date',$day_date)->update([
                        $channel => $day_huitou
                    ]);
                }

//                //统计昨天回头率
//                $rate = Db::name('HuitouRate')->where('date',$zt_date)->find();
//                if(!$rate){
//                    Db::name('HuitouRate')->insert([
//                        'date' => $zt_date,
//                        $channel => $zt_huitou==0?0:($zt_huitou/$zt_reg_count)*100
//                    ]);
//                }else{
//                    Db::name('HuitouRate')->where('date',$zt_date)->update([
//                        $channel => $zt_huitou==0?0:($zt_huitou/$zt_reg_count)*100
//                    ]);
//                }
//                //统计今天回头率
//                $day_rate = Db::name('HuitouRate')->where('date',$day_date)->find();
//                if(!$rate){
//                    Db::name('HuitouRate')->insert([
//                        'date' => $day_date,
//                        $channel => $day_huitou==0?0:($day_huitou/$day_reg_count)*100
//                    ]);
//                }else{
//                    Db::name('HuitouRate')->where('date',$day_date)->update([
//                        $channel => $day_huitou==0?0:($day_huitou/$day_reg_count)*100
//                    ]);
//                }
            }
        }else{
            if($huitou['date'] == $day_date){
                //var_dump(11);
                foreach ($channels as $channel){
                    //$day_reg_count = Db::name('Member')->whereBetween('create_time',[$day_start,$day_end])->where('channel',$channel)->count();//统计今日各渠道注册人数
                    $day_huitou = Db::name('AppStart')->whereBetween('date_time',[$day_start,$day_end])->where(['channel'=>$channel,'type'=>'android'])->count();//统计今日回头访问人数
                    Db::name('Huitou')->where('date',$day_date)->update([
                        $channel => $day_huitou
                    ]);
//                    Db::name('HuitouRate')->where('date',$day_date)->update([
//                        $channel => $day_huitou==0?0:($day_huitou/$day_reg_count)*100
//                    ]);
                }
            }elseif ($huitou['date'] == $zt_date){
                //如果最后一个数据是昨天的，说明今天还未有数据
                foreach ($channels as $channel){
                    //$day_reg_count = Db::name('Member')->whereBetween('create_time',[$day_start,$day_end])->where('channel',$channel)->count();//统计今日各渠道注册人数
                    $day_huitou = Db::name('AppStart')->whereBetween('date_time',[$day_start,$day_end])->where(['channel'=>$channel,'type'=>'android'])->count();//统计今日回头访问人数
                    $day_order = Db::name('Huitou')->where('date',$day_date)->find();//查找是否有今日的数据
                    if(!$day_order){
                        Db::name('Huitou')->insert([
                            'date' => $day_date,
                            $channel => $day_huitou
                        ]);
                    }else{
                        Db::name('Huitou')->where('date',$day_date)->update([
                            $channel => $day_huitou
                        ]);
                    }
//                    $day_rate = Db::name('HuitouRate')->where('date',$day_date)->find();
//                    if(!$day_rate){
//                        Db::name('HuitouRate')->insert([
//                            'date' => $day_date,
//                            $channel => $day_huitou==0?0:($day_huitou/$day_reg_count)*100
//                        ]);
//                    }else{
//                        Db::name('HuitouRate')->where('date',$day_date)->update([
//                            $channel => $day_huitou==0?0:($day_huitou/$day_reg_count)*100
//                        ]);
//                    }
                }
            }else{
                //如果是几天前，则循环添加到当天
                $begin = $huitou['date'];
                $begintime = strtotime($begin);$endtime = strtotime($day_date);
                for ($start = $begintime; $start <= $endtime; $start += 24 * 3600) {
                    $date_time = date("Y-m-d", $start);
                    foreach ($channels as $channel){
                        $day_start1 = strtotime($date_time.' 00:00:00');//今日开始时间
                        $day_end1 = strtotime($date_time.' 23:59:59');//今日结束时间
                        //$day_reg_count = Db::name('Member')->whereBetween('create_time',[$day_start,$day_end])->where('channel',$channel)->count();//统计今日各渠道注册人数
                        $day_huitou = Db::name('AppStart')->whereBetween('date_time',[$day_start1,$day_end1])->where(['channel'=>$channel,'type'=>'android'])->count();//统计今日回头访问人数
                        $day_order = Db::name('Huitou')->where('date',$date_time)->find();//查找是否有今日的数据
                        if(!$day_order){
                            Db::name('Huitou')->insert([
                                'date' => $date_time,
                                $channel => $day_huitou
                            ]);
                        }else{
                            Db::name('Huitou')->where('date',$date_time)->update([
                                $channel => $day_huitou
                            ]);
                        }
                    }
                }
            }
        }
    }

    public function testApi()
    {
        $url1 = "https://api.qimai.cn/andapp/appinfo?analysis=diBXBCxKUwF%2BWlcNeEcYWQpQVxFAH1FAQFleXgt3G10EA1UEAAgDAgUDeEcG&appid=7637568&market=4";
        $list1 = file_get_contents($url1);
        $list1 = json_decode($list1,true);
        //var_dump($list1['appInfo']['market_name']);//小米
        //var_dump($list1['appInfo']['app_version']);//版本号
        $key1 = "app_xiaomi";
        //cache($key1,'1.0.5');die;
        if(cache('?'.$key1)){
            $app_xiaomi = cache($key1);
            if($app_xiaomi != $list1['appInfo']['app_version']){
                $res1 = Db::name('Notice')->where(['app_name'=>'Android'])->update([
                    'xiaomi_status'=>1,
                ]);
                if($res1){
                    cache($key1,$list1['appInfo']['app_version']);
                }
            }
        }else{
            cache($key1,$list1['appInfo']['app_version']);
        }

        $url2 = "https://api.qimai.cn/andapp/appinfo?analysis=dg5XBCxKUwF%2BWlcNeEcYWQpQVxFAH1FAQFleXgt3G10EA1UDCAEGCQcCeEcG&appid=7637568&market=6";
        $list2 = file_get_contents($url2);
        $list2 = json_decode($list2,true);
        //var_dump($list2['appInfo']['market_name']);//华为
        //var_dump($list2['appInfo']['app_version']);
        $key2 = "app_huawei";
        if(cache('?'.$key2)){
            $app_huawei = cache($key2);
            if($app_huawei != $list2['appInfo']['app_version']){
                $res2 = Db::name('Notice')->where(['app_name'=>'Android'])->update([
                    'huawei_status'=>1
                ]);
                if($res2){
                    cache($key2,$list2['appInfo']['app_version']);
                }
            }
        }else{
            cache($key2,$list2['appInfo']['app_version']);
        }

        $url3 = "https://api.qimai.cn/andapp/appinfo?analysis=dh5tTC9KZQJ%2FdFcNeEcYWQpQVxFAH1FAQFleXgt3G10EA1UEBQYDAQcJeEcG&appid=7637568&market=8";
        $list3 = file_get_contents($url3);
        $list3 = json_decode($list3,true);
        //var_dump($list3['appInfo']['market_name']);//vivo
        //var_dump($list3['appInfo']['app_version']);
        $key3 = "app_vivo";
        if(cache('?'.$key3)){
            $app_vivo = cache($key3);
            if($app_vivo != $list3['appInfo']['app_version']){
                $res3 = Db::name('Notice')->where(['app_name'=>'Android'])->update([
                    'vivo_status'=>1
                ]);
                if($res3){
                    cache($key3,$list3['appInfo']['app_version']);
                }
            }
        }else{
            cache($key3,$list3['appInfo']['app_version']);
        }

        $url4 = "https://api.qimai.cn/andapp/appinfo?analysis=dh5tTC9KZQJ%2FdFsNeEcYWQpQVxFAH1FAQFleXgt3G10EA1UEBgMIBAAAeEcG&appid=7637568&market=9";
        $list4 = file_get_contents($url4);
        $list4 = json_decode($list4,true);
        //var_dump($list['appInfo']['market_name']);//oppo
        //var_dump($list['appInfo']['app_version']);
        $key4 = "app_oppo";
        if(cache('?'.$key4)){
            $app_oppo = cache($key4);
            if($app_oppo != $list4['appInfo']['app_version']){
                $res4 = Db::name('Notice')->where(['app_name'=>'Android'])->update([
                    'oppo_status'=>1
                ]);
                if($res4){
                    cache($key4,$list4['appInfo']['app_version']);
                }
            }
        }else{
            cache($key4,$list4['appInfo']['app_version']);
        }

        $url5 = "https://api.qimai.cn/andapp/appinfo?analysis=dh5XBCxKUwF%2BWlcNeEcYWQpQVxFAH1FAQFleXgt3G10EA1UEBgkJAwAGeEcG&appid=7637568&market=7";
        $list5 = file_get_contents($url5);
        $list5 = json_decode($list5,true);
        //var_dump($list['appInfo']['market_name']);//魅族
        //var_dump($list['appInfo']['app_version']);3
        $key5 = "app_meizu";
        if(cache('?'.$key5)){
            $app_meizu = cache($key5);
            if($app_meizu != $list5['appInfo']['app_version']){
                $res5 = Db::name('Notice')->where(['app_name'=>'Android'])->update([
                    'meizu_status'=>1
                ]);
                if($res5){
                    cache($key5,$list5['appInfo']['app_version']);
                }
            }
        }else{
            cache($key5,$list5['appInfo']['app_version']);
        }

        $url6 = "https://api.qimai.cn/andapp/appinfo?analysis=dR5XBCxKUwF%2BWlcNeEcYWQpQVxFAH1FAQFleXgt3G10EA1UEBwUACAkAeEcG&appid=7637568&market=3";
        $list6 = file_get_contents($url6);
        $list6 = json_decode($list6,true);
        //var_dump($list['appInfo']['market_name']);//应用宝（default）
        //var_dump($list['appInfo']['app_version']);
        $key6 = "app_default";
        if(cache('?'.$key6)){
            $app_default = cache($key6);
            if($app_default != $list6['appInfo']['app_version']){
                $res6 = Db::name('Notice')->where(['app_name'=>'Android'])->update([
                    'status'=>1
                ]);
                if($res6){
                    cache($key6,$list6['appInfo']['app_version']);
                }
            }
        }else{
            cache($key6,$list6['appInfo']['app_version']);
        }
    }
}