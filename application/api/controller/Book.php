<?php
namespace app\api\controller;


use think\App;
use think\Cache;
use think\Db;
use site\myDb;
use site\myHttp;
use site\myCache;
use site\myValidate;
use app\common\model\mdBook;
use app\common\model\mdOrder;
use app\common\model\mdComments;

class Book extends Common{
	
	//获取书架推荐书籍列表
	public function getRecommendBookList(){
		checkSign('get');
		$rules = [
			'page' 	=> ['require|number|gt:0',['require'=>'分页参数异常','number'=>'分页参数异常','gt'=>'分页参数异常']],
			'limit' => ['require|number|gt:0',['require'=>'分页参数异常','number'=>'分页参数异常','gt'=>'分页参数异常']]
		];
		$list = [];
        $arr1 = [];
		$get = myValidate::getData($rules,'get');
		$cache = myCache::getBookRecommendIds();
		if($cache){
            //$notice_list = $this->getNotice();
			$page = $get['page'] - 1;
			$arr = array_chunk($cache, $get['limit']);
			if(isset($arr[$page])){

				foreach ($arr[$page] as $book_id){
					$book = myCache::getBook($book_id);
					if($book && $book['status'] == 1){
						$one = [
							'id' => $book['id'],
							'type' => intval($book['type']),
							'name' => $book['name'],
							'cover' => $book['cover'],
							'hot_num' => formatNumber(myCache::getBookHotNum($book_id))
						];
						$list[] = $one;
					}
				}
                //$list['notice_list'] = $notice_list;//广告列表
                //$arr1[] = $list;
			}
		}
		res_api($list);
	}
	
	//获取书籍首页轮播图
	public function getBanners(){
		checkSign('get');
		$rules = [
			'type'=>['require|in:1,2',['require'=>'请选择书籍类型','in'=>'未指定该书籍类型']],
			'gender_type' => ['require|in:1,2',['require'=>'未检测到请求性别','in'=>'未指定该读者性别']]
		];
		$get = myValidate::getData($rules,'get');
		$key = $get['type'] == 1 ? 'novel' : 'cartoon';
		$key .= '_banner_'.($get['gender_type'] == 1 ? 'man' : 'woman');
		$config = getMyConfig($key);
		if($config){
			foreach ($config as &$v){
				$v['book_id'] = 0;
				if($v['link']){
					if(substr($v['link'], 0, 4) !== 'http'){
						if(strpos($v['link'],'book_id=') !== false){ 
							preg_match('/book_id=\d{1,}/', $v['link'],$matchs);
							if(isset($matchs[0])){
								$params = explode('=', $matchs[0]);
								$v['book_id'] = $params[1];
							}
						}
						$v['link'] = '';
					}
				}
			}
		}else{
			$config = 'ok';
		}
		res_api($config);
	}
	
	//获取各发布区域书籍列表
	public function getAreaBookList(){
		checkSign('get');
		$rules = [
			'type'=>['require|in:1,2',['require'=>'请选择书籍类型','in'=>'未指定该书籍类型']],
			'gender_type' => ['require|in:1,2',['require'=>'未检测到请求性别','in'=>'未指定该读者性别']]
		];
		$get = myValidate::getData($rules,'get');
		$list = mdBook::getAreaBookList($get['type'], $get['gender_type']);
		res_api($list);
	}
	
	//获取更多书籍列表
	public function getAreaMoreList(){
		checkSign('get');
		$rules = [
			'page' => ['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数错误','gt'=>'分页参数错误']],
			'type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'gender_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'area' => ['require|max:20',['require'=>'参数错误','max'=>'参数错误']]
		];
		$get = myValidate::getData($rules,'get');
		$area_id = 0;
		switch ($get['area']){
			case '精品推荐':
				$area_id = 1;
				break;
			case '本周热搜':
				$area_id = 2;
				break;
			case '热门推荐':
				$area_id = 3;
				break;
			case '新书推荐':
				$area_id = 4;
				break;
		}
		$list = [];
		if($area_id){
			$res = mdBook::getAreaBookInfo($get['type'], $get['gender_type'], $area_id, $get['page'], 10);
			$list = $res['list'] ? $res['list'] : [];
		}
		res_api($list);		
	}
	
	//获取精选书籍
	public function getSelectedBookList(){
		checkSign('get');
		$rules = [
			'type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'gender_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
		];
		$get = myValidate::getData($rules,'get');
		$list = [];
		$cache = myCache::getHotIds($get['type'], $get['gender_type']);
		if($cache){
			$ids = array_slice($cache, 0,6);
			$list = mdBook::getListBookInfo($ids,false);
		}
		res_api($list);
	}
	
	//获取精选书籍
	public function getSelectedMoreList(){
		checkSign('get');
		$rules = [
			'page' => ['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数错误','gt'=>'分页参数错误']],
			'type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'gender_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
		];
		$get = myValidate::getData($rules,'get');
		$list = [];
		$cache = myCache::getHotIds($get['type'], $get['gender_type']);
		if($cache){
			$page = $get['page'] - 1;
			$arr = array_chunk($cache, 10);
			if(isset($arr[$page])){
				$list = mdBook::getListBookInfo($arr[$page],false);
			}
		}
		res_api($list);
	}
	
	//获取精选书籍
	public function getSelectedPageList(){
		checkSign('get');
		$rules = [
			'page' => ['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数错误','gt'=>'分页参数错误']],
			'type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'gender_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
		];
		$get = myValidate::getData($rules,'get');
		$list = [];
		$cache = myCache::getHotIds($get['type'], $get['gender_type']);
		if($cache){
			$page = $get['page'] - 1;
			$arr = array_chunk($cache, 6);
			if(isset($arr[$page])){
				$list = mdBook::getListBookInfo($arr[$page],false);
			}
		}
		res_api($list);
	}
	
	//获取出版书籍
	public function getPublishBookList(){
		checkSign('get');
		$rules = [
			'page' => ['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数错误','gt'=>'分页参数错误']],
			'type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'gender_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'over_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
		];
		$get = myValidate::getData($rules,'get');
		$page = $get['page'];
		$where = [
			['type','=',$get['type']],
			['status','=',1],
			['gender_type','=',$get['gender_type']],
            ['over_type','=',$get['over_type']]
			//['free_type','=',1]
		];
		$field = 'id,name,cover,summary,category';
		$list = Db::name('Book')->where($where)->field($field)->page($page,10)->order('hot_num','desc')->select();
		if($list){
			foreach ($list as &$v){
				$v['category'] = $v['category'] ? explode(',',trim($v['category'],',')) : [];
			}
		}
		$list = $list ? : [];
		res_api($list);
	}
	
	//获取限免书籍
	public function getFreeBookList(){
		checkSign('get');
		$rules = [
			'page' => ['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数错误','gt'=>'分页参数错误']],
			'type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'gender_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']]
		];
		$get = myValidate::getData($rules,'get');
		$list = mdBook::getFreeBookData($get['type'], $get['gender_type'],$get['page']);
		res_api($list);
	}
	
	//获取书籍分类列表
	public function getCategory(){
		checkSign('get');
		$rules = ['type'=>['require|in:1,2',['require'=>'请选择书籍类型','in'=>'未指定该书籍类型']]];
		$type = myValidate::getData($rules,'get');
		$key = $type == 1 ? 'novel_category' : 'cartoon_category';
		$config = getMyConfig($key);
		$config = $config ? : 'ok';
		res_api($config);
	}
	
	//获取书库书籍列表
	public function getCategoryBookList(){
		checkSign();
		$rules = [
			'type' => ['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'gender_type' => ['in:1,2',['in'=>'参数错误']],
			'category' => ['max:20',['max'=>'参数错误']],
			'orderby' => ['require|between:1,3',['require'=>'参数错误','between'=>'参数错误']],
			'page' =>['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数错误','gt'=>'分页参数错误']]
		];
		$data = myValidate::getData($rules);
		$list = mdBook::getCategoryList($data,false);
		$list = $list ? : [];
		res_api($list);
	}
	
	//获取排行列表
	public function getRankList(){
		checkSign('get');
		$rules = [
			'type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'gender_type'=>['require|in:1,2',['require'=>'非法访问','in'=>'非法访问']],
			'flag'=>['require|between:1,4',['require'=>'非法访问','between'=>'非法访问']]
		];
		$get = myValidate::getData($rules,'get');
		$list = [];
		$ids = myCache::getBookRankIds($get['flag'], $get['type'], $get['gender_type']);
		if($ids){
			$list = mdBook::getListBookInfo($ids);
		}
		res_api($list);
	}
	
	//获取书籍详情
	public function getBookInfo(){
		checkSign('get');
		checkToken(false);
		global $loginId;
		$book_id = myHttp::getId('书籍','book_id');
		$book = myCache::getBook($book_id);
		if(!$book){
			res_api('书籍异常');
		}
		if(!in_array($book['status'], [1,2])){
			res_api('该书籍已被删除');
		}
		mdBook::addHot($book['id']);
		$res = [
			'id' => $book['id'],
			'cover' => $book['cover'],
			'type' => intval($book['type']),
			'author' => $book['author'],
			'name' => $book['name'],
			'category' => $book['category'],
			'summary' => $book['summary'],
			'hot_num' => myCache::getBookHotNum($book_id),
			'over_type' => intval($book['over_type']),
			'word_number' => formatNumber($book['word_number']),
			'is_collect' => 2
		];
		$chapter = [
			'num' => myCache::getBookChapterNum($book['id']),
			'first'=> null,
			'end' => null
		];
		$chapter_list = myCache::getBookChapterList($book['id']);
		if($chapter_list){
			reset($chapter_list);
			$first = array_shift($chapter_list);
			$chapter['first'] = [
				'name' => $first['name'],
				'number' => $first['number']
			];
			if($chapter_list){
				$end = end($chapter_list);
				$chapter['end'] = [
					'name' => $end['name'],
					'number' => $end['number']
				];
			}else{
				$chapter['end'] = $chapter['first'];
			}
		}
		$res['chapter'] = $chapter;
		if($loginId){
			$collectIds = $loginId ? myCache::getCollectBookIds($loginId) : [];
			if($collectIds && in_array($book_id, $collectIds)){
				$res['is_collect'] = 1;
			}
		}
		$comments = [];
		$commentsList = myCache::getBookComments($book['id']);
		if($commentsList){
			$comments['count'] = count($commentsList);
			$comments['list'] = array_slice($commentsList, 0,3);
			$zanIds = $loginId ? myCache::getZanIds($loginId) : [];
			foreach ($comments['list'] as &$cv){
				$cv['is_zan'] = $zanIds && in_array($cv['id'], $zanIds) ? 1 : 2;
				$cv['zan_num'] = myCache::getCommentsZannum($cv['id']);
			}
		}
		$res['comments'] = $comments ? : null;
		$reward = myCache::getRewardCache($book['id']);
		$res['reward'] = $reward ? : null;
		res_api($res);
	}
	
	//获取打赏记录列表
	public function getRewardList(){
		checkSign('get');
		$rules = [
			'book_id' 	=> ['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数不规范','gt'=>'书籍参数不规范']],
			'page' 		=> ['require|number|gt:0',['require'=>'分页参数异常1','number'=>'分页参数异常1','gt'=>'分页参数异常1']],
			'limit' 	=> ['require|number|gt:0',['require'=>'分页参数异常','number'=>'分页参数异常','gt'=>'分页参数异常']]
		];
		$get = myValidate::getData($rules,'get');
		$field = 'id,uid,gift,count,create_time';
		$list = Db::name('OrderReward')->where('type',1)->where('pid',$get['book_id'])->field($field)->page($get['page'],$get['limit'])->order('id','desc')->select();
		if($list){
			foreach ($list as &$v){
				$headimg = '';
				$v['nickname'] = '未知';
				$user = myCache::getMember($v['uid']);
				if($user){
					$headimg = $user['headimgurl'];
					$v['nickname'] = $user['nickname'];
				}
				$v['gift'] = json_decode($v['gift'],true);
				$v['headimg'] = $headimg ? : $_SERVER['HTTP_HOST'].'/static/templet/default/headimg.jpeg';
				$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
			}
		}
		$list = $list ? : [];
		res_api($list);
	}
	
	//获取礼物列表
	public function getGiftList(){
		checkSign();
		checkToken(false);
		global $loginId;
		$money = 0;
		if($loginId){
			$money = myCache::getMemberMoney($loginId);
		}
		$config = getMyConfig('reward');
		$config = $config ? array_values($config) : [];
		$data = [
			'money' => $money,
			'gift' => $config
		];
		res_api($data);
	}
	
	//获取章节目录列表
	public function getChapter(){
		checkSign('get');
		$book_id = myHttp::getId('书籍','book_id');
		$book = myCache::getBook($book_id);
		if(!$book){
			res_api('书籍不存在');
		}
		if(!in_array($book['status'], [1,2])){
			res_api('书籍已下架');
		}
		$num = 0;
		$list = myCache::getBookChapterList($book['id']);
		if($list){
			$list = array_values($list);
			$is_free = myCache::checkBookFree($book['id']);
			if(!$is_free && $book['free_type'] == 2){
				foreach ($list as &$v){
					if($v['number'] > $book['free_chapter']){
						if(!$v['money']){
							$v['money'] = $book['money'];
						}
					}
				}
			}else{
				foreach ($list as &$v){
					$v['money'] = 0;
				}
			}
		}
		$list = $list ? : [];
		res_api($list);
	}
	
	//获取书籍评论
	public function getComments(){
		checkSign('get');
		checkToken(false);
		global $loginId;
		$rules = [
			'book_id' => ['require|number|gt:0',['require'=>'评论对象参数错误','number'=>'评论对象参数不规范','gt'=>'评论参数对象不规范']],
			'page' => ['require|number|gt:0',['require'=>'分页参数异常','number'=>'分页参数异常','gt'=>'分页参数异常']]
		];
		$list = null;
		$get = myValidate::getData($rules,'get');
		$cache = myCache::getBookComments($get['book_id']);
		if($cache){
			$page = $get['page'] - 1;
			$temp = array_chunk($cache, 20);
			if(isset($temp[$page])){
				$list = $temp[$page];
				$zanIds = $loginId ? myCache::getZanIds($loginId) : [];
				foreach ($list as &$v){
					$v['is_zan'] = $zanIds && in_array($v['id'], $zanIds) ? 1 : 2;
					$v['zan_num'] = myCache::getCommentsZannum($v['id']);
				}
			}
		}
		$list = $list ? : [];
		res_api($list);
	}
	
	//提交评论
	public function doComments(){
		checkToken();
		global $loginId;
		$rules = [
			'pid' => ['require|number|gt:0',['require'=>'评论对象参数错误','number'=>'评论对象参数不规范','gt'=>'评论参数对象不规范']],
			'content' => ['require|max:200',['require'=>'请输入评论内容','max'=>'评论字数超出限制']],
		];
		$data = myValidate::getData($rules);
		$data['type'] = 1;
		$data['status'] = 0;
		$data['pname'] = '未知';
		$data['uid'] = $loginId;
		$data['create_time'] = time();
		$data['content'] = htmlspecialchars($data['content']);
		$book = myCache::getBook($data['pid']);
		if(!$book){
			res_api('评论失败,书籍已被删除');
		}
		$data['pname'] = $book['name'];
		$re = Db::name('Comments')->insert($data);
		if($re){
			res_api();
		}else{
			res_api('发表失败，请重试');
		}
	}
	
	//提交点赞
	public function doCommentsZan(){
		checkToken();
		global $loginId;
		$pid = myHttp::getId('评论','cid');
		$zanIds = myCache::getZanIds($loginId);
		if($zanIds && in_array($pid, $zanIds)){
			res_api('您已经点赞过该条评论了');
		}
		$re = mdComments::doZan($pid, $loginId);
		if($re){
			myCache::incCommentsZan($pid);
			myCache::addZanIds($loginId, $pid);
			res_api();
		}else{
			res_api('点赞失败');
		}
	}
	
	//收藏
	public function doCollect(){
		checkToken();
		global $loginId;
		$rules = [
			'book_id'=>['require|number|gt:0',['require'=>'书籍信息异常','number'=>'书籍信息异常','gt'=>'书籍信息异常']],
			'event'=>['require|in:join,remove',['require'=>'操作参数异常','in'=>'操作参数异常']]
		];
		$post = myValidate::getData($rules,'get');
		$ids = myCache::getCollectBookIds($loginId);
		switch ($post['event']){
			case 'join':
				if($ids && in_array($post['book_id'], $ids)){
					res_api();
				}
				$data = [
					'uid' => $loginId,
					'book_id' => $post['book_id'],
					'create_time' => time()
				];
				$re = myDb::add('MemberCollect', $data);
				break;
			case 'remove':
				if($ids && !in_array($post['book_id'], $ids)){
					res_api();
				}
				$re = myDb::delByWhere('MemberCollect', [['uid','=',$loginId],['book_id','=',$post['book_id']]]);
				break;
		}
		if($re){
			myCache::rmCollectBookIds($loginId);
			res_api();
		}else{
			res_api('收藏失败');
		}
	}
	
	//提交打赏
	public function doReward(){
		checkToken();
		global $loginId;
		$member = myCache::getMember($loginId);
		$rules = [
			'pid'=>['require|number|gt:0',['require'=>'书籍信息异常','number'=>'书籍信息异常','gt'=>'书籍信息异常']],
			'money'=>['require|number|gt:0',['require'=>'礼物信息异常','number'=>'礼物信息异常','gt'=>'礼物信息异常']],
			'count'=>['require|number|gt:0',['require'=>'礼物数量异常','number'=>'礼物数量异常','gt'=>'礼物数量异常']],
			'gift_key'=>['require|alphaNum',['require'=>'礼物信息异常','alphaNum'=>'礼物信息异常']]
		];
		$data = myValidate::getData($rules);
		$gifts = getMyConfig('reward');
		if($gifts && isset($gifts[$data['gift_key']])){
			$gift = $gifts[$data['gift_key']];
			unset($data['gift_key']);
			if($data['money'] != $gift['money']){
				res_api('礼物信息异常');
			}
			$money = $gift['money'] * $data['count'];
			$userMoney = myCache::getMemberMoney($loginId);
			if($userMoney < $money){
				res_api('您的金币余额不足');
			}
			$data['channel_id'] = $member['channel_id'];
			$data['agent_id'] = $member['agent_id'];
			$data['uid'] = $loginId;
			$data['type'] = 1;
			$data['gift'] = json_encode($gift);
			$data['create_time'] = time();
			$book = myCache::getBook($data['pid']);
			$book_name = $book ? $book['name'] : '未知';
			$re = mdOrder::doReward($data,$book_name);
			if($re){
				myCache::doMemberMoney($loginId, $money,'dec');
				myCache::rmRewardCache($data['pid']);
				res_api();
			}else{
				res_api('打赏失败，请重试');
			}
		}else{
			res_api('礼物已下架');
		}
	}
	
	//处理阅读
	public function doRead(){
		//checkSign('get');//application/api/commom.php
		checkToken(false);
		global $loginId;
		$rules = [
			'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
			'number'=>['require|number|gt:0',['require'=>'章节参数错误','number'=>'章节参数错误','gt'=>'章节参数错误']],
            //'channel'=>['require',['require'=>'参数错误']],
            //'type' => ['require',['require'=>'类型错误']]
		];
		$get = myValidate::getData($rules,'get');
        $channel = $this->request->get('channel');//渠道
        $type = $this->request->get('type');//类型，安卓或ios
        //dump($channel);
//        if(!$loginId){
//            res_api('您尚未登录，是否立即登录',3);
//        }
		$book = myCache::getBook($get['book_id']);
		if(!$book){
			res_api('书籍不存在');
		}
		if(!in_array($book['status'], [1,2])){
			res_api('书籍已下架');
		}
		$totalChapter = myCache::getBookChapterNum($book['id']);
		if($totalChapter == 0){
			res_api('该书籍尚无可读章节');
		}
		if($get['number'] > $totalChapter){
			res_api('已经是最后一章了');
		}

        //先判断有没page_notice值，如果没有page_notice值，则清除相应的缓存
        $book_key = 'book_chapter_list_'.$book['id'];
        if(cache('?'.$book_key)){
            $book_list = cache($book_key);
            if(!isset($book_list[1]['page_notice'])){
                cache($book_key,null);
            }
        }

		$chapterList = myCache::getBookChapterList($book['id']);
		if(!isset($chapterList[$get['number']])){
			res_api('该章节不存在');
		}
		$chapter = $chapterList[$get['number']];

		$is_free = myCache::checkBookFree($book['id']);
		$member = '';
        $notice_list="";//广告列表
        $is_pay = '';//区分收费章节
		if($loginId){
            myCache::delInfo($loginId);//用户信息缓存
			$member = myCache::getMember($loginId);//获取用户字段信息   extend/site/myCache
		}else{
			if(!$is_free){
				if($book['free_type'] == 2 && $get['number'] > $book['free_chapter']){
                    $data = [
                        'username' => '',
                        //'type' => $type,
                        'status' => 2,
                        'login_type'=>'',
                        'remark' => '您尚未登录，是否立即登录',
                        //'login_ip' => '',
                        'create_time' => time(),
                        'is_online'=>2
                    ];
                    Db::name('Log')->insert($data);
					res_api('您尚未登录，是否立即登录',3);
				}
			}
		}
		$content = getBlockContent($get['number'],'book/'.$book['id']);
		if(!$content){
			res_api('章节内容丢失');
		}
		if($book['type'] == 2){
			$content = self::getCartoonImgList($content);
			if(!$content){
				res_api('漫画地址提取异常');
			}
		}
		if($member){
            if($type){
                Db::transaction(function () use($channel,$member,$type){
                    $time = time();
                    $is_start = Db::name('AppStart')->where(['channel'=>$channel,'phone'=>$member['phone'],'type'=>$type])->find();
                    if($is_start){
                        if(date('Y-m-d',$member['create_time']) == date('Y-m-d',strtotime('-1 day'))){
                            //如果用户是昨天注册的
                            Db::name('AppStart')->insert([
                                'channel' => $channel,
                                'phone' => $member['phone'],
                                'date_time' => $time,
                                'update_time' => $time,
                                'phone_register' => $member['create_time'],
                                'type' => $type
                            ]);
                        }
//                        else{
//                            Db::name('AppStart')->where(['channel'=>$channel,'phone'=>$member['phone'],'type'=>$type])->update([
//                                'update_time' => $time
//                            ]);
//                        }
                    }else{
                        Db::name('AppStart')->insert([
                            'channel' => $channel,
                            'phone' => $member['phone'],
                            'date_time' => $time,
                            'update_time' => $time,
                            'phone_register' => $member['create_time'],
                            'type' => $type
                        ]);
                    }

                    Db::name('AppStartDetail')->insert([
                        'channel' => $channel,
                        'phone' => $member['phone'],
                        'date_time' => $time,
                        'phone_register' => $member['create_time'],
                        'phone_channel' => $member['channel'],
                        'type' => $type
                    ]);
                });
            }

			$is_read = myCache::checkIsRead($member['id'], $book['id'], $get['number']);//extend/site/myCache
			//var_dump($is_read);
			if(!$is_read){
                //var_dump($is_free);
				$isAdd = true;
				if(!$is_free){
				    //var_dump(7878);
					if($book['free_type'] == 2 && $get['number'] > $book['free_chapter']){ //收费章节vip免广告，有金币的扣金币，无vip无金币的有广告

					    //var_dump(8888);
						$is_money = true;
						if($member['viptime']){
						    //是vip会员免广告
						    //var_dump(585);
                            //mdBook::addReadhistory1($book,$book['money'],$chapter, $member);//添加阅读历史   application\common\model\mdBook::addReadhistory1
							$is_money = false;
							if($member['viptime'] != 1){
								if(time() > $member['viptime']){
									$is_money = true;
								}
							}
						}
						//var_dump($is_money);
						if($is_money){
						    //var_dump(7887);
							$isAdd = false;
                            myCache::delMoney($loginId);
                            //var_dump(323);
							$money = myCache::getMemberMoney($loginId);//更新用户账户余额缓存
							$book_money = $chapter['money'] ? $chapter['money'] : $book['money'];
							if($money < $book_money){
							    //金币不足，有广告
							    //var_dump(4444);
                                $is_pay = 1;
                                mdBook::addReadhistory1($book,$book_money,$chapter, $member);
                                $notice_list = $this->getNotice($chapter['page_notice'],$channel);//获取广告列表
								//res_api(['is_charge'=>1]);//原来的写法，无会员或金币不足时，不能阅读收费的章节
							}else{
							    //有金币的，扣除相应金币
							    //var_dump(5555);
                                $res = mdBook::addReadhistory($book,$book_money,$chapter, $member,true);//application\common\model\mdBook::addReadhistory
                                if(!$res){
                                    res_api('章节购买失败，请重试');
                                }
                            }
						}
					}else{  //免费章节的vip免广告，不扣金币，无vip无金币的有广告
                        if ($member['viptime'] > time()) {
                            //vip
                            //$msg = "vip用户免广告";
                        } elseif ($member['money'] > 0 && $member['money'] > $book['money']) {
                            //有金币
//                            $newMoney = $member['money'] - $book['money'];//1348
//                            Db::transaction(function () use ($member, $newMoney, $loginId) {
//                                Db::name("Member")->where('phone', $member['phone'])->update(['money' => $newMoney]);//扣除章节设置的金币数
//                                myCache::delInfo($loginId);
//
//                                $key1 = 'member_money_' . $loginId;
//                                if (cache('?' . $key1)) {
//                                    cache($key1, $newMoney);//更新金币缓存
//                                }
//                            });
                        } else {
                            //无vip和金币
                            $notice_list = $this->getNotice($chapter['page_notice'],$channel);//获取广告列表（application\api\controller\Common.php）
                        }
                    }
				}
				//var_dump(9999);
				if($isAdd){
                    mdBook::addReadhistory($book,0,$chapter,$member);
				}
			}else{
                if($book['free_type'] == 2 && $get['number'] > $book['free_chapter']) { ////收费章节vip免广告，有金币的扣金币，无vip无金币的有广告
                    //$is_pay = 1;
                    if ($member['viptime'] > time()) {
                        //vip
                        //$msg = "vip用户免广告";
                    } elseif ($member['money'] > 0 && $member['money'] > $book['money']) {
                        //有金币
                        $newMoney = $member['money'] - $book['money'];//1348
                        Db::transaction(function () use ($member, $newMoney, $loginId) {
                            Db::name("Member")->where('phone', $member['phone'])->update(['money' => $newMoney]);//扣除章节设置的金币数
                            myCache::delInfo($loginId);

                            $key1 = 'member_money_' . $loginId;
                            if (cache('?' . $key1)) {
                                cache($key1, $newMoney);//更新金币缓存
                            }
                        });
                    } else {
                        //无vip和金币
                        $is_pay = 1;
                        $notice_list = $this->getNotice($chapter['page_notice'],$channel);//获取广告列表（application\api\controller\Common.php）
                    }
                }else{  //免费章节的vip免广告，不扣金币，无vip无金币的有广告
                    if ($member['viptime'] > time()) {
                        //vip
                        //$msg = "vip用户免广告";
                    } elseif ($member['money'] > 0 && $member['money'] > $book['money']) {
                        //有金币
//                    $newMoney = $member['money'] - $book['money'];//1348
//                    Db::transaction(function () use ($member, $newMoney, $loginId) {
//                        Db::name("Member")->where('phone', $member['phone'])->update(['money' => $newMoney]);//扣除章节设置的金币数
//                        myCache::delInfo($loginId);
//
//                        $key1 = 'member_money_' . $loginId;
//                        if (cache('?' . $key1)) {
//                            cache($key1, $newMoney);//更新金币缓存
//                        }
//                    });
                    } else {
                        //无vip和金币
                        $notice_list = $this->getNotice($chapter['page_notice'],$channel);//获取广告列表（application\api\controller\Common.php）
                    }
                }
            }
		}else{  //没有登录的，都有广告
            if($book['free_type'] == 2 && $get['number'] > $book['free_chapter']) {
                $is_pay = 1;
            }
            $notice_list = $this->getNotice($chapter['page_notice'],$channel);//获取广告列表
        }

        //访问书籍记录
        $book_name = Db::name('read_log')->where('book_name',$book['name'])->find();
		$phone = $member?$member['phone']:'';//如果登录就获取用户明，否则未登录为空
        if(!$book_name){
            $read_id = Db::name('read_log')->insertGetId([
                'book_id'=>$book['id'],
                'book_name'=>$book['name'],
                'create_time'=>time(),
                'update_time'=>time(),
            ]);
            $this->insertReadDetailData($read_id,$phone,$chapter['name'],$book['name']);
//                Db::name('read_detail')->insert([
//                    'read_id'=>$read_id,
//                    'username'=>$member['phone'],
//                    'create_time'=>time(),
//                    'book_zhangjie'=>$chapter['name'],
//                    'book_name'=>$book['name']
//                ]);
        }else{
            Db::name('read_log')->where('id',$book_name['id'])->update(['update_time'=>time()]);
            $this->insertReadDetailData($book_name['id'],$phone,$chapter['name'],$book['name']);
//                Db::name('read_detail')->insert([
//                    'read_id'=>$book_name['id'],
//                    'username'=>$member['phone'],
//                    'create_time'=>time(),
//                    'book_zhangjie'=>$chapter['name'],
//                    'book_name'=>$book['name']
//                ]);
        }
		$data = [
			'book_id' => $book['id'],
			'book_name' => $book['name'],
			'chapter_title' => $chapter['name'],
			'content' => $content,
            'is_pay'=>$is_pay,
            'notice_list'=>$notice_list,
		];
        //array_push($arr,$data);
		res_api($data);
	}

    /**
     * 写入用户阅读详情表
     * @param $read_id   阅读书籍自增的id，对应书籍名称
     * @param $phone    登录用户
     * @param $chapter_name   章节名称
     * @param $book_name      书本名称
     */
    private static function insertReadDetailData($read_id,$phone,$chapter_name,$book_name)
    {
        Db::name('read_detail')->insert([
            'read_id'=>$read_id,
            'username'=>$phone,
            'create_time'=>time(),
            'book_zhangjie'=>$chapter_name,
            'book_name'=>$book_name
        ]);
	}

    public function test()
    {
        //cache(cache('member_info_6803')['money'],50);
        //var_dump(cache('member_info_6803'));
        //$key = cache('member_info_6803');
        //var_dump(cache($key['money'],'50'));
        //var_dump($key);

//        foreach ($key as $k=>$val){
//            var_dump($k['money']);
//        }

        $notice_list = Db::name("Notice")->where(['status'=>1,'notice_type'=>'开屏广告'])->select();
        $data = [
            'book_id' => 1,
            'book_name' => 'aa',
            'chapter_title' => 'aaaa',
            'content' => 'dfdfs',
            'notice_list'=>$notice_list
        ];
        res_api($data);
	}
	
	//抖音广告
    public function doDouYinNotice()
    {
        $type = '';
        $__AID__ = '';//广告计划id
        $__CID__ = '';//广告创意 id，长整型
        $__IDFA__ = '';//IOS 6+的设备id字段，32位
        $__IMEI__ = '';//安卓的设备 ID 的 md5 摘要，32位
        $__MAC__ = '';//移动设备mac地址,转换成大写字母,去掉“:”，并且取md5摘要后的结果
        $__OAID__ = '';//Android Q及更高版本的设备号，32位
        $__ANDROIDID__ = '';//安卓id原值的md5，32位
        $__TS__ = time();//客户端发生广告点击事件的时间，以毫秒为单位时间戳
        $__CALLBACK_URL__ = '';//直接把调用事件回传接口的url生成出来，广告主可以直接使用
        $url = '';
        if($type == 'ios'){//ios
            $__OS__ = 1;
            $url = "https://xxxx.xxx.com?adid={$__AID__}&cid={$__CID__}&idfa={$__IDFA__}&mac={$__MAC__}&os={$__OS__}&TIMESTAMP={$__TS__}&callback_url={$__CALLBACK_URL__}";
        }elseif($type == 'android'){//android
            $__OS__ = 0;
            $url = "https://xxxx.xxx.com?adid={$__AID__}&cid={$__CID__}&imei={$__IMEI__}&mac={$__MAC__}&oaid={$__OAID__}&androidid={$__ANDROIDID__}&os={$__OS__}&TIMESTAMP={$__TS__}&callback_url={$__CALLBACK_URL__}";
        }
        self::getRequest($url);

    }

    //get请求
    private static function getRequest($url){
        $ch = curl_init();

        // set url
        curl_setopt($ch, CURLOPT_URL, $url);

        // set method
        curl_setopt ($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        // return the transfer as a string
        curl_setopt ($ch, CURLOPT_RETURNTRANSFER, 1);

        // send the request and save response to $response
        $response = curl_exec($ch);

        // stop if fails
        if (!$response) {
            die('Error: "' . curl_error($ch) . '" - Code: ' . curl_errno($ch));
        }

        echo 'HTTP Status Code: ' . curl_getinfo($ch, CURLINFO_HTTP_CODE) . PHP_EOL;
        echo 'Response Body: ' . $response . PHP_EOL;

        // close curl resource to free up system resources
        curl_close($ch);
        return $ch;
    }
	
	//投诉
	public function doComplaint(){
		checkToken();
		global $loginId;
		$rules = [
			'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
			'number'=>['require|number|gt:0',['require'=>'章节参数错误','number'=>'章节参数错误','gt'=>'章节参数错误']],
			'type' => ['require|between:1,7',['require'=>'请选择反馈类型','between'=>'未指定该反馈类型']],
			'content' => ['max:100',['max'=>'反馈内容最大支持100个字符']],
			'phone' => ['mobile',['mobile'=>'手机号格式不规范']]
		];
		$data = myValidate::getData($rules);
		$book = myCache::getBook($data['book_id']);
		if(!$book){
			res_api('书籍信息异常');
		}
		$user = myCache::getMember($loginId);
		$data['channel_id'] = $user['channel_id'];
		$data['agent_id'] = $user['agent_id'];
		$data['uid'] = $user['id'];
		$data['book_name'] = $book['name'];
		$data['create_time'] = time();
		$re = Db::name('Complaint')->insert($data);
		if($re){
			res_api();
		}else{
			res_api('投诉失败，请重试');
		}
	}
	
	//下载书籍
	public function download(){
		checkToken();
		global $loginId;
		$user = myCache::getMember($loginId);
		$user_money = myCache::getMemberMoney($loginId);
		$rules = [
			'book_id' => ['require|number|gt:0',['require'=>'请选择要下载的书籍','number'=>'书籍参数格式不规范','gt'=>'书籍参数格式不规范']],
			'is_confirm' => ['in:1,2',['in'=>'未指定该参数']]
		];
		$get = myValidate::getData($rules,'get');
		$book_id = $get['book_id'];
		$is_confirm = $get['is_confirm'] ? : 2;
		$book = myCache::getBook($book_id);
		if(!$book){
			res_api('书籍不存在');
		}
		if($book['type'] != 1){
			res_api('仅小说可下载');
		}
		$money = 0;
		$list = myCache::getBookChapterList($book['id']);
		if(!$list){
			res_api('书籍章节不存在');
		}
		$temp = [];
		$filepath = env('root_path').'static/block/book/'.$book['id'];
		$is_free = myCache::checkBookFree($book['id']);
		$read_cache = myCache::getReadCache($loginId);
		$read_key = 'book_'.$book['id'];
		$read_numbers = isset($read_cache[$read_key]) ? $read_cache[$read_key]['numbers'] : [];
		$cur_time = time();
		$is_vip = false;
		if($user['viptime']){
			if($user['viptime'] == 1 || $user['viptime'] > $cur_time){
				$is_vip = true;
			}
		}
		$date = date('Y-m-d');
		if($is_vip){
			$download = myDb::getList('BookDownload', [['uid','=',$user['id']],['create_date','=',$date]]);
			if($download){
				if(count($download) >= 2){
					$downloadBookIds = array_column($download, 'book_id');
					$downloadBookIds = array_unique($downloadBookIds);
					if(count($downloadBookIds) >= 2){
						if(!in_array($book['id'], $downloadBookIds)){
							res_api('VIP用户每日最多下载两次');
						}
					}
				}
			}
		}
		foreach ($list as $v){
			$name = $v['number'].'.html';
			$filename = $filepath.'/'.$name;
			if(@is_file($filename)){
				$one = ['filename'=>$filename,'name'=>$name];
				if(!in_array($v['number'], $read_numbers)){
					if(!$is_vip){
						if(!$is_free && $book['free_type'] == 2){
							if($book['free_chapter'] < $v['number']){
								$money += $v['money'] ? : $book['money'];
							}
						}
					}
				}
				$temp[] = $one;
			}
		}
		if(empty($temp)){
			res_api('该书籍暂无可下载章节');
		}
		if($money){
			$is_download = Db::name('BookDownload')->where('uid',$user['id'])->where('book_id',$book['id'])->value('id');
			if($is_download){
				$money = 0;
			}
			if($is_confirm == 2){
				res_api(['need_money'=>$money]);
			}
			if($user_money < $money){
				res_api(['flag'=>2,'msg'=>'您的书币余额不足，请及时充值','need_money'=>$money,'user_money'=>$user_money,'zip_file'=>'']);
			}
		}
		if($is_confirm == 2){
			res_api(['need_money'=>$money]);
		}
		$zip = new \ZipArchive();
		$path = './files/zip/'.date('Ymd');
		if(!is_dir($path)){
			mkdir($path,0777,true);
		}
		$zip_name = $path.'/'.md5(microtime().mt_rand(0,99999999)).'.zip';
		if($zip->open($zip_name,\ZipArchive::CREATE) === true){
			foreach ($temp as $val){
				$zip->addFile($val['filename'],$val['name']);
			}
		}
		$zip->close();
		$data = [
				'uid' => $user['id'],
				'book_id' => $book['id'],
				'create_date' => $date
		];
		Db::startTrans();
		$flag = false;
		$re = Db::name('BookDownload')->insert($data);
		if($re){
			if($money){
				$res = Db::name('Member')->where('id',$user['id'])->setDec('money',$money);
				if($res){
					$flag = true;
				}
			}else{
				$flag = true;
			}
		}
		if($flag){
			Db::commit();
			if($money){
				myCache::doMemberMoney($user['id'], $money,'dec');
			}
		}else{
			Db::rollback();
		}
		if($flag){
			$zip_file = 'http://'.$_SERVER['HTTP_HOST'].ltrim($zip_name);
			res_api(['flag'=>1,'zip_file'=>$zip_file]);
		}else{
			@unlink($zip_name);
			res_api('生成下载文件失败');
		}
	}
	
	//获取图片列表
	private function getCartoonImgList($content){
		$pattern="/<img.*?src=[\'|\"](.*?)[\'|\"].*?[\/]?>/";
		preg_match_all($pattern,htmlspecialchars_decode($content),$match);
		if(isset($match[1])){
			return $match[1];
		}
		return null;
	}
	
}