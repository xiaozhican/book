<?php
namespace app\api\controller;

use think\Db;
use site\myValidate;
use site\myCache;

class Search extends Common{
	
	//热门搜索
	public function getHotSearch(){
		checkSign();
		$list = [];
		$cache = myCache::getBookRecommendIds();
		if($cache){
			$ids = array_slice($cache, 0,6);
			foreach ($ids as $book_id){
				$book = myCache::getBook($book_id);
				if($book && $book['status'] == 1){
					$list[] = $book['name'];
				}
			}
		}
		res_api($list);
	}
	
	//搜索书籍
	public function doSearch(){
		checkSign();
		checkToken(false);
		global $loginId;
		$rules = ['keyword' => ['require|chsAlphaNum|max:100',['require'=>'请输入搜索关键字','chsAlphaNum'=>'搜索关键字仅支持汉字、字母、数字','max'=>'搜索关键字长度超出限制']]];
		$keyword = myValidate::getData($rules);
		$field = 'id,name,type,cover,summary,category';
		$list = Db::name('Book')
		->where('name|author','like','%'.$keyword.'%')
		->where('status',1)
		->field($field)
		->order('hot_num','desc')
		->limit(20)
		->select();
		if($list){
			foreach ($list as &$v){
				$v['category'] = $v['category'] ? explode(',', trim($v['category'],',')) : [];
			}
		}else{
			$list = 'ok';
		}
		res_api($list);
	}
}