<?php
namespace app\api\controller;

use site\myCache;

class Activity extends Common{
	
	//获取活动详情
	public function getInfo(){
		checkToken();
		global $loginId;
		$activity_id = myCache::getLastActivityId();
		if($activity_id){
			$activity = myCache::getActivity($activity_id);
			if($activity && $activity['status'] == 1){
				$time = time();
				if($time > $activity['start_time'] && $time < $activity['end_time']){
					$user = myCache::getMember($loginId);
					if($user['channel_id']){
						if($user['agent_id']){
							$delIds = myCache::getActivityDelIds($user['agent_id']);
							if($delIds && in_array($user['agent_id'], $activity_id)){
								$activity = null;
							}
						}else{
							$delIds = myCache::getActivityDelIds($user['channel_id']);
							if($delIds && in_array($user['channel_id'], $activity_id)){
								$activity = null;
							}
						}
					}
					if($activity){
						$data = [
							'user' => [
								'id' => addZero($user['id']),
								'nickname' => $user['nickname'],
								'headimgurl' => $user['headimgurl'],
								'money' => myCache::getMemberMoney($user['id'])
							],
							'activity' => [
								'id' => $activity['id'],
								'name' => $activity['name'],
								'start_time' => date('Y-m-d H:i',$activity['start_time']),
								'end_time' => date('Y-m-d H:i',$activity['end_time']),
								'coin' => $activity['send_money']
							]
						];
						res_api($data);
					}
				}
			}
		}
		res_api('当前没有活动喔');
	}
}