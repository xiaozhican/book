<?php
namespace app\api\controller;

use app\common\model\mdCount;
use app\common\model\mdLogin;
use app\common\model\mdOnline;
use app\home\controller\Login;
use think\Db;
use site\myDb;
use site\myMsg;
use site\myImg;
use site\myCache;
use site\myValidate;
use app\common\model\mdMember;
use site\myHttp;
use think\facade\Log;

class User extends Common{
	
	//注册
	public function doRegister(){
		checkSign();
		$rules = [
			'phone' => ['require|mobile',['require'=>'请输入注册手机号','mobile'=>'手机号格式错误']],
			'password' => ['require|length:6,16',['require'=>'请输入密码','length'=>'密码不少于6-16位']],
			'code' => ['require|number|length:4',['require'=>'请输入验证码','number'=>'验证码为6位数字','length'=>'验证码为6位数字']],
			'device_type' => ['require|in:1,2',['require'=>'设备类型参数错误','in'=>'未指定该设备类型']],
			'jpush_reg_id' => ['require',['require'=>'极光推送参数异常']],
            //'channel' => ['require',['require'=>'渠道参数异常']]
		];
		$post = myValidate::getData($rules);
		$channel = $this->request->post('channel');
		$channel = $channel==null?'default':$channel;
		Log::info('用户注册，渠道名称为：'.$channel);
		$repeat_uid = Db::name('Member')->where('phone',$post['phone'])->value('id');
		if($repeat_uid){
			res_api('该手机号已被注册');
		}
		$res = myMsg::check($post['phone'], $post['code']);
		if($res !== true){
			res_api($res);
		}
		$time = time();
		$headimgurl = 'http://img.b.0756hhr.com/images/20190910/9c02931c7abbcdd689e173d1c8964889.jpeg';
		$nickname = self::createNickname();
		$data = [
			'invite_code' => myDb::createCode('Member','invite_code'),
			'phone' => $post['phone'],
			'openid' => '',
			'password' => createPwd($post['password']),
			'headimgurl' => $headimgurl,
			'nickname' => $nickname,
			'sex' => 1,
			'spread_id' => 0,
			'create_time' => $time,
            'channel' => $channel,
            'old_phone' => $post['phone']
		];
		$agentInfo = [];
		$inviteUid = 0;
		$is_other = false;
		$invite_code = $this->request->post('invite_code');
		if($invite_code){
			$inviteUid = myCache::getUidByCode($invite_code);
			if($inviteUid){
				$inviteUser = myCache::getMember($inviteUid);
				if($inviteUser){
					$is_other = true;
					$data['channel_id'] = $inviteUser['channel_id'];
					$data['agent_id'] = $inviteUser['agent_id'];
					$data['wx_id'] = $inviteUser['wx_id'];
				}
			}
		}
		if(!$is_other){
			$spread_id = $this->request->post('spread_id');
			if($spread_id && is_numeric($spread_id)){
				$spread = myCache::getSpread($spread_id);
				if($spread){
					if($spread['channel_id']){
						$channel = myCache::getChannel($spread['channel_id']);
						if($channel){
							$data['spread_id'] = $spread['id'];
							if($channel['type'] == 1){
								$data['channel_id'] = $channel['id'];
							}else{
								$data['channel_id'] = $channel['parent_id'];
								$data['agent_id'] = $channel['id'];
							}
						}
					}else{
						$data['spread_id'] = $spread['id'];
					}
				}
			}
		}
		Db::startTrans();
		$flag = $is_money = false;
		$uid = Db::name('Member')->insertGetId($data);
		if($uid){
			$res = Db::name('MemberCharge')->insert(['uid'=>$uid]);
			if($res){
				$token = md5('member#'.$uid.'#'.microtime());
				$expire_time = $time+7*86400;
				$tokenData = [
					'uid' => $uid,
					'token' => $token,
					'expire_time' => $expire_time,
					'device_type' => $post['device_type'],
					'jpush_reg_id' => $post['jpush_reg_id']
				];
				$tokenRes = Db::name('MemberInfo')->insert($tokenData);
				if($tokenRes){
					$mRes = true;
					if($inviteUid){
						$shareConfig = getMyConfig('site_share');
						if($shareConfig && $shareConfig['reg_money']){
							$mRes = false;
							$inviteData = [
								'uid' => $uid,
								'from_uid' => $inviteUid,
								'money' => $shareConfig['reg_money'],
								'create_date' => date('Y-m-d')
							];
							$inviteRes = Db::name('Share')->insert($inviteData);
							if($inviteRes){
								$mData = [
									'money' => Db::raw('money+'.$inviteData['money']),
									'total_money' => Db::raw('total_money+'.$inviteData['money'])
								];
								$meRes = Db::name('Member')->where('id',$inviteUid)->update($mData);
								if($meRes){
									$log = [
										'uid' => $inviteUid,
										'money' => '+'.$inviteData['money'].'书币',
										'summary' => '邀请用户赠送',
										'create_time' => time()
									];
									$ls = Db::name('MemberChargeLog')->insert($log);
									if($ls){
										$mRes = true;
										$is_money = true;
									}
								}
							}
						}
					}
					if($mRes){
						$flag = true;
					}
				}
			}
		}
		if($flag){
			Db::commit();
			if($is_money){
				myCache::doMemberMoney($inviteUid, $inviteData['money']);
			}
			$token_msg = ['uid'=>$uid,'expire_time'=>$tokenData['expire_time']];
			cache($token,$token_msg,$token_msg['expire_time']);
			myCache::JpushRegId($uid,$post['jpush_reg_id']);
			res_api(['usertoken'=>$token]);
		}else{
			Db::rollback();
			res_api('注册失败');
		}
	}
	
	//处理登录
	public function doLogin(){
		checkSign();
		$rules = [
			'phone' => ['require|mobile',['require'=>'请输入手机号','mobile'=>'手机号格式错误']],
			'password' => ['require|length:6,16',['require'=>'请输入密码','length'=>'密码6-16位']],
			'device_type' => ['require|in:1,2',['require'=>'设备类型参数错误'],['in'=>'未指定该设备类型']],
			'jpush_reg_id' => ['require',['require'=>'极光推送参数异常']],
            //'login_ip'=>['require|127.0.0.1',['require'=>'请输入登录ip']],
            'login_type'=>['require',['require'=>'请输入登录的设备类型']]
		];
		$post = myValidate::getData($rules);
        //var_dump($post['login_ip']);
		$user = Db::name('Member')->field('id,password,status')->where('phone',$post['phone'])->find();
		if(!$user){
		    $error = '用户不存在';
		    self::saveLog($post['phone'],$post['login_type'],$error);
			res_api($error);
		}
		$password = createPwd($post['password']);
		if($user['password'] !== $password){
            $error = '密码输入错误';
            self::saveLog($post['phone'],$post['login_type'],$error);
			res_api($error);
		}
		if($user['status'] != 1){
            $error = '您已被禁用，请联系客服';
            self::saveLog($post['phone'],$post['login_type'],$error);
			res_api($error);
		}
		$time = time();
		$token = md5('member#'.$user['id'].'#'.microtime());
		$over_time = 7*86400;
		$expire_time = $time + $over_time;
		$data = [
			'uid' => $user['id'],
			'token' => $token,
			'expire_time' => $expire_time,
			'device_type' => $post['device_type'],
			'jpush_reg_id' => $post['jpush_reg_id']
		];
		$cur = Db::name('MemberInfo')->where('uid',$user['id'])->field('id,token')->find();
		if($cur){
			$re = myDb::save('MemberInfo', [['id','=',$cur['id']]], $data);
		}else{
			$re = myDb::add('MemberInfo', $data);
		}
		if($re){
			if($cur){
				cache($cur['token'],null);
			}
			$token_msg = ['uid'=>$user['id'],'expire_time'=>$expire_time];
			cache($token,$token_msg,$over_time);
            self::saveLog($post['phone'],$post['login_type']);//记录登录日志
            mdOnline::countOnline($post['phone']);//统计在线人数和日活人数
			myCache::JpushRegId($user['id'],$data['jpush_reg_id']);
			res_api(['usertoken'=>$token]);
		}else{
			res_api('登录失败,请重试');
		}
	}
	
	//一键注册登录
    public function keyLogin()
    {
        checkSign();
        $rules = [
            //'loginToken' => ['require',['require'=>'请输入token']],
            'device_type' => ['require|in:1,2',['require'=>'设备类型参数错误'],['in'=>'未指定该设备类型']],
            'jpush_reg_id' => ['require',['require'=>'极光推送参数异常']],
            'login_type'=>['require',['require'=>'请输入登录的设备类型']],
            'type' => ['require',['require'=>'类型不能为空']]
        ];
        $post = myValidate::getData($rules);
        $channel = $this->request->post('channel');
        $channel = $channel==null?'default':$channel;
        $type = $post['type'];
        if($type==1){
            //手机号一键登录
            $loginToken = $this->request->post('loginToken');
            if(!$loginToken){
                res_api('token值不能为空！');
            }
            $result = $this->JgLogin($loginToken);
            if(!$result){
                res_api('没有获取到手机号码，请重新获取！');
            }
            $user = Db::name('Member')->field('id,password,status')->where('phone',$result)->find();
            if(!$user){
                //如果用户不存在，则注册
                $headimgurl = 'http://img.b.0756hhr.com/images/20190910/9c02931c7abbcdd689e173d1c8964889.jpeg';
                $nickname = self::createNickname();
                $data = [
                    'invite_code' => myDb::createCode('Member','invite_code'),
                    'phone' => $result,
                    'openid' => '',
                    'password' => createPwd('123456'),
                    'headimgurl' => $headimgurl,
                    'nickname' => $nickname,
                    'sex' => 1,
                    'spread_id' => 0,
                    'create_time' => time(),
                    'channel' => $channel,
                    'old_phone' => $result
                ];
                Db::name('Member')->insert($data);
            }

            $users = !$user?Db::name('Member')->field('id,password,status')->where('phone',$result)->find():$user;
            $time = time();
            $token = md5('member#'.$users['id'].'#'.microtime());
            $over_time = 7*86400;
            $expire_time = $time + $over_time;
            $data = [
                'uid' => $users['id'],
                'token' => $token,
                'expire_time' => $expire_time,
                'device_type' => $post['device_type'],
                'jpush_reg_id' => $post['jpush_reg_id']
            ];
            $cur = Db::name('MemberInfo')->where('uid',$users['id'])->field('id,token')->find();
            if($cur){
                $re = myDb::save('MemberInfo', [['id','=',$cur['id']]], $data);
            }else {
                $re = myDb::add('MemberInfo', $data);
            }
            if($re){
                if($cur){
                    cache($cur['token'],null);
                }
                $token_msg = ['uid'=>$users['id'],'expire_time'=>$expire_time];
                cache($token,$token_msg,$over_time);
                self::saveLog($result,$post['login_type'],'用户手机号一键登录');//记录登录日志
                mdOnline::countOnline($result);//统计在线人数和日活人数
                myCache::JpushRegId($users['id'],$data['jpush_reg_id']);
                res_api(['usertoken'=>$token]);
            }else{
                res_api('登录失败,请重试');
            }
        }else{
            //微信登录
            $this->getCode($channel,$post['device_type'],$post['jpush_reg_id'],$post['login_type']);
        }
    }

    //极光获取手机号，一键注册登录
    public function JgLogin($loginToken)
    {
        $url = "https://api.verification.jpush.cn/v1/web/loginTokenVerify";
        $header[] = "Content-Type: application/json";
        $appkey  = base64_encode("00babd14604e2cecced4c312:7ebd355c9adfbc64605e1a25");
        $header[] = "Authorization: Basic {$appkey}";

        $data = ['loginToken'=>$loginToken,'exID'=>'123456'];
        $curl_post = $this->curlpost($url,$header,json_encode($data));
        $curl_post = json_decode($curl_post,true);
        //var_dump($post['phone']);die;
        /**
        array(5) {
        ["id"]=>
        int(383200201768263681)
        ["code"]=>
        int(8000)
        ["content"]=>
        string(17) "get phone success"
        ["exID"]=>
        string(6) "123456"
        ["phone"]=>
        string(172) "jfgHuGYYrToilC7uZqcmOXQvST13UeYA8fLAY6jDFTHDozfLxfBE0mWewK9DGwjfgfJn5e4kedyJgHpd+K3vzgnlvTT5522kG+RiKsY0V4efjFhhvpZnWQrg37FTbJvO7jtVLtUy5Kmfn7hzRFO/3CZFfaeNIiltWHw0gsXqZAk="
        }
         */
        // https://www.php.net/manual/en/function.openssl-private-decrypt.php
        $prefix = '-----BEGIN RSA PRIVATE KEY-----';
        $suffix = '-----END RSA PRIVATE KEY-----';
        $result = '';
        //$encrypted = "jfgHuGYYrToilC7uZqcmOXQvST13UeYA8fLAY6jDFTHDozfLxfBE0mWewK9DGwjfgfJn5e4kedyJgHpd+K3vzgnlvTT5522kG+RiKsY0V4efjFhhvpZnWQrg37FTbJvO7jtVLtUy5Kmfn7hzRFO/3CZFfaeNIiltWHw0gsXqZAk=";
        $encrypted = $curl_post['phone'];
        $prikey = "MIICdgIBADANBgkqhkiG9w0BAQEFAASCAmAwggJcAgEAAoGBAMxsAocsz+nIYdhX
y/1AV4dJVs/hg5FxFtjXMSwbJmWbaNgf/rsdx82AW92b52eH0fV2QBIQ3lXsQNhY
wkF4tQR6xPxekDjxZOkm9JUE0WJqANpkUG2jHvy4jrd3gzKlslcvf0M5TEWeKXAY
aAjCBjE6klo2IVHnJeAGydwvenrtAgMBAAECgYA63kvoE46YW31Z6Qa0mqfaLNOU
L/RMmQ2kSmw9sF/hWNzbiHM5/XyBgJpz+iasmN9s2a3fxf3RLqUJ6J2gbxtpd/Vx
2zA8pLjiDkAQdL0KYeQoA30uppnL+fCKNnEKmrVbTZ9bK0eWk5rWJt1KkEcq7Ie2
p1KYczfiWA/Ql6pNoQJBAO6iRt9lZlYc/gBlsSa4Z9JK/96JI+TcbO6LH/1g1+9f
+npfxcv24T+YrX/DEfuotf76u+19L2Hyd+DQ4u3feukCQQDbTF4bSqyPIjnqwFYO
EWUPZRe+SO8p1TKzV0uVtQLwz84bE8ln7LfJnBb0/4ANS/sKSm9E68/cl2VF/u7f
fvVlAkEAnvBSiAQ/bueMfbM9lG6KJ9RwK0XoNExj+VWMjvRrnjGs4MjdMwZR7EvC
cy/30EgEP7m8ZWdVocUe9wvpuUl50QJAQvay1cqbnqlo3hIVNP6CyYypD2XeAfwk
GzdQ0UoQt8/8s6/7FEzoYhvTtsQ+KdFFuwK3r2gCdO2V8yXBc2WlMQJAfaX5KMYS
dcOEfrkaOnQcbcU1bKWMNctEeaNzZD4I0HXVVBQStaALRxSTsmPg3jzIaZW/CiMQ
9qN4b+MDI0k6+w==";

        $key = $prefix . "\n" . $prikey . "\n" . $suffix;
        //var_dump($key);die;
        $r = openssl_private_decrypt(base64_decode($encrypted), $result, openssl_pkey_get_private($key));
        return $result;
    }

    //微信登录
    //?channel=$channel&device_type=$device_type&jpush_reg_id=$jpush_reg_id&login_type=$login_type
    public function getCode($channel,$device_type,$jpush_reg_id,$login_type){
        //基本配置
        $appid = config('weixin.appid');//这里填写你的appid
        $redirect_uri = urlencode(config('weixin.url')."?channel=$channel&device_type=$device_type&jpush_reg_id=$jpush_reg_id&login_type=$login_type");//这里用你的网页授权回调地址替换一下
        $url="https://open.weixin.qq.com/connect/oauth2/authorize?appid=".$appid."&redirect_uri=".$redirect_uri."&response_type=code&scope=snsapi_userinfo&state=123&connect_redirect=1#wechat_redirect";
        header("location:".$url);
    }
    //获取微信用户的openid
    public function getOpenInfo(){
        $channel = $_GET['channel'];
        $device_type = $_GET['device_type'];
        $jpush_reg_id = $_GET['jpush_reg_id'];
        $login_type = $_GET['login_type'];
        $appid  = config('weixin.appid');//这里添加你的微信公众号appid
        $secret = config('weixin.appsecret');//这里添加你的微信公众号secret
        //这里获取到了code
        if(!isset($_GET['code'])){
            $this->getCode($channel,$device_type,$jpush_reg_id,$login_type);//如果没有获取到code，重复获取
        }
        $code = $_GET['code'];
        //第一步:取得openid
        $oauth2Url = "https://api.weixin.qq.com/sns/oauth2/access_token?appid=".$appid."&secret=".$secret."&code=".$code."&grant_type=authorization_code";
        $oauth2 = $this->http_curl($oauth2Url);
        $oauth2 = json_decode($oauth2,true);
        //var_dump($oauth2);
        //accestoken
        $access_token = $oauth2["access_token"];
        //openid
        $openid = $oauth2['openid'];

        //第二步:根据全局access_token和openid查询用户信息
        $get_user_info_url = "https://api.weixin.qq.com/sns/userinfo?access_token=".$access_token."&openid=".$openid."&lang=zh_CN";
        $userInfo = $this->http_curl($get_user_info_url);
        //dump($userInfo);//string(308) "{"openid":"oMKph5hqMUcc3FDlqlq4DAcqDedI","nickname":"馨空","sex":1,"language":"zh_CN","city":"福州","province":"福建","country":"中国","headimgurl":"https:\/\/thirdwx.qlogo.cn\/mmopen\/vi_32\/Q0j4TwGTfTIrguIjuTDxaCXPPWwMn5Gm2bD7Z88CupEal7lgy08VEICcMazTgXHqfHkTWPjwS1T74CPDH2lcew\/132","privilege":[]}"
        $userInfo = json_decode($userInfo,true);//json转数组格式

        $openid = $userInfo['openid'];
        $nickname = $userInfo['nickname'];
        $sex = $userInfo['sex'];
        $city = $userInfo['city'];
        $province = $userInfo['province'];
        $country = $userInfo['country'];
        $headimgurl = $userInfo['headimgurl'];

        $user = Db::name('Member')->field('id,phone,openid,password,status')->where('openid',$openid)->find();
        if(!$user){
            //如果用户不存在，则注册
            //$headimgurl = 'http://img.b.0756hhr.com/images/20190910/9c02931c7abbcdd689e173d1c8964889.jpeg';
            //$nickname = self::createNickname();
            $data = [
                'invite_code' => myDb::createCode('Member','invite_code'),
                'phone' => '',
                'openid' => $openid,
                'password' => createPwd('123456'),
                'headimgurl' => $headimgurl,
                'nickname' => $nickname,
                'sex' => $sex,
                'city' => $city,
                'province' => $province,
                'country' => $country,
                'spread_id' => 0,
                'create_time' => time(),
                'channel' => $channel,
                'old_phone' => ''
            ];
            Db::name('Member')->insert($data);
        }

        $users = !$user?Db::name('Member')->field('id,phone,openid,password,status')->where('openid',$openid)->find():$user;
        $time = time();
        $token = md5('member#'.$users['id'].'#'.microtime());
        $over_time = 7*86400;
        $expire_time = $time + $over_time;
        $data = [
            'uid' => $users['id'],
            'token' => $token,
            'expire_time' => $expire_time,
            'device_type' => $device_type,
            'jpush_reg_id' => $jpush_reg_id
        ];
        $cur = Db::name('MemberInfo')->where('uid',$users['id'])->field('id,token')->find();
        if($cur){
            $re = myDb::save('MemberInfo', [['id','=',$cur['id']]], $data);
        }else {
            $re = myDb::add('MemberInfo', $data);
        }
        if($re){
            if($cur){
                cache($cur['token'],null);
            }
            $token_msg = ['uid'=>$users['id'],'expire_time'=>$expire_time];
            cache($token,$token_msg,$over_time);
            self::saveLog($openid,$login_type,'用户微信授权登录');//记录登录日志
            mdOnline::countOnline($openid);//统计在线人数和日活人数
            myCache::JpushRegId($users['id'],$data['jpush_reg_id']);
            res_api(['usertoken'=>$token,'phone'=>$users['phone'],'openid'=>$openid]);
        }else{
            res_api('登录失败,请重试');
        }
    }

    private function http_curl($url){
        //用curl传参
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_RETURNTRANSFER,1);
        //关闭ssl验证
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        curl_setopt($ch,CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    private function curlpost($url, $headers,$data){
        //初始化curl
        $ch = curl_init();
        curl_setopt($ch,CURLOPT_URL,$url);
        //设置获取的信息以文件流的形式返回，而不是直接输出。
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        //设置头文件的信息作为数据流输出
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
        //设置为post方式请求
        curl_setopt($ch, CURLOPT_POST, true);
        //添加参数
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        //关闭请求资源
        $output = curl_exec($ch);
        curl_close($ch);
        return $output;
    }

    //保存安卓和ios端登录日志
    private static function saveLog($login_name,$login_type,$error=''){
        $status = $error ? 2 : 1;
        $is_online = $error ? 3 : 1;
        $data = [
            'username' => $login_name,
            //'type' => $type,
            'status' => $status,
            'login_type'=>$login_type,
            'remark' => $error,
            //'login_ip' => $login_ip,
            'create_time' => time(),
            'is_online'=>$is_online
        ];
        Db::name('Log')->insert($data);
    }
	
	//忘记密码
	public function doForgetPwd(){
		checkSign();
		$rules = [
			'phone' => ['require|mobile',['require'=>'请输入手机号','mobile'=>'手机号格式错误']],
			'code' => ['require|number|length:4',['require'=>'请输入验证码','number'=>'验证码为6位数字','length'=>'验证码为6位数字']],
			'password' => ['require|length:6,16',['require'=>'请输入密码','alphaDash'=>'密码支持字母数字下划线及破折号','length'=>'密码6-16位']],
		];
		$data = myValidate::getData($rules);
		$res = myMsg::check($data['phone'], $data['code']);
		if($res !== true){
			res_api($res);
		}
		$repeat_uid = Db::name('Member')->where('phone',$data['phone'])->value('id');
		if(!$repeat_uid){
			res_api('该手机号尚未绑定');
		}
		$password = createPwd($data['password']);
		$re = Db::name('Member')->where('id',$repeat_uid)->setField('password',$password);
		if($re !== false){
			res_api();
		}else{
			res_api('修改失败,请重试');
		}
	}
	
	//修改密码
	public function changePwd(){
		checkToken();
		global $loginId;
		$rules = [
			"old_pwd" => ['require|length:6,16',['require'=>'请输入原密码','length'=>'密码6-16位']],
			"new_pwd" => ['require|length:6,16',['require'=>'请输入新密码','length'=>'密码6-16位']]
		];
		$data = myValidate::getData($rules);
		$user = Db::name('Member')->where('id',$loginId)->field('id,password')->find();
		if(!$user){
			res_api('用户信息异常,请重新登录',3);
		}
		$old_pwd = createPwd($data['old_pwd']);
		if($old_pwd != $user['password']){
			res_api('原密码输入有误');
		}
		$new_pwd = createPwd($data['new_pwd']);
		$re = Db::name('Member')->where('id',$loginId)->setField('password',$new_pwd);
		if($re !== false){
			res_api();
		}else{
			res_api('修改失败,请重试');
		}
	}
	
	//获取用户信息
	public function getUserInfo(){
		checkToken();
		global $loginId;
		$siteCache = getMyConfig('website');
		if(!$siteCache){
			res_api('平台参数配置异常');
		}
		myCache::changeMember($loginId);//更新用户金币和清除用户信息缓存
		$cache = myCache::getMember($loginId);//extend/site/myCache
		$user = [
			'id' => addZero($loginId),
			'nickname' => $cache['nickname'],
			'headimgurl' => $cache['headimgurl'],
			'phone' => $cache['phone'],
			'money' => intval(myCache::getMemberMoney($loginId)),
			'sex' => intval($cache['sex']),
			'vip_str' => '',
			'feedback_msg' => ''
		];
		if($cache['viptime'] > 0){
			if($cache['viptime'] > 1){
				if(time() < $cache['viptime']){
					$user['vip_str'] = date('Y-m-d',$cache['viptime']).'到期';
				}
			}else{
				$user['vip_str'] = '终身会员';
			}
		}
		$is_adopt = myCache::getMemberIsAdopt($loginId);
		if($is_adopt === 'yes'){
			$user['feedback_msg'] = '您的意见已被采纳，书币已到账';
		}
		res_api($user);
	}
	
	//获取分享图片
	public function getShareImg(){
		checkToken();
		global $loginId;
		$user = myCache::getMember($loginId);
		$share_img = myImg::getShareImg($user['id'], $user['invite_code']);
		if($share_img){
			res_api(['url'=>$share_img]);
		}else{
			res_api('图片生成失败');
		}
	}
	
	//获取签到详情
	public function getSignInfo(){
		checkToken();
		global $loginId;
		$config = getMyConfig('site_sign');
		if($config['is_sign'] != 1){
			res_error('签到功能未启用');
		}
		$signInfo = myCache::getUserSignInfo($loginId);
		res_api($signInfo);
	}
	
	//签到
	public function doSign(){
		checkToken();
		global $loginId;
		$config = getMyConfig('site_sign');
		if($config['is_sign'] != 1){
			res_error('签到功能未启用');
		}
		$signInfo = myCache::getUserSignInfo($loginId);
		if($signInfo['cur_sign'] === 'yes'){
			res_api();
		}
		$data = [
			'uid' => $loginId,
			'date' => date('Ymd'),
			'create_time' => time()
		];
		foreach ($signInfo['list'] as &$v){
			if($v['is_sign'] === 'no'){
				$data['money'] = $v['money'];
				$data['days'] = $v['day'];
				$v['is_sign'] = 'yes';
				break;
			}
		}
		if(!isset($data['money'])){
			res_api('签到参数有误');
		}
		$re = mdMember::doSign($data, $loginId);
		if($re){
			$signInfo['add_coin'] = intval($data['money']);
			$signInfo['cur_sign'] = 'yes';
			$key = md5($data['date'].'_signuser_'.$loginId);
			cache($key,$signInfo,86400);
			mdMember::addChargeLog($loginId, '+'.$data['money'].' 书币', '签到送书币');
			res_api($signInfo);
		}else{
			res_api('签到失败');
		}
	}
	
	//修改手机号
	public function changePhone(){
		checkToken();
		global $loginId;
		$rules = [
			'phone' => ["require|mobile",['require'=>'请输入手机号','mobile'=>'请输入正确格式的手机号']],
			'code' => ["require|number|length:4",['require'=>'请输入短信验证码','number'=>'验证码为4位纯数字','length'=>'验证码为4位纯数字']],
		];
		$post = myValidate::getData($rules);
		$flag = myMsg::check($post['phone'], $post['code']);
		if(is_string($flag)){
			res_api($flag);
		}
		$repeat = myDb::getValue('Member', [['phone','=',$post['phone']]], 'id');
		if($repeat){
			res_api('该手机号已被绑定');
		}
		$re = myDb::setField('Member', [['id','=',$loginId]], 'phone',$post['phone']);
		if($re){
			myCache::updateMember($loginId, 'phone', $post['phone']);
			res_api('ok');
		}else{
			res_api('绑定失败');
		}
	}
	
	//处理反馈
	public function doFeedback(){
		checkToken();
		global $loginId;
		$user = myCache::getMember($loginId);
		$rules = [
			'content' =>  ['require|max:200',['require'=>'请输入反馈内容','max'=>'反馈内容最多支持200个字符']],
			'phone' =>  ["mobile",['mobile'=>'手机号格式不正确']]
		];
		$data = myValidate::getData($rules);
		$data['content'] = htmlspecialchars($data['content']);
		$data['channel_id'] = $user['channel_id'];
		$data['agent_id'] = $user['agent_id'];
		$data['uid'] = $loginId;
		$data['create_time'] = time();
		$re = Db::name('Feedback')->insert($data);
		if($re){
			myCache::setMemberIsAdopt($loginId);
			res_api();
		}else{
			res_api('提交失败，请重试');
		}
	}
	
	//我的收藏
	public function getMyCollect(){
		checkToken();
		global $loginId;
		//var_dump($loginId);//6803
        //$loginId=41;
		//$list = mdMember::getMyCollect1($loginId,'api');
		$list = mdMember::getMyCollect($loginId);//application/common/model/mdMember.php
		$list = $list ? : 'ok';
		res_api($list);
	}
	
	//阅读记录
	public function getMyHistory(){
		checkToken();
		global $loginId;
		$list = mdMember::getReadHistory($loginId);
		$list = $list ? : 'ok';
		res_api($list);
	}
	
	//消费记录
	public function getMyConsumeList(){
		checkToken();
		global $loginId;
		$page = myHttp::getId('分页','page');
		$list = mdMember::getMyConsumeList($loginId, $page);
		$list = $list ? : 'ok';
		res_api($list);
	}
	
	//充值记录
	public function getMyChargeList(){
		checkToken();
		global $loginId;
		$page = myHttp::getId('分页','page');
		$list = mdMember::getMyChargeList($loginId, $page);
		$list = $list ? : 'ok';
		res_api($list);
	}
	
	//移除书籍
	public function doRemoveBook(){
		checkToken();
		global $loginId;
		$rules = [
			'flag' => ['require|in:1,2',['require'=>'请选择移除书籍出处','in'=>'未指定该出处']],
			'book_ids' => ['require',['require'=>'请选择需要移除的书籍']]
		];
		$post = myValidate::getData($rules);
		$bookIds = explode(',',trim($post['book_ids'],','));
		$res = false;
		$where = [['uid','=',$loginId],['book_id','in',$bookIds]];
		switch ($post['flag']){
			case 1:
				$re = Db::name('ReadHistory')->where($where)->setField('status',2);
				if($re !== false){
					myCache::rmReadCache($loginId);
					$res = true;
				}
				break;
			case 2:
				$re = Db::name('MemberCollect')->where($where)->delete();
				if($re !== false){
					myCache::rmCollectBookIds($loginId);
					$res = true;
				}
				break;
		}
		$str = $res ? 'ok' : '删除失败,请重试';
		res_api($str);
	}
	
	//获取关于我们信息
	public function getAboutMsg(){
		checkSign();
		$config = getMyConfig('website');
		$res = [
			'name' => $config['name'],
			'logo' => $config['logo'],
			'url' => $config['url'],
			'tel' => '',
			'email' => '',
			'qrcode' => ''
		];
		$contact = getMyConfig('site_contact');
		if($contact){
			$res['tel'] = $contact['tel'];
			$res['email'] = $contact['email'];
			$res['qrcode'] = $contact['qrcode'];
		}
		res_api($res);
	}
	
	//修改用户信息
	public function changeUserMsg(){
		checkToken();
		global $loginId;
		$rules = [
			'headimgurl' => ['require|url',['require'=>'请上传用户头像','url'=>'头像地址异常']],
			'nickname' => ['require|max:12',['require'=>'请输入您的昵称','max'=>'用户昵称最多12个字符']],
			'sex' => ['require|in:1,2',['require'=>'请选择您的性别','in'=>'未指定该性别']],
		];
		$data = myValidate::getData($rules);
		$data['nickname'] = htmlspecialchars($data['nickname']);
		$re = myDb::save('Member', [['id','=',$loginId]], $data);
		if($re){
			myCache::updateMember($loginId, $data);
			res_api();
		}else{
			res_api('保存失败');
		}
	}
	
	//创建昵称
	private function createNickname(){
		$str = 'reader_';
		$last_id = Db::name('Member')->max('id');
		$last_id = ($last_id+1) ? : 1;
		$last_id_str = ($last_id <= 999999) ? str_pad($last_id,6,"0",STR_PAD_LEFT) : $last_id;
		$str .= $last_id_str;
		return $str;
	}

	//注销账号
    public function delUser()
    {
        checkToken();
        global $loginId;
        //$loginId=174;
        $re = myDb::delUser('Member',[['id','=',$loginId]]);//extend/site/myDb.php
        if($re){
            res_api();
        }else{
            res_api('注销账号失败');
        }
    }

}