<?php
namespace app\api\controller;

use app\common\model\mdConfig;
use think\Db;
use site\myMsg;
use site\myValidate;
use site\myAliyunoss;
use site\myCache;
use think\facade\Log;

class Com extends Common{
	
	//发送短信
    public function sendMsg(){
        //checkSign();
        $rules = [
            'type' => ['require|in:1,2',['require'=>'验证码场景值错误','in'=>'未指定该验证码场景值']],
            'phone' => ['require|mobile',['require'=>'请输入手机号','mobile'=>'手机号格式错误']],
        ];

        $website = mdConfig::getConfig('website');//select `key`,value from sy_config where `key`='website';
        if (!isset($website['sms_type'])){
            $website['sms_type'] = 1;
        }
        $post = myValidate::getData($rules);
        switch ($post['type']){
            case 1:
                //注册
                $repeat_uid = Db::name('Member')->where('phone',$post['phone'])->value('id');
                if($repeat_uid){
                    res_api('该手机号已被注册');
                }
                //$res = myMsg::saiyouSend($post['phone']);
                break;
            case 2:
                //找回密码
                $repeat_uid = Db::name('Member')->where('phone',$post['phone'])->value('id');
                if(!$repeat_uid){
                    res_api('该手机号尚未绑定');
                }
                //$res = myMsg::saiyouSend($post['phone']);
                break;
        }
        if ($website['sms_type'] == 1){
            $res = myMsg::saiyouSend($post['phone']);
        }elseif($website['sms_type'] == 2){
            Log::info(date('Y-m-d H:i:s').'----开始发送短信');
            $res = myMsg::dxtonSend($post['phone']);//extend/site/myMsg.php
        }else{
            $res = myMsg::saiyouSend($post['phone']);
        }

        if($res){
            res_api();
        }else{
            res_api('发送失败,请重试');
        }
    }
	
	//上传图片
	public function uploadImg(){
		checkToken();
		global $loginId;
		$config = getMyConfig('alioss');
		if(!$config){
			res_api('系统参数配置异常');
		}
		myAliyunoss::$config = $config;
		$file = request()->file('file');
		if(!$file){
			res_api('未检测的图片');
		}
		$validate = array(
			'size' => 1024*1024*5,
			'ext' => 'jpg,jpeg,png,gif,bmp'
		);
		$date = date('Ymd');
		$ext = strtolower(pathinfo($file->getInfo('name'), PATHINFO_EXTENSION));
		$path = env('root_path').'static/temp/images';
		$name = md5('user_id_'.$loginId.'_'.microtime()).'.'.$ext;
		$info = $file
		->validate($validate)
		->move($path,$name);
		if($info){
			$filename = $path.'/'.$name;
			$savename = 'images/'.$date.'/'.$name;
			$url = myAliyunoss::putLocalFile($savename, $filename);
			@unlink($filename);
			if($url){
				res_api(['url'=>$url]);
			}else{
				res_api('上传失败，请重试');
			}
		}else{
			res_api($file->getError());
		}
	}
	
	//检测漫画板块是否关闭
	public function checkCartoon(){
		checkSign('get');
		$str = 'off';
		$config = getMyConfig('website');
		if($config && $config['is_cartoon'] == 1){
			$str = 'on';
		}
		res_api(['flag'=>$str]);
	}
	
	//获取最后更新版本号
	public function getVersion(){
		checkSign('get');
		$rules = ['device_type' => ['require|in:1,2',['require'=>'客户端类别错误','in'=>'未指定该客户端类别']]];
		$data = null;
		$device_type = myValidate::getData($rules,'get');
		$config = getMyConfig('site_version');
		if($config){
			$data = ['summary' => $config['summary']];
			switch ($device_type){
				case 1:
					$data['code'] = $config['android_code'];
					$data['download_url'] = $config['android_download_url'];
					break;
				case 2:
					$data['code'] = $config['ios_code'];
					$data['download_url'] = $config['ios_download_url'];
					break;
			}
		}
		res_api('ok',1,$data);
	}
	
	//获取启动图片
	public function getStartImg(){
		checkSign('get');
        $channel = $this->request->get('channel');//应用商店
        $key = 'app_start_img';
//        //cache($key,null);die;
//        if(cache('?'.$key)){
//            $app_start_img = cache($key);
//            if(!isset($app_start_img['notice'])){
//                cache($key,null);
//            }
//        }
		$data = getMyConfig($key,'api',$channel);//application/common.php
		$data = $data && $data['is_on'] == 1 ? $data : 'ok';
		res_api($data);
	}
	
	public function getMiIcon(){
		
		$flag = 2;
		
		res_api(['flag'=>$flag]);//application/common.php
        //return json_encode(['code'=>2,'msg'=>'ok','data'=>['flag'=>$flag]]);
	}
	
	//获取分享信息
	public function getShareMsg(){
		checkToken();
		global $loginId;
		$user = myCache::getMember($loginId);
		$site = getMyConfig('website');
		$share_url = $site['download_url'];
		$share_url .= strpos($share_url, '?') !== false ? '&' : '?';
		$share_url .= 'invite_code='.$user['invite_code'];
		$config = getMyConfig('site_share');
		$res = [
			'url' => $share_url,
			'title' => $config['title'],
			'desc' => $config['desc'],
			'img_url' => $site['logo']
		];
		res_api($res);
	}
	
}