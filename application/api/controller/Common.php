<?php
namespace app\api\controller;

use think\Controller;
use site\myValidate;
use think\Db;

class Common extends Controller{
	
	//获取分页
	protected function getPage($method='post'){
		$rules = ['page' => ['number|gt:0',['number'=>'分页参数不规范','gt'=>'分页参数不规范']]];
		$page = myValidate::getData($rules,$method);
		$page = $page ? : 1;
		return $page;
	}
	
	//获取单参数ID
	protected function getId($field,$title="",$method='post'){
		$rules = [$field => ['require|number|gt:0',['require'=>$title.'参数异常','number'=>$title.'参数不规范','gt'=>$title.'参数不规范']]];
		$id = myValidate::getData($rules,$method);
		return $id;
	}

	//获取小说
    protected function getNotice($page_notice="",$channel=""){
        //$notice_list='';
        $field = 'id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status';
	    if($channel=='default'){
            $notice_list = Db::name("Notice")->where(['status'=>1,'notice_type'=>'插屏广告'])->field($field)->select();
        }else{
            $where = [
                'notice_type' => '插屏广告',
                $channel => $channel,
                $channel.'_status' => 1
            ];
            $notice_list = Db::name("Notice")->where($where)->field($field)->select();
        }
//	    elseif ($channel=='huawei'){
//            $notice_list = Db::name("Notice")->where(['huawei_status'=>1,'notice_type'=>'插屏广告','huawei'=>$channel])->field('id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status')->select();
//        }elseif ($channel=='meizu'){
//            $notice_list = Db::name("Notice")->where(['meizu_status'=>1,'notice_type'=>'插屏广告','meizu'=>$channel])->field('id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status')->select();
//        }elseif ($channel=='oppo'){
//            $notice_list = Db::name("Notice")->where(['oppo_status'=>1,'notice_type'=>'插屏广告','oppo'=>$channel])->field('id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status')->select();
//        }elseif ($channel=='vivo'){
//            $notice_list = Db::name("Notice")->where(['vivo_status'=>1,'notice_type'=>'插屏广告','vivo'=>$channel])->field('id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status')->select();
//        }elseif ($channel=='xiaomi'){
//            $notice_list = Db::name("Notice")->where(['xiaomi_status'=>1,'notice_type'=>'插屏广告','xiaomi'=>$channel])->field('id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status')->select();
//        }
        //$notice_list = Db::name("Notice")->where(['status'=>1,'notice_type'=>'插屏广告'])->field(['create_time'],true)->select();
        if($page_notice != null){
            foreach ($notice_list as $key=>$val){
                $notice_list[$key]['page_notice'] = $page_notice;
            }
        }
        return $notice_list;
    }
}