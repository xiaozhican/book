<?php


namespace app\api\controller;


use site\myCache;
use site\myDb;
use site\myHttp;
use site\myValidate;
use think\Db;
use think\facade\Cache;

class Notice extends Common
{
    //获取广告
//    public function getNotice()
//    {
//        checkToken(false);
//        $table = Db::name('Notice')->field(['code','create_time'],true);
//        //识别手机类型
//        $type = $_SERVER['HTTP_USER_AGENT'];
//        if(strpos($type,'Android') !== false){
//            //安卓手机
//            $res = $table->where(['app_name'=>'Android','status'=>1])->select();
//        }elseif (strpos($type,'iPhone') !== false){
//            //苹果手机
//            $res = $table->where(['app_name'=>'IOS','status'=>1])->select();
//        }else{
//            $res = $table->where('status',1)->select();
//        }
//        $res =  $res??[];
//        res_api($res);
//    }
    public function test()
    {
        $res = Db::name('Count_copy1')->order('id desc')->limit(1)->find();
        if(!$res){
            $start_day = strtotime('2020-08-01 00:00:00');//如果表是空的，则从2020-08-01开始统计
        }else{
            $start_day = strtotime($res['date_time'].' 00:00:00');//从表中最后一条的时间开始统计
        }
        //$start_day = strtotime(date('Y-m-d').' 00:00:00');
        //$start_day = strtotime(date('Y-m-d',strtotime("-1 day")).' 00:00:00');//昨天的零点开始
        $end_day = strtotime(date('Y-m-d').' 23:59:59');//当天
        //计算昨天和今天的在线人数
        $sql = "SELECT count(id) renshu,login_type,FROM_UNIXTIME(create_time,'%Y-%m-%d') create_time  from sy_log_copy1 where STATUS=1 and create_time BETWEEN {$start_day} and {$end_day} GROUP BY login_type,FROM_UNIXTIME(create_time,'%Y-%m-%d') ORDER BY create_time asc";
        $sql1 = "select count(t.renshu) day_renshu,t.create_time from (
SELECT count(id) renshu,username,FROM_UNIXTIME(create_time,'%Y-%m-%d') create_time  from sy_log_copy1 where STATUS=1 and create_time BETWEEN {$start_day} and {$end_day} GROUP BY username,FROM_UNIXTIME(create_time,'%Y-%m-%d') ORDER BY create_time asc
)as t GROUP BY t.create_time;";//统计日活人数
        $list = Db::query($sql);
        $list1 = Db::query($sql1);


        if($list){
            foreach ($list as $k=>$v){
                if($v['login_type']=='android'){
                    $android = $v['renshu'];
                }
                if($v['login_type']=='ios'){
                    $ios = $v['renshu'];
                }
                if($v['login_type']=='h5'){
                    $h5 = $v['renshu'];
                }
                if($v['login_type']=='pc'){
                    $pc = $v['renshu'];
                }
                $where = [
                    'date_time'=>$v['create_time']
                ];
                $query = Db::name('Count_copy1')->where($where)->find();
                //var_dump($query);
                if(!$query){
                    Db::name('Count_copy1')->insert([
                        'date_time'=>$v['create_time'],
                        'android'=>$android??0,
                        'ios'=>$ios??0,
                        'h5'=>$h5??0,
                        'pc'=>$pc??0,
                    ]);
                }else{
                    Db::name('Count_copy1')->where($where)->update([
                        //'date_time'=>$v['create_time'],
                        'android'=>$android??0,
                        'ios'=>$ios??0,
                        'h5'=>$h5??0,
                        'pc'=>$pc??0,
                    ]);
                }
            }
        }

        if($list1){
            foreach ($list1 as $val){
                $where1 = [
                    'date_time'=>$val['create_time']
                ];
                $query = Db::name('Count_copy1')->where($where1)->find();
                if(!$query){
                    Db::name('Count_copy1')->insert([
                        'date_time'=>$val['create_time'],
                        'day_activity'=>$val['day_renshu']
                    ]);
                }else{
                    Db::name('Count_copy1')->where($where1)->update([
                        'day_activity'=>$val['day_renshu']
                    ]);
                }
            }
        }
    }

    public function login()
    {
//        $name = 'android';
//        $username = '15880043714';
//        $this->count($username,$name);
        $book_key = 'book_chapter_list_5486';
        $list = cache($book_key);
        //var_dump($list);
        return !isset($list[1]['page_notice'])?111:222;
    }
    private function count($username,$field){
        $redis = new \Redis();
        $redis->connect('127.0.0.1',6379);
        $date = date('Y-m-01');
        $res = Db::name('count1')->where('date_time',$date)->find();
        //var_dump($res);die;
        if(!$res){
            Db::name('count1')->insert([
                'date_time'=>$date,
                $field=>1
            ]);
        }else{
            Db::name('count1')->where('date_time',$date)->setInc($field);
        }
        $redis->pfAdd('login.'.$date,[$username]);
        $rihuo = $redis->pfcount('login.'.$date);
        Db::name('count1')->where('date_time',$date)->update(['day_activity'=>$rihuo]);
    }
    public function HotBookPush(){
        echo 'a';
        $redis = self::redisConnect();
        echo 'b';
        $key = 'HotBookPush';
        $is_key = $redis->exists($key);
        echo 'c';
        if($is_key == 0){
            echo 'd';
            //key不存在
            self::HotBookData($key);
        }
        echo 'e';
        $length = $redis->lLen($key);//获取长度
        if($length > 0){
            echo 1;
            //说明列表里面有数据
            $book_id = $redis->lPop($key);
            echo $book_id;
            $book_push = Db::name('Book')->where('id',$book_id)->field('id,type,name')->find();//推送的书籍
            $msg = [
                'title' => '热门书籍推荐',
                'content' => '今日为您推荐《'.$book_push['name'].'》，点我阅读吧',
                'extras' => [
                    'page' => "2",
                    'book_id' => ''.$book_push['id'],
                    'book_type' => ''.$book_push['type']
                ]
            ];
            //self::doPush('all', $msg);
        }
    }

    private static function HotBookData($key){
        $books = Db::name('Book')->where('status',1)->order('hot_num','desc')->field('id,type,name')->limit(5)->select();//取出人气高的前10条数据
        $redis = self::redisConnect();
        //$key = 'HotBookPush';
        foreach ($books as $book) {
            //循环存入redis中
            $redis->rPush($key,$book['id']);
        }
    }
    private static function redisConnect(){
        $redis = new \Redis();
        $redis->connect('127.0.0.1',6379);
        return $redis;
    }

    public function testNotice()
    {
//        $chapter = '2,3,6';
//        $notice_list = $this->getNotice($chapter);
//        $data = [
//            'notice_list'=>$notice_list
//        ];
//        res_api($data);

//        $arr = ['穿山甲'=>'穿山甲','广点通'=>'广点通'];
//        $rand = array_rand($arr,1);
//        //dump($rand);
//        $notice_list = Db::name("Notice")->where(['status'=>1,'notice_type'=>'插屏广告','advertisers'=>$rand])->field(['create_time'],true)->select();
//        dump($notice_list);

//        $key = 'member_info_6803';
//        if(cache('?'.$key)){
//            //cache($key,null);
//            $field = 'id,openid,wx_id,channel_id,agent_id,invite_code,phone,sex,nickname,headimgurl,subscribe,subscribe_time,viptime,money,status,spread_id,is_charge,is_auto,create_time,city';
//            $member = Db::name('Member')->where('id',6803)->field($field)->find();
//            //var_dump(cache($key));
//            var_dump(array_diff(cache($key),$member));
//        }
/*
        $key_money = 'member_money_6803';//用户金币缓存
        if('?'.$key_money){
            cache($key_money,null);
            //var_dump(cache($key_money));
        }
*/
//        $key = 'member_info_6803';//用户信息缓存
//        if(cache('?'.$key)){
//            //cache($key,null);
//            var_dump(cache($key));
//        }
    }
    
    //修改广告状态
    public function changeStatus()
    {
        $type = 'Android';
        $channel = $this->request->get('channel');
        //checkSign();
//        $rules = [
//            'phone' => ['require|mobile',['require'=>'请输入注册手机号','mobile'=>'手机号格式错误']],
//            'channel' => ['require',['require'=>'渠道参数异常']]
//        ];
//        $post = myValidate::getData($rules);
        //$res = Db::name('Notice')->where(['app_name'=>$type,$channel.'_status'=>$channel])->select();
        //var_dump($res);die;
        $status = $this->request->get('status');
        if($status==2){
            Db::name('Notice')->where(['app_name'=>$type])->update([
                'status' => 2,
                'huawei_status' => 2,
                'meizu_status' => 2,
                'oppo_status' => 2,
                'vivo_status' => 2,
                'xiaomi_status' => 2,
            ]);
        }elseif ($status==1){
            if(!$channel){
                Db::name('Notice')->where(['app_name'=>$type])->update(['status'=>1]);
            }else{
                Db::name('Notice')->where(['app_name'=>$type])->update([$channel.'_status'=>1]);
            }
        }
    }

    public function adduser()
    {
        //Db::name('Member')->where('id',6806)->update(['phone'=>'15880043714']);
        //$token = cookie('LOGIN_TOKEN');
        //cache($token,null);
        //cookie('LOGIN_TOKEN',null);
        //die;
        $headimgurl = 'http://img.b.0756hhr.com/images/20190910/9c02931c7abbcdd689e173d1c8964889.jpeg';
        $nickname = self::createNickname();
        $data = [
            'invite_code' => myDb::createCode('Member','invite_code'),
            'phone' => '15880043714',
            'openid' => '',
            'password' => createPwd('123456'),
            'headimgurl' => $headimgurl,
            'nickname' => $nickname,
            'sex' => 1,
            'spread_id' => 0,
            'create_time' => time(),
            'channel' => 'default'
        ];
        Db::name('Member')->insert($data);
    }
    private function createNickname(){
        $str = 'reader_';
        $last_id = Db::name('Member')->max('id');
        $last_id = ($last_id+1) ? : 1;
        $last_id_str = ($last_id <= 999999) ? str_pad($last_id,6,"0",STR_PAD_LEFT) : $last_id;
        $str .= $last_id_str;
        return $str;
    }

    //测试短信接口
    public function duanxin()
    {
//        $phone = '15880043714';
//        $key = $phone.'_code';
//        $code = mt_rand(1000,9999);
//        cache($key, $code, 120);
//        die;

        $url = 'http://sms.106jiekou.com/utf8/sms.aspx';
        $phone = '15880043714';
        $key = $phone.'_code';
        $code = cache($key);
        if(!$code){
            $code = mt_rand(1000,9999);
        }
        $data = [
            'account' => 'sonia10101',
            'password'=> '5199280356',
            'mobile'=>$phone,
            'content'=> rawurlencode(str_replace('CODE',$code,"您的验证码是：CODE。请不要把验证码泄露给其他人。如非本人操作，可不用理会！"))
        ];
        $curl_data = urldecode(http_build_query($data));
        //var_dump($curl_data);die;
        $re = myHttp::dxtonPost($curl_data,$url);
        var_dump($re);
        if($re == 100) {
            cache($key, $code, 120);
        }
    }
}