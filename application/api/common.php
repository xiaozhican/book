<?php

use think\Db;
use site\myHttp;

//验证签名
function checkSign($method='post'){
	$pass_key = getHeaderValue('passkey');
	if($pass_key && $pass_key === 'sy'){
		return true;
	}
	$header_sign = getHeaderValue('signature');
    //\think\facade\Log::info('header_sign='.$header_sign);
	if(!$header_sign){
		res_api('signature error');
	}
	$data = null;
	switch ($method){
		case 'get':
			$data = myHttp::getData('','get');
			break;
		case 'post':
			$data = myHttp::getData();
			break;
	}
	//\think\facade\Log::info($data);
	$logs = $data ? $data : [];
	$logs['signature'] = $header_sign;
	$logs['timestamp'] = getHeaderValue('timestamp');
	//dump($data);
	$sign = createSignature($data);
    //\think\facade\Log::info('sign='.$sign);
	if($sign !== $header_sign){
		res_api('check sign error');
	}
	return true;
}

//创建签名
function createSignature($data){
	$timestamp = getHeaderValue('timestamp');
	if(!$timestamp){
		res_api('signature params error');
	}
	$params = [
		'api_key' => '74656d7dd06607834867e4726bc97360',
		'timestamp' => $timestamp
	];
	foreach ($data as $k=>$v){
		if(strlen($v) > 0){
			$params[$k] = $v;
		}
	}
	ksort($params);
	$string = http_build_query($params);
	$str = urldecode($string);
	//dump($str);//api_key=74656d7dd06607834867e4726bc97360&amp;book_id=5489&amp;channel=default&amp;number=1&amp;timestamp=1603095328
	$sign = sha1(md5($str));
	return $sign;
}

//验证token 
function checkToken($is_must=true){
	global $loginId;
	$user_token = getHeaderValue('usertoken');
	if(!$user_token){
		if($is_must){
			res_api('登录已过期,请重新登录',3);
		}
		$loginId = 0;
	}else{
		$time = time();
		$token_msg = cache($user_token);
		if(!$token_msg){
			$tokenInfo = Db::name('MemberInfo')
			->where('token',$user_token)
			->field('id,uid,expire_time')
			->find();
			if(!$tokenInfo){
				res_api('您已在其他地方登录,请重新登录',3);
			}
			if($time > $tokenInfo['expire_time']){
				res_api('登录已过期,请重新登录',3);
			}
			if(($time -86400) < $tokenInfo['expire_time']){
				$over_time = 7*86400;
				$expire_time = $time + $over_time;
				$re = Db::name('MemberInfo')->where('id','=',$tokenInfo['id'])->setField('expire_time',$expire_time);
				if($re){
					$token_msg = ['uid'=>$tokenInfo['uid'],'expire_time'=>$expire_time];
					cache($user_token,$token_msg,$over_time);
				}
			}
			$loginId = $tokenInfo['uid'];
		}else{
			if($time > $token_msg['expire_time']){
				res_api('登录已过期,请重新登录',3);
			}
			if(($time -86400) < $token_msg['expire_time']){
				$over_time = 7*86400;
				$expire_time = $time + $over_time;
				$re = Db::name('MemberInfo')->where('uid',$token_msg['uid'])->setField('expire_time',$expire_time);
				if($re){
					$token_msg['expire_time'] = $expire_time;
					cache($user_token,$token_msg,$over_time);
				}
			}
			$loginId = $token_msg['uid'];
		}
	}
}

//检查用户状态
function checkMemberStatus($member){
	if($member){
		if($member['status'] != 1){
			res_api('您已被禁用，请联系客服',3);
		}
	}else{
		res_api('用户信息异常，请重新登录',3);
	}
}

//获取header自定义参数
function getHeaderValue($field){
	$res = false;
	$key = strtoupper('http_'.$field);
    //\think\facade\Log::info($field.'='.$key);
	//dump($key);
	if(isset($_SERVER[$key])){
        \think\facade\Log::info(11);
		$res = $_SERVER[$key];
	}
    //\think\facade\Log::info(22);
	//dump($res);
	return $res;
}