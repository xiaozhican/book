<?php

use think\Db;
use site\myHttp;
use think\facade\Request;

// +----------------------------------------------------------------------
// | ThinkPHP [ WE CAN DO IT JUST THINK ]
// +----------------------------------------------------------------------
// | Copyright (c) 2006-2016 http://thinkphp.cn All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: 流年 <liu21st@gmail.com>
// +----------------------------------------------------------------------

// 应用公共文件

/**
 * 打印数据
 * @param mixed $data 需要打印的数据
 * @param string $isdump 是否打印数据类型
 */
function my_print($data,$isdump=false){
    echo '<meta charset="utf-8"><pre>';
    if(!$isdump){
        print_r($data);
    }else{
        var_dump($data);
    }
    exit;
}

/**
 * 通用api返回信息
 * @param string $msg 消息
 * @param number $code 消息代码
 * @param mixed $data 返回数据
 */
function res_api($msg='ok',$code=2,$data=null){
	if(is_array($msg)){
		$data = $msg;
		$msg = 'ok';
		$code = $code === 2 ? 1 : $code;
	}else{
		$code = ($msg === 'ok' && $code === 2) ? 1 : $code;
	}
	$res = ['code' => $code,'msg' => $msg,'data' => $data];
	exit(json_encode($res,JSON_UNESCAPED_UNICODE));
}

/**
 * 输出错误信息
 * @param string $msg
 */
function res_error($msg){
	if(Request::isAjax()){
		$res = [
			'code' => 2,
			'msg' => $msg,
			'data' => null,
		];
		exit(json_encode($res,JSON_UNESCAPED_UNICODE));
	}else{
		header("Content-type:text/html;charset=utf-8");
		$str = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">';
		$str .= '<html>';
		$str .= '<head>';
		$str .= '<title>出错了！！！</title>';
		$str .= '<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">';
		$str .= '<style type="text/css">';
		$str .= 'html,body,div,span,a,img,header{margin:0;padding:0;border:0;font-size:100%;font:inherit;vertical-align:baseline;}';
		$str .= 'header{display: block;}';
		$str .= 'a{text-decoration:none;}';
		$str .= 'img{max-width:100%;}';
		$str .= 'body{background: url(/static/common/images/errorbg.png);font-family: "Century Gothic",Arial, Helvetica, sans-serif;font-weight:100%;}';
		$str .= '.header{height:80px;}';
		$str .= '.content p{margin: 15px 0px 22px 0px;font-family: "Century Gothic";font-size: 1.5em;color:red;text-align:center;}';
		$str .= '.content  span,.logo h1 a{color:#e54040;}';
		$str .= '.content span{font-size: 1.5em;}';
		$str .= '.content{text-align:center;padding: 25px 0px 0px 0px;margin: 0px 12px;}';
		$str .= '.content a{color:#fff;font-family: "Century Gothic";background: #666666;padding: 8px 17px;}';
		$str .= '.content a:hover{color:#e54040;}';
		$str .= '</style>';
		$str .= '</head>';
		$str .= '<body>';
		$str .= '<div class="wrap">';
		$str .= '<div class="header"></div>';
		$str .= '<div class="content">';
		$str .= '<img src="/static/common/images/errorimg.png" title="error" />';
		$str .= '<p>'.$msg.'</p>';
		$str .= '<a href="javascript:void(0);" onclick="javascript:history.go(-1);">Back</a>';
		$str .= '</div>';
		$str .= '</div>';
		$str .= '</body>';
		$str .= '</html>';
		echo $str;
		exit;
	}
	
}

/**
 * 返回表格数据
 * @param mixed $data
 * @param number $count
 */
function res_table($data,$count=0){
	$res = [
		'code' => 1,
		'msg' => 'ok',
		'data' => $data,
		'count' => $count
	];
	exit(json_encode($res,JSON_UNESCAPED_UNICODE));
}

/**
 * 构建url
 * @param string $url 模块名
 * @param array $param 参数
 * @param array $is_http 是否加入域名
 */
function my_url($url,$param=[],$is_http=false){
    $link = url($url);
    if($param){
        $str = is_array($param) ? http_build_query($param) : $param;
        $link .= '?'.$str;
    }
    return $link;
}

//获取js资源路径
function getJs($name,$module='admin',$param=[]){
	$path = '/static/resource/'.$module.'/'.$name.'.js';
	if(!empty($param) && is_array($param)){
		$path .= '?'.http_build_query($param);
	}
	return $path;
}

/**
 * 构建单选html
 * @param array $config 单选配置
 * @param string $val 默认选中值
 * @return string 单选表单
 */
function createRadioHtml($config,$val=''){
    $str = '';
    foreach ($config['option'] as $v){
        $checked = '';
        if(strlen($val) > 0){
            $checked = ($val == $v['val']) ? 'checked="checked"' : '';
        }else{
            $checked = $v['default'] ? 'checked="checked"' : '';
        }
        $str .= '<input type="radio" lay-filter="'.$config['name'].'" name="'.$config['name'].'" value="'.$v['val'].'" title="'.$v['text'].'" '.$checked.' />';
    }
    return $str;
}

/**
 * 构建下拉框选项列表
 * @param array $list 选项值
 * @param string $val 选中值
 * @param string $default_str 没有选中值时选项
 * @return string
 */
function createSelectHtml($list,$val='',$default_str=''){
    $str = '';
    if($default_str){
        $str = '<option value="">'.$default_str.'</option>';
    }
    foreach ($list as $v){
        $selected = '';
        if($val != '' && $val == $v['id']){
            $selected = 'selected="selected"';
        }
        $str .= '<option value="'.$v['id'].'" '.$selected.'>'.$v['name'].'</option>';
    }
    return $str;
}

//构建复选框
function createCheckBoxHtml($list,$name,$cur){
	$str = '';
	if($list && is_array($list)){
		$check = [];
		if($cur){
			if(!is_array($cur)){
				$check = explode(',', trim($cur,','));
			}else{
				$check = $cur;
			}
		}
		
		foreach ($list as $v){
			$checked = '';
			if(is_array($v)){
				if(in_array($v['id'], $check)){
					$checked = 'checked';
				}
				$str .= '<input type="checkbox" name="'.$name.'[]" value="'.$v['id'].'" lay-skin="primary" title="'.$v['name'].'" '.$checked.' >';
			}else{
				if(in_array($v,$check)){
					$checked = 'checked';
				}
				$str .= '<input type="checkbox" name="'.$name.'[]" value="'.$v.'" lay-skin="primary" title="'.$v.'" '.$checked.' >';
			}
		}
	}
	return $str;
}

/**
 * 读取级联菜单
 * @param array $list
 * @param string $pk
 * @param string $pid
 * @param string $child
 * @param number $startPid
 * @return unknown
 */
function list_to_tree($list, $pk='id', $pid = 'pid', $child = '_child', $startPid = 0) {
    $tree = array();
    if(is_array($list)) {
        $refer = array();
        foreach ($list as $key => $data) {
            $refer[$data[$pk]] =& $list[$key];
        }
        foreach ($list as $key => $data) {
            $parentId =  $data[$pid];
            if ($startPid == $parentId) {
                $tree[] =& $list[$key];
            }else{
                if(isset($refer[$parentId])) {
                    $parent =& $refer[$parentId];
                    $parent[$child][] =& $list[$key];
                }
            }
        }
    }
    $tree = getIslast($tree);
    return $tree;
}

/**
 * 判断级联菜单在同一级中是否为最后一个
 * @param unknown $list
 * @param number $level
 * @return unknown
 */
function getIslast($list,$level=0){
    foreach($list as $k=>&$v){
        $v['level'] = $level+1;
        $count = count($list)-1;
        if($k==$count){
            $v['islast'] = "Y";
        }else{
            $v['islast'] = "N";
        }
        if(isset($v['_child'])){
            getIslast($v['_child'],$v['level']);
        }
    }
    return $list;
}

/**
 * 构建后台主菜单
 * @param array $menu 菜单数组
 * @param string $target 指向iframe
 */
function createMenu($menu){
    $str = '';
    foreach ($menu as $v){
        $line_str = '';
        if(!array_key_exists('child', $v)){
            $line_str .= 'lay-tips="'.$v['name'].'" lay-direction="2" ';
            if($v['url']){
                if(stripos($v['url'],'http')){
                    $line_str .= 'href="'.$v['url'].'" target="iframe-main"';
                }else{
                    $line_str .= 'href="'.my_url($v['url']).'" target="iframe-main"';
                }
            }
        }
        $str .= '<li class="layui-nav-item">';
        $str .= '<a '.$line_str.' >';
        $str .= '<i class="layui-icon '.$v['icon'].'"></i>';
        $str .= '<cite>'.$v['name'].'</cite>';
        $str .= '</a>';
        if(array_key_exists('child', $v)){
            $str .= createChildMenu($v['child']);
        }
        $str .= '</li>';
    }
    return $str;
}

/**
 * 构建后台子菜单
 * @param array $menu
 * @param string $target 指向iframe
 * @return string
 */
function createChildMenu($menu){
    $str = '<dl class="layui-nav-child">';
    foreach ($menu as $v){
        if(array_key_exists('child', $v)){
            $str .= '<dd class="layui-nav-item">';
            $str .= '<a href="javascript:;">'.$v['name'].'</a>';
            $str .= createChildMenu($v['child']);
        }else{
            $line_str = '';
            if(!array_key_exists('child', $v)){
                $line_str .= 'lay-tips="'.$v['name'].'" lay-direction="2" ';
                if($v['url']){
                    if(stripos($v['url'], 'http')){
                        $line_str .= 'href="'.$v['url'].'" target="iframe-main"';
                    }else{
                        $line_str .= 'href="'.my_url($v['url']).'" target="iframe-main"';
                    }
                }
            }
            $str .= '<dd>';
            $str .= '<a '.$line_str.'>'.$v['name'].'</a>';
        }
        $str .= '</dd>';
    }
    $str .= '</dl>';
    return $str;
}

/**
 * 获取我的配置
 * @param string $key
 * @return number|mixed|\think\cache\Driver|boolean
 */
function getMyConfig($key,$type="",$channel=""){
    $data = Db::name('Config')->where('key','=',$key)->where('status','=',1)->value('value');
    $data = json_decode($data,true);
    //要api接口调用的才获取广告信息
    if($type&&$channel){
        //$notice_list = Db::name("Notice")->where(['status'=>1,'notice_type'=>'开屏广告'])->field('id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status')->select();
        $field = 'id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status';
        if($channel=='default'){
            $notice_list = Db::name("Notice")->where(['status'=>1,'notice_type'=>'开屏广告'])->field($field)->select();
        }else{
            $where = [
                'notice_type' => '开屏广告',
                $channel => $channel,
                $channel.'_status' => 1
            ];
            $notice_list = Db::name("Notice")->where($where)->field($field)->select();
        }
        $data['notice'] = $notice_list;
    }
    $data = $data ? : 0;
	return $data;
}

//备份
function getMyConfig1($key,$type="",$channel=""){
    $re = cache('?'.$key);
    //cache($key,null);die;
    if($re){
        $data = cache($key);
    }else{
        $data = Db::name('Config')->where('key','=',$key)->where('status','=',1)->value('value');
        $data = json_decode($data,true);
        //要api接口调用的才获取广告信息
        if($type&&$channel){
            $notice_list="";
            //$notice_list = Db::name("Notice")->where(['status'=>1,'notice_type'=>'开屏广告'])->field('id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status')->select();
            if($channel=='defaultd'){
                $notice_list = Db::name("Notice")->where(['status'=>1,'notice_type'=>'开屏广告'])->field('id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status')->select();
            }elseif ($channel=='huawei'){
                $notice_list = Db::name("Notice")->where(['huawei_status'=>1,'notice_type'=>'开屏广告','huawei'=>$channel])->field('id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status')->select();
            }elseif ($channel=='meizu'){
                $notice_list = Db::name("Notice")->where(['meizu_status'=>1,'notice_type'=>'开屏广告','meizu'=>$channel])->field('id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status')->select();
            }elseif ($channel=='oppo'){
                $notice_list = Db::name("Notice")->where(['oppo_status'=>1,'notice_type'=>'开屏广告','oppo'=>$channel])->field('id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status')->select();
            }elseif ($channel=='vivo'){
                $notice_list = Db::name("Notice")->where(['vivo_status'=>1,'notice_type'=>'开屏广告','vivo'=>$channel])->field('id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status')->select();
            }elseif ($channel=='xiaomi'){
                $notice_list = Db::name("Notice")->where(['xiaomi_status'=>1,'notice_type'=>'开屏广告','xiaomi'=>$channel])->field('id,app_name,notice_name,notice_type,app_id,notice_id,advertisers,status')->select();
            }
            $data['notice'] = $notice_list;
        }
        $data = $data ? : 0;
        cache($key,$data);
    }
    return $data;
}


//创建登录密码
function createPwd($str){
    $key = 'kaichiwangluo';
    $str .= $key;
    $res = md5(sha1($str));
    return $res;
}

function isMobile() {
    // 如果有HTTP_X_WAP_PROFILE则一定是移动设备
    if (isset($_SERVER['HTTP_X_WAP_PROFILE'])) {
        return true;
    }
    // 如果via信息含有wap则一定是移动设备,部分服务商会屏蔽该信息
    if (isset($_SERVER['HTTP_VIA'])) {
        // 找不到为flase,否则为true
        return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
    }
    // 脑残法，判断手机发送的客户端标志,兼容性有待提高。其中'MicroMessenger'是电脑微信
    if (isset($_SERVER['HTTP_USER_AGENT'])) {
        $clientkeywords = array('nokia','sony','ericsson','mot','samsung','htc','sgh','lg','sharp','sie-','philips','panasonic','alcatel','lenovo','iphone','ipod','blackberry','meizu','android','netfront','symbian','ucweb','windowsce','palm','operamini','operamobi','openwave','nexusone','cldc','midp','wap','mobile','MicroMessenger');
        // 从HTTP_USER_AGENT中查找手机浏览器的关键字
        if (preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
            return true;
        }
    }
    // 协议法，因为有可能不准确，放到最后判断
    if (isset ($_SERVER['HTTP_ACCEPT'])) {
        // 如果只支持wml并且不支持html那一定是移动设备
        // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
            return true;
        }
    }
    return false;
}
function isWeixin() {
    if (strpos($_SERVER['HTTP_USER_AGENT'], 'MicroMessenger') !== false) {
        return true;
    } else {
        return false;
    }
}
function is_mobile(){

// returns true if one of the specified mobile browsers is detected

// 如果监测到是指定的浏览器之一则返回true

$regex_match="/(nokia|iphone|android|motorola|^mot\-|softbank|foma|docomo|kddi|up\.browser|up\.link|";

$regex_match.="htc|dopod|blazer|netfront|helio|hosin|huawei|novarra|CoolPad|webos|techfaith|palmsource|";

$regex_match.="blackberry|alcatel|amoi|ktouch|nexian|samsung|^sam\-|s[cg]h|^lge|ericsson|philips|sagem|wellcom|bunjalloo|maui|";

$regex_match.="symbian|smartphone|midp|wap|phone|windows ce|iemobile|^spice|^bird|^zte\-|longcos|pantech|gionee|^sie\-|portalmmm|";

$regex_match.="jig\s browser|hiptop|^ucweb|^benq|haier|^lct|opera\s*mobi|opera\*mini|320x320|240x320|176x220";

$regex_match.=")/i";

// preg_match()方法功能为匹配字符，既第二个参数所含字符是否包含第一个参数所含字符，包含则返回1既true

return preg_match($regex_match, strtolower($_SERVER['HTTP_USER_AGENT']));
}



//写入静态缓存文件
function saveBlock($html,$fileKey,$dirname='book'){
    $res = false;
    $path = env('root_path');
    $path .= 'static/block/'.$dirname.'/';
    if(!is_dir($path)){
        mkdir($path,0777,true);
    }
    if(is_dir($path) && is_writable($path)){
        $path .= $fileKey.'.html';
        $obj = fopen($path,'w');
        fwrite($obj, $html);
        fclose($obj);
        if(is_file($path)){
            $res = true;
        }
    }
    return $res;
}

//获取静态缓存内容
function getBlockContent($filekey,$dirname='book'){
    $path = env('root_path');
    $path .= 'static/block/'.$dirname.'/'.$filekey.'.html';
    $content = '';
    if(@is_file($path)){
        $content = file_get_contents($path);
    }
    return $content;
}

//读取缓存静态文件
function getBlock($filekey,$dirname='book'){
    $filename = env('root_path');
    $filename .= 'static/block/'.$dirname.'/'.$filekey.'.html';
    if(@is_file($filename)){
        require_once $filename;
    }
}

function newBook($list){
    //<div class="box"><div class="title"><h2>新书推荐</h2><a href="/index/book/more.html?type=1&gender_type=1&area_id=4">查看更多<img src="/static/resource/index/images/next_left.png"></a></div><div class="row_content"><a href="/index/Book/info.html?book_id=11307" class="item"><img class="cover" src="https://shuwu.oss-cn-hongkong.aliyuncs.com/images/15922820620616/c5acadf341047cf9734df0cd8d8a1425.jpg"><p class="name">破局者/墨绿青苔</p><div class="read"><img src="/static/resource/index/images/eye.png"><span>6.3w</span></div></a><a href="/index/Book/info.html?book_id=11342" class="item"><img class="cover" src="https://shuwu.oss-cn-hongkong.aliyuncs.com/images/15922887180616/ddcc90fbd1932029f1541a279fcaf545.jpg"><p class="name">都市怪谈</p><div class="read"><img src="/static/resource/index/images/eye.png"><span>12.2w</span></div></a><a href="/index/Book/info.html?book_id=11391" class="item"><img class="cover" src="https://shuwu.oss-cn-hongkong.aliyuncs.com/images/15922909730616/a461543979dd6952ed9503f8bd16ef53.jpg"><p class="name">续城之半生浮图</p><div class="read"><img src="/static/resource/index/images/eye.png"><span>9w</span></div></a><a href="/index/Book/info.html?book_id=11426" class="item"><img class="cover" src="https://shuwu.oss-cn-hongkong.aliyuncs.com/images/15922926640616/e4ff7d53d3d08c772bea3077de8e8161.jpg"><p class="name">丧尸爆发之全家求生路</p><div class="read"><img src="/static/resource/index/images/eye.png"><span>5.1w</span></div></a><a href="/index/Book/info.html?book_id=11466" class="item"><img class="cover" src="https://shuwu.oss-cn-hongkong.aliyuncs.com/images/15922941990616/cedeb5cc9c33f9f2f59e53d5e0f628a5.jpg"><p class="name">阴魂禁忌/水上君子</p><div class="read"><img src="/static/resource/index/images/eye.png"><span>9w</span></div></a><a href="/index/Book/info.html?book_id=11492" class="item"><img class="cover" src="https://shuwu.oss-cn-hongkong.aliyuncs.com/images/15922951830616/0714286157bffcdb2e9c76691d22d07d.jpg"><p class="name">寻魂记</p><div class="read"><img src="/static/resource/index/images/eye.png"><span>14.2w</span></div></a></div></div>
    $str = '';
    if($list){
        $str = '<div class="box"><div class="title"><h2>新书推荐</h2><a href="/index/book/more.html?type=1&gender_type=1&area_id=4">查看更多<img src="/static/resource/index/images/next_left.png"></a></div><div class="row_content">';
        foreach ($list as $val){
            //var_dump($val['id']);
            $rand = randFloat();
            $str .= '<a href="/index/Book/info.html?book_id="'.$val['id'].'" class="item"><img class="cover" src="'.$val['cover'].'"><p class="name">'.$val['name'].'</p><div class="read"><img src="/static/resource/index/images/eye.png"><span>'.$rand.'w</span></div></a>';
        }
        $str .= '</div></div>';
    }
    return $str;
}
function manJx(){
    $str = '
    <div class="box"><div class="title"><h2>男生精选</h2><a href="/index/book/genderMore.html?type=1&gender_type=1">查看更多<img src="/static/resource/index/images/next_left.png"></a></div><div class="column_content"><a href="/index/Book/info.html?book_id=5489" class="item"><img class="cover" src="https://shuwu.oss-cn-hongkong.aliyuncs.com/images/15876818420424/0c9ec857e5b82dc131c04dd308251d94.jpg"><div class="right"><p class="name">婿者无疆</p><p class="main">被所有人当做窝囊废的上门女婿楚凡，竟然是一条蓄势待飞的真龙！
    若有人谤我、辱我、轻我、笑我、欺我、贱我，当如何处治乎？
    楚凡：“呵，他们敢吗？”</p><p class="article_type"><span>男生都市</span></p></div></a><a href="/index/Book/info.html?book_id=13289" class="item"><img class="cover" src="https://shuwu.oss-cn-hongkong.aliyuncs.com/images/20201124/07ff115b5ba9b800dee70c356c121105.jpg"><div class="right"><p class="name">最佳豪婿</p><p class="main">入赘三年，所有人都当他只是个会做饭的家庭煮夫，肆意凌辱！殊不知他真正的身份......</p><p class="article_type"><span>男生都市</span></p></div></a><a href="/index/Book/info.html?book_id=5481" class="item"><img class="cover" src="https://shuwu.oss-cn-hongkong.aliyuncs.com/images/15876815420424/d310ad2d38d0e5d052f831e843bca8ac.jpg"><div class="right"><p class="name">中华龙婿</p><p class="main">“少爷，你这花钱速度太慢了，家族给了你十个亿，怎么一个月了还没花完？”
    作为顶级豪门的继承人，陈峰很烦恼，别人都是不努力上班，就要回家种地。而陈峰不努力当上门女婿，就只能回去继承万亿家产。
    唉，生活太难了。


    </p><p class="article_type"><span>男生都市</span></p></div></a><a href="/index/Book/info.html?book_id=5482" class="item"><img class="cover" src="https://shuwu.oss-cn-hongkong.aliyuncs.com/images/15876815820424/6acd49aa51f59382b68d029b739700f3.jpg"><div class="right"><p class="name">无敌长生女婿</p><p class="main">自从吞服了女蜗炼制的仙丹后，丁毅便成了一个怪物，五千多年来，丁毅换了上百种身份，神医华佗的授业恩师，教项羽剑法的神秘剑客，统御六合八荒的帝王……这次，他竟然是一个上门女婿…… </p><p class="article_type"><span>男生都市</span></p></div></a><a href="/index/Book/info.html?book_id=6176" class="item"><img class="cover" src="https://shuwu.oss-cn-hongkong.aliyuncs.com/images/15876913190424/0e6abc439cdae386ef00cfacbb85fabc.jpg"><div class="right"><p class="name">武霸乾坤</p><p class="main">浩瀚乾坤，万物圣祖；缘生缘灭，圣枪伏龙；蛮荒祖体，吞天噬地……看主角千浩如何在这个不属于他前世认知的世界里迷倒万千少女，闯下属于自己的逍遥天下！</p><p class="article_type"><span>男生玄幻</span></p></div></a><a href="/index/Book/info.html?book_id=12613" class="item"><img class="cover" src="https://shuwu.oss-cn-hongkong.aliyuncs.com/images/15923932310617/5491d58de24dd75e87eec7a7722f00d2.jpg"><div class="right"><p class="name">都市大仙尊/小小财神</p><p class="main">在历史的洪流之中，宛如过客一般存在了数万年的苏武，再度重临都市，装逼泡妞，纵横花都，捍卫至亲。</p><p class="article_type"><span>男生都市</span></p></div></a></div></div>
     ';
    return $str;
}
//随机生成小数保留两位小数点
function randFloat($min = 1, $max = 10) {
    $rand = $min + mt_rand() / mt_getrandmax() * ($max - $min);
    return floatval(number_format($rand,1));
}

/**
 * 写日志
 * @param mixed $data 记录内容
 * @param string $title 日志标题
 */
function writeLogs($data,$title=''){
	$log = [
			'create_time' => date('Y-m-d H:i:s'),
			'title' => $title,
			'data' => $data
	];
	$old_log = cache('sys_log');
	if($old_log){
		$old_log[] = $log;
		$old_log = array_reverse($old_log);
	}else{
		$old_log = [$log];
	}
	cache('sys_log',$old_log);
}

//处理跨域图片地址
function returnImg($url){
	if(stripos($url,'http') !== false){
		return '/img?url='.$url;
	}else{
		return $url;
	}
}

/**
 * 长链接转为短链接
 * @param  string $url_long 要转换的链接
 * @return string 短链接
 */
function getShortUrl($url_long){
	$url = urlencode($url_long);
	$request_url = "http://sa.sogou.com/gettiny?url=".$url;
	$res = myHttp::doGet($request_url,'','string');
	$short_url = (substr($res, 0,4) === 'http') ? $res : '';
	return $short_url;
}

//格式化数值
function formatNumber($number){
	$number = is_numeric($number) ? $number : 0;
	if ($number >= 1000 && $number < 10000){
		$number = round($number/1000,1).'k';
	}elseif ($number >= 10000){
		$number = round($number/10000,1).'w';
	}
	return $number;
}

function checkIsWeixin(){
	if (strpos($_SERVER['HTTP_USER_AGENT'],'MicroMessenger') !== false ){
		return true;
	}
	return false;
}

function addZero($number){
	if(is_numeric($number) && $number > 0){
		return str_pad($number,6,"0",STR_PAD_LEFT);
	}
	return $number;
}

//检测跳转
function checkLocation($obj,$url,$client=1){
	$cur_client = request()->isMobile() ? 2 : 1;
	if($cur_client != $client){
		redirect($url);
	}
}