<?php

//创建首页导航
function createBookNav($key=''){
    //$pcNavs = \think\Db::name('pc_nav')->order('id asc')->select();
	/*
    $list = [
		'index' => ['name'=>'首页','is_show'=>$pcNavs[0]['is_show'],'url'=>'/home','click'=>''],
		'novel_man' => ['name'=>'男生','is_show'=>$pcNavs[1]['is_show'],'url'=>'/home/index/index.html?type=1&gender_type=1','click'=>''],
		'novel_woman' => ['name'=>'女生','is_show'=>$pcNavs[2]['is_show'],'url'=>'/home/index/index.html?type=1&gender_type=2','click'=>''],
		'cartoon' => ['name'=>'漫画','is_show'=>$pcNavs[3]['is_show'],'url'=>'','click'=>'','child'=>[
				['key'=>'man','name'=>'男','url'=>'/home/index/index.html?type=2&gender_type=1'],
				['key'=>'woman','name'=>'女','url'=>'/home/index/index.html?type=2&gender_type=2']
			]
		],
		'category' => ['name'=>'书库','is_show'=>$pcNavs[4]['is_show'],'url'=>'/home/book/category.html','click'=>''],
		'rank' => ['name'=>'榜单','is_show'=>$pcNavs[5]['is_show'],'url'=>'/home/book/rank.html','click'=>''],
		'publish' => ['name'=>'出版','is_show'=>$pcNavs[6]['is_show'],'url'=>'/home/book/publish','click'=>''],
		'activity' => ['name'=>'活动','is_show'=>$pcNavs[7]['is_show'],'url'=>'/home/book/activity','click'=>''],
        'free' => ['name'=>'限免','is_show'=>$pcNavs[8]['is_show'],'url'=>'/home/book/free','click'=>''],
        'news' => ['name'=>'公告资讯','is_show'=>$pcNavs[9]['is_show'],'url'=>'/home/book/news','click'=>''],
        'map' => ['name'=>'网站地图','is_show'=>$pcNavs[10]['is_show'],'url'=>'/home/book/map','click'=>''],
		'charge' => ['name'=>'充值','is_show'=>$pcNavs[11]['is_show'],'url'=>'/home/charge/index','click'=>'']
	];
    */
//    $list = [
//        'index' => ['name'=>$pcNavs[0]['name'],'is_show'=>$pcNavs[0]['is_show'],'url'=>$pcNavs[0]['url'],'click'=>''],
//        'novel_man' => ['name'=>$pcNavs[1]['name'],'is_show'=>$pcNavs[1]['is_show'],'url'=>$pcNavs[1]['url'],'click'=>''],
//        'novel_woman' => ['name'=>$pcNavs[2]['name'],'is_show'=>$pcNavs[2]['is_show'],'url'=>$pcNavs[2]['url'],'click'=>''],
//        'cartoon' => ['name'=>$pcNavs[3]['name'],'is_show'=>$pcNavs[3]['is_show'],'url'=>$pcNavs[3]['url'],'click'=>'','child'=>[
//            ['key'=>'man','name'=>'男','url'=>'/home/index/index.html?type=2&gender_type=1'],
//            ['key'=>'woman','name'=>'女','url'=>'/home/index/index.html?type=2&gender_type=2']
//        ]
//        ],
//        'category' => ['name'=>$pcNavs[4]['name'],'is_show'=>$pcNavs[4]['is_show'],'url'=>$pcNavs[4]['url'],'click'=>''],
//        'rank' => ['name'=>$pcNavs[5]['name'],'is_show'=>$pcNavs[5]['is_show'],'url'=>$pcNavs[5]['url'],'click'=>''],
//        'publish' => ['name'=>$pcNavs[6]['name'],'is_show'=>$pcNavs[6]['is_show'],'url'=>$pcNavs[6]['url'],'click'=>''],
//        'activity' => ['name'=>$pcNavs[7]['name'],'is_show'=>$pcNavs[7]['is_show'],'url'=>$pcNavs[7]['url'],'click'=>''],
//        'free' => ['name'=>$pcNavs[8]['name'],'is_show'=>$pcNavs[8]['is_show'],'url'=>$pcNavs[8]['url'],'click'=>''],
//        'overtype' => ['name'=>$pcNavs[14]['name'],'is_show'=>$pcNavs[14]['is_show'],'url'=>$pcNavs[14]['url'],'click'=>''],
//        'overtype2' => ['name'=>$pcNavs[15]['name'],'is_show'=>$pcNavs[15]['is_show'],'url'=>$pcNavs[15]['url'],'click'=>''],
//        'news' => ['name'=>$pcNavs[9]['name'],'is_show'=>$pcNavs[9]['is_show'],'url'=>$pcNavs[9]['url'],'click'=>''],
//        'map' => ['name'=>$pcNavs[10]['name'],'is_show'=>$pcNavs[10]['is_show'],'url'=>$pcNavs[10]['url'],'click'=>''],
//        'charge' => ['name'=>$pcNavs[11]['name'],'is_show'=>$pcNavs[11]['is_show'],'url'=>$pcNavs[11]['url'],'click'=>''],
//    ];
    $list = \site\myCache::getPcNavList();
    $str = '';
    foreach ($list as $k=>$v){
        $class = $key === $k || (stripos($key,$k) !== false) ? 'nav_active' : '';
        $url = $v['url'] ? : 'javascript:void(0);';
        if($k === 'charge'){
            $str .= '<li class="nav_item recharge '.$class.'"><a href="javascript:void(0);" onclick="javascript:checkJump(\''.$url.'\');">';
            $str .= '<img class="recharge_icon" src="/static/resource/home/images/recharge_icon.png">';
            $str .= '<span>'.$v['name'].'</span>';
            $str .= '</a>';
        }else{
            if ($v['is_show'] != 2) {
                $click = $v['click'] ? 'onclick="javascript:' . $v['click'] . '();"' : '';
                $str .= '<li class="nav_item main_item ' . $class . '" style="margin-right:22px;">';
                $str .= '<a style="padding: 0 8px;clear:both;" href="' . $url . '" ' . $click . '>' . $v['name'] . '</a>';
                if (isset($v['child'])) {
                    $str .= '<div class="sub_box" style="width: 80px;">';
                    foreach ($v['child'] as $val) {
                        if ($val['is_show'] != 2){
                            $ck = $k . '_' . $val['key'];
                            $child_class = $ck === $key ? 'nav_active' : '';
                            $str .= '<a style="padding: 0 0 0 10px;text-align:left;" href="' . $val['url'] . '" class="sub_item ' . $child_class . '">' . $val['name'] . '</a>';
                        }
                    }
                    $str .= '</div>';
                }
            }
        }
        $str .= '</li>';
    }
    return $str;
}

//创建个人中心导航
function createUserNav($key=''){
	$list = [
		['name'=>'我的书架','url'=>'/home/user/bookself.html','key'=>1],
		['name'=>'阅读历史','url'=>'/home/user/readhistory.html','key'=>2],
		['name'=>'消费记录','url'=>'/home/user/log.html','key'=>3],
		['name'=>'福利中心','url'=>'/home/user/sign.html','key'=>4],
		['name'=>'意见反馈','url'=>'/home/user/feedback.html','key'=>5],
		//['name'=>'关于我们','url'=>'/home/user/about.html','key'=>6],
        ['name'=>'账户设置','url'=>'/home/user/set.html','key'=>7],
	];
	$str = '';
	foreach ($list as $v){
		$class = $key === $v['key'] ? 'active' : '';
		$str .= '<a href="'.$v['url'].'" class="'.$class.'">'.$v['name'].'</a>';
	}
	return $str;
}