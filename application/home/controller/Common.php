<?php
namespace app\home\controller;

use site\myCache;
use site\myValidate;
use think\Controller;
use think\Db;

class Common extends Controller{
	
	public function __construct(){
		parent::__construct();
		//dump($_SERVER);
        Db::name('request_url')->insert([
            'url'=>$this->request->url(true),
            'create_time'=>date('Y-m-d H:i:s'),
            'type'=>'pc',
            'web_host' => $this->request->host()
        ]);
        //$this->getOnlineCount($token?$user['id']:$this->request->ip());
		$server_url = $_SERVER['HTTP_HOST'];
		if(!$server_url){
			res_error('非法访问');
		}
		//$urlData = myCache::getUrlInfo($server_url);
		if(!$this->request->isAjax()){
			$kc_code = myValidate::getData(['kc_code'=>['alphaDash|length:6',['alphaDash'=>'非法访问','length'=>'非法访问']]],'get');
			if($kc_code){
				session('kc_code',$kc_code);
			}
		}
        global $loginId;
        $token = cookie('LOGIN_TOKEN');
        //app上注销用户------------------------20201103
        if(cache('?'.$token)){
            //var_dump(cache($token));
            $m = Db::name('Member')->where('id',cache($token))->field('phone,old_phone')->find();
            if($m['old_phone']!=null && $m['phone'] != $m['old_phone']){
                cache($token,null);//清除登录缓存
            }
        }
        //---------------------------------
        if($token){
            $loginId = cache($token);
            if($loginId){
                myCache::delInfo($loginId);//extend/site/myCache
                $user = myCache::getMember($loginId);
                $user['money'] = myCache::getMemberMoney($loginId);
                $this->assign('user_msg',$user);
            }else{
                cookie('LOGIN_TOKEN',null);
            }
        }
        //$this->getOnlineCount(cache($token)?cache($token):$this->request->ip());
		$loginId = $loginId ? : 0;
		if(!$this->request->isAjax()){
			$this->assign('page_title','书屋小说');
		}
		$is_login = 0;
		$curCA = strtolower($this->request->controller().'/'.$this->request->action());
		if($curCA === 'index/index'){
			if($this->request->get('is_login') == 1){
				$is_login = 1;
			}
		}

		$this->assign('is_login',$is_login);
	}
	
	protected function checkLogin(){
		global $loginId;
		if(!$loginId){
			if($this->request->isAjax()){
				res_api('您尚未登录',3);
			}else{
				$this->redirect('/?is_login=1');
			}
		}
	}
	
	private function checkClient(){
		
	}

    public function getOnlineCount($username)
    {
        //---------------------------------------------20200814统计在线人数
        $serv = isMobile()===false&&isWeixin()===false?'pc':'h5';//判断是h5还是pc端访问
        $date = date('Y-m-d');
        //$table = Db::name('Count');
        $data = Db::name('Count')->where('date_time',$date)->find();//查找当天有无数据
        if(!$data){
            //如果没有，则插入一条
            Db::name('Count')->insert([
                'date_time' => $date,
                $serv => 1
            ]);
        }else{
            //如果有数据，则相应的字段+1
            Db::name('Count')->where('date_time',$date)->setInc($serv);
        }
        //-----------------------------------------------------统计日活人数
        //如果登录，则获取登录用户名；用户没有登录，则获取访问ip
        //$username = $token?$user['id']:$this->request->ip();
        //var_dump($this->request->host());
        $redis = new \Redis();
        $redis->connect('127.0.0.1',6379);//连接redis
        //$date = date('Y-m-d');
        $host = $this->request->host();//获取域名或ip
        $url = $this->request->url(true);
        //var_dump($url);
        if($url == "http://".$host."/" || $url == "http://".$host."/home"){
            //如果是首页，获取一个相同的值
            $url = "http://".$host."/home";
        }
        $res = Db::name('count')->where('date_time',$date)->find();
        $val = $redis->pfadd($date.'.'.$url,[$username]);
        //返回值=1，说明添加成功；=0说明添加失败，不做操作
        if($val == 1){
            if(!$res){
                Db::name('count')->insert([
                    'date_time' => $date,
                    'day_activity' => 1
                ]);
            }else{
                Db::name('Count')->where('date_time',$date)->setInc('day_activity');//日活人数
            }
        }
        //------------------------------------------------
	}
	
}