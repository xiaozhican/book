<?php
namespace app\home\controller;

use think\Db;
use site\myValidate;

class Search extends Common{
	
	public function index(){
		$keyword = $this->request->get('keyword');
		$keyword = htmlspecialchars(urldecode($keyword));
		if($this->request->isAjax()){
			$rules = [
				'free_type' => ['number|in:1,2',['number'=>'收费属性选择有误','in'=>'未指定该收费属性']],
				'over_type' => ['number|in:1,2',['number'=>'书籍进度选择有误','in'=>'未指定该书籍进度']],
				'word_type' => ['number|between:1,5',['number'=>'书籍字数选择有误','between'=>'未指定该书籍字数']],
				'page' => ['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数错误','gt'=>'分页参数错误']]
			];
			$data = myValidate::getData($rules);
			$where = [['status','=',1],['name|author','like','%'.$keyword.'%']];
			if($data['free_type']){
				$where[] = ['free_type','=',$data['free_type']];
			}
			if($data['over_type']){
				$where[] = ['over_type','=',$data['over_type']];
			}
			if($data['word_type']){
				switch ($data['word_type']){
					case 1:
						$where[] = ['word_number','<',300000];
						break;
					case 2:
						$where[] = ['word_number','between',[300000,500000]];
						break;
					case 3:
						$where[] = ['word_number','between',[500000,800000]];
						break;
					case 4:
						$where[] = ['word_number','between',[800000,1000000]];
						break;
					case 5:
						$where[] = ['word_number','>',1000000];
						break;
				}
			}
			$count = $totalNum = 0;
			$list = Db::name('Book')->where($where)->field('id,name,cover,author,summary,category')->page($data['page'],6)->order('hot_num','desc')->select();
			if($list){
				foreach ($list as &$v){
					$v['category'] = $v['category'] ? explode(',',trim($v['category'],',')) : [];
				}
				if($data['page'] == 1){
					$totalNum = Db::name('Book')->where($where)->count();
					$count = ceil($totalNum/6);
				}
			}
			res_api(['list'=>$list,'count'=>$count,'total_num'=>$totalNum]);
		}else{
			
			return $this->fetch('index',['keyword'=>$keyword]);
		}
		
	}
}