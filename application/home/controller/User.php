<?php
namespace app\home\controller;

use think\Db;
use site\myDb;
use site\myMsg;
use site\myHttp;
use site\myCache;
use site\myValidate;
use app\common\model\mdMember;

class User extends Common{

    public function __construct(){
        parent::__construct();
        parent::checkLogin();
    }

    //我的书架
    public function bookself(){
        if($this->request->isAjax()){
            global $loginId;
            myCache::delMoney($loginId);//修改金币缓存和删除用户信息缓存
            $page = myHttp::postId('分页','page');
            $list = [];
            $count = 0;
            $cache = myCache::getCollectBookIds($loginId);
            if($cache){
                $page -= 1;
                $count = ceil(count($cache)/10);
                $arr = array_chunk($cache, 10);
                if(isset($arr[$page])){
                    $readlist = myCache::getReadCache($loginId);
                    $readlist = $readlist ? : [];
                    foreach ($arr[$page] as $book_id){
                        $book = myCache::getBook($book_id);
                        if($book && $book['status'] == 1){
                            $key = 'book_'.$book['id'];
                            $one = [
                                'id' => $book['id'],
                                'name' => $book['name'],
                                'author' => $book['author'],
                                'cover' => $book['cover'],
                                'summary' => $book['summary'],
                                'category' => $book['category'],
                                'number' => isset($readlist[$key]) ? $readlist[$key]['last_number'] : 1
                            ];
                            $list[] = $one;
                        }
                    }
                }
            }
            res_api(['list'=>$list,'count'=>$count]);
        }else{

            return $this->fetch();
        }
    }

    //阅读历史
    public function readhistory(){
        if($this->request->isAjax()){
            global $loginId;
            $page = myHttp::postId('分页','page');
            $list = [];
            $count = 0;
            $cache = myCache::getReadCache($loginId);
            if($cache){
                $page -= 1;
                $count = ceil(count($cache)/10);
                $arr = array_chunk($cache, 10);
                if(isset($arr[$page])){
                    foreach ($arr[$page] as $v){
                        $book = myCache::getBook($v['book_id']);
                        if($book && $book['status'] == 1){
                            $key = 'book_'.$book['id'];
                            $one = [
                                'id' => $book['id'],
                                'name' => $book['name'],
                                'author' => $book['author'],
                                'cover' => $book['cover'],
                                'summary' => $book['summary'],
                                'category' => $book['category'],
                                'number' => $v['last_number']
                            ];
                            $list[] = $one;
                        }
                    }
                }
            }
            res_api(['list'=>$list,'count'=>$count]);
        }else{

            return $this->fetch();
        }
    }

    //移除书籍
    public function doRemoveBook(){
        global $loginId;
        $rules = [
            'flag' => ['require|in:1,2',['require'=>'请选择移除书籍出处','in'=>'未指定该出处']],
            'book_ids' => ['require',['require'=>'请选择需要移除的书籍']]
        ];
        $post = myValidate::getData($rules);
        $bookIds = explode(',',trim($post['book_ids'],','));
        $res = false;
        $where = [['uid','=',$loginId],['book_id','in',$bookIds]];
        switch ($post['flag']){
            case 1:
                $re = Db::name('ReadHistory')->where($where)->setField('status',2);
                if($re !== false){
                    myCache::rmReadCache($loginId);
                    $res = true;
                }
                break;
            case 2:
                $re = Db::name('MemberCollect')->where($where)->delete();
                if($re !== false){
                    myCache::rmCollectBookIds($loginId);
                    $res = true;
                }
                break;
        }
        $str = $res ? 'ok' : '删除失败,请重试';
        res_api($str);
    }

    //签到
    public function sign(){
        global $loginId;
        $config = getMyConfig('site_sign');
        if($config['is_sign'] != 1){
            res_error('签到功能未启用');
        }
        $signInfo = myCache::getUserSignInfo($loginId);
        if($this->request->isAjax()){
            if($signInfo['cur_sign'] === 'yes'){
                res_api();
            }
            $data = [
                'uid' => $loginId,
                'date' => date('Ymd'),
                'create_time' => time()
            ];
            foreach ($signInfo['list'] as &$v){
                if($v['is_sign'] === 'no'){
                    $data['money'] = $v['money'];
                    $data['days'] = $v['day'];
                    $v['is_sign'] = 'yes';
                    break;
                }
            }
            if(!isset($data['money'])){
                res_api('签到参数有误');
            }
            $re = mdMember::doSign($data, $loginId);
            if($re){
                $signInfo['add_coin'] = intval($data['money']);
                $signInfo['cur_sign'] = 'yes';
                $key = md5($data['date'].'_signuser_'.$loginId);
                cache($key,$signInfo,86400);
                mdMember::addChargeLog($loginId, '+'.$data['money'].' 书币', '签到送书币');
                res_api($signInfo);
            }else{
                res_api('签到失败');
            }
        }else{
            $variable = ['cur'=>$signInfo];
            return $this->fetch('sign',$variable);
        }
    }

    //意见反馈
    public function feedback(){
        global $loginId;
        $user = myCache::getMember($loginId);
        if($this->request->isAjax()){
            $rules = [
                'content' => ['require|max:100',['require'=>'请输入反馈内容','max'=>'反馈内容最大支持100个字符']],
                'phone' => ['require|mobile',['require'=>'请输入手机号','mobile'=>'手机号格式不规范']]
            ];
            $data = myValidate::getData($rules);
            $data['content'] = htmlspecialchars($data['content']);
            $data['channel_id'] = $user['channel_id'];
            $data['agent_id'] = $user['agent_id'];
            $data['uid'] = $user['id'];
            $data['create_time'] = time();
            $re = myDb::add('Feedback', $data);
            if($re){
                myCache::setMemberIsAdopt($loginId);
                res_api();
            }else{
                res_api('提交失败');
            }
        }else{

            return $this->fetch('feedback',['phone'=>$user['phone']]);
        }
    }

    //设置
    public function set(){
        if($this->request->isAjax()){
            global $loginId;
            $rules = [
                'headimgurl' => ['require|url',['require'=>'请上传用户头像','url'=>'头像地址异常']],
                'nickname' => ['require|max:20',['require'=>'请输入您的昵称','max'=>'用户昵称最多20个字符']],
                'sex' => ['require|in:1,2',['require'=>'请选择您的性别','in'=>'未指定该性别']]
            ];
            $data = myValidate::getData($rules);
            $re = myDb::save('Member', [['id','=',$loginId]],$data);
            if($re){
                myCache::updateMember($loginId, $data);
                res_api();
            }else{
                res_api('设置失败');
            }
        }else{

            return $this->fetch();
        }
    }

    //修改手机号
    public function bindPhone(){
        if($this->request->isAjax()){
            global $loginId;
            $rules = [
                'phone' => ["require|mobile",['require'=>'请输入手机号','mobile'=>'请输入正确格式的手机号']],
                'code' => ["require|number|length:4",['require'=>'请输入短信验证码','number'=>'验证码为4位纯数字','length'=>'验证码为4位纯数字']],
            ];
            $post = myValidate::getData($rules);
            $flag = myMsg::check($post['phone'], $post['code']);
            if(is_string($flag)){
                res_api($flag);
            }
            $repeat = myDb::getValue('Member', [['phone','=',$post['phone']]], 'id');
            if($repeat){
                res_api('该手机号已被绑定');
            }
            $re = myDb::setField('Member', [['id','=',$loginId]], 'phone',$post['phone']);
            if($re){
                myCache::updateMember($loginId, 'phone', $post['phone']);
                res_api('ok');
            }else{
                res_api('绑定失败');
            }
        }else{

            return $this->fetch('bindPhone');
        }
    }

    //我的日志
    public function log(){
        $type = myValidate::getData(['type'=>['number|between:1,3',['number'=>'参数错误','between'=>'参数错误']]],'get');
        $type = $type ? : 1;
        if($this->request->isAjax()){
            $list = [];
            $count = 0;
            global $loginId;
            $page = myHttp::postId('分页','page');
            switch ($type){
                case 1:
                    $db = 'MemberChargeLog';
                    $list = mdMember::getMyChargeList($loginId, $page);
                    break;
                case 2:
                    $db = 'OrderReward';
                    $list = mdMember::getMyRewardList($loginId, $page);
                    break;
                case 3:
                    $db = 'MemberConsume';
                    $list = mdMember::getMyConsumeList($loginId, $page);
                    break;
            }
            if($page == 1){
                if($list){
                    $num = Db::name($db)->where('uid',$loginId)->count();
                    $count = $num ? ceil($num/20) : 0;
                }
            }
            $list = $list ? : '';
            res_api(['list'=>$list,'count'=>$count]);
        }else{
            switch ($type){
                case 1:$tpl='user/log/charge';break;
                case 2:$tpl='user/log/reward';break;
                case 3:$tpl='user/log/consume';break;
            }
            return $this->fetch($tpl);
        }
    }

    //修改密码
    public function doPwd(){
        if($this->request->isAjax()){

        }else{

            return $this->fetch('doPwd');
        }
    }

    //检查原密码
    public function checkOldPwd(){
        global $loginId;
        $rules = ['password' => ["require|length:6,16",['require'=>'请输入登录密码','length'=>'密码为6-16位']]];
        $password = myValidate::getData($rules);
        $cur = myDb::getById('Member', $loginId,'id,password');
        if($cur['password'] != createPwd($password)){
            res_api('原密码输入不正确');
        }
        $token = md5($loginId.microtime());
        $key = 'user_pc_token_'.$loginId;
        cache($key,$token,1800);
        res_api(['token'=>$token]);
    }

    public function checkPwdPhone(){
        global $loginId;
        $rules = [
            'phone' => ["require|mobile",['require'=>'请输入手机号','mobile'=>'请输入正确格式的手机号']],
            'code' => ["require|number|length:4",['require'=>'请输入短信验证码','number'=>'验证码为4位纯数字','length'=>'验证码为4位纯数字']],
        ];
        $post = myValidate::getData($rules);
        $user = myCache::getMember($loginId);
        if(!$user['phone']){
            res_api('您尚未绑定手机号');
        }
        if($user['phone'] != $post['phone']){
            res_api('您提交的手机号与绑定的手机号不一致');
        }
        $flag = myMsg::check($post['phone'], $post['code']);
        if(is_string($flag)){
            res_api($flag);
        }
        $token = md5($loginId.microtime());
        $key = 'user_pc_token_'.$loginId;
        cache($key,$token,1800);
        res_api(['token'=>$token]);
    }

    public function doSetPwd(){
        global $loginId;
        $rules = [
            'token' => ['require|length:32',['require'=>'非法请求','length'=>'非法请求']],
            "pwd" => ["require|length:6,16",["require"=>"请输入新密码","length"=>"请输入6到16位新密码"]],
            "re_pwd"  => ["require|confirm:pwd",["require"=>"请再次输入新密码","confirm"=>"两次输入密码不一致"]],
        ];
        $post = myValidate::getData($rules);
        $pwd = createPwd($post['pwd']);
        $re = myDb::setField('Member', [['id','=',$loginId]], 'password', $pwd);
        if($re){
            res_api();
        }else{
            res_api('密码设置失败');
        }
    }

    public function about(){
        $config = getMyConfig('site_contact');
        if(!$config){
            $config = myDb::buildArr('tel,email');
        }
        return $this->fetch('',['cur'=>$config]);
    }


}