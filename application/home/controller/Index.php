<?php
namespace app\home\controller;

use app\common\model\mdConfig;
use site\myCache;
use site\myMsg;
use site\myValidate;
use app\common\model\mdBook;
use think\Db;

class Index extends Common{
	
	//首页
	public function index(){
//        Db::name('request_url')->insert([
//            'url'=>$this->request->url(true),
//            'create_time'=>date('Y-m-d H:i:s'),
//            'type'=>'pc'
//        ]);
        global $loginId;
        //var_dump($loginId);
        //var_dump(cache('LOGIN_TOKEN'));
        $this->getOnlineCount($loginId!=0?$loginId:$this->request->ip());
		$rules = [
			'type' => ['number|in:1,2',['number'=>'书籍类型选择有误','in'=>'未指定该书籍内容']],
			'gender_type' => ['number|in:1,2',['number'=>'读者性别选择有误','in'=>'未指定该读者性别']],
		];
		$get = myValidate::getData($rules,'get');
		$type = $get['type'] ? : 1;
		$gender_type = $get['gender_type'] ? : 1;
		if($this->request->isMobile()){
			$action = $get['type'] == 1 ? 'index' : 'cartoon';
			$this->redirect('/index/index/'.$action.'.html?gender_type='.$gender_type);
		}
		switch ($type){
			case 1:
				$key =  'novel';
				break;
			case 2:
				$key = 'cartoon';
				break;
		}
		$nav_key = $gender_type == 1 ? $key.'_man' : $key.'_woman';
		$banner_key = $key.'_banner_'.($gender_type == 1 ? 'man' : 'woman');
		$banner = getMyConfig($banner_key);
		$area_book = mdBook::getAreaBookList($type, $gender_type);//application/common/model/mdBook
		//$free_book = self::getFreeBookList($type, $gender_type);//限免
		$over_type1 = self::getOverTypeList($type,$gender_type,1);//连载
		$over_type2 = self::getOverTypeList($type,$gender_type,2);//连载
		$rank_book = self::getRankBookList($type, $gender_type);
		$publish_book = self::getPublishBookList($type, $gender_type);
        $links = myCache::getLinksList();
		$variable = [
			'type' => $type,
			'nav_key' => $nav_key,
            'banner' => $banner,
			'area_book' => $area_book,
            //'free_book' => $free_book,
            'over_type1' => $over_type1,
            'over_type2' => $over_type2,
            'links' => $links,//友情链接
			'rank_book' => $rank_book,
			'publish_book' => $publish_book,
			'gender_type' => $gender_type
		];
		return $this->fetch('index',$variable);
	}
	
	//获取限免书籍列表
	private function getFreeBookList($type,$gender_type){
		$list = [];
		$cache = myCache::getBookFreeIds($type, $gender_type);
		if($cache){
			$ids = array_slice($cache,0,6);
			foreach ($ids as $v){
				$book = myCache::getBook($v['id']);
				if($book && $book['status'] == 1){
					$list[] = [
						'id' => $book['id'],
						'name' => $book['name'],
						'author' => $book['author'],
						'cover' => $book['cover']
					];
				}
			}
		}
		return $list;
	}

	//连载,完结
    private function getOverTypeList($type,$gender_type,$over_type){
        $list = [];
        $cache = myCache::getBookOverTypeIds($type, $gender_type,$over_type);
        //var_dump($cache);
        if($cache){
            $ids = array_slice($cache,0,6);
            foreach ($ids as $v){
                $book = myCache::getBook($v['id']);
                if($book && $book['status'] == 1){
                    $list[] = [
                        'id' => $book['id'],
                        'name' => $book['name'],
                        'author' => $book['author'],
                        'cover' => $book['cover']
                    ];
                }
            }
        }
        return $list;
    }

    //完结
//    private function getOverType2List($type,$gender_type,$over_type2){
//        $list = [];
//        $cache = myCache::getBookOverType2Ids($type, $gender_type,$over_type2);
//        if($cache){
//            $ids = array_slice($cache,0,6);
//            foreach ($ids as $v){
//                $book = myCache::getBook($v['id']);
//                if($book && $book['status'] == 1){
//                    $list[] = [
//                        'id' => $book['id'],
//                        'name' => $book['name'],
//                        'author' => $book['author'],
//                        'cover' => $book['cover']
//                    ];
//                }
//            }
//        }
//        return $list;
//    }
	
	//获取排行列表
	public function getRankBookList($type,$gender_type){
		$data = [];
		for ($i=1;$i<=4;$i++){
			$key = 'flag_'.$i;
			$list = [];
			$ids = myCache::getBookRankIds($i, $type, $gender_type);
			if($ids){
				$list = mdBook::getListBookInfo($ids);
			}
			$data[$key] = $list;
		}
		return $data;
	}
	
	//获取出版书籍
	private function getPublishBookList($type,$gender_type){
		$list = [];
		$cache = myCache::getBookPublishIds($type, $gender_type);
		if($cache){
			$ids = array_slice($cache,0,4);
			$list = mdBook::getListBookInfo($ids);
		}
		return $list;
	}
	
	//获取出版书籍分页列表
	public function getPublishPageList(){
		$rules = [
			'type' => ['require|number|in:1,2',['require'=>'书籍分类参数错误','number'=>'书籍分类有误','in'=>'未指定该书籍分类']],
			'gender_type' => ['require|number|in:1,2',['require'=>'读者性别参数有误','number'=>'读者性别选择有误','in'=>'未指定该读者性别']],
			'page' => ['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数错误','gt'=>'分页参数错误']]
		];
		$post = myValidate::getData($rules);
		$list = [];
		$cache = myCache::getBookPublishIds($post['type'], $post['gender_type']);
		if($cache){
			$page = $post['page'] - 1;
			$arr = array_chunk($cache,4);
			$ids = isset($arr[$page]) ? $arr[$page] : [];
			if($ids){
				$list = mdBook::getListBookInfo($ids);
			}
		}
		$list = $list ? : 0;
		res_table($list);
	}

	public function test(){
//	    $res=myMsg::dxtonSend(18380585624);
//        if ($res){
//            echo '发送成功';
//        }
        $web = mdConfig::getConfig('website');
        dump($web);
        $web2 = getMyConfig('website');
        dump($web2);
	}

    public function getOnlineCount($username)
    {
        //---------------------------------------------20200814统计在线人数
        $serv = isMobile()===false&&isWeixin()===false?'pc':'h5';//判断是h5还是pc端访问
        $date = date('Y-m-d');
        //$table = Db::name('Count');
        $data = Db::name('Count')->where('date_time',$date)->find();//查找当天有无数据
        if(!$data){
            //如果没有，则插入一条
            Db::name('Count')->insert([
                'date_time' => $date,
                $serv => 1
            ]);
        }else{
            //如果有数据，则相应的字段+1
            Db::name('Count')->where('date_time',$date)->setInc($serv);
        }
        //-----------------------------------------------------统计日活人数
        //如果登录，则获取登录用户名；用户没有登录，则获取访问ip
        //$username = $token?$user['id']:$this->request->ip();
        //var_dump($this->request->host());
        $redis = new \Redis();
        $redis->connect('127.0.0.1',6379);//连接redis
        //$date = date('Y-m-d');
        $host = $this->request->host();//获取域名或ip
        $url = $this->request->url(true);
        //var_dump($url);
        //判断域名是否含有www
        if(strpos($host,'www') === false){
            $host = "www.".$host;
        }
        if($url == "http://".$host."/" || $url == "http://".$host."/home"){
            //如果是首页，获取一个相同的值
            $url = "http://".$host."/";
        }
        $res = Db::name('count')->where('date_time',$date)->find();
        $val = $redis->pfadd($date.'.'.$url,[$username]);
        //返回值=1，说明添加成功；=0说明添加失败，不做操作
        if($val == 1){
            if(!$res){
                Db::name('count')->insert([
                    'date_time' => $date,
                    'day_activity' => 1
                ]);
            }else{
                Db::name('Count')->where('date_time',$date)->setInc('day_activity');//日活人数
            }
        }
        //------------------------------------------------
    }

}