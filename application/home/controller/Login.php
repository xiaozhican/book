<?php
namespace app\home\controller;

use app\common\model\mdCount;
use app\common\model\mdLogin;
use app\common\model\mdOnline;
use site\myDb;
use site\myMsg;
use site\myValidate;
use app\common\model\mdMember;
use think\Db;

class Login extends Common{
	
	//登录
	public function doLogin(){
		$rules = [
			'phone' => ["require|mobile",['require'=>'请输入手机号','mobile'=>'请输入正确格式的手机号']],
			'password' => ["require|length:6,16",['require'=>'请输入登录密码','length'=>'密码为6-16位']],
		];
		$post = myValidate::getData($rules);
		$user = myDb::getCur('Member', [['phone','=',$post['phone']]], 'id,password,status,nickname,headimgurl');
        $serv = isMobile()===false&&isWeixin()===false?'pc':'h5';
		if(!$user){
		    $error = '用户不存在';
            \site\mdLogin::saveLog($post['phone'],$serv,$error);
			res_api($error);
		}
		$password = createPwd($post['password']);
		if($password !== $user['password']){
            $error = '您的登录密码输入有误';
            \site\mdLogin::saveLog($post['phone'],$serv,$error);
			res_api($error);
		}
		if($user['status'] != 1){
            $error = '您已被禁止登录';
            \site\mdLogin::saveLog($post['phone'],$serv,$error);
			res_api($error);
		}
		$token = md5('PCLOGIN'.time().'#'.$user['id']);
		cache($token,$user['id']);
		cookie('LOGIN_TOKEN',$token);

		//self::saveLog($post['phone'],$serv);//记录登录日志
        \site\mdLogin::saveLog($post['phone'],$serv);
        mdOnline::countOnline($post['phone']);//统计日登人数

		res_api(['nickname'=>$user['nickname'],'headimgurl'=>$user['headimgurl']]);
	}
	
	//注册
	public function doRegister(){
		$rules = [
			'phone' => ["require|mobile",['require'=>'请输入手机号','mobile'=>'请输入正确格式的手机号']],
			'code' => ["require|number|length:4",['require'=>'请输入短信验证码','number'=>'验证码为4位纯数字','length'=>'验证码为4位纯数字']],
			'password' => ["require|length:6,16",['require'=>'请输入登录密码','length'=>'密码为6-16位']],
		];
		$post = myValidate::getData($rules);
		$flag = myMsg::check($post['phone'], $post['code']);
		if(is_string($flag)){
			res_api($flag);
		}
		$user = myDb::getValue('Member', [['phone','=',$post['phone']]], 'id');
		if($user){
			res_api('该手机号已注册');
		}
		$userMsg = mdMember::doPhoneRegister($post['phone'],$post['password']);
		if($userMsg){
			$token = md5('PCLOGIN'.time().'#'.$userMsg['id']);
			cache($token,$userMsg['id']);
			cookie('LOGIN_TOKEN',$token);
			res_api(['nickname'=>$user['nickname'],'headimgurl'=>$user['headimgurl']]);
		}else{
			res_api('注册失败');
		}
	}

	//退出
	public function logout(){
        $token = cookie('LOGIN_TOKEN');
        cache($token,null);
        cookie('LOGIN_TOKEN',null);
        res_api();
    }
	
	//忘记密码
	public function doForget(){
		
	}

    //保存h5和pc端登录日志
    private static function saveLog($login_name,$login_type,$error=''){
        $status = $error ? 2 : 1;
        $is_online = $error ? 3 : 1;
        $data = [
            'username' => $login_name,
            //'type' => $type,
            'status' => $status,
            'login_type'=>$login_type,
            'remark' => $error,
            'login_ip' => request()->ip(),
            'create_time' => time(),
            'is_online'=>$is_online
        ];
        Db::name('Log')->insert($data);
    }
}