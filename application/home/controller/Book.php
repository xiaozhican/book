<?php
namespace app\home\controller;

use app\common\model\mdActivity;
use site\myDb;
use site\myCache;
use site\mySearch;
use site\myValidate;
use app\common\model\mdBook;
use site\myHttp;
use think\Db;

class Book extends Common{
	
	//书籍反馈
	public function feedback(){
		global $loginId;
		$rules = [
			'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
			'number'=>['require|number|gt:0',['require'=>'章节参数错误','number'=>'章节参数错误','gt'=>'章节参数错误']],
		];
		$get = myValidate::getData($rules,'get');
		if($this->request->isAjax()){
			if(!$loginId){
				res_api('您尚未登陆',3);
			}
			$user = myCache::getMember($loginId);
			$rules = [
				'type' => ['require|between:1,7',['require'=>'请选择反馈类型','between'=>'未指定该反馈类型']],
				'content' => ['max:100',['max'=>'反馈内容最大支持100个字符']],
				'phone' => ['mobile',['mobile'=>'手机号格式不规范']]
			];
			$data = myValidate::getData($rules);
			$book = myCache::getBook($get['book_id']);
			if(!$book){
				res_api('书籍信息异常');
			}
			$data['book_id'] = $book['id'];
			$data['number'] = $get['number'];
			$data['channel_id'] = $user['channel_id'];
			$data['agent_id'] = $user['agent_id'];
			$data['uid'] = $loginId;
			$data['book_name'] = $book['name'];
			$data['create_time'] = time();
			$re = myDb::add('Complaint', $data);
			if($re){
				res_api();
			}else{
				res_api('投诉失败，请重试');
			}
		}else{
			
			return $this->fetch();
		}
	}
	
	//更多
	public function more(){
		$rules = [
			'type' => ['number|in:1,2',['number'=>'书籍类型选择有误','in'=>'未指定该书籍内容']],
			'gender_type' => ['number|in:1,2',['number'=>'读者性别选择有误','in'=>'未指定该读者性别']],
			'area_id' => ['require|between:1,4',['require'=>'参数错误','between'=>'未指定该类型']],
		];
		$get = myValidate::getData($rules,'get');
		if($this->request->isAjax()){
			$page = myHttp::postId('分页','page');
			$res = mdBook::getAreaBookInfo($get['type'], $get['gender_type'], $get['area_id'], $page, 10);
			res_api(['list'=>$res['list'],'count'=>$res['count']]);
		}else{
			if(request()->isMobile()){
				$uri = str_replace('home','index', $_SERVER['REQUEST_URI']);
				$this->redirect($uri);
			}
			$name = '';
			switch ($get['area_id']){
				case 1:$name='精品推荐';break;
				case 2:$name='本周热搜';break;
				case 3:$name='热门推荐';break;
				case 4:$name='新书推荐';break;
			}
			return $this->fetch('more',['area'=>$name]);
		}
	}
	
	//限免
	public function free(){
		if($this->request->isAjax()){
			$rules = [
				'type' => ['number|in:1,2',['number'=>'书籍类型选择有误','in'=>'未指定该书籍内容']],
				'gender_type' => ['number|in:1,2',['number'=>'读者性别选择有误','in'=>'未指定该读者性别']],
				'flag' => ['number|between:1,4',['number'=>'榜单类别选择有误','between'=>'未指定该榜单类别']],
				'page' => ['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数错误','gt'=>'分页参数错误']]
			];
			$post = myValidate::getData($rules,'post');
			global $loginId;
			$list = [];
			$count = 0;
			$type = $post['type'] ? : 1;
			$flag = $post['flag'] ? : 1;
			$gender_type = $post['gender_type'] ? : 1;
			$cache = myCache::getBookFreeIds($type, $gender_type);
			if($cache){
				$page = $post['page'] - 1;
				$arr = array_chunk($cache, 10);
				$count = $page == 0 ? count($arr) : 0;
				if(isset($arr[$page])){
					$collectIds = [];
					if($loginId){
						$collectIds = myCache::getCollectBookIds($loginId);
						$collectIds = $collectIds ? : [];
					}
					foreach ($arr[$page] as $v){
						$book = myCache::getBook($v['id']);

                        $chapter = Db::name('book_chapter')->where(['book_id'=>$v['id']])
                            ->order('number desc')->find();

						if($book && $book['status'] == 1){
							$list[] = [
								'id' => $book['id'],
								'name' => $book['name'],
								'author' => $book['author'],
								'cover' => $book['cover'],
								'summary' => $book['summary'],
								'category' => $book['category'],
								'start_date' => $v['start_date'],
								'end_date' => $v['end_date'],
								'is_collect' => in_array($v['id'], $collectIds) ? 1 : 2,
                                'type' => $book['type'],
                                'word_number' => $book['word_number'],
                                'over_type' => $book['over_type'] == 1 ?'连载中':'已完结',
                                'best_new_chapter' => $chapter['name'],
                                'edit_time' => date('Y-m',$chapter['create_time'])
							];
						}
					}
				}
				
			}
			$list = $list ? : '';
			res_table(['list'=>$list,'count'=>$count]);
		}else{
			
			return $this->fetch();
		}
	}

    //连载,完结
    public function overtype(){
        if($this->request->isAjax()){
            $rules = [
                'type' => ['number|in:1,2',['number'=>'书籍类型选择有误','in'=>'未指定该书籍内容']],
                'gender_type' => ['number|in:1,2',['number'=>'读者性别选择有误','in'=>'未指定该读者性别']],
                //'flag' => ['number|between:1,4',['number'=>'榜单类别选择有误','between'=>'未指定该榜单类别']],
                'page' => ['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数错误','gt'=>'分页参数错误']],
                'over_type' => ['require|in:1,2',['require'=>'参数错误','in'=>'未指定类别']]
            ];
            $post = myValidate::getData($rules,'post');
            global $loginId;
            $list = [];
            $count = 0;
            $type = $post['type'] ? : 1;
            //$flag = $post['flag'] ? : 1;
            $gender_type = $post['gender_type'] ? : 1;
            $over_type = $post['over_type'] ? : 1;
            $cache = myCache::getBookOverTypeIds($type, $gender_type,$over_type);
            if($cache){
                $page = $post['page'] - 1;
                $arr = array_chunk($cache, 10);
                $count = $page == 0 ? count($arr) : 0;
                if(isset($arr[$page])){
                    $collectIds = [];
                    if($loginId){
                        $collectIds = myCache::getCollectBookIds($loginId);
                        $collectIds = $collectIds ? : [];
                    }
                    //var_dump($arr[$page]);
                    foreach ($arr[$page] as $v){
                        $book = myCache::getBook($v['id']);

                        $chapter = Db::name('book_chapter')->where(['book_id'=>$v['id']])
                            ->order('number desc')->find();

                        if($book && $book['status'] == 1){
                            $list[] = [
                                'id' => $book['id'],
                                'name' => $book['name'],
                                'author' => $book['author'],
                                'cover' => $book['cover'],
                                'summary' => $book['summary'],
                                'category' => $book['category'],
                                //'start_date' => $v['start_date'],
                                //'end_date' => $v['end_date'],
                                'is_collect' => in_array($v['id'], $collectIds) ? 1 : 2,
                                'type' => $book['type'],
                                'word_number' => $book['word_number'],
                                'over_type' => $book['over_type'] == 1 ?'连载中':'已完结',
                                'best_new_chapter' => $chapter['name'],
                                'edit_time' => date('Y-m',$chapter['create_time'])
                            ];
                        }
                    }
                }

            }
            $list = $list ? : '';
            res_api(['list'=>$list,'count'=>$count]);
        }else{
            return $this->fetch();
        }
    }

    //出版
    public function publish(){
        if($this->request->isAjax()){
            $rules = [
                'type' => ['number|in:1,2',['number'=>'书籍类型选择有误','in'=>'未指定该书籍内容']],
                'gender_type' => ['number|in:1,2',['number'=>'读者性别选择有误','in'=>'未指定该读者性别']],
                'page' => ['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数错误','gt'=>'分页参数错误']]
            ];
            $post = myValidate::getData($rules,'post');
            global $loginId;
            $list = [];
            $count = 0;
            $type = $post['type'] ? : 1;
            $gender_type = $post['gender_type'] ? : 1;
            $cache = myCache::getBookPublishIds($type, $gender_type);
            if($cache){
                $page = $post['page'] - 1;
                $arr = array_chunk($cache, 10);
                if($page == 0){
                    $count = count($arr);
                }
                if(isset($arr[$page])){
                    $collectIds = [];
                    if($loginId){
                        $collectIds = myCache::getCollectBookIds($loginId);
                        $collectIds = $collectIds ? : [];
                    }
                    foreach ($arr[$page] as $book_id){
                        $book = myCache::getBook($book_id);

                        $chapter = Db::name('book_chapter')->where(['book_id'=>$book_id])
                            ->order('number desc')->find();

                        if($book && $book['status'] == 1){
                            $list[] = [
                                'id' => $book['id'],
                                'name' => $book['name'],
                                'author' => $book['author'],
                                'cover' => $book['cover'],
                                'summary' => $book['summary'],
                                'category' => $book['category'],
                                'type' => $book['type'],
                                'word_number' => $book['word_number'],
                                'over_type' => $book['over_type'] == 1 ?'连载中':'已完结',
                                'best_new_chapter' => $chapter['name'],
                                'edit_time' => date('Y-m',$chapter['create_time']),
                                'is_collect' => in_array($book_id, $collectIds) ? 1 : 2
                            ];
                        }
                    }
                }

            }
            $list = $list ? : '';
            res_table(['list'=>$list,'count'=>$count]);
        }else{

            return $this->fetch();
        }
    }

    //活动
    public function activity(){
        if($this->request->isAjax()){
            $config = [
                'default' => [['a.status','between',[1,2]]],
                'eq' => 'status:a.status',
                'like' => 'keyword:a.name',
                'rules' => ['status'=>'in:1,2']
            ];
            $pages = myHttp::getPageParams();
            $where = mySearch::getWhere($config);
            $where=['a.status'=>1];
            $res = mdActivity::getListInSite($where,$pages);
            $time = time();
            if($res['data']){
                foreach ($res['data'] as &$v){
                    if($time > $v['end_time']){
                        $v['between_time'] = '<font class="text-red">活动已结束</font>';
                        $v['show_btn'] =2;
                    }elseif ($time < $v['start_time']){
                        $v['between_time'] = '<font class="text-red">活动未开始</font>';
                        $v['show_btn'] =2;
                    }else{
                        $v['between_time'] = date('Y-m-d H:i',$v['start_time']).'~'.date('Y-m-d H:i',$v['end_time']);
                        $v['show_btn'] =1;
                    }
                    $v['content'] = '充'.$v['money'].'元送'.$v['send_money'].'书币';
                    $v['do_url'] = my_url('doActivity',['id'=>$v['id']]);
                    $v['copy_url'] = my_url('copyLink',['id'=>$v['id']]);
                    $v['first_str'] = $v['is_first'] == 1 ? '仅限一次' : '不限次数';
                }
            }
            res_table(['list'=>$res['data'],'count'=>1]);
        }else{
            return $this->fetch();
        }
    }

    //公告资讯列表
    public function news(){
        $list = Db::name('news')->field('id,title,create_time')->order('create_time desc')->select();
        if($this->request->isAjax()){
            $rules = [
                'type' => ['number|in:1,2',['number'=>'书籍类型选择有误','in'=>'未指定该书籍内容']],
                'gender_type' => ['number|in:1,2',['number'=>'读者性别选择有误','in'=>'未指定该读者性别']],
                'page' => ['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数错误','gt'=>'分页参数错误']]
            ];
            $post = myValidate::getData($rules,'post');
            global $loginId;
            $count = 0;

            if ($list) {
                foreach ($list as $k => $v) {
                    $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
                }

                $page = $post['page'] - 1;
                $arr = array_chunk($list, 10);
                if ($page == 0) {
                    $count = count($arr);
                }
                $list = $arr[$page];
            }
            $list = $list ? : '';
            res_table(['list'=>$list,'count'=>$count]);
        }else{
            $total_row = count($list);
            $this->assign('total_row',$total_row);
            return $this->fetch();
        }
    }

    //网站地图
    public function map(){
        $list = Db::name('news')->field('id,title,create_time')->order('create_time desc')->select();
        if($this->request->isAjax()){
            $rules = [
                'type' => ['number|in:1,2',['number'=>'书籍类型选择有误','in'=>'未指定该书籍内容']],
                'gender_type' => ['number|in:1,2',['number'=>'读者性别选择有误','in'=>'未指定该读者性别']],
                'page' => ['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数错误','gt'=>'分页参数错误']]
            ];
            $post = myValidate::getData($rules,'post');
            global $loginId;
            $count = 0;

            if ($list) {
                foreach ($list as $k => $v) {
                    $list[$k]['create_time'] = date('Y-m-d H:i:s', $v['create_time']);
                }

                $page = $post['page'] - 1;
                $arr = array_chunk($list, 10);
                if ($page == 0) {
                    $count = count($arr);
                }
                $list = $arr[$page];
            }
            $list = $list ? : '';
            res_table(['list'=>$list,'count'=>$count]);
        }else{
            $total_row = count($list);
            $this->assign('total_row',$total_row);
            return $this->fetch();
        }
    }

    //公告资讯内容
    public function newsContent(){
	    $new_id = input('new_id');
	    if (!is_numeric($new_id)){
	        res_error('非法访问');
        }
	    $re = Db::name('news')->where(['id'=>$new_id])->find();
	    if (!$re){
	        res_error('没有找到该资讯');
        }
	    $re['create_time'] = date('Y年m月d日 H:i');
	    $this->assign('re',$re);
        return $this->fetch();
    }
	
	//榜单
	public function rank(){
		if($this->request->isAjax()){
			$rules = [
				'type' => ['number|in:1,2',['number'=>'书籍类型选择有误','in'=>'未指定该书籍内容']],
				'gender_type' => ['number|in:1,2',['number'=>'读者性别选择有误','in'=>'未指定该读者性别']],
				'flag' => ['number|between:1,4',['number'=>'榜单类别选择有误','between'=>'未指定该榜单类别']]
			];
			$post = myValidate::getData($rules,'post');
			global $loginId;
			$list = [];
			$type = $post['type'] ? : 1;
			$flag = $post['flag'] ? : 1;
			$gender_type = $post['gender_type'] ? : 1;
			$ids = myCache::getBookRankIds($flag, $type, $gender_type);
			if($ids){
				$collectIds = [];
				if($loginId){
					$collectIds = myCache::getCollectBookIds($loginId);
					$collectIds = $collectIds ? : [];
				}
				foreach ($ids as $book_id){
					$book = myCache::getBook($book_id);

                    $chapter = Db::name('book_chapter')->where(['book_id'=>$book_id])
                        ->order('number desc')->find();
					if($book && $book['status'] == 1){
						$list[] = [
							'id' => $book['id'],
							'name' => $book['name'],
							'author' => $book['author'],
							'cover' => $book['cover'],
							'summary' => $book['summary'],
                            'category' => $book['category'],
                            'type' => $book['type'],
                            'word_number' => $book['word_number'],
                            'over_type' => $book['over_type'] == 1 ?'连载中':'已完结',
							'is_collect' => in_array($book_id, $collectIds) ? 1 : 2,
                            'best_new_chapter' => $chapter['name'],
                            'edit_time' => date('Y-m',$chapter['create_time']),

						];
					}
				}
			}
			$list = $list ? : '';
			res_table(['list'=>$list,'count'=>0]);
		}else{
			
			return $this->fetch();
		}
	}
	
	//书库
	public function category(){
		if($this->request->isAjax()){
			global $loginId;
			$rules = [
				'type' => ['number|in:1,2',['number'=>'书籍类型选择有误','in'=>'未指定该书籍内容']],
				'gender_type' => ['number|in:1,2',['number'=>'读者性别选择有误','in'=>'未指定该读者性别']],
				'over_type' => ['number|in:1,2',['number'=>'书籍进度选择有误','in'=>'未指定该书籍进度']],
				'category' => ['chsAlphaNum',['chsAlphaNum'=>'书籍类型不符合规范']],
				'page' => ['require|number|gt:0',['require'=>'分页参数错误','number'=>'分页参数错误','gt'=>'分页参数错误']]
			];
			$post = myValidate::getData($rules);
			$where = [['status','=',1]];
			$type = $post['type'] ? : 1;
			$where[] = ['type','=',$type];
			if($post['gender_type']){
				$where[] = ['gender_type','=',$post['gender_type']];
			}
			if($post['over_type']){
				$where[] = ['over_type','=',$post['over_type']];
			}
			if($post['category']){
				$where[] = ['category','like','%'.$post['category'].'%'];
			}
			$count = 0;
			$list = myDb::getOnlyPageList('Book', $where, 'id,name,author,cover,summary,category,word_number,type,over_type', ['page'=>$post['page'],'limit'=>10]);
			if($list){
				if($post['page'] == 1){
					$num = myDb::getCount('Book', $where);
					$count = ceil($num/10);
				}
				$collectIds = [];
				if($loginId){
					$collectIds = myCache::getCollectBookIds($loginId);
					$collectIds = $collectIds ? : [];
				}
				foreach ($list as &$v){
					$v['is_collect'] = in_array($v['id'], $collectIds) ? 1 : 2;
					$v['category'] = $v['category'] ? explode(',', trim($v['category'],',')) : [];
					$v['over_type'] = $v['over_type'] == 1 ? '连载中':'已完结';
				    $chapter = Db::name('book_chapter')->where(['book_id'=>$v['id']])
                        ->order('number desc')->find();
                    $v['best_new_chapter'] = $chapter['name'];
                    $v['edit_time'] = date('Y-m',$chapter['create_time']);
				}
			}
			res_api(['list'=>$list,'count'=>$count]);
		}else{
			if(request()->isMobile()){
				$this->redirect('/index/book/category?type=1');
			}
			$novel_category = getMyConfig('novel_category');
			$cartoon_category = getMyConfig('cartoon_category');
			$variable = [
				'novel' => $novel_category,
				'cartoon' => $cartoon_category
			];
			return $this->fetch('category',$variable);
		}
	}
	
	//书籍详情
	public function info(){
		$rules = [
			'type'=>['number|between:1,3',['number'=>'参数错误','between'=>'参数错误']],
			'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'数据参数错误','gt'=>'数据参数错误']],
		];
		$get = myValidate::getData($rules,'get');
		$type = $get['type'] ? : 1;
		$book = myCache::getBook($get['book_id']);
		if(!$book){
			res_error('书籍已删除');
		}
		if(!in_array($book['status'], [1,2])){
			res_error('书籍已下架');
		}
		$is_collect = 'no';
		global $loginId;
		if($loginId){
			$collectIds = myCache::getCollectBookIds($loginId);
			if($collectIds && in_array($book['id'], $collectIds)){
				$is_collect = 'yes';
			}
		}
		$book['hot_num'] = myCache::getBookHotNum($book['id']);
		$book['chapter_num'] = myCache::getBookChapterNum($book['id']);
		$nav_key = $book['type'] == 1 ? 'novel' : 'cartoon';
		$variable = ['book'=>$book,'nav_key'=>$nav_key,'is_collect'=>$is_collect];
		switch ($type){
			case 1:
				$tpl = 'book_info_dir';
				$chapter = myCache::getBookChapterList($book['id']);
				if($chapter){
					$is_free = myCache::checkBookFree($book['id']);
					if($is_free || $book['free_type'] == 1){
						foreach ($chapter as &$v){
							$v['money'] = 0;
						}
					}else{
						foreach ($chapter as &$v){
							if($book['free_type'] == 2 && $v['number'] > $book['free_chapter'] && !$v['money']){
								$v['money'] = $book['money'];
							}
						}
					}
				}
				$variable['chapter'] = $chapter;
				mdBook::addHot($book['id']);
				break;
			case 2:
				$tpl = 'book_info_reward';
				$gift = getMyConfig('reward');
				$variable['gift_list'] = $gift;
				$user = ['id'=>0,'money'=>0];
				if($loginId){
					$user['id'] = $loginId;
					$user['money'] = myCache::getMemberMoney($loginId);
				}
				$variable['user'] = $user;
				$variable['reward_rank'] = myCache::getRewardCache($book['id']);
				break;
			case 3:
				$tpl = 'book_info_comments';
				break;
		}
		return $this->fetch($tpl,$variable);
	}
	
	//阅读页面
	public function read(){
		global $loginId;
		$kc_code = myValidate::getData(['kc_code'=>['alphaDash|length:6',['alphaDash'=>'非法访问','length'=>'非法访问']]],'get');
		if($kc_code){
			$codeInfo = myCache::getKcCodeInfo($kc_code);
			if($codeInfo && $codeInfo['type'] == 1){
				$spread = myCache::getSpread($codeInfo['sid']);
				if(!$spread){
					res_error('该链接已失效');
				}
				myDb::add('SpreadView',['sid'=>$spread['id'],'create_time'=>time()]);
				$get = [
					'book_id' => $spread['book_id'],
					'number' => $spread['chapter_number']
				];
				$get['number'] = self::getReadNumber($get['book_id'],$get['number']);
				session('spread_id',$spread['id']);
				if(!checkIsWeixin()){
					session('kc_code',$kc_code);
				}
			}else{
				res_error('非法访问');
			}
		}else{
			$rules = [
					'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
					'number'=>['number|gt:0',['require'=>'章节参数错误','number'=>'章节参数错误','gt'=>'章节参数错误']],
			];
			$get = myValidate::getData($rules,'get');
			if(!$get['number']){
				$get['number'] = self::getReadNumber($get['book_id']);
			}
		}
		$book = myCache::getBook($get['book_id']);
		if(!$book){
			res_error('书籍不存在');
		}
		if(!in_array($book['status'], [1,2])){
			res_error('书籍已下架');
		}
		$totalChapter = myCache::getBookChapterNum($book['id']);
		if($totalChapter == 0){
			res_error('该书籍尚无可读章节');
		}
		$chapterList = myCache::getBookChapterList($book['id']);
		$number = ($get['number'] > $totalChapter) ? $totalChapter : $get['number'];
		if(!isset($chapterList[$number])){
			res_error('该章节不存在');
		}
		$book['hot_num'] = myCache::getBookHotNum($book['id']);
		$next_number = (($number + 1) > $totalChapter) ? 0 : ($number + 1);
		$chapter = $chapterList[$number];
		$is_free = myCache::checkBookFree($book['id']);
		$is_collect = 'no';
		$member = '';
		if($loginId){
			$collectIds = myCache::getCollectBookIds($loginId);
			if($collectIds && in_array($book['id'], $collectIds)){
				$is_collect = 'yes';
			}
			$member = myCache::getMember($loginId);
		}else{
			if(!$is_free){
				if($book['free_type'] == 2 && $get['number'] > $book['free_chapter']){
					res_error('您尚未登录');
				}
			}
		}
		if(session('?spread_id')){
			$spread = myCache::getSpread(session('spread_id'));
			if($spread){
				if($get['number'] >= $spread['number']){
					switch ($spread['click_type']){
						case 1:
							if(!$loginId){
								res_error('您尚未登录');
							}
							break;
						case 2:
							$config = getMyConfig('website');
							$url = $config['download_url'];
							$url .= strpos($config['download_url'], '?') !== false ? '&' : '?';
							$url .= 'spread_id='.$spread['id'];
							$this->redirect($url);
							break;
					}
				}
			}
		}
		if($member){
			$is_read = myCache::checkIsRead($member['id'], $book['id'], $number);
			if(!$is_read){
				$isAdd = true;
				if(!$is_free){
					if($book['free_type'] == 2 && $number > $book['free_chapter']){
						$is_money = true;
						if($member['viptime']){
							$is_money = false;
							if($member['viptime'] != 1){
								if(time() > $member['viptime']){
									$is_money = true;
								}
							}
						}
						if($is_money){
							$isAdd = false;
							$money = myCache::getMemberMoney($loginId);
							$book_money = $chapter['money'] ? $chapter['money'] : $book['money'];
							if($money < $book_money){
								res_error('您的书币余额不足');
							}
							$res = mdBook::addReadhistory($book,$book_money,$chapter, $member,true);
							if(!$res){
								res_error('章节购买失败，请重试');
							}
						}
					}
				}
				if($isAdd){
					mdBook::addReadhistory($book,0,$chapter,$member);
				}
			}
		}
		if($is_free || $book['free_type'] == 1){
			foreach ($chapterList as &$v){
				$v['money'] = 0;
			}
		}else{
			foreach ($chapterList as &$v){
				if($v['number'] > $book['free_chapter']){
					if(!$v['money']){
						$v['money'] = $book['money'];
					}
				}
			}
		}
		$variable = [
			'book' => $book,
			'is_collect' => $is_collect,
			'dir' => 'book/'.$book['id'],
			'chapter_list' => $chapterList,
			'chapter_title' => $chapter['name'],
			'total_chapter' => $totalChapter,
			'number' => ['current'=>$number,'prev'=>($number-1),'next'=>$next_number]
		];
		mdBook::addChapterRead($chapter['id']);
		$tpl = $book['type'] == 1 ? 'book/read/novel' : 'book/read/cartoon';
		return $this->fetch($tpl,$variable);
		
	}
	
	//检测章节
	public function checkChapter(){
		global $loginId;
		$rules = [
			'book_id'=>['require|number|gt:0',['require'=>'书籍参数错误','number'=>'书籍参数错误','gt'=>'书籍参数错误']],
			'number'=>['require|number|gt:0',['require'=>'章节参数错误','number'=>'章节参数错误','gt'=>'章节参数错误']],
		];
		$post = myValidate::getData($rules);
		$book = myCache::getBook($post['book_id']);
		if(!$book){
			res_api('书籍不存在');
		}
		if(!in_array($book['status'], [1,2])){
			res_api('书籍已下架');
		}
		$totalChapter = myCache::getBookChapterNum($book['id']);
		if($totalChapter == 0){
			res_api('该书籍尚无可读章节');
		}
		if($post['number'] > $totalChapter){
			res_api('已经是最后一章了');
		}
		$chapterList = myCache::getBookChapterList($book['id']);
		if(!isset($chapterList[$post['number']])){
			res_api('该章节不存在');
		}
		$chapter = $chapterList[$post['number']];
		$is_free = myCache::checkBookFree($book['id']);
		$member = '';
		if($loginId){
			$member = myCache::getMember($loginId);
		}else{
			if(!$is_free){
				if($book['free_type'] == 2 && $post['number'] > $book['free_chapter']){
					res_api('您尚未登录，是否立即登录1',3);
				}
			}
		}
		if(session('?spread_id')){
			$spread = myCache::getSpread(session('spread_id'));
			if($spread){
				if($post['number'] >= $spread['number']){
					switch ($spread['click_type']){
						case 1:
							if(!$loginId){
								res_api('您尚未登录，是否立即登录2',3);
							}
							break;
						case 2:
							$config = getMyConfig('website');
							$url = $config['download_url'];
							$url .= strpos($config['download_url'], '?') !== false ? '&' : '?';
							$url .= 'spread_id='.$spread['id'];
							res_api('阅读下章节需要下载APP，是否立即下载',4,['url'=>$url]);
							break;
					}
				}
			}
		}
		if($member){
			if($book['free_type'] == 2 && $post['number'] > $book['free_chapter']){
				$is_read = myCache::checkIsRead($member['id'], $book['id'], $post['number']);
				if(!$is_read){
					if(!$is_free){
						$is_money = true;
						if($member['viptime'] > 0){
							$is_money = false;
							if($member['viptime'] != 1){
								if(time() > $member['viptime']){
									$is_money = true;
								}
							}
						}
						if($is_money){
							$money = myCache::getMemberMoney($loginId);
							$book_money = $chapter['money'] ? $chapter['money'] : $book['money'];
							if($money < $book_money){
								res_api('您的书币余额不足，是否立即充值',4,['url'=>my_url('Charge/index',['book_id'=>$book['id']])]);
							}
						}
					}
				}
			}
		}
		res_api(['url'=>my_url('read',['book_id'=>$book['id'],'number'=>$post['number']])]);
	}
	
	
}