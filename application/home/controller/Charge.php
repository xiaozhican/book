<?php
namespace app\home\controller;

use app\common\model\mdOrder;
use site\myCache;
use site\myDb;
use site\myHttp;
use site\myValidate;

class Charge extends Common{

    public function index(){
        parent::checkLogin();
        $book_id = $this->request->get('book_id');
        if($this->request->isAjax()){
            self::doCharge();
        }else{
            global $loginId;
            $user = myCache::getMember($loginId);
            $money = myCache::getMemberMoney($loginId);
            $charge = myCache::getCharge($user['channel_id']);
            if($charge){
                $content = myCache::getSiteCharge();
                foreach ($charge as $k=>&$v){
                    $key = $v['pid'] ? : $v['id'];
                    if(!isset($content[$key])){
                        unset($charge[$k]);
                        continue;
                    }
                    $v['money'] = $content[$key]['money'];
                    $v['content'] = $content[$key]['content'];
                    if($content[$key]['pay_times']){
                        $charge_num = myCache::getChargeNum($loginId, $v['id']);
                        if($charge_num >= $content[$key]['pay_times']){
                            unset($charge[$k]);
                        }
                    }
                }
            }
            //my_print($charge);
            $variable = [
                'money' => $money,
                'charge' => $charge,
                'book_id'=> $book_id,
            ];
            return $this->fetch('index',$variable);
        }
    }

    //普通充值
    private function doCharge(){
        global $loginId;
        $rules = [
            'book_id' => ['number|gt:0',['number'=>'书籍参数错误','gt'=>'书籍参数错误']],
            'charge_id' => ['require|number|gt:0',['require'=>'充值信息异常','number'=>'充值信息异常','gt'=>'充值信息异常']],
            'pay_type' => ['require|number|gt:0',['require'=>'充值信息异常','number'=>'充值信息异常','gt'=>'充值信息异常']]
        ];
        $post = myValidate::getData($rules);
        $chargePid = myCache::getChargePid($post['charge_id']);
        $content = myCache::getSiteCharge();
        if(!$content || !isset($content[$chargePid])){
            res_api('套餐已下架');
        }
        $time = time();
        $charge = $content[$chargePid];
        if($charge['pay_times']){
            $chargeNum = myCache::getChargeNum($loginId, $post['charge_id']);
            if($chargeNum >= $charge['pay_times']){
                res_api('该套餐仅限充值'.$charge['pay_times'].'次');
            }
        }
        $member = myCache::getMember($loginId);
        $data = mdOrder::createOrderInfo($member);
        $data['charge_id'] = $post['charge_id'];
        $data['money'] = $charge['money'];
        if($charge['type'] == 1){
            $data['send_money'] = $charge['content']['coin'];
            if($charge['content']['send_coin']){
                $data['send_money'] += $charge['content']['send_coin'];
            }
        }else{
            $data['package'] = $charge['content']['vip_type'];
        }
        $data['type'] = 1;
        $data['create_date'] = date('Ymd',$time);
        $data['create_time'] = $time;
        $data['order_no'] = mdOrder::createOrderNo();
        $config = getMyConfig('website');
        //$data['pay_type'] = isset($config['pay_type']) && in_array($config['pay_type'], [1,2,3]) ? $config['pay_type'] : 1;
        $data['pay_type'] = $post['pay_type'];
        if($post['book_id']){
            $data['ptype'] = 1;
            $data['pid'] = $post['book_id'];
            if(session('?spread_id')){
                $spread = myCache::getSpread(session('spread_id'));
                if($spread && $spread['book_id'] == $post['book_id']){
                    $data['spread_id'] = $member['spread_id'];
                }
            }
        }
        $count_info = [];
        if($data['channel_id']){
            $curInfo =  mdOrder::getCountInfo($data['channel_id'], $data['money']);
            if($curInfo){
                $count_info['channel'] = $curInfo;
            }
        }
        if($data['agent_id']){
            $curInfo = mdOrder::getCountInfo($data['agent_id'], $data['money']);
            if($curInfo){
                $count_info['agent'] = $curInfo;
            }
        }
        $data['count_info'] = json_encode($count_info);
        $res = myDb::add('Order', $data);
        if($res){
            //$back_url = 'http://'.$_SERVER['HTTP_HOST'].'/home/user/log.html';
            $back_url = '';
            $pay_url = mdOrder::createPayUrl($data['order_no'], $data['pay_type'],$back_url);
            res_api(['url'=>$pay_url]);
        }else{
            res_api('创建订单失败');
        }
    }

    //活动充值
    public function doActivityCharge(){
        global $loginId;
        $member = myCache::getMember($loginId);
        $activity_id = myHttp::postId('活动','activity_id');
        $activity = myCache::getActivity($activity_id);
        if(!$activity){
            res_api('该活动不存在');
        }
        if($activity['is_first'] == 1){
            $acUids = myCache::getActivityUids($activity_id);
            if(in_array($loginId, $acUids)){
                res_api('该活动仅限充值一次');
            }
        }
        $time = time();
        if($activity['start_time'] > $time){
            res_api('该活动尚未开始');
        }

        if($activity['end_time'] < $time){
            res_api('该活动已结束');
        }
        if(floatval($activity['money']) <= 0){
            res_api('充值金额异常');
        }
        $data = mdOrder::createOrderInfo($member);
        $data['type'] = 2;
        $data['create_time'] = $time;
        $data['order_no'] = mdOrder::createOrderNo();
        $data['ptype'] = 0;
        $data['pid'] = $activity['id'];
        $data['money'] = $activity['money'];
        $data['send_money'] = $activity['send_money'];
        if(floatval($data['money']) <= 0){
            res_api('充值金额异常');
        }
        if($member['spread_id']){
            $data['spread_id'] = $member['spread_id'];
        }
        $config = getMyConfig('website');
        //$data['pay_type'] = isset($config['pay_type']) && in_array($config['pay_type'], [1,2,3]) ? $config['pay_type'] : 1;
        $data['pay_type'] = 5;
        $count_info = [];
        if($data['channel_id']){
            $curInfo =  mdOrder::getCountInfo($data['channel_id'], $data['money']);
            if($curInfo){
                $count_info['channel'] = $curInfo;
            }
        }
        if($data['agent_id']){
            $curInfo = mdOrder::getCountInfo($data['agent_id'], $data['money']);
            if($curInfo){
                $count_info['agent'] = $curInfo;
            }
        }
        $data['count_info'] = json_encode($count_info);
        $res = myDb::add('Order', $data);
        if($res){
            //$back_url = 'http://'.$_SERVER['HTTP_HOST'].'/index/User/index.html';
            //$pay_url = mdOrder::createPayUrl($data['order_no'],$data['pay_type'],$back_url);
            $pay_url = mdOrder::createPayUrl($data['order_no'], $data['pay_type']);
            res_api(['url'=>$pay_url]);
        }else{
            res_api('创建订单失败');
        }
    }
}