<?php
namespace app\home\controller;

use app\common\model\mdConfig;
use think\Db;
use site\myDb;
use site\myMsg;
use site\myHttp;
use site\myCache;
use site\myValidate;
use app\common\model\mdOrder;
use app\common\model\mdComments;

class Com extends Common{

    //书籍收藏
    public function doCollect(){
        global $loginId;
        $rules = [
            'book_id'=>['require|number|gt:0',['require'=>'书籍信息异常','number'=>'书籍信息异常','gt'=>'书籍信息异常']],
            'event'=>['require|in:join,remove',['require'=>'操作参数异常','in'=>'操作参数异常']]
        ];
        $post = myValidate::getData($rules);
        $ids = myCache::getCollectBookIds($loginId);
        switch ($post['event']){
            case 'join':
                if($ids && in_array($post['book_id'], $ids)){
                    res_api();
                }
                $data = [
                    'uid' => $loginId,
                    'book_id' => $post['book_id'],
                    'create_time' => time()
                ];
                $re = myDb::add('MemberCollect', $data);
                break;
            case 'remove':
                if($ids && !in_array($post['book_id'], $ids)){
                    res_api();
                }
                $re = myDb::delByWhere('MemberCollect', [['uid','=',$loginId],['book_id','=',$post['book_id']]]);
                break;
        }
        if($re){
            myCache::rmCollectBookIds($loginId);
            res_api();
        }else{
            res_api('收藏失败');
        }
    }

    //发送验证码
    public function sendCode(){
        //res_api();
        $rules = [
            'type' => ['require|in:1,2,3',['require'=>'验证码场景值错误','in'=>'未指定该验证码场景值']],
            'phone' => ['require|mobile',['require'=>'请输入手机号','mobile'=>'手机号格式错误']],
        ];
        $website = mdConfig::getConfig('website');
        if (!isset($website['sms_type'])){
            $website['sms_type'] = 1;
        }
        $post = myValidate::getData($rules);
        switch ($post['type']){
            case 1:
                //注册
                $repeat_uid = myDb::getValue('Member', [['phone','=',$post['phone']]], 'id');
                if($repeat_uid){
                    res_api('该手机号已被注册');
                }
                //$res = myMsg::saiyouSend($post['phone']);
                break;
            case 2:
                //找回密码
                $repeat_uid = myDb::getValue('Member', [['phone','=',$post['phone']]], 'id');
                if(!$repeat_uid){
                    res_api('该手机号尚未绑定');
                }
                //$res = myMsg::saiyouSend($post['phone']);
                break;
            case 3:
                //修改密码
                global $loginId;
                if(!$loginId){
                    res_api('您尚未登录',3);
                }
                $cur = myCache::getMember($loginId);
                if(!$cur['phone']){
                    res_api('您尚未绑定手机号');
                }
                if($cur['phone'] != $post['phone']){
                    res_api('您提交的手机号与绑定的手机号不匹配');
                }
                //$res = myMsg::saiyouSend($post['phone']);
                break;
        }

        if ($website['sms_type'] == 1){
            $res = myMsg::saiyouSend($post['phone']);
        }elseif($website['sms_type'] == 2){
            $res = myMsg::dxtonSend($post['phone']);
        }else{
            $res = myMsg::saiyouSend($post['phone']);
        }

        if($res === true || $res == 100){
            res_api();
        }elseif($res == 101){
            res_api('验证失败');
        }elseif($res == 102){
            res_api('手机号码格式不正确');
        }elseif($res == 103){
            res_api('会员级别不够');
        }elseif($res == 104){
            res_api('内容未审核');
        }elseif($res == 105){
            res_api('内容过多');
        }elseif($res == 106){
            res_api('账户余额不足');
        }elseif($res == 107){
            res_api('Ip受限');
        }elseif($res == 108){
            res_api('手机号码发送太频繁，请换号或隔天再发');
        }elseif($res == 109){
            res_api('帐号被锁定');
        }elseif($res == 110){
            res_api('手机号发送频率持续过高，黑名单屏蔽数日');
        }elseif($res == 120){
            res_api('系统升级');
        }else{
            res_api('发送失败,请重试');
        }
    }

    //打赏
    public function doReward(){
        global $loginId;
        if(!$loginId){
            res_api('您尚未登陆,是否立即登录？',3);
        }
        $member = myCache::getMember($loginId);
        if(!$member){
            session('INDEX_LOGIN_ID',null);
            res_api('您尚未登陆,是否立即登录？',3);
        }
        $rules = [
            'pid'=>['require|number|gt:0',['require'=>'书籍信息异常','number'=>'书籍信息异常','gt'=>'书籍信息异常']],
            'money'=>['require|number|gt:0',['require'=>'礼物信息异常','number'=>'礼物信息异常','gt'=>'礼物信息异常']],
            'count'=>['require|number|gt:0',['require'=>'礼物数量异常','number'=>'礼物数量异常','gt'=>'礼物数量异常']],
            'gift_key'=>['require|alphaNum',['require'=>'礼物信息异常','alphaNum'=>'礼物信息异常']]
        ];
        $data = myValidate::getData($rules);
        $gifts = getMyConfig('reward');
        if($gifts && isset($gifts[$data['gift_key']])){
            $gift = $gifts[$data['gift_key']];
            unset($data['gift_key']);
            $money = $gift['money'] * $data['count'];
            if($money != $data['money']){
                res_api('礼物信息异常');
            }
            $userMoney = myCache::getMemberMoney($loginId);
            if($userMoney < $money){
                res_api('您的金币余额不足');
            }
            $data['channel_id'] = $member['channel_id'];
            $data['agent_id'] = $member['agent_id'];
            $data['uid'] = $loginId;
            $data['type'] = 1;
            $data['gift'] = json_encode($gift);
            $data['create_time'] = time();
            $book = myCache::getBook($data['pid']);
            $book_name = $book ? $book['name'] : '未知';
            $re = mdOrder::doReward($data,$book_name);
            if($re){
                myCache::doMemberMoney($loginId, $money,'dec');
                myCache::rmRewardCache($data['pid']);
                res_api();
            }else{
                res_api('打赏失败，请重试');
            }
        }else{
            res_api('礼物已下架');
        }
    }

    //获取评论列表
    public function getCommentsList(){
        global $loginId;
        $rules = [
            'cate' => ['require|in:1,2',['require'=>'评论对象参数错误','in'=>'评论对象参数不规范']],
            'book_id' => ['require|number|gt:0',['require'=>'评论对象参数错误','number'=>'评论对象参数不规范','gt'=>'评论参数对象不规范']],
            'page' => ['require|number|gt:0',['require'=>'分页参数异常','number'=>'分页参数异常','gt'=>'分页参数异常']]
        ];
        $post = myValidate::getData($rules);
        $order_field = $post['cate'] == 1 ? ['id'=>'desc'] : ['zan_num'=>'desc','id'=>'desc'];
        $where = [['type','=',1],['pid','=',$post['book_id']],['status','=',1]];
        $list = Db::name('Comments')->where($where)->field('id,uid,content,zan_num,create_time')->page($post['page'],10)->order($order_field)->select();
        $count = 0;
        if($list){
            $zanIds = myCache::getZanIds($loginId);
            foreach ($list as &$v){
                $v['is_zan'] = $zanIds && in_array($v['id'], $zanIds) ? 1 : 2;
                $user = myCache::getMember($v['uid']);
                $v['nickname'] = $user['nickname'];
                $v['headimgurl'] = $user['headimgurl'];
                $v['create_time'] = date('Y-m-d H:i',$v['create_time']);
            }
            if($post['page'] == 1){
                $num = Db::name('Comments')->where($where)->count();
                $count = ceil($num/10);
            }
        }
        res_api(['list'=>$list,'count'=>$count]);
    }

    //提交评论
    public function doComments(){
        global $loginId;
        if(!$loginId){
            res_api('您尚未登陆,是否立即登录',3);
        }
        $rules = [
            'pid' => ['require|number|gt:0',['require'=>'评论对象参数错误','number'=>'评论对象参数不规范','gt'=>'评论参数对象不规范']],
            'content' => ['require|max:200',['require'=>'请输入评论内容','max'=>'评论字数超出限制']],
        ];
        $data = myValidate::getData($rules);
        $data['type'] = 1;
        $data['status'] = 0;
        $data['pname'] = '未知';
        $data['uid'] = $loginId;
        $data['create_time'] = time();
        $data['content'] = htmlspecialchars($data['content']);
        $book = myCache::getBook($data['pid']);
        if($book){
            $data['pname'] = $book['name'];
        }
        $re = myDb::add('Comments', $data);
        if($re){
            res_api();
        }else{
            res_api('发表失败，请重试');
        }
    }

    //评论点赞
    public function doZan(){
        global $loginId;
        if(!$loginId){
            res_api('您尚未登陆,是否立即登录',3);
        }
        $pid = myHttp::postId('评论','pid');
        $repeat = myDb::getValue('CommentsZan', [['pid','=',$pid],['uid','=',$loginId]],'id');
        if($repeat){
            res_api('您已经点过赞了');
        }
        $re = mdComments::doZan($pid, $loginId);
        if($re){
            myCache::incCommentsZan($pid);
            myCache::addZanIds($loginId, $pid);
            res_api();
        }else{
            res_api('点赞失败');
        }
    }

}