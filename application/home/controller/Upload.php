<?php
namespace app\home\controller;

use site\myAliyunoss;

class Upload extends Common{
	
	//裁剪图片
	public function headimg(){
		if($this->request->isAjax()){
			$image = $this->request->post('img');
			if(!empty($image)){
				$config = getMyConfig('alioss');
				if(!$config){
					res_api('您尚未配置阿里云oss参数');
				}
				myAliyunoss::$config = $config;
				$content = base64_decode($image);
				$name = md5(microtime().mt_rand(10000,99999)).'.jpg';
				$savename = 'images/'.date('Ymd').'/'.$name;
				$url = myAliyunoss::putObject($savename, $content);
				if($url){
					res_api(['url'=>$url]);
				}else{
					res_api('上传失败,请重试');
				}
			}else{
				res_api('未检测到上传文件');
			}
		}else{
			$config = ['width'=>200,'height'=>200,'ratio'=>1];
			return $this->fetch('common@upload/crop',['config'=>$config]);
		}
	}
}