<?php
namespace app\home\controller;

use think\Controller;

class Doc extends Common {
	
	//隐私政策
	public function privacyPolicy(){
		return $this->fetch('privacyPolicy');
	}
	
	//用户协议
	public function agreement(){
		$title = '用户协议';
		$is_back = $this->request->get('is_back');
		$is_back = in_array($is_back, ['yes','no']) ? $is_back : 'no';
		return $this->fetch('index',['is_back'=>$is_back,'title'=>$title,'key'=>'agreement']);
	}
	
	//版权声明
	public function statement(){
		$title = '版权声明';
		return $this->fetch('index',['is_back'=>'no','title'=>$title,'key'=>'statement']);
	}

    public function about(){
        $config = getMyConfig('site_contact');
        if(!$config){
            $config = myDb::buildArr('tel,email');
        }
        return $this->fetch('',['cur'=>$config]);
    }
}

