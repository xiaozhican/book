var is_upload = false;
var image = document.querySelector('#crop_image');
var options = {
	aspectRatio: $crop_ratio,
       ready: function () {
           var clone = this.cloneNode();
           clone.className = ''
           clone.style.cssText = (
             'display: block;' +
             'width: 100%;' +
             'min-width: 0;' +
             'min-height: 0;' +
             'max-width: none;' +
             'max-height: none;'
           );
         }
};
var cropper = new Cropper(image, options);
layui.use(['upload' ], function() {
	var upload = layui.upload;
	upload.render({
		elem : '#doChoose',
		accept : 'images',
		exts : 'jpg|jpeg|png',
		size : 5000,
		drag : false,
		auto : false,
		choose : function(obj) {
			obj.preview(function(index, file, result) {
				if (/^image\/\w+/.test(file.type)) {
					image.src = result;
					cropper.destroy();
					cropper = new Cropper(image, options);
				} else {
					layError('图片格式有误');
				}
			});
		}
	});
});

function uploadImage(callback){
	if(is_upload){
		layError('文件正在上传中');
		return false;
	}
	is_upload = true;
	layui.use(['jquery','layer'],function(){
		var $ = layui.jquery,
		layer = layui.layer,
		loadindex;
		var $imgData = cropper.getCroppedCanvas({width:$crop_width,height:$crop_height});
		if(!$imgData){
			layError('请选择要裁剪的图片');
			is_upload = false;
			return false;
		}
	    var dataurl = $imgData.toDataURL('image/jpg');
	    var imgData = dataurl.replace(/^data:image\/(png|jpg|jpeg);base64,/,"");
	    loadindex = layer.load(2,{
	    	shade : [0.5,'#000'],
	    	content : '图片上传中...',
	    	success: function (layero) {
	            layero.find('.layui-layer-content').css({
	                'padding-left': '39px',
	                'width': '100px',
	                'line-height':'32px',
	                'color':'#fff'
	            });
	    	}
	    });
	    $.ajax({
	    	type : 'post',
	    	url : window.location.href,
	    	data : {img:imgData},
	    	dataType : 'json',
	    	success : function(res){
	    		is_upload = false;
	    		layer.close(loadindex);
	   			if(res.code === 1){
	   				callback(res.data.url);
	           	}else{
	           		layError(res.msg);
	           	}
	    	},
	    	error : function(){
	    		is_upload = false;
	    		layer.close(loadindex);
	    		layError('网络出错');return false;
	    	}
	    });
	});
}