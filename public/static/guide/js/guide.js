var is_qrcode = false,spread_id = 0,end_number = 1,template_id = 5;
getContent();
new Clipboard('#btn-copy-title').on('success', function (e) {
    e.clearSelection();
    layer.msg('标题复制成功',{icon:1,offset:'200px',time:1000});
});
new Clipboard('#btn-copy-content').on('success', function (e) {
    e.clearSelection();
    layer.msg('正文复制成功',{icon:1,offset:'200px',time:1000});
});
new Clipboard('#btn-copy-link').on('success', function (e) {
    e.clearSelection();
    layer.msg('原文复制成功',{icon:1,offset:'200px',time:1000});
});

function createPicture(){
	var content = $("#wx-article-body").get(0);
	html2canvas(content).then(function(canvas){
	    var canvasWidth = canvas.width;
	    var canvasHeight = canvas.height;
	    var dataURL = canvas.toDataURL("image/jpeg");
	    layer.photos({
	    	photos : {
	   		    data: [{src: dataURL,thumb: dataURL}]
	    	},
	    	anim : 3
	    });
	});
};

function getContent(){
	var data = {number:end_number,book_id:book_id,template_id:template_id};
	ajaxPost(U('getGuideChapter'),data,function(res){
		$('#wx-article-body').html(res.info);
	})
}

function getAnyChapter(number){
	end_number = parseInt(number);
	getContent();
	$('body,html').animate({scrollTop: 0},200);
}

function getAnyBody(cur_id){
	template_id = parseInt(cur_id);
	getContent();
	$('body,html').animate({scrollTop: 0},200);
}

function getCover(){
	layer.open({
		  type: 1,
		  shade: ['0.8','#fff'],
		  shadeClose : true,
		  scrollbar : false,
		  offset : '200px',
		  area : ['50%','60%'],
		  title: '选择文案图片',
		  content: $('.coverBox'),
		});
}

function setFooter(src){
	$('#wx-article-footer').html('<img style="width:100%" src="'+src+'">');
	var bodyHeight = $(document).height();
    $("body,html").animate({"scrollTop": bodyHeight + "px"}, 500);
}

function setCover(src){
	$('#wx-article-cover>img').attr('src',src);
	$('body,html').animate({scrollTop: 0},200);
	layer.closeAll();
}

function setTitle(text){
	$('#wx-article-title').text(text);
	$('body,html').animate({scrollTop: 0},200);
}

//设置底部二维码
function setFooterQr(filekey){
	if(!is_qrcode){
		if(!spread_id){
			layer.confirm('您尚未生成原文链接，是否立即生成？', {
			  offset : '200px',
			  btn: ['确定','取消']
			}, function(index){
				layer.close(index);
				createLink();
			});
		}else{
			ajaxPost(U('createQrcode'),{spread_id:spread_id},function(){
				is_qrcode = true;
			});
		}
	}
	if(is_qrcode){
		var img_src = '/qrcode/'+spread_id+'/qrcode_pic_'+filekey+'.png';
		setFooter(img_src);
	}
}

//生成推广链接
function createLink(){
	var number = end_number + 1;
	var url = U('createLink')+'?book_id='+book_id+'&number='+number;
	doLinkPage(url,1);
}

//编辑链接
function editLink(){
	if(!spread_id){
		layer.msg('网络繁忙，请稍后再试',{icon:2,offset:'200px',time:1000});
		return false;
	}
	var url = U('editLink')+'?spread_id='+spread_id;
	doLinkPage(url,2);
}

function doLinkPage(url,do_flag){
	var do_str = do_flag === 1 ? '生成原文链接' : '编辑原文链接';
	layer.open({
	  type: 2,
	  title: do_str,
	  closeBtn: 1,
	  shade: [0],
	  area: ['800px','630px'],
	  offset : '100px',
	  time: 0,
	  anim: 2,
	  scrollbar :false,
	  content: [url,'yes'],
	  btn : [do_str],
	  yes : function(index,layero){
		  var iframeWindow = $(layero).find('iframe')[0].contentWindow,
            submitID = 'pageSubmit',
            submit = layero.find('iframe').contents().find('#'+ submitID);
            iframeWindow.layui.form.on('submit('+ submitID +')', function(obj){
              ajaxPost(url,obj.field,function(res){
            	  if(do_flag === 1){
            		  spread_id = res.id;
    	       		  is_qrcode = false;
    	       		  $('#txt-referral-link').val(res.url);
    	       		  $('#btn-copy-link').attr('data-clipboard-text',res.url);
    	       		  $('.panel-referral-link').show();
    	       		  $('.create-link-btn').hide();
    	       		  $('.edit-link-btn').show();
    	       		  $("body,html").animate({"scrollTop": bodyHeight + "px"}, 500);
            	  }
            	  layer.close(index);
              });
            });  
            submit.trigger('click');
	  }
	});
}



//构建url
function U($uri){
	var arr = $uri.split('/');
	var len = arr.length;
	var url = '';
	var urlPath = window.location.pathname;
	var tmps = urlPath.split('/');
	switch(len){
		case 3:
			url = '/'+arr[0]+'/'+arr[1]+'/'+arr[2]+'.html';
			break;
		case 2:
			url = '/'+tmps[1]+'/'+arr[0]+'/'+arr[1]+'.html';
			break;
		case 1:
			url = '/'+tmps[1]+'/'+tmps[2]+'/'+$uri+'.html';
			break;
	}
	return url;
}


function ajaxPost(url,data,callback){
	var load;
	$.ajax({
     		type : 'post',
     		url : url,
     		data : data,
     		dataType : 'json',
     		beforeSend : function(){
     			load = layer.load(2,{offset:'300px'});
     		},
     		success : function(res){
     			layer.close(load);
     			if(res.code === 1){
     				callback(res.data);
     			}else{
     				layer.msg(res.msg,{icon:2,offset:'200px',time:1000});
     			}
     		},
     		error : function(){
     			layer.close(load);
     			layer.msg('网络繁忙，请稍后再试',{icon:2,offset:'200px',time:1000});
     		}
     	});
}