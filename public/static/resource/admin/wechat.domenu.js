layui.use(['jquery','form'],function(){
		var $ = layui.jquery, 
		form = layui.form;
		
		changeHDom($type);
		
		form.on('radio(type)',function(obj){
			var value = parseInt(obj.value);
			changeHDom(value);
		});
		
		form.on('submit(dosubmit)',function(obj){
			var data = obj.field;
			ajaxPost('',data,'确定要保存吗？',function(d){
				layOk('保存成功');
				setTimeout(function(){
					location.href = $backUrl;
				},1000);
			});
		});
		
		function changeHDom(value){
			switch(value){
			case 0:
				$('#menukey').addClass('layui-hide');
				$('#xiaoprogram').addClass('layui-hide');
				break;
			case 1:
				$('#xiaoprogram').addClass('layui-hide');
				$('#menukey').removeClass('layui-hide');
				$('#contentremark').text('请填写跳转url');
				break;
			case 2:
				$('#xiaoprogram').removeClass('layui-hide');
				$('#menukey').removeClass('layui-hide');
				$('#contentremark').text('请填写小程序地址');
				break;
			case 3:
				$('#xiaoprogram').addClass('layui-hide');
				$('#menukey').removeClass('layui-hide');
				$('#contentremark').text('请填写点击菜单推送关键字');
				break;
			}
		}
		
	});