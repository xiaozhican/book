layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'keyword', title: '关键字',align:'center',minWidth:120},
		    {title: '回复类型',align:'center',minWidth:300,templet:function(d){
		    	var str = '';
		    	switch(parseInt(d.type)){
		    	case 1:str='文本';break;
		    	case 2:str='图文';break;
		    	}
		    	return str;
		    }},
		    {title: '状态',align:'center',minWidth:200,templet:function(d){
		    	var str = '--';
		    	switch(parseInt(d.status)){
			    	case 1:
			    		str = '启用&nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="off">禁用</a>';
			    		break;
			    	case 2:
			    		str = '禁用&nbsp;<a class="layui-btn layui-btn-noraml layui-btn-xs" lay-event="on">启用</a>';
			    		break;
		    	}
		    	return str;
		    }},
		    {title: '操作',align:'center',minWidth:320,templet:function(d){
		    	var $html = '';
		    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
			    	$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
		    	return $html;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.id,event:obj.event};
		var asked = '';
		switch(obj.event){
			case 'on':
				asked = '确定要启用该关键字吗？';
				break;
			case 'off':
				asked = '确定要禁用该关键字吗？';
				break;
			case 'delete':
				asked = '确定要将该删除该关键字吗？';
				break;
		}
		if(asked){
			ajaxPost(U('doReplyEvent'),data,asked,function(){
				layOk('操作成功');
				setTimeout(function(){
					layLoad('数据加载中...');
					table.reload('table-block');
				},1400);
			});
		}else{
			layError('该按钮未绑定事件');
		}
	});
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});