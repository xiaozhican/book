layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
		    {field: 'pname', title: '评论对象',align:'center',minWidth:160},
		    {field: 'content', title: '评论内容',align:'center',minWidth:300},
		    {field: 'nickname', title: '评论人',align:'center',minWidth:120},
		    {title: '状态',align:'center',minWidth:160,templet:function(d){
		    	var str = '';
		    	switch(parseInt(d.status)){
			    	case 0:
			    		str += '待审核&nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="pass">通过</a>';
			    		str += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="refuse">驳回</a>';
			    		break;
			    	case 1:
			    		str += '正常&nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="off">隐藏</a>';
			    		break;
			    	case 2:
			    		str += '隐藏&nbsp;<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="on">显示</a>';
			    		break;
		    	}
		    	return str;
		    }},
		    {title: '操作',align:'center',minWidth:120,templet:function(d){
		    	return '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		switch(obj.event){
			case 'pass':
				doState({id:field.id,event:obj.event},'确定要通过该评论吗？');
				break;
			case 'refuse':
				doState({id:field.id,event:obj.event},'确定要驳回该评论吗？');
				break;
			case 'on':
				doState({id:field.id,event:obj.event},'确定要显示该评论吗？');
				break;
			case 'off':
				doState({id:field.id,event:obj.event},'确定要隐藏该评论吗？');
				break;
			case 'delete':
				doState({id:field.id,event:obj.event},'确定要将该删除该评论吗？');
				break;
		}
	});
	
	function doState(data,asked){
		ajaxPost(U('doState'),data,asked,function(){
			layOk('操作成功');
			setTimeout(function(){
				layLoad('数据加载中...');
				table.reload('table-block');
			},1400);
		});
	}
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});