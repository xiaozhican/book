layui.use(['jquery','form','upload'],function(){
	var $ = layui.jquery,
		form = layui.form,
		upload = layui.upload;
	changeLocation($is_location);
	form.on('radio(is_location)',function(obj){
		var val = parseInt(obj.value);
		changeLocation(val);
	});
	function changeLocation(value){
		if(value === 1){
			$('#locationUrlBox').removeClass('layui-hide');
		}else{
			$('#locationUrlBox').addClass('layui-hide');
		}
	}
	upload.render({
		  elem: '#doUpload',
		  url: U('Upload/doUploadImg'),
		  accept : 'images',
		  exts : 'jpg|jpeg|png',
		  size : 1024,
		  drag : false,
		  done : function(res){
			  if(res.code === 1){
				  var src = res.data.url;
				  $('.showimg').attr('src',src);
				  $('.hideval').val(src);
			  }else{
				  layError(res.msg);
			  }
		  },
		  error : function(){
			  layError('上传失败,请重试');
		  }
	});
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
			$("#SiteName",window.parent.document).html(data.name);
		});
	});
});