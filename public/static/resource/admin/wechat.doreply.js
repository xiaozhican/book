layui.use(['jquery','layer','form'],function(){
		var $ = layui.jquery,
		layer = layui.layer,
		form = layui.form;
		
		changeHDom($type);
		
		form.on('radio(type)',function(obj){
			var value = parseInt(obj.value)
			changeHDom(value);
		});
		form.on('submit(dosubmit)',function(obj){
			var data = obj.field;
			ajaxPost('',data,'确定要保存吗？',function(d){
				layOk('保存成功');
				setTimeout(function(){
					location.href = $backUrl;
				},1000);
			});
		});
		
		function changeHDom($value){
			if($value === 2){
				$('#materialBox').removeClass('layui-hide');
				$('#textBox').addClass('layui-hide');
			}else{
				$('#materialBox').addClass('layui-hide');
				$('#textBox').removeClass('layui-hide');
			}
		}
		
	});