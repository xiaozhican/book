layui.use(['jquery','layer'],function(){
	var $ = layui.jquery,
	layer = layui.layer;
	var myChart = echarts.init(document.getElementById('memberCharts'),'macarons');
	ajaxPost(U('getUserChartData'),{},'',function(res){
		var option = {
			    tooltip : {
			        trigger: 'axis'
			    },
			    legend: {
			        data:['新增用户','关注用户']
			    },
			    toolbox: {
			        show : true,
			        feature : {
			            mark : {show: true},
			            dataView : {show: true, readOnly: false},
			            magicType : {show: true, type: ['line', 'bar']},
			            restore : {show: true},
			            saveAsImage : {show: true}
			        }
			    },
			    calculable : true,
			    xAxis : [
			        {
			            type : 'category',
			            boundaryGap : false,
			            data : res['key']
			        }
			    ],
			    yAxis : [
			        {
			            type : 'value',
			            axisLabel : {
			                formatter: '{value}'
			            }
			        }
			    ],
			    series : [
			        {
			            name:'新增用户',
			            type:'line',
			            data:res['add'],
			            
			            markPoint : {
			                data : [
			                    {type : 'max', name: '最大值'},
			                    {type : 'min', name: '最小值'}
			                ]
			            }
			        },
			        {
			            name:'关注用户',
			            type:'line',
			            data:res['sub'],
			            markPoint : {
			                data : [
			                	{type : 'max', name: '最大值'},
			                    {type : 'min', name: '最小值'}
			                ]
			            }
			        }
			    ]
			};
		myChart.setOption(option); 
	});
	
	$('.doReply').click(function(){
		var obj = $(this);
		var id = obj.attr('data-id');
		layer.prompt({title: '请输入回复内容',formType:2}, function(text,index){
		    layer.close(index);
		    var data = {id:id,reply:text};
		    ajaxPost(U('Member/doFeedback'),data,'确定要提交该回复吗？',function(){
		    	layOk('提交成功');
		    	obj.remove();
		    });
		});
	});
	
});