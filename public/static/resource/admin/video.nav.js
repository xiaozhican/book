layui.use(['jquery','form'],function(){
	var $ = layui.jquery,
	form = layui.form;
	var len = $('.icon-input-div').length;
	len = len ? (len-1) : 0;
	$('#addInput').click(function(){
		var cur_len = $('.icon-input-div').length;
		if(cur_len >= 5){
			layError('仅可添加5个菜单');
			return false;
		}
		len++;
		var html = '';
		html += '<div class="layui-col-md6 layui-col-sm6">';
		html += '<div class="icon-input-div">';
		html += '<a href="javascript:void(0);" class="layui-btn layui-btn-sm layui-btn-danger delete-btn"><i class="layui-icon layui-icon-delete"></i>删除</a>';
		html += '<div class="layui-form-item">';
		html += '<div class="layui-inline">';
		html += '<input type="text" class="layui-input" name="text['+len+']" placeholder="请输入图标标题"/>';
		html += '</div>';
		html += '</div>';
		html += '<div class="layui-form-item">';
		html += '<input type="text" class="layui-input" name="link['+len+']" placeholder="请输入跳转链接"/>';
		html += '</div>';
		html += '</div>';
		html += '</div>';
		$('#iconBox').append(html);
	});
	
	$('body').on('click','.delete-btn',function(){
		$(this).parent().parent().remove();
	});
	
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
		});
	});
});