layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'login_name', title: '账号',align:'center',width:200},
		    {field: 'remark', title: '备注',align:'center',minWidth:300},
		    {title: '用户状态',align:'center',width:200,templet:function(d){
		    	var str = '';
		    	switch(parseInt(d.status)){
			    	case 1:
			    		str += '启用&nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="off">禁用</a>';
			    		break;
			    	case 2:
			    		str += '禁用&nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="on">启用</a>';
			    		break;
		    	}
		    	return str;
		    }},
		    {title: '操作',align:'center',width:320,templet:function(d){
		    	var str = '';
		    	str += '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="resetpwd">重置登录密码</a>';
		    	str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
		    	str += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
		    	return str;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.id,event:obj.event};
		var asked = '';
		switch(obj.event){
			case 'on':
				asked = '确定要启用该用户吗？';
				break;
			case 'off':
				asked = '确定要禁用该用户吗？';
				break;
			case 'delete':
				asked = '确定要将该删除该用户吗？';
				break;
			case 'resetpwd':
				asked = '确定要将该用户登录密码重置为123456吗？';
				break;
		}
		if(asked){
			ajaxPost(U('doVipEvent'),data,asked,function(){
				layOk('操作成功');
				if(obj.event !== 'resetpwd'){
					setTimeout(function(){
						layLoad('数据加载中...');
						table.reload('table-block');
					},1400);
				}
			});
		}else{
			layError('该按钮未绑定事件');
		}
	});
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});