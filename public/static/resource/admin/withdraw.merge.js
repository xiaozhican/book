layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {title: '充值金额',align:'center',minWidth:100,templet:function(d){
		    	return '¥'+d.charge_money;
		    }},
		    {field: 'charge_num', title: '充值单数',align:'center',minWidth:100},
		    {title: '提现金额',align:'center',minWidth:100,templet:function(d){
		    	return '¥'+d.money;
		    }},
		    {title: '提现周期',align:'center',minWidth:200,templet:function(d){
		    	return d.start_date+'/'+d.end_date;
		    }},
		    {field: 'pay_time', title: '支付时间',align:'center',minWidth:180},
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.ids,event:obj.event};
		switch(obj.event){
			case 'pass':
				doCheck(data,'确定已支付该笔申请吗？');
				break;
		}
	});
	
	function doCheck(data,asked){
		ajaxPost(U('doWithdraw'),data,asked,function(){
			layLoad('数据加载中...');
			setTimeout(function(){
				table.reload('table-block');
			},1400);
		});
	}
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});