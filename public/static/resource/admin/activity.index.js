layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
		    {title: '活动标题',align:'center',minWidth:160,templet:function(d){
		    	var $html = d.name+'&nbsp;'
	    		$html += '<a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="copy" title="点击复制链接"><i class="layui-icon layui-icon-link"></i></a>';
	    		return $html;
		    }},
		    {field: 'content', title: '活动内容',align:'center',minWidth:200},
		    {field: 'between_time', title: '活动时间',align:'center',minWidth:300},
		    {field: 'charge_total', title: '累计充值',align:'center',minWidth:100},
		    {field: 'charge_nums', title: '充值笔数',align:'center',minWidth:100},
		    {field: 'first_str', title: '充值限制',align:'center',minWidth:100},
		    {title: '状态',align:'center',minWidth:160,templet:function(d){
		    	var str = '';
		    	switch(parseInt(d.status)){
			    	case 1:
			    		str += '正常&nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="off">禁用</a>';
			    		break;
			    	case 2:
			    		str += '禁用&nbsp;<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="on">启用</a>';
			    		break;
		    	}
		    	return str;
		    }},
		    {title: '操作',align:'center',minWidth:300,templet:function(d){
		    	var str = '';
		    		str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
			    	str += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
		    	return str;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		switch(obj.event){
			case 'on':
				doState({id:field.id,event:obj.event},'确定要启用该活动吗？');
				break;
			case 'off':
				doState({id:field.id,event:obj.event},'确定要禁用该活动吗？');
				break;
			case 'delete':
				doState({id:field.id,event:obj.event},'确定要将该删除该活动吗？');
				break;
			case 'copy':
				layPage('复制活动链接【'+field.name+'】',field.copy_url,'800px','500px')
				break;
		}
	});
	
	function doState(data,asked){
		ajaxPost(U('doActivityEvent'),data,asked,function(){
			layOk('操作成功');
			setTimeout(function(){
				layLoad('数据加载中...');
				table.reload('table-block');
			},1400);
		});
	}
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});