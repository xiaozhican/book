layui.use(['jquery','layer','table','form','laydate'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form,
	laydate = layui.laydate;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
		    {title: '打赏礼物',align:'center',width:200,templet:function(d){
		    	var str = '<img src="'+d.gift.src+'" width="50"/><br />'+d.gift.name;
		    	return str;
		    }},
		    {field: 'count', title: '打赏数量',align:'center',width:100},
		    {field: 'money', title: '消费书币',align:'center',width:100},
		    {title: '用户信息',align:'left',width:270,templet:function(d){
		    	var str = '';
		    	if(d.userinfo){
		    		str += '<div lay-event="userinfo" style="cursor:pointer;">';
		    		str += '<p><img src="'+d.userinfo.headimgurl+'" width="22" />&nbsp;'+d.userinfo.nickname+'('+d.userinfo.id+')</p>';
		    		str += '<p>'+d.userinfo.openid+'</p>';
		    		str += '<p>注册时间：'+d.userinfo.create_time+'</p>';
		    		str += '</div>';
		    	}else{
		    		str = '--';
		    	}
		    	return str;
		    }},
		    {title: '所属渠道',align:'center',minWidth:100,templet:function(d){
		    	var str = '';
		    	if(d.channel_name){
		    		str = d.channel_name;
		    	}else{
		    		str = '总站';
		    	}
		    	return str;
		    }},
		    {field: 'book_name', title: '打赏书籍',align:'center',minWidth:160},
		    {field: 'create_time', title: '打赏时间',align:'center',minWidth:100}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	laydate.render({elem:'#between_time',type:'datetime',range:'~'});
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		if(obj.event === 'userinfo'){
			layPage('用户详情',field.userinfo_url,'90%','80%');
		}
	});
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});