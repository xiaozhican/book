layui.use(['jquery','form','upload'],function(){
		var $ = layui.jquery,
		form = layui.form,
		upload = layui.upload,
		uploader,cur_obj;
		var len = $('.icon-input-div').length;
		len = len ? (len-1) : 0;
		$('#addInput').click(function(){
			var cur_len = $('.icon-input-div').length;
			if(cur_len >= 5){
				layError('仅可添加5个图标');
				return false;
			}
			len++;
			var html = '';
			html += '<div class="layui-col-md6 layui-col-sm6">';
			html += '<div class="icon-input-div">';
			html += '<a href="javascript:void(0);" class="layui-btn layui-btn-sm layui-btn-danger delete-btn"><i class="layui-icon layui-icon-delete"></i>删除</a>';
			html += '<div class="layui-form-item">';
			html += '<div class="image-div">';
			html += '<div class="image-box size-100x100 show-upload" title="点击上传图标">';
			html += '<img src="/static/common/images/300x300.png" class="showimg"/>';
			html += '<input type="hidden" class="hideval" name="src['+len+']" value=""/>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			html += '<div class="layui-form-item">';
			html += '<div class="layui-inline">';
			html += '<input type="text" class="layui-input" name="text['+len+']" placeholder="请输入图标标题"/>';
			html += '</div>';
			html += '</div>';
			html += '<div class="layui-form-item">';
			html += '<input type="text" class="layui-input" name="link['+len+']" placeholder="请输入跳转链接"/>';
			html += '</div>';
			html += '</div>';
			html += '</div>';
			$('#iconBox').append(html);
		});
		
		$('body').on('click','.show-upload',function(){
			cur_obj = $(this);
			$('.upload-loading').hide();
			$('.upload-remark').show();
			uplayer = layer.open({
				type: 1,
				  shade: false,
				  title: '上传图标',
				  shade: [0],
				  offset : '100px',
				  area : '630px',
				  content: $('.imgLayer')
			});
		});
		
		$('body').on('click','.delete-btn',function(){
			$(this).parent().parent().remove();
		});
		
		upload.render({
			  elem: '.zip-btn-div',
			  url: U('Upload/doUploadIcon'),
			  accept : 'images',
			  acceptMime : 'image/jpg,image/jpeg,image/png',
			  exts : 'jpg|jpeg|png',
			  size : 100,
			  drag : false,
			  choose: function(obj){
				  var str = '<div class="upload-state-div"><span></span><font>文件上传中...</font></div>';
				  $('.upload-loading').html(str);
				  $('.upload-loading').show();
				  $('.upload-remark').hide();
			  },
			  done : function(res){
				  if(res.code === 1){
					  var src = res.data.url;
					  cur_obj.find('.showimg').attr('src',src);
					  cur_obj.find('.hideval').val(src);
					  layer.close(uplayer);
				  }else{
					  var str = '<div class="upload-state-div"><font class="text-red">'+res.msg+'</font></div>';
					  $('.upload-loading').html(str);
				  }
			  },
			  error : function(){
				  layError('上传失败,请重试');
				  $('.upload-loading').hide();
				  $('.upload-remark').show();
			  }
		});
		form.on('submit(dosubmit)',function(obj){
			var data = obj.field;
			ajaxPost('',data,'确定要保存吗？',function(d){
				layOk('保存成功');
			});
		});
	});