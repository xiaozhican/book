layui.use(['table','element'],function(){
		var table = layui.table,
		element = layui.element;
		var theurl = U('charge');
		//实例化活动充值
		table.render({
			elem : '#novel-table',
			url : theurl+'?type=1',
			loading : true,
			cols : [[
				{type: 'numbers',title: '序号'},
			    {field: 'name', title: '书名',align:'center',minWidth:100},
			    {field: 'yesterday',title: '昨日充值',align:'center',minWidth:100},
			    {field: 'money', title: '总充值',align:'center',minWidth:200}
			]],
			page : true,
			height : 'full',
			response: {statusCode:1}
		});
		
		//实例化打赏记录
		table.render({
			elem : '#activity-table',
			url : theurl+'?type=2',
			loading : true,
			cols : [[
				{type: 'numbers',title: '序号'},
			    {field: 'name', title: '活动名称',align:'center',minWidth:100},
			    {field: 'yesterday',title: '昨日充值',align:'center',minWidth:100},
			    {field: 'money', title: '总充值',align:'center',minWidth:200}
			]],
			page : true,
			height : 'full',
			response: {statusCode:1}
		});
	});