layui.use(['layedit','form','layer','jquery'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	form = layui.form, 
	layedit = layui.layedit;
	layedit.set({
        tool: [
            'html' , 'strong', 'italic', 'underline', 'del', 'addhr', '|', 'link', 'unlink'
            , '|', 'left', 'center', 'right'
        ]
        , height: '300px'
    });
	var textindex = layedit.build('content');
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		var content = layedit.getContent(textindex);
		data['content'] = content;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
			setTimeout(function(){
				window.history.go(-1);
			},1000);
		});
	});
});