layui.use(['jquery','layer','table','form','laydate'],function(){
    var $ = layui.jquery,
        layer = layui.layer,
        table = layui.table,
        form = layui.form,
        laydate = layui.laydate;
    layLoad('数据加载中...');
    table.render({
        elem : '#table-block',
        url : window.location.href,
        loading : true,
        cols : [[
            //{field: 'datetime', title: '下载日期',align:'center',width:200},
            {field: 'channel', title: '渠道名称',align:'center',width:200},
            {field: 'app_start', title: '总回头人数',align:'center',width:200},
            {field: 'day_reg', title: '今日注册人数',align:'center',width:200},
            {field: 'zuotian_reg', title: '昨天注册人数',align:'center',width:200},
            {field: 'zuotian_huitou', title: '昨天回头人数',align:'center',width:200},
            {field: 'day_huitou', title: '今日回头人数',align:'center',width:200},
            //{field: 'num', title: '渠道下载次数',align:'center',width:200},
            {title: '操作',align:'center',width:300,templet:function(d){
                    var str = '';
                    str += '<a class="layui-btn layui-btn-default layui-btn-xs" href="'+d.do_app_start+'">回头客详情</a>';
                    str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">查看详情</a>';
                    return str;
                }}
        ]],
        page : true,
        height : 'full-220',
        response: {statusCode:1},
        id : 'table-block',
        done:function (res) {
            layer.closeAll();
        }
    });

    //laydate.render({elem:'#between_time',type:'datetime',range:'~'});

    form.on('submit(table-search)',function(obj){
        var where = obj.field;
        layLoad('数据加载中...');
        table.reload('table-block',{
            where : where,
            page: {curr:1}
        });
    });
});