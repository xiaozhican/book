layui.use(['jquery','layer','table','form','laydate'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	laydate = layui.laydate,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'uid', title: '粉丝ID',align:'center',width:120},
		    {field: 'nickname', title: '粉丝昵称',align:'center',width:120},
		    {title: '反馈内容',align:'left',minWidth:160,templet:function(d){
		    	return d.content ? d.content : '--';
		    }},
		    {title: '联系电话',align:'center',width:160,templet:function(d){
		    	return d.phone ? d.phone : '--';
		    }},
		    {title: '是否采纳',align:'left',width:200,templet:function(d){
		    	var str = '';
		    	switch(parseInt(d.is_adopt)){
		    		case 1:
		    			str += '已采纳&nbsp;';
		    			str += '<br />奖励书币：' + d.adopt_coin;
		    			str += '<br />采纳时间：' + d.adopt_time;
		    			break;
		    		case 2:
		    			str += '未采纳&nbsp;';
		    			str += '<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="adopt">立即采纳</a>';
		    			break;
		    	}
		    	return str;
		    }},
		    {field: 'create_time', title: '反馈时间',align:'center',width:160},
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		if(obj.event === 'adopt'){
			layer.prompt({title: '请输入采纳奖励书币', formType: 0,value: 20}, function(coin, index){
				  layer.close(index);
				  ajaxPost(U('doAdopt'),{id:field.id,adopt_coin:coin},'',function(){
					  layOk('操作成功');
					  setTimeout(function(){
						  layLoad('数据加载中...');
						  table.reload('table-block');
					  },1400);
				  });
			});
		}
	});
	laydate.render({elem:'#between_time',type:'datetime',range:'~'});
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});