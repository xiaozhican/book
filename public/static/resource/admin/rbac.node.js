layui.config({
	base : '/static/layadmin/lay_extend/tree/'
}).use(['jquery','layer','table','treetable'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	treetable = layui.treetable;
	function renderTable(){
		layLoad('数据加载中...');
		treetable.render({
			treeColIndex: 0,
	        treeSpid: "0",
	        treeIdName: 'id',
	        treePidName: 'pid',
	        treeDefaultClose: true,
	        treeLinkage: false,
	        elem: '#table-block',
	        url: window.location.href,
	        page: false,
	        height : 'full-220',
	        id : 'table-block',
	        cols: [[
	        	{field:'name',minWidth:300, title:'节点名称'},
	            {field:'menu_name',minWidth:100,title:'节点类型',align:'center'},
	            {field:'url',minWidth:100, title: '链接'},
	            {title:'排序',minWidth:140,align:'center',templet:function(d){
	            	var is_menu = parseInt(d.is_menu);
	            	var str = '';
	            	if(is_menu === 1){
	            		str += '<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="sortUp">向上排序</a>';
	    				str += '<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="sortDown">向下排序</a>';
	            	}
	            	return str;
	            }},
	            {title:'操作',minWidth:200,align:'center',templet:function(d){
					var str = '';
					var is_menu = parseInt(d.is_menu);
					if(is_menu === 1){
						str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.add_url+'">新增子节点</a>';
					}
					str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
					str += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
					return str;
				}}
	        ]],
		    done:function (res) {
		    	layer.closeAll();
		    	//treetable.expandAll('#table-block');
		    }
		});
	}
	renderTable();
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.id,event:obj.event};
		if(data.event === 'delete'){
			ajaxPost(U('doNodeEvent'),data,'删除节点会将所有子节点一并删除，确认要继续吗？',function(){
				layOk('删除成功');
				setTimeout(function(){
					layLoad('数据加载中...');
					renderTable();
				},1300);
			});
		}else{
			if(data.event === 'sortUp' || data.event === 'sortDown'){
				ajaxPost(U('doNodeEvent'),data,'',function(){
					layLoad('数据加载中...');
					renderTable();
				})
			}else{
				layError('该按钮未绑定事件');
			}
		}
	});
	
	$('#openAll').click(function () {
        treetable.expandAll('#table-block');
    });

    $('#hideAll').click(function () {
        treetable.foldAll('#table-block');
    });
});