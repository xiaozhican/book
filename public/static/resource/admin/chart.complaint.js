layui.use(['table','form'],function(){
	var table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
		    {field: 'name', title: '书籍名称',align:'center',minWidth:200},
		    {field: 'type1', title: '色情',align:'center',minWidth:100},
		    {field: 'type2', title: '血腥',align:'center',minWidth:100},
		    {field: 'type3', title: '暴力',align:'center',minWidth:100},
		    {field: 'type4', title: '违法',align:'center',minWidth:100},
		    {field: 'type5', title: '盗版',align:'center',minWidth:100},
		    {field: 'type6', title: '其他',align:'center',minWidth:100},
		    {field: 'total', title: '总举报次数',align:'center',minWidth:100}
		]],
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where
		});
	});
});