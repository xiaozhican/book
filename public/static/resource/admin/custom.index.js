layui.use(['jquery','layer','table','form','laydate'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form,
	laydate = layui.laydate;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'name', title: '任务名称',align:'center',minWidth:200},
		    {field: 'send_time', title: '发送时间',align:'center',minWidth:120},
		    {title: '发送状态',align:'center',minWidth:200,templet:function(d){
		    	if(parseInt(d.status) === 1){
		    		return '已发送';
		    	}else{
		    		return '未发送';
		    	}
		    }},
		    {title: '发送明细',align:'center',minWidth:300,templet:function(d){
		    	if(parseInt(d.status) === 1){
		    		return '成功:<font class="text-red">'+d.suc_num+'</font>人,失败:<font class="text-red">'+d.fail_num+'</font>人';
		    	}else{
		    		return '--';
		    	}
		    }},
		    {field: 'create_time', title: '创建时间',align:'center',minWidth:200},
		    {title: '操作',align:'center',minWidth:180,templet:function(d){
		    	var $html = '';
		    	var status = parseInt(d.status);
		    		if(status == 2){
		    			$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
		    		}else{
		    			$html += '<a class="layui-btn layui-btn-xs layui-btn-disabled">编辑</a>';
		    		}
		    		$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
		    	return $html;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.id,event:obj.event};
		switch(obj.event){
			case 'delete':
				ajaxPost(field.del_url,data,'确认要删除该任务消息吗？',function(){
					layOk('操作成功');
					setTimeout(function(){
						layLoad('数据加载中...');
						table.reload('table-block');
					},1400);
				});
			break;
		}
	});
	laydate.render({elem:'#between_time',type:'datetime',range:'~'});
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});