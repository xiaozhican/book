layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'name', title: '申请来源',align:'center',minWidth:150},
		    {title: '充值金额',align:'center',width:100,templet:function(d){
		    	return '¥'+d.charge_money;
		    }},
		    {field: 'charge_num', title: '充值单数',align:'center',width:100},
		    {title: '提现金额',align:'center',width:100,templet:function(d){
		    	return '¥'+d.money;
		    }},
		    {title: '提现周期',align:'center',width:200,templet:function(d){
		    	return d.start_date+'/'+d.end_date;
		    }},
		    {title: '提现账号信息',align:'left',width:260,templet:function(d){
		    	var str = '';
		    	str += d.bank_user+'<br />';
		    	str += d.bank_name+'<br />';
		    	str += d.bank_no;
		    	return str;
		    }},
		    {field: 'create_time', title: '申请时间',align:'center',width:180},
		    {field: 'remark', title: '备注',align:'center',mWidth:180},
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
});