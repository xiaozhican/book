layui.config({
	base : '/static/layadmin/lay_extend/ckplayer/'
}).use(['jquery','ckplayer'],function(){
	var $ = layui.jquery,
	ckplayer = layui.ckplayer;
	var vUrl = $('#video').data('url'),
	videoObject = {
    container: '#video',
    loop: true,
    autoplay: true,
    video: [
        [vUrl, 'video/mp4']
    ]
};
var player = new ckplayer(videoObject);
});