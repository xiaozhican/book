layui.use(['jquery','layer','table','form','laydate'],function(){
    var $ = layui.jquery,
        layer = layui.layer,
        table = layui.table,
        form = layui.form,
        laydate = layui.laydate;
    layLoad('数据加载中...');
    table.render({
        elem : '#table-block',
        url : window.location.href,
        loading : true,
        cols : [[
            {field: 'username', title: '登录用户',align:'center',width:200},
            {field: 'login_type', title: '登录类型',align:'center',width:200},
            //{field: 'login_ip', title: '登录IP',align:'center',width:200},
            {title: '在线状态',align:'center',width:200,templet:function(d){
                    var str = '';
                    switch(parseInt(d.is_online)){
                        case 1:
                            str += '上线';
                            break;
                        case 2:
                            str += '下线';
                            break;
                    }
                    return str;
                }},
            {title: '登录状态',align:'center',width:200,templet:function(d){
                    var str = '';
                    switch(parseInt(d.status)){
                        case 1:
                            str += '登录成功';
                            break;
                        case 2:
                            str += '登录失败';
                            break;
                    }
                    return str;
                }},
            {field: 'remark', title: '备注',align:'center',width:200},
            // //{field: 'code', title: '代码块',align:'center',minWidth:300},
            {field: 'create_time', title: '创建时间',align:'center',width:200},
            // {title: '操作',align:'center',width:200,templet:function(d){
            //         var str = '';
            //         str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
            //         str += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
            //         return str;
            //     }}
        ]],
        page : true,
        height : 'full-220',
        response: {statusCode:1},
        id : 'table-block',
        done:function (res) {
            layer.closeAll();
        }
    });

    laydate.render({elem:'#between_time',type:'datetime',range:'~'});

    form.on('submit(table-search)',function(obj){
        var where = obj.field;
        layLoad('数据加载中...');
        table.reload('table-block',{
            where : where,
            page: {curr:1}
        });
    });
});