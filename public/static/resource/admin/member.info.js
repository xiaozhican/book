layui.use(['table','element'],function(){
		var table = layui.table,
		element = layui.element;
		
		//实例化充值记录
		table.render({
			elem : '#charge-table',
			url : chargeUrl,
			loading : true,
			cols : [[
			    {field: 'order_no', title: '订单号',align:'center',minWidth:200},
			    {title: '是否扣量',align:'center',minWidth:80,templet:function(d){
			    	var str = '否';
			    	if(parseInt(d.is_count) == 2){
			    		str = '<a class="layui-btn layui-btn-danger layui-btn-xs">扣</a>';
			    	}
			    	return str;
			    }},
			    {field: 'money', title: '充值金额',align:'center',minWidth:100},
			    {field: 'from_name', title: '订单来源',align:'center',minWidth:160},
			    {title: '状态',align:'center',width:120,templet:function(d){
			    	var str = '未知';
			    	switch(parseInt(d.status)){
				    	case 0:str = '已关闭';break;
				    	case 1:str = '待支付';break;
				    	case 2:str = '已支付';break;
			    	}
			    	return str;
			    }},
			    {field: 'create_time', title: '下单时间',align:'center',minWidth:200}
			]],
			page : true,
			height : 'full',
			response: {statusCode:1}
		});
		
		//实例化未支付充值记录
		table.render({
			elem : '#charge-table1',
			url : chargeUrl+'&status=1',
			loading : true,
			cols : [[
				{field: 'order_no', title: '订单号',align:'center',minWidth:200},
				{field: 'money', title: '充值金额',align:'center',minWidth:100},
				{field: 'from_name', title: '订单来源',align:'center',minWidth:160},
				{title: '状态',align:'center',width:120,templet:function(d){
					var str = '未知';
					switch(parseInt(d.status)){
					case 0:str = '已关闭';break;
					case 1:str = '待支付';break;
					case 2:str = '已支付';break;
					}
					return str;
				}},
				{field: 'create_time', title: '下单时间',align:'center',minWidth:200}
				]],
				page : true,
				height : 'full',
				response: {statusCode:1}
		});
		//实例化已支付充值记录
		table.render({
			elem : '#charge-table2',
			url : chargeUrl+'&status=2',
			loading : true,
			cols : [[
				{field: 'order_no', title: '订单号',align:'center',minWidth:200},
				{field: 'money', title: '充值金额',align:'center',minWidth:100},
				{field: 'from_name', title: '订单来源',align:'center',minWidth:160},
				{title: '状态',align:'center',width:120,templet:function(d){
					var str = '未知';
					switch(parseInt(d.status)){
					case 0:str = '已关闭';break;
					case 1:str = '待支付';break;
					case 2:str = '已支付';break;
					}
					return str;
				}},
				{field: 'create_time', title: '下单时间',align:'center',minWidth:200}
				]],
				page : true,
				height : 'full',
				response: {statusCode:1}
		});
		
		//实例化打赏记录
		table.render({
			elem : '#reward-table',
			url : rewardUrl,
			loading : true,
			cols : [[
				{title: '打赏礼物',align:'center',width:200,templet:function(d){
			    	var str = '<img src="'+d.gift.src+'" width="50"/><br />'+d.gift.name;
			    	return str;
			    }},
			    {field: 'count', title: '打赏数量',align:'center',width:100},
			    {field: 'money', title: '打赏书币',align:'center',width:100},
			    {field: 'from_name', title: '打赏来源',align:'center',minWidth:160},
			    {field: 'create_time', title: '打赏时间',align:'center',minWidth:100}
			]],
			page : true,
			height : 'full',
			response: {statusCode:1}
		});
		
		//实例化阅读记录
		table.render({
			elem : '#read-table',
			url : readUrl,
			loading : true,
			cols : [[
			    {field: 'create_time', title: '阅读时间',align:'center',minWidth:200},
			    {field: 'name', title: '书籍名称',align:'center',minWidth:100},
			    {field: 'chapter_name', title: '章节',align:'center',minWidth:160},
			]],
			page : false,
			height : 'full',
			response: {statusCode:1}
		});
		
		//实例化签到记录
		table.render({
			elem : '#sign-table',
			url : signUrl,
			loading : true,
			cols : [[
			    {title: '序号',type:'numbers'},
			    {field: 'create_time', title: '签到时间',align:'center',minWidth:200},
			    {field: 'money', title: '获得书币',align:'center',minWidth:160}
			]],
			page : true,
			height : 'full',
			response: {statusCode:1}
		});
		
		//实例化消费记录
		table.render({
			elem : '#consume-table',
			url : consumeUrl,
			loading : true,
			cols : [[
				{title: '序号',type:'numbers'},
			    {field: 'money', title: '消费书币',align:'center',minWidth:100},
			    {field: 'summary', title: '描述',align:'center',minWidth:160},
			    {field: 'create_time', title: '消费时间',align:'center',minWidth:200}
			]],
			page : true,
			height : 'full',
			response: {statusCode:1}
		});
		
		//实例化邀请记录
		table.render({
			elem : '#invite-table',
			url : inviteUrl,
			loading : true,
			cols : [[
				{title: '序号',type:'numbers'},
				{field: 'nickname', title: '邀请用户',align:'center',minWidth:100},
				{field: 'phone', title: '绑定手机',align:'center',minWidth:100},
				{field: 'money', title: '奖励书币',align:'center',minWidth:100},
				{field: 'create_date', title: '邀请日期',align:'center',minWidth:200}
				]],
				page : true,
				height : 'full',
				response: {statusCode:1}
		});
		
	});