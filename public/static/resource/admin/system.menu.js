layui.config({
	base : '/static/layadmin/lay_extend/tree/'
}).use(['jquery','layer','table','treetable'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	treetable = layui.treetable;
	function renderTable(){
		layLoad('数据加载中...');
		treetable.render({
			treeColIndex: 0,
	        treeSpid: "0",
	        treeIdName: 'id',
	        treePidName: 'pid',
	        treeDefaultClose: true,
	        treeLinkage: false,
	        elem: '#table-block',
	        url: window.location.href,
	        page: false,
	        height : 'full-220',
	        id : 'table-block',
	        cols: [[
	        	{field:'name',minWidth:300, title:'菜单名称'},
	            {field:'url',minWidth:100, title: '链接'},
	            {title:'排序',minWidth:140,align:'center',templet:function(d){
	            	var str = '';
	            		str += '<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="sortUp">向上排序</a>';
	    				str += '<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="sortDown">向下排序</a>';
	            		return str;
	            }},
	            {title:'操作',minWidth:200,align:'center',templet:function(d){
					var str = '';
					str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.add_url+'">新增子菜单</a>';
					str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
					var status = parseInt(d.status);
					switch(status){
						case 1:
							str += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="off">禁用</a>';
							break;
						case 2:
							str += '<a class="layui-btn layui-btn-primary layui-btn-xs" lay-event="on">启用</a>';
							break;
					}
					return str;
				}}
	        ]],
		    done:function (res) {
		    	layer.closeAll();
		    	treetable.expandAll('#table-block');
		    }
		});
	}
	renderTable();
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.id,event:obj.event};
		switch(data.event){
		case 'sortUp':
			doMenuEvent('',data,0);
			break;
		case 'sortDown':
			doMenuEvent('',data,0);
			break;
		case 'on':
			doMenuEvent('确定要启用该菜单吗？',data,1);
			break;
		case 'off':
			doMenuEvent('确定要禁用该菜单吗？',data,1);
			break;
		default:
			layError('该按钮尚未绑定事件')
			break;
		}
	});
	
	function doMenuEvent(asked,data,is_wait){
		ajaxPost(event_url,data,asked,function(){
			if(is_wait === 1){
				layOk('操作成功');
				setTimeout(function(){
					layLoad('数据加载中...');
					renderTable();
				},1300);
			}else{
				layLoad('数据加载中...');
				renderTable();
			}
		})
	}
	
	$('#openAll').click(function () {
        treetable.expandAll('#table-block');
    });

    $('#hideAll').click(function () {
        treetable.foldAll('#table-block');
    });
});