layui.use(['jquery','form','layer','laydate'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	form = layui.form,
	laydate = layui.laydate;
	getUserCount();
	laydate.render({elem:'#send_time',type:'datetime'});
	
	form.on('radio(is_all)',function(obj){
		var val = parseInt(obj.value);
		if(val === 2){
			$('#whereItem').removeClass('layui-hide');
			getUserCount();
		}else{
			$('#whereItem').addClass('layui-hide');
		}
	});
	form.on('radio(sex)',function(obj){
		getUserCount();
	});
	form.on('radio(is_charge)',function(obj){
		getUserCount();
	});
	form.on('radio(money)',function(obj){
		getUserCount();
	});
	form.on('radio(subscribe_time)',function(obj){
		getUserCount();
	});
	
	$('.get-near-time').click(function(e){
		e.stopPropagation();
		var min = parseInt($(this).attr('data-min'));
		if(min > 0){
			var myDate = new Date();
			var time = myDate.getTime();
			var current = time + min*60*1000;
			var obj = new Date(current);
			var y = obj.getFullYear();
			var m = obj.getMonth()+1;
			var d = obj.getDate();
			var h = obj.getHours();
			var mm = obj.getMinutes();
			var s = obj.getSeconds();
			var str = y + '-'+ m + '-' + d + ' ' + h + ':' + mm + ':' + s;
			$('#send_time').val(str);
		}
	});
	
	function getUserCount(){
		var is_all = $('input[name="is_all"]:checked').val();
		var sex = $('input[name="sex"]:checked').val();
		var is_charge = $('input[name="is_charge"]:checked').val();
		var money = $('input[name="money"]:checked').val();
		var subscribe_time = $('input[name="subscribe_time"]:checked').val();
		if(parseInt(is_all) == 2){
			var data = {
					sex : sex,
					is_charge : is_charge,
					money : money,
					subscribe_time : subscribe_time
		 		};
			ajaxPost(U('getUserCount'),data,'',function(res){
				$('#userCount').val(res.count+'人');
			});
		}
	}
	
	form.on('submit(testsend)',function(obj){
		var data = obj.field;
		ajaxPost(U('sendCustom'),data,'确定要测试发送给该用户吗？',function(d){
			layOk('发送成功');
		});
	});
	
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
			setTimeout(function(){
				location.href = $backUrl;
			},1000);
		});
	});
});