layui.use(['jquery','layer','form'],function(){
		var $ = layui.jquery, 
		layer = layui.layer,
		form = layui.form;
		changeFreeType($free_type);
		var len = 0;
		var uploader = new plupload.Uploader({
			runtimes : 'html5,flash,silverlight,html4',
			browse_button : 'doUploadZip', 
			url : U('Upload/doUploadFile'),
			flash_swf_url : '/static/plugins/plupload/Moxie.swf',
			silverlight_xap_url : '/static/plugins/plupload/Moxie.xap',
			multi_selection : true,
			chunk_size: '500kb', 
			save_key : true,
			filters : {
				max_file_size : '20mb',
				mime_types: [
					{title : "Zip files", extensions : 'zip'},
				]
			},
			init: {
				FilesAdded: function(up, files) {
					plupload.each(files, function(file) {
						var html = '';
						html += '<div class="zip-item" id="file-'+file.id+'">';
						html += '<div class="zip-item-img">';
						html += '<img src="/static/common/images/zip.png"/>';
						html += '</div>';
						html += '<div class="zip-item-title">';
						html += '<p style="">'+file.name+'</p>';
						html += '</div>';
						html += '<div class="zip-item-load">';
						html += '<span class="progress-val">等待上传</span>';
						html += '</div>';
						html += '<input type="hidden" name="zip_title['+len+']" class="zip_title" value="'+file.name+'"/>';
						html += '<input type="hidden" name="zip_filename['+len+']" class="zip_filename" />';
						html += '</div>';
						$('#zip-list-body').append(html);
						len++;
					});
					uploader.start();
				},
				UploadProgress : function(up, file) {
					var percent = file.percent;
					var id = '#file-'+file.id;
					$(id).find('.progress-val').text(percent+'%');
				},
				FileUploaded : function (up,file,res) {
		            var data = JSON.parse(res.response);
		            var id = '#file-'+file.id;
		            if(data.msg === 'ok'){
		            	$(id).find('.progress-val').text('上传成功');
		            	$(id).find('.zip_filename').val(data.data.filename);
		            }else{
		            	$(id).find('.progress-val').text(data.msg);
		            }
		        },
				Error: function(up, err) {
					layError(err.message);
				}
			}
		});
		uploader.init();
		form.on('radio(free_type)',function(obj){
			var value = parseInt(obj.value);
			changeFreeType(value);
		});
		
		function changeFreeType(value){
			if(value == 2){
				$('.free-input-line').removeClass('layui-hide')
			}else{
				$('.free-input-line').addClass('layui-hide')
			}
		}
		
		form.on('submit(dosubmit)',function(obj){
			var data = obj.field;
			ajaxPost('',data,'确定要提交吗？',function(res){
				var str = '<div class="layui-fluid"><ul>';
				$.each(res,function(i,item){
					str += '<li class="err-li">'+item+'</li>';
				});
				str += '</ul></div>';
				layer.open({
					type: 1,
					  shade: false,
					  title: '批量上传日志',
					  shade: [0],
					  offset : '200px',
					  area : ['400px','300px'],
					  content: str,
					  btn : '返回小说列表',
					  yes : function(){
						  window.location.href = '/admin/Novel/index.html';
					  }
				});
			});
		});
	});