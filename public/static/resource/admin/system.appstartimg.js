layui.use(['jquery','form','upload'],function(d){
	var $ = layui.jquery,
	form = layui.form,
	upload = layui.upload;
	
	upload.render({
		  elem: '#uplogo',
		  url: U('Upload/doUploadImg'),
		  accept : 'images',
		  exts : 'gif|jpg|jpeg|png',
		  size : 1024,
		  drag : false,
		  done : function(res){
			  if(res.code === 1){
				  $('#logoInp').val(res.data.url);
				  $('#logoShow').attr('src',res.data.url);
			  }else{
				  layError(res.msg);
			  }
		  },
		  error : function(){
			  layError('上传失败,请重试');
		  }
	});
	
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
		});
	});
	
});