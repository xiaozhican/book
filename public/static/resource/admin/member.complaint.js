layui.use(['jquery','layer','table','form','laydate'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	laydate = layui.laydate,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'uid', title: '粉丝ID',align:'center',width:120},
		    {field: 'nickname', title: '粉丝昵称',align:'center',width:120},
		    {title: '书籍信息',align:'center',width:160,templet:function(d){
		    	return '第'+d.number+'章<br />'+d.book_name;
		    }},
		    {title: '投诉类型',align:'center',width:140,templet:function(d){
		    	var str = '未知';
		    	switch(d.type){
			    	case 1:str = '色情低俗';break;
			    	case 2:str = '政治敏感';break;
			    	case 3:str = '违法犯罪';break;
			    	case 4:str = '广告骚扰';break;
			    	case 5:str = '无法继续阅读';break;
			    	case 6:str = '标题或封面图片低俗';break;
			    	case 7:str = '其他';break;
		    	}
		    	return str;
		    }},
		    {title: '投诉内容',align:'left',minWidth:160,templet:function(d){
		    	return d.content ? d.content : '--';
		    }},
		    {title: '联系电话',align:'center',width:160,templet:function(d){
		    	return d.phone ? d.phone : '--';
		    }},
		    {field: 'create_time', title: '反馈时间',align:'center',width:160},
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	laydate.render({elem:'#between_time',type:'datetime',range:'~'});
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});