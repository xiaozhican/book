layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'name', title: '角色名称',align:'center',minWidth:120},
		    {field: 'summary', title: '角色描述',minWidth:200},
		    {field: 'create_time', title: '创建时间',align:'center',minWidth:160},
		    {title: '角色状态',align:'center',minWidth:100,templet:function(d){
		    	var $html = d.status_name+'&nbsp;';
		    	var status = parseInt(d.status);
		    	switch(status){
			    	case 1:
			    		$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="off">禁用</a>';
			    		break;
			    	case 2:
			    		$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="on">启用</a>';
			    		break;
		    	}
		    	return $html;
		    }},
		    {title: '操作',align:'center',minWidth:180,templet:function(d){
		    	var $html = '';
		    	$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
		    	$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
		    	return $html;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.id,event:obj.event};
		var asked = '';
		console.log(data);
		switch(obj.event){
			case 'on':
				asked = '确定要启用该角色吗？';
				break;
			case 'off':
				asked = '确定要禁用该角色吗？';
				break;
			case 'delete':
				asked = '确定要将该删除该角色吗？';
				break;
		}
		if(asked){
			ajaxPost(U('doRoleEvent'),data,asked,function(){
				layOk('操作成功');
				setTimeout(function(){
					layLoad('数据加载中...');
					table.reload('table-block');
				},1400);
			});
		}else{
			layError('该按钮未绑定事件');
		}
	});
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});