layui.config({
	base : '/static/layadmin/lay_extend/tree/'
}).use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
		layer = layui.layer,
		table = layui.table,
		form = layui.form;
	layLoad('数据加载中...');
	table.render({
		treeColIndex: 0,
		treeSpid: "0",
		treeIdName: 'id',
		treePidName: 'pid',
		treeDefaultClose: true,
		treeLinkage: false,
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		height : 'full-220',
		id : 'table-block',
		cols : [[
			{title: '栏目名称',align:'left',minWidth:60,templet:function(d){
					var str = '';
					switch(parseInt(d.level)){
						case 0:
							str += '>&nbsp;&nbsp;'+d.name;
							break;
						case 1:
							str += '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'+d.name;
							break;
					}
					return str;
				}},
			{title: '栏目级别',align:'center',minWidth:50,templet:function(d){
					var str = '';
					switch(parseInt(d.level)){
						case 0:
							str += '①';
							break;
						case 1:
							str += '②';
							break;
					}
					return str;
				}},
			{title: 'PC首页显示状态',align:'center',minWidth:60,templet:function(d){
					var str = '';
					switch(parseInt(d.is_show)){
						case 1:
							str += '正常&nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="off">隐藏</a>';
							break;
						case 2:
							str += '隐藏&nbsp;<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="on">显示</a>';
							break;
					}
					return str;
				}},
			{field: 'key', title: 'active标识',align:'center',minWidth:60},
			{title: '操作',align:'center',minWidth:400,templet:function(d){
					var $html = '';
					if (d.level == 0){
						$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_add_child_url+'">新增子栏目</a>';
					}
					$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
					$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
					return $html;
				}}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1},
		id : 'table-block',
		done:function (res) {
			layer.closeAll();
		}
	});

	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		switch(obj.event){
			case 'pass':
				doState({id:field.id,event:obj.event},'确定要通过该评论吗？');
				break;
			case 'refuse':
				doState({id:field.id,event:obj.event},'确定要驳回该评论吗？');
				break;
			case 'on':
				doState({id:field.id,event:obj.event},'确定要显示该栏目吗？');
				break;
			case 'off':
				doState({id:field.id,event:obj.event},'确定要隐藏该栏目吗？');
				break;
			case 'delete':
				doState({id:field.id,event:obj.event},'确定要删除该栏目吗？');
				break;
		}
	});

	function doState(data,asked){
		ajaxPost(U('doStates'),data,asked,function(){
			layOk('操作成功');
			setTimeout(function(){
				layLoad('数据加载中...');
				table.reload('table-block');
			},1400);
		});
	}

	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});