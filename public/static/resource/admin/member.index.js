layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'id', title: '粉丝ID',align:'center',minWidth:120},
		    {field: 'channel_name', title: '所属渠道',align:'center',minWidth:120},
		    {field: 'agent_name', title: '所属代理',align:'center',minWidth:120},
		    {field: 'nickname', title: '粉丝昵称',align:'center',minWidth:120},
		    {title: '书币余额',align:'center',minWidth:130,templet:function(d){
		    	var $html = d.money;
		    		$html += '&nbsp;';
		    		$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="charge">书币调整</a>';
		    	return $html;
		    }},
		    {field: 'is_subscribe', title: '是否关注',align:'center',minWidth:100},
		    {field: 'phone', title: '绑定手机号',align:'center',minWidth:100},
		    {title: 'VIP',align:'center',minWidth:260,templet:function(d){
		    	var $html = d.vip_str+'&nbsp;';
		    	if(parseInt(d.viptime) > 0){
		    		$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="vipoff">取消vip</a>';
		    	}else{
		    		$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="vipon">设置vip</a>';
		    	}
		    	return $html;
		    }},
		    {field: 'create_time', title: '注册时间',align:'center',minWidth:200},
		    {title: '用户状态',align:'center',minWidth:100,templet:function(d){
		    	var $html = d.status_name+'&nbsp;';
		    	var status = parseInt(d.status);
		    	switch(status){
			    	case 1:
			    		$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="statusoff">禁用</a>';
			    		break;
			    	case 2:
			    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="statuson">启用</a>';
			    		break;
		    	}
		    	return $html;
		    }},
		    {title: '更多',align:'center',minWidth:120,templet:function(d){
		    	var $html = '';
		    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="info">查看详情</a>';
		    	return $html;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.id,event:obj.event};
		switch(obj.event){
			case 'charge':
				layer.prompt({title: '书币调整（减少请填负数）', formType: 0}, function(pass, index){
					  layer.close(index);
					  data['money'] = pass;
					  ajaxPost(U('doMemberEvent'),data,'确定要继续吗？',function(){
						  layOk('调整成功');
						  setTimeout(function(){
							  layLoad('数据加载中...');
							  table.reload('table-block');
						  },1400);
					  });
				});
				break;
			case 'statuson':
				doState('确定要启用该用户吗？',data);
				break;
			case 'statusoff':
				doState('确定要禁用该用户吗？',data);
				break;
			case 'vipon':
				layer.prompt({title: 'vip设置（单位：月）', formType: 0}, function(month, index){
					  layer.close(index);
					  data['month'] = month;
					  ajaxPost(U('doMemberEvent'),data,'确定要继续吗？',function(){
						  layOk('设置成功');
						  setTimeout(function(){
							  layLoad('数据加载中...');
							  table.reload('table-block');
						  },1400);
					  });
				});
				break;
			case 'vipoff':
				doState('确定要取消该用户vip身份吗？',data);
				break;
			case 'info':
				layPage('用户详情',field.info_url,'90%','80%');
				break;
			default:
				layError('该按钮未绑定事件');
				break;
		}
	});
	
	function doState(asked,data){
		ajaxPost(U('doMemberEvent'),data,asked,function(){
			layOk('操作成功');
			setTimeout(function(){
				layLoad('数据加载中...');
				table.reload('table-block');
			},1400);
		});
	}
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});