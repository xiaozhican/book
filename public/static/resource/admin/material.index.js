layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad();
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '素材图片',minWidth:100,align:'center',templet:function(d){
				var $html = '<font class="text-red">暂无封面</font>';
				if(d.cover){
					$html = '<img src="'+d.cover+'" style="max-width:200px !important;" />';
				}
				return $html;
			}},
		    {field: 'title', title: '素材标题',align:'center',minWidth:200},
		    {title: '操作',align:'center',minWidth:260,templet:function(d){
		    	var $html = '';
		    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
			    	$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
		    	return $html;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.id,event:obj.event};
		var asked = '';
		switch(obj.event){
			case 'delete':
				ajaxPost(U('delMaterial'),data,'确定要将该删除该素材吗？',function(){
					layOk('操作成功');
					setTimeout(function(){
						layLoad('数据加载中...');
						table.reload('table-block');
					},1400);
				});
				break;
		}
	});
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});