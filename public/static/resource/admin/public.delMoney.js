layui.use(['jquery','form'],function(){
    var $ = layui.jquery,
        form = layui.form;
    form.on('submit(dosubmit)',function(obj){
        var data = obj.field;
        ajaxPost('',data,'确定要扣除吗？',function(d){
            layOk('操作成功');
            setTimeout(function(){
                location.href = U('index');
            },1000);
        });
    });
});