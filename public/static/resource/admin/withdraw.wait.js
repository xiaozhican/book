layui.config({
	base : '/static/layadmin/lay_extend/excel/'
}).use(['jquery','layer','table','form','laydate','excel'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form,
	laydate = layui.laydate,
	excel = layui.excel;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '选择',type:'checkbox'},
		    {title: '申请来源',align:'left',minWidth:150,templet:function(d){
		    	var str = '';
		    	str += d.name;
		    	switch(parseInt(d.account_type)){
		    	case 1:
		    		str += '&nbsp;<a class="layui-btn layui-btn-xs layui-btn-danger">VIP</a>';
		    		str += '<br />';
			    	str += '账号备注：'+d.remark;
		    		break;
		    	case 2:
		    		str += '&nbsp;<a class="layui-btn layui-btn-xs layui-btn-primary">代理</a>';
		    		break;
		    	}
		    	return str;
		    }},
		    {title: '充值金额',align:'center',width:140,templet:function(d){
		    	return '¥'+d.charge_money;
		    }},
		    {field: 'charge_num', title: '充值单数',align:'center',width:140},
		    {title: '提现金额',align:'center',width:200,templet:function(d){
		    	var str = '¥'+d.money;
		    	if(d.detail_url){
		    		str += '&nbsp;<a class="layui-btn layui-btn-xs layui-btn-normal" lay-event="detail">明细</a>';
		    	}
		    	return str;
		    }},
		    {field: 'cur_date',title: '账单日期',align:'center',width:140},
		    {title: '提现账号信息',align:'left',width:300,templet:function(d){
		    	var str = '';
		    	str += d.bank_info.bank_user+'<br />';
		    	str += d.bank_info.bank_name+'<br />';
		    	str += d.bank_info.bank_no;
		    	return str;
		    }},
		    {title: '操作',align:'center',width:200,templet:function(d){
		    	str = '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="pass">确认支付</a>';
		    	return str;
		    }}
		]],
		page : true,
		height : 'full-380',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	laydate.render({elem:'#between_time',type:'date',range:'~'});	
	
	$('#doCheckLine').click(function(){
		var checkStatus = table.checkStatus('table-block');
		if(checkStatus.data.length === 0){
			layError('您尚未选择要支付的账单');
			return false;
		}
		var signs = [];
		$.each(checkStatus.data,function(i,item){
			signs.push(item.sign);
		});
		var data = {sign:signs};
		doCheck(data,'确认所选账单已支付吗？');
	});
	
	$('#exportData').click(function(){
		ajaxPost(U('exportData'),{},'',function(d){
			var data = excel.filterExportData(d, [
				'account'
				,'account_remark'
				,'name'
				,'week'
				,'money'
				,'state'
			]);
			data.unshift({account:'账号',account_remark:'账号备注',name:'渠道',week:'提现日期',money:'待支付金额',state:'状态'});
			 var dateObj = new Date();
			 var date = dateObj.getFullYear()+"-" + (dateObj.getMonth()+1) + "-" + dateObj.getDate();
			excel.exportExcel(data,date+'日待支付结算数据.xlsx', 'xlsx');
		});
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {sign:[field.sign]};
		switch(obj.event){
			case 'pass':
				doCheck(data,'确定该账单已支付吗？');
				break;
			case 'detail':
				layPage('提现明细',field.detail_url,'70%','60%');
				break;
		}
	});
	
	function doCheck(data,asked){
		ajaxPost(U('doWithdraw'),data,asked,function(d){
			var str = '已处理成功'+d.num+'条申请记录';
			layOk(str);
			setTimeout(function(){
				layLoad('数据加载中...');
				table.reload('table-block');
			},1400);
		});
	}
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});