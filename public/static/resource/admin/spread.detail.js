layui.use(['jquery','layer','table'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	var firstSecond = 0;
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title:'序号',type:'numbers'},
		    {field: 'create_date', title: '日期',align:'center',minWidth:150},
		    {field: 'view_num', title: '点击量',align:'center',minWidth:100},
		    {field: 'add_num', title: '新增游客',align:'center',minWidth:100},
		    {title: '新增关注',align:'left',width:110,templet:function(d){
		    	var str = d.sub_num;
		    	str += '<br />';
		    	str += '关注率:'+d.sub_ratio;
		    	return str;
		    }},
		    {title: '充值比例',align:'center',width:110,templet:function(d){
		    	var str = d.charge_num+'笔';
		    	str += '<br />';
		    	str += d.charge_ratio;
		    	return str;
		    }},
		    {title: '充值金额',align:'center',minWidth:160,templet:function(d){
		    	return '¥'+d.charge_money;
		    }}
		]],
		page : true,
		limit : 20,
		height : 'full-80',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	doRefresh();
	function doRefresh(){
		if(firstSecond === 0){
			var sec = parseInt($('#showTime').attr('data-sec'));
			if(sec > 0){
				$('#showTime').attr('data-sec',0);
				firstSecond = sec;
			}else{
				firstSecond = 3600;
				table.reload('table-block');
			}
		}
		firstSecond--;
		var min = parseInt(firstSecond/60);
		min = min > 9 ? min : '0'+min;
		var second = (firstSecond%60);
		second = second > 9 ? second : '0'+second;
		var str = min +':'+second;
		$('#showTime').text(str);
		setTimeout(function(){
			doRefresh();
		},999);
	}
});