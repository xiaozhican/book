layui.use(['jquery','layer','table','form','laydate'],function(){
    var $ = layui.jquery,
        layer = layui.layer,
        table = layui.table,
        form = layui.form,
        laydate = layui.laydate;
    layLoad('数据加载中...');
    table.render({
        elem : '#table-block',
        url : window.location.href,
        loading : true,
        cols : [[
            {field: 'date_time', title: '日期',align:'center',width:200},
            {field: 'android', title: 'Android',align:'center',width:200},
            {field: 'ios', title: 'IOS',align:'center',width:200},
            {field: 'h5', title: 'H5',align:'center',width:200},
            {field: 'pc', title: 'pc',align:'center',width:200},
            {field: 'day_activity', title: '日活人数',align:'center',width:200},
            {field: 'day_login', title: '日登人数',align:'center',width:200},

            // {title: '状态',align:'center',width:200,templet:function(d){
            //         var str = '';
            //         switch(parseInt(d.status)){
            //             case 1:
            //                 str += '开启&nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="off">关闭</a>';
            //                 break;
            //             case 2:
            //                 str += '关闭&nbsp;<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="on">开启</a>';
            //                 break;
            //         }
            //         return str;
            //     }},
            // //{field: 'code', title: '代码块',align:'center',minWidth:300},
            // {field: 'create_time', title: '创建时间',align:'center',width:200},
            // {title: '操作',align:'center',width:200,templet:function(d){
            //         var str = '';
            //         str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
            //         str += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
            //         return str;
            //     }}
        ]],
        page : true,
        height : 'full-220',
        response: {statusCode:1},
        id : 'table-block',
        done:function (res) {
            layer.closeAll();
        }
    });

    laydate.render({elem:'#between_time',type:'datetime',range:'~'});

    form.on('submit(table-search)',function(obj){
        var where = obj.field;
        layLoad('数据加载中...');
        table.reload('table-block',{
            where : where,
            page: {curr:1}
        });
    });
});