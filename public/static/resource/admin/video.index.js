layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '封面',minWidth:100,align:'center',templet:function(d){
				var $html = '<font class="text-red">暂无封面</font>';
				if(d.cover){
					$html = '<img src="'+d.cover+'" style="width:50px;" />';
				}
				return $html;
			}},
		    {title: '视频标题',align:'center',minWidth:200,templet:function(d){
		    	var $html = d.name+'&nbsp;'
		    		$html += '<a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="copy" title="点击复制链接"><i class="layui-icon layui-icon-link"></i></a>';
		    	return $html;
		    }},
		    {field: 'free_str', title: '是否收费',align:'center',minWidth:120},
		    {field: 'read_num', title: '播放次数',align:'center',minWidth:120},
		    {title: '状态',align:'center',minWidth:200,templet:function(d){
		    	var $html = d.status_name;
		    	var status = parseInt(d.status);
		    	switch(status){
			    	case 1:
			    		$html += '&nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="off">下架</a>';
			    		break;
			    	case 2:
			    		$html += '&nbsp;<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="on">上架</a>';
			    		break;
		    	}
		    	return $html;
		    }},
		    {title: '操作',align:'center',minWidth:400,templet:function(d){
		    	var $html = '';
		    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="play">播放</a>';
		    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.customer_url+'">客服消息</a>';
		    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
			    	$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
		    	return $html;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	$('body').on('click','#refresh',function(){
		ajaxPost(U('refreshCache'),{},'',function(){
			layOk('更新成功');
		});
	});
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		switch(obj.event){
			case 'on':
				doState({id:field.id,event:obj.event},'确定要上架该视频吗？');
				break;
			case 'off':
				doState({id:field.id,event:obj.event},'确定要下架该视频吗？');
				break;
			case 'delete':
				doState({id:field.id,event:obj.event},'删除后不可恢复，确定要将该删除该视频吗？');
				break;
			case 'copy':
				layPage('复制视频链接【'+field.name+'】',field.copy_url,'800px','500px')
				break;
			case 'play':
				layPage('播放【'+field.name+'】',field.play_url,'800px','500px');
				break;
		}
	});
	
	function doState(data,asked){
		ajaxPost(U('doVideoEvent'),data,asked,function(){
			layOk('操作成功');
			setTimeout(function(){
				layLoad('数据加载中...');
				table.reload('table-block');
			},1400);
		});
	}
	
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});