layui.use(['jquery','layer','form','laydate'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	form = layui.form,
	laydate = layui.laydate;
	laydate = layui.laydate;
	getUserCount();
	laydate.render({elem:'#send_time',type:'datetime'});
	form.on('select(template)',function(data){
		var value = data.value;
		if(value){
			ajaxPost(U('getContentHtml'),{template_id:value},'',function(str){
				if(str){
					$('#templateDetail').html(str);
					$('#templateDetail').parent().removeClass('layui-hide');
				}
			});
		}else{
			$('#templateDetail').html('');
			$('#templateDetail').parent().addClass('layui-hide');
		}
	});
	
	$('body').on('click','.refresh-btn',function(){
		var obj = $(this).parents('.templetBox');
		$('#templateDetail').html('');
		$('#templateDetail').parent().addClass('layui-hide');
		ajaxPost(U('doRefresh'),{},'',function(list){
			var str = '';
			if(list){
				str += '<div class="selectBox">';
				str += '<select name="template_id" lay-filter="template">';
				str += '<option value="">请选择模版</option>';
				$.each(list,function(i,item){
					str += '<option value="'+item.id+'">'+item.name+'</option>';
				});
				str += '</select>';
				str +='</div>'
				str += '<a class="layui-btn layui-btn-normal layui-btn-sm line-refresh-btn refresh-btn">刷新</a>';
				obj.html(str);
				form.render('select');
			}else{
				str += '<div class="selectBox">';
				str += '<input type="hidden" name="template_id" value="" />';
				str += '<span class="text-line">';
				str += '对不起，您还没有模板，请先到公众号后台申请。<br />如已申请，请点击下方按钮刷新<br />';
				str += '<a class="layui-btn layui-btn-normal layui-btn-sm refresh-btn">刷新模版列表</a>';
				str += '</span>';
				str += '</div>';
				obj.html(str);
			}
		});
	});
	
	$('body').on('click','.edit-btn',function(){
		var curobj = $(this).parent('.keyfield');
		var curtext = curobj.find('.keyvalue').text();
		var curcolor = curobj.find('.keyvalue').attr('data-color');
		layer.open({
			  type: 2,
			  title: '配置模版消息',
			  closeBtn: 1,
			  shade: [0],
			  area: ['500px','400px'],
			  offset : '100px',
			  time: 0,
			  anim: 2,
			  scrollbar :false,
			  content: [U('setTemplateValue'),'yes'],
			  btn : ['确定','取消'],
			  yes : function(index,layero){
				  var iframeWin = window[layero.find('iframe')[0]['name']];
				  iframeWin.layui.form.on('submit(pagesubmit)', function(obj){
					  var data = obj.field;
					  curobj.find('.keyvalue').text(data.text);
					  curobj.find('.keyvalue').css('color',data.color);
					  curobj.find('.keyvalue').attr('data-color',data.color);
					  layer.close(index);
		          });  
				  iframeWin.document.getElementById("pagesubmit").click();
			  },
			  success : function(layero,index){
				  var body = layer.getChildFrame('body', index);
				  if(curtext){body.find('textarea[name="text"]').val(curtext);}
				  if(curcolor){
					  body.find('select[name="color"]').val(curcolor);
					  var iframeWin = window[layero.find('iframe')[0]['name']];
					  iframeWin.layui.form.render('select');
				  }
			  }
		});
	});
	
	form.on('radio(is_all)',function(obj){
		var val = parseInt(obj.value);
		if(val === 2){
			$('#whereItem').removeClass('layui-hide');
			getUserCount();
		}else{
			$('#whereItem').addClass('layui-hide');
		}
	});
	form.on('radio(sex)',function(obj){
		getUserCount();
	});
	form.on('radio(is_charge)',function(obj){
		getUserCount();
	});
	form.on('radio(money)',function(obj){
		getUserCount();
	});
	form.on('radio(subscribe_time)',function(obj){
		getUserCount();
	});
	
	$('.get-near-time').click(function(e){
		e.stopPropagation();
		var min = parseInt($(this).attr('data-min'));
		if(min > 0){
			var myDate = new Date();
			var time = myDate.getTime();
			var current = time + min*60*1000;
			var obj = new Date(current);
			var y = obj.getFullYear();
			var m = obj.getMonth()+1;
			var d = obj.getDate();
			var h = obj.getHours();
			var mm = obj.getMinutes();
			var s = obj.getSeconds();
			var str = y + '-'+ m + '-' + d + ' ' + h + ':' + mm + ':' + s;
			$('#send_time').val(str);
		}
	});
	
	function getUserCount(){
		var is_all = $('input[name="is_all"]:checked').val();
		var sex = $('input[name="sex"]:checked').val();
		var is_charge = $('input[name="is_charge"]:checked').val();
		var money = $('input[name="money"]:checked').val();
		var subscribe_time = $('input[name="subscribe_time"]:checked').val();
		if(parseInt(is_all) == 2){
			var data = {
					sex : sex,
					is_charge : is_charge,
					money : money,
					subscribe_time : subscribe_time
		 		};
			ajaxPost(U('getUserCount'),data,'',function(res){
				$('#userCount').val(res.count+'人');
			});
		}
	}
	
	form.on('submit(testsend)',function(obj){
		var field = obj.field;
		if(!field.member_id){
			layError('请输入用户ID');
			return false;
		}
		if(!field.template_id){
			layError('您尚未选择模版消息');
			return false;
		}
		var obj = getTemplateContent();
		if(!obj){
			layError('模版信息尚未补齐');
			return false;
		}
		var data = {
			template_id : field.template_id,
			url : field.url,
			content : obj,
			member_id : field.member_id
		};
		ajaxPost(U('sendTemplate'),data,'确定要测试发送给该用户吗？',function(d){
			layOk('发送成功');
		});
	});
	
	$('#chooseLink').click(function(){
		var obj = $(this).parent().find('input[name="url"]');
		layer.open({
			  type: 2,
			  title: '选择推广链接',
			  closeBtn: 1,
			  shade: [0],
			  area: ['700px','70%'],
			  offset : '100px',
			  time: 0,
			  anim: 2,
			  scrollbar :false,
			  content: [U('Option/selLink'),'yes'],
			  btn : ['确定选择','取消'],
			  yes : function(index,layero){
				  var childWindow = $(layero).find('iframe')[0].contentWindow;
				  var checkStatus = childWindow.layui.table.checkStatus('table-block');
				  if(checkStatus.data.length === 0){
					  layError('您尚未选择链接');
					  return false;
				  }
				  obj.val(checkStatus['data'][0]['url']);
				  layer.close(index);
			  }
		});
	});
	
	function getTemplateContent(){
		var obj = {};
		checkRes = true;
		$('.keyfield').each(function(){
			var ck = $(this).attr('data-key');
			var text = $(this).find('.keyvalue').text();
			if(!text){
				checkRes = false;
				return checkRes;
			}
			var color = $(this).find('.keyvalue').attr('data-color');
			color = color ? color : '#000000';
			obj[ck] = {value:text,color:color};
		});
		if(!checkRes){
			return false;
		}
		return obj;
	}
	
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		if(!data.template_id){
			layError('您尚未选择模版');
			return false;
		}
		var obj = getTemplateContent();
		if(!obj){
			layError('模版信息尚未补齐');
			return false;
		}
		data['content'] = obj;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
			setTimeout(function(){
				location.href = U('template');
			},1000);
		});
	});
});