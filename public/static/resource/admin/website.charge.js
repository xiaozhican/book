layui.use(['jquery','form','table'],function(){
	var $ = layui.jquery,
	form = layui.form,
	table = layui.table;
	var len = $('.table-input-line').length;
	len = len ? (len-1) : 0;
	$('#addInput').click(function(){
		len++;
		var html = '';
		html += '<tr class="table-input-line">';
		html += '<td><div class="table-input-div"><input type="text" value="0" class="layui-input" name="money['+len+']"/><span class="value-unit">元</span></div></td>';
		html += '<td><div class="table-input-div"><input type="text" value="0" class="layui-input" name="coin['+len+']"/><span class="value-unit">书币</span></div></td>';
		html += '<td><div class="table-input-div"><input type="text" value="0" class="layui-input" name="reward['+len+']"/><span class="value-unit">书币</span></div></td>';
		html += '<td><select class="layui-select" name="is_hot['+len+']"><option value="0">请选择</option><option value="1">是</option><option value="2">否</option></select></td>';
		html += '<td><select class="layui-select" name="package['+len+']"><option value="0">请选择</option><option value="1">包日</option><option value="2">包月</option><option value="3">包季度</option><option value="4">包半年</option><option value="5">包整年</option><option value="6">包终身</option></select></td>';
		html += '<td><select class="layui-select" name="is_on['+len+']"><option value="0">请选择</option><option value="1">是</option><option value="2">否</option></select></td>';
		html += '<td><select class="layui-select" name="is_checked['+len+']"><option value="0">请选择</option><option value="1">是</option><option value="2">否</option></select></td>';
		html += '<td align="center"><a class="layui-btn layui-btn-danger layui-btn-xs deleteline">删除</a></td>';
		html += '</tr>';
		$('#configBody').append(html);
		form.render('select');
	});
	$('body').on('click','.deleteline',function(){
		$(this).parents('tr').remove();
	});
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
		});
	});
});