layui.use(['jquery','form'],function(){
	var $ = layui.jquery,
		form = layui.form;
	changeLocation($is_location);
	form.on('radio(is_location)',function(obj){
		var val = parseInt(obj.value);
		changeLocation(val);
	});
	function changeLocation(value){
		if(value === 1){
			$('#locationUrlBox').removeClass('layui-hide');
		}else{
			$('#locationUrlBox').addClass('layui-hide');
		}
	}
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
			$("#SiteName",window.parent.document).html(data.name);
		});
	});
});