layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'title', title: '标题',align:'center',minWidth:120},
			{field: 'desc', title: '描述',align:'center',minWidth:200},
			{title: '链接地址',align:'center',minWidth:200,templet:function (d) {
					var url = d.url;
					return '<a href="'+url+'" target="_blank" style="color: #337ab7">'+url+'</a>';
			}},
			{field: 'sort', title: '排序',align:'center',minWidth:200},
			{title: '操作',align:'center',minWidth:320,templet:function(d){
		    	var $html = '';
		    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
			    	$html += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
		    	return $html;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.id,event:obj.event};
		var asked = '';
		switch(obj.event){
			case 'on':
				asked = '确定要启用该用户吗？';
				break;
			case 'off':
				asked = '确定要禁用该用户吗？';
				break;
			case 'delete':
				asked = '确定要将该删除该用户吗？';
				break;
		}
		if(asked){
			ajaxPost(U('doLinksEvent'),data,asked,function(){
				layOk('操作成功');
				if(obj.event !== 'resetpwd'){
					setTimeout(function(){
						layLoad('数据加载中...');
						table.reload('table-block');
					},1400);
				}
			});
		}else{
			layError('该按钮未绑定事件');
		}
	});
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});