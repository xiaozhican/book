layui.use(['layer','table'],function(){
	var layer = layui.layer,
	table = layui.table;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'name', title: '模版名称',align:'center',minWidth:120},
		    {field: 'summary', title: '模版描述',align:'center',minWidth:300},
		    {title: '操作',align:'center',minWidth:180,templet:function(d){
		    	var $html = '';
		    		$html += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">更新</a>';
		    	return $html;
		    }}
		]],
		page : false,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
});