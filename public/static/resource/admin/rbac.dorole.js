layui.config({
	base : '/static/layadmin/lay_extend/tree/'
}).use(['jquery','form','eleTree'],function(){
	var $ = layui.jquery, 
	form = layui.form,
	eleTree = layui.eleTree;
	
	eleTree.render({
        elem: '.treelist',
        url: $nodeUrl,
        type: "post",
        where : {role_id:$role_id},
        showCheckbox: true
    });
	
	form.on('submit(dosubmit)',function(obj){
		var data = obj.field;
		var checkList = eleTree.checkedData('.treelist');
		var content = [];
		if(checkList.length > 0){
		  $.each(checkList,function(i,item){
			  content.push(item.id);
		  });
	  	} 
		data['content'] = content;
		ajaxPost('',data,'确定要保存吗？',function(d){
			layOk('保存成功');
			setTimeout(function(){
				location.href = $backUrl;
			},1000);
		});
	});
});