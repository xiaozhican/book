layui.use(['jquery','element'],function(){
	var $ = layui.jquery,
	element = layui.element;
	var num = 1;
	$('#doChange').click(function(){
		getOnlyNum();
		var name = 'qrcode_pic_'+num+'.png';
		var filename = $path+name;
		$('#showImg').attr('src',filename);
		$('#picdownload').attr('download',name);
		$('#picdownload').attr('href',filename);
	});
	
	function getOnlyNum(){
		var randnum = getRandomNum(1,5);
		if(num == randnum){
			getOnlyNum();
		}else{
			num = randnum;
		}
	}
});