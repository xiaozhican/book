layui.use(['jquery','layer','table','form','laydate'],function(){
    var $ = layui.jquery,
        layer = layui.layer,
        table = layui.table,
        form = layui.form,
        laydate = layui.laydate;
    layLoad('数据加载中...');
    table.render({
        elem : '#table-block',
        url : window.location.href,
        loading : true,
        cols : [[
            {field: 'app_name', title: '应用名称',align:'center',width:200},
            {field: 'app_id', title: '应用名称ID',align:'center',width:200},
            {field: 'notice_name', title: '广告名称',align:'center',width:300},
            {field: 'notice_id', title: '广告(代码位)名称ID',align:'center',width:200},
            {field: 'notice_type', title: '广告类型',align:'center',width:200},
            {field: 'advertisers', title: '广告商',align:'center',width:200},
            {title: '安卓默认状态或IOS状态',align:'center',width:200,templet:function(d){
                    var str = '';
                    switch(parseInt(d.status)){
                        case 1:
                            str += '开启&nbsp;<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="off">关闭</a>';
                            break;
                        case 2:
                            str += '关闭&nbsp;<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="on">开启</a>';
                            break;
                    }
                    return str;
                }},
            //{field: 'code', title: '代码块',align:'center',minWidth:300},
            {field: 'create_time', title: '创建时间',align:'center',width:200},
            {title: '操作',align:'center',width:200,templet:function(d){
                    var str = '';
                    str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.do_url+'">编辑</a>';
                    str += '<a class="layui-btn layui-btn-danger layui-btn-xs" lay-event="delete">删除</a>';
                    return str;
                }}
        ]],
        page : true,
        height : 'full-220',
        response: {statusCode:1},
        id : 'table-block',
        done:function (res) {
            layer.closeAll();
        }
    });
    table.on('tool(table-block)',function(obj){
        var field = obj.data;
        switch(obj.event){
            case 'on':
                doState({id:field.id,event:obj.event},'确定要启用该广告吗？');
                break;
            case 'off':
                doState({id:field.id,event:obj.event},'确定要关闭该广告吗？');
                break;
            case 'delete':
                doState({id:field.id,event:obj.event},'确定要将该广告删除吗？');
                break;
        }
    });
    function doState(data,asked){
        ajaxPost(U('doNoticeEvent'),data,asked,function(){
            layOk('操作成功');
            setTimeout(function(){
                layLoad('数据加载中...');
                table.reload('table-block');
            },1400);
        });
    }

    laydate.render({elem:'#between_time',type:'datetime',range:'~'});

    form.on('submit(table-search)',function(obj){
        var where = obj.field;
        layLoad('数据加载中...');
        table.reload('table-block',{
            where : where,
            page: {curr:1}
        });
    });
});