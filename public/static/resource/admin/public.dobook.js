layui.use(['jquery','form'],function(){
		var $ = layui.jquery, 
		form = layui.form;
		changeFreeType($free_type);
		form.on('radio(free_type)',function(obj){
			var value = parseInt(obj.value);
			changeFreeType(value);
		});
		form.on('submit(dosubmit)',function(obj){
			var data = obj.field;
			ajaxPost('',data,'确定要保存吗？',function(d){
				layOk('保存成功');
				setTimeout(function(){
					location.href = U('index');
				},1000);
			});
		});
		function changeFreeType(value){
			if(value == 2){
				$('.free-input-line').removeClass('layui-hide')
			}else{
				$('.free-input-line').addClass('layui-hide')
			}
		}
	});