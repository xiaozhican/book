var countdown = 0;
//获取验证码
function getCode(){
	var code = $('#code').val();
	var reg = /^[\d]{4}$/;
	if(!reg.test(code)){
		    return false;
	    }
	return code;
}

//获取手机号
function getPhone(){
	var phone = $('#phone').val();
	var reg = /^1[3456789]\d{9}$/;
    if(!reg.test(phone)){
	    countdown = 0;
	    return false;
    }
    return phone;
}

function sendCode(obj){
	if(countdown !== 0){
		return false;
	}
	countdown = 120
	var phone = getPhone();
	if(!phone){
		layMsg('手机号格式有误,请确认');
		return false;
	}
   	  $.ajax({
   		  type : 'post',
   		  url : U('Com/sendMessageCode'),
   		  data : {phone:phone},
   		  dataType : 'json',
   		  success : function(res){
   			  if(res.code == 1){
   					settime(obj);
   			  }else{
   				  countdown = 0;
   				  layMsg(res.msg);
   			  }
   		  },
   		  error : function(){
   			  countdown = 0;
   			  layMsg('网络错误,请稍后再试');
   		  }
   	  });
}

function settime(obj) { 
	  if (countdown == 0) { 
		  $(obj).text("重新获取"); 
		  return;
	  } else { 
		  $(obj).text(countdown + "s"); 
		  countdown--; 
	  } 
	  setTimeout(function() { 
	  		settime(obj); 
	  },1000) 
}



//绑定手机提交
function doBindPhone(){
	var phone = getPhone();
	if(!phone){
		layMsg('请输入正确格式的手机号');
		return false;
	}
	var code = getCode();
	if(!code){
		layMsg('请输入正确格式的验证码');
		return false;
	}
	var pwd = $('#password').val();
	if(pwd.length === 0){
		layMsg('请输入密码');
		return false;
	}
	var data = {phone:phone,code:code,password:pwd};
	ajaxPost('',data,function(d){
		var str = '';
		str += '<div class="succ_popup popup" style="display:flex;">';
		str += '<div class="main">';
		str += '<img src="/static/resource/index/images/success_icon.png">';
		str += '<p class="font_status">提交成功</p>';
		str += '<a href="/index/user/index.html" class="popup_btn">返回个人中心</a>';
		str += '</div>';
		str += '</div>';
        $('body').append(str);
	});
}

function doRegPhone(){
	var phone = getPhone();
	if(!phone){
		layMsg('请输入正确格式的手机号');
		return false;
	}
	var code = getCode();
	if(!code){
		layMsg('请输入正确格式的验证码');
		return false;
	}
	var new_pwd = $('#new_pwd').val();
	if(new_pwd.length < 6 || new_pwd.length > 16){
		layMsg('请输入6-16位登录密码');
		return false;
	}
	var re_pwd = $('#re_pwd').val();
	if(new_pwd != re_pwd){
		layMsg('两次输入密码不一致');
		return false;
	}
	var data = {phone:phone,code:code,password:new_pwd};
	ajaxPost('',data,function(d){
		window.location.href = d.url;
	});
}

function doPhoneLogin(){
	var phone = getPhone();
	if(!phone){
		layMsg('请输入正确格式的手机号');
		return false;
	}
	var new_pwd = $('#password').val();
	if(new_pwd.length < 6 || new_pwd.length > 16){
		layMsg('请输入6-16位登录密码');
		return false;
	}
	var data = {phone:phone,password:new_pwd};
	ajaxPost('',data,function(d){
		window.location.href = d.url;
	});
}

//绑定手机提交
function doCodePhone(){
	var phone = getPhone();
	if(!phone){
		layMsg('请输入正确格式的手机号');
		return false;
	}
	var code = getCode();
	if(!code){
		layMsg('请输入正确格式的验证码');
		return false;
	}
	var data = {phone:phone,code:code};
	ajaxPost('',data,function(d){
		switch(d.type){
		case 1:
			var str = '';
			str += '<div class="succ_popup popup" style="display:flex;">';
			str += '<div class="main">';
			str += '<img src="/static/resource/index/images/success_icon.png">';
			str += '<p class="font_status">提交成功</p>';
			str += '<a href="/index/user/index.html" class="popup_btn">返回个人中心</a>';
			str += '</div>';
			str += '</div>';
	        $('body').append(str);
			break;
		case 2:
			window.location.href = d.url;
			break;
		}
	});
}