$(function () {
	var page = 1,is_end=false;
	getData();
    //获取搜索数据
    function getData(){
    	if(!is_end){
    		is_end = true;
    		var where = getWhere();
    		ajaxPost('',where,function($list){
    			if($list){
    				$('.column_content').parent().removeClass('hide');
    				$.each($list,function(i,item){
						var str = '';
						str += '<a href="'+item.info_url+'" class="item">';
						str += '<img class="cover" src="'+item.cover+'">';
						str += '<div class="right">';
						str += '<p class="name">'+item.name+'</p>';
						str += '<p class="main">'+item.summary+'</p>';
						if(item.category){
							str += '<p class="article_type">';
							for(var i=0;i<item.category.length;i++){
								str += '<span>'+item.category[i]+'</span>';
							}
							str += '</p>';
						}
						str += '</div>';
						str += '</a>';
						$('.column_content').append(str);
					});
    				page++;
    				is_end = false;
    			}else{
    				if(page === 1){
    					
    				}
    			}
    			console.log(is_end);
    		});
    	}
    }
    
    //获取条件
    function getWhere(){
    	var data = {};
    	var List = ['gender_type','category','orderby'];
    	for(var i=0;i<List.length;i++){
    		var key = List[i];
    		data[key] = $('input[name="'+key+'"]:checked').val(); 
    	}
    	data['page'] = page;
    	data['type'] = $('input[name="type"]').val();
    	return data;
    }
    
    $('input[type="radio"]').click(function(){
    	$('.column_content').html('');
    	page = 1;
    	is_end = false;
    	getData();
    });
    
    $(window).scroll(function() {
   		var scrollTop = $(this).scrollTop();
   		var scrollHeight = $(document).height();
   		var windowHeight = $(this).height();
   		if (scrollTop + windowHeight == scrollHeight) {
   			if(!is_end){
   	   			getData();
   			}
   		}
    });
});