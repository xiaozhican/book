//构建url
function U($uri){
	var arr = $uri.split('/');
	var len = arr.length;
	var url = '';
	var urlPath = window.location.pathname;
	var tmps = urlPath.split('/');
	switch(len){
		case 3:
			url = '/'+arr[0]+'/'+arr[1]+'/'+arr[2]+'.html';
			break;
		case 2:
			url = '/'+tmps[1]+'/'+arr[0]+'/'+arr[1]+'.html';
			break;
		case 1:
			url = '/'+tmps[1]+'/'+tmps[2]+'/'+$uri+'.html';
			break;
	}
	return url;
}

//消息弹出层
function layMsg(msg){
	layer.open({
	    content: msg,
	    skin: 'msg',
	    time: 2
	 });
}

//ajax提交
function ajaxPost(url,data,callback,is_load){
	is_load = is_load !== true ? false : true;
	var loadindex;
	$.ajax({
		type : 'post',
		url : url,
		data : data,
		dataType : 'json',
		beforeSend : function(){
			if(is_load){
				loadindex = layer.open({type: 2,shade : 'background-color: rgba(0,0,0,.5)',shadeClose:false});
			}
		},
		success : function(res){
			switch(res.code){
			case 1:
				callback(res.data);
				break;
			case 2:
				layMsg(res.msg);
				break;
			case 3:
				layer.open({
					content: res.msg,
					btn: ['确定', '取消'],
					yes: function(index){
						layer.close(index);
						var location_url = res.data ? res.data.url : '/index/Login/index';
						window.location.href = location_url;
					}
				});
				break;
			case 4:
				layer.open({
			        content: res.msg
			        , shadeClose: false
			        ,className :'chaseLayer'
			        ,style: 'background-color:#fff;color:#fff; border:none;border-radius:20px;width:80%;padding:0;margin:0;'
			    });
				break;
			}
		},
		complete: function(){
			if(is_load){
				layer.close(loadindex);
			}
		},
		error : function(){
			if(is_load){
				layer.close(loadindex);
			}
		}
	});
}

function doWeixinShare(share_info){
	$.getScript('http://res.wx.qq.com/open/js/jweixin-1.4.0.js',function(){
		wx.config({
			  debug: false,
			  appId: share_info.config.appid,
			  timestamp: share_info.config.timestamp,
			  nonceStr: share_info.config.noncestr,
			  signature: share_info.config.signature,
			  jsApiList: ['updateAppMessageShareData','updateTimelineShareData']
		});
		wx.ready(function(){
			wx.updateAppMessageShareData({
				title: share_info.info.title,
				desc: share_info.info.desc,
				link: share_info.info.url,
				imgUrl: share_info.info.img
			});
		   wx.updateTimelineShareData({
			   title: share_info.info.title,
			   link: share_info.info.url,
			   imgUrl: share_info.info.img
			});
		});
	});
}

function checkBookChapter(book_id,number){
	ajaxPost('/index/Book/checkChapter',{book_id:book_id,number:number},function(res){
		window.location.href = res.url;
	});
}

function doZan(pid,obj){
	ajaxPost('/index/Comments/doZan',{pid:pid},function(){
		$(obj).find('.zan_icon').addClass('active');
		$(obj).removeAttr('onclick');
		var num = Number($(obj).find('.number').text());
		num += 1;
		$(obj).find('.number').text(num);
		
	});
}

function doActivityPay($id){
	ajaxPost('/index/Charge/doActivityCharge',{activity_id:$id},function(res){
		window.location.href = res.url;
	});
}

function doComplaint(type,book_id){
	ajaxPost('',{book_id:book_id,type:type},function(){
		layer.open({
            content: $('.report-proup').html()
            , shadeClose: false
            ,className :'layerPadding'
            ,style: 'background-color:#fff; color:#fff; border:none; width:80%'
        });
    })
}

