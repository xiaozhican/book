setRecord();
function doSearch(keyword){
	if(!keyword){
		keyword = $('#search_value').val();
	}else{
		$('#search_value').val(keyword);
	}
	if(keyword.length == 0){
		layMsg('请输入搜索关键字');
		return false;
	}
	var search_list = $.cookie('search_record');
	if(search_list){
		var searchArr = search_list.split(',');
		var index = $.inArray(keyword, searchArr);
		console.log(index);
		if(!(index >= 0)){
			search_list += ','+keyword;
		}
	}else{
		search_list = keyword;
	}
	$.cookie('search_record',search_list);
	$('.recommend_search').hide();
	ajaxPost('',{keyword:keyword},function($list){
		var num = 0;
		if($list){
			var str = '';
			$('.book-content').removeClass('hide');
			$('.book-content .box .column_content').html('');
			$.each($list,function(i,item){
				str += '<a href="'+item.info_url+'" class="item">';
				str += '<img class="cover" src="'+item.cover+'">';
				str += '<div class="right">';
				str += '<p class="name">'+item.name+'</p>';
				str += '<p class="main">'+item.summary+'</p>';
				if(item.category){
					str += '<p class="article_type">';
					for(var i=0;i<item.category.length;i++){
						str += '<span>'+item.category[i]+'</span>';
					}
					str += '</p>';
				}
				str += '</div>';
				str += '</a>';
			});
			$('.book-content .box .column_content').html(str);
		}else{
			$('.none').removeClass('hide');
		}
	});
}

function setRecord(){
	var search_list = $.cookie('search_record');
	if(search_list && search_list.length > 0){
		var searchArr = search_list.split(',');
		var str = '';
		str += '<div class="box search_list_box">';
		str += '<div class="title">';
		str += '<img class="icon" src="/static/resource/index/images/hot.png">';
		str += '<span>搜索历史</span>';
		str += '<img class="icon" src="/static/resource/index/images/delete.png" onclick="javascript:delAll(this);">';
		str += '</div>';
		str += '<p class="content search-tag">';
		$.each(searchArr,function(i,item){
			str += '<span onclick="javascript:doSearch(\''+item+'\');">'+item+'</span>';
		});
		str += '</p>';
		str += '</div>';
		$('.recommend_search').append(str);
	}
}

function delAll(obj){
	$.cookie('search_record','');
	$('.search_list_box').remove();
}