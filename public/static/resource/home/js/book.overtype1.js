var list_page = 1;
var param = getQueryParam();
makeParam(param);
//var is_free_page = IsFreePage();
getData();
function makeParam(data){
    for(key in data){
        var dom_class = '.key_'+key;
        if($(dom_class)){
            $(dom_class).find('.span_item').removeClass('blue');
            $(dom_class).find('.span_item[data-val="'+data[key]+'"]').addClass('blue');
        }
    }
}

function doChooseType(obj,type){
    $('.category_item').hide();
    $(obj).addClass('blue').siblings().removeClass('blue');
    $('category_item,.type'+type).show();
    param['type'] = type;
    $('.type'+type).find('.span_item').removeClass('blue');
    $('.type'+type).find('.span_item:eq(0)').addClass('blue');
    if('category' in param){
        delete param['category'];
    }
    clearPage();
    getData();
}

function doChooseItem(obj,key,val){
    if(val){
        param[key] = val;
    }else{
        if(key in param){
            delete param[key];
        }
    }
    $(obj).addClass('blue').siblings().removeClass('blue');
    $(obj).addClass('blue');
    clearPage();
    getData();
}

function getData(){
    param['page'] = list_page;
    ajaxPost('',param,function(res){
        createHtml(res.list);
        if(list_page === 1 && res.count > 1){
            myPageInit({
                pages: res.count,
                currentPage: 1,
                element: '.my-page',
                callback: function(page) {
                    if(parseInt(page) !== list_page){
                        list_page = parseInt(page);
                        getData();
                    }
                }
            });
        }
    });
}

function clearPage(){
    list_page = 1;
    $('.my-page').html('');
}

function doCollect(book_id,event,obj){
    if (checkLogin()) {
        ajaxPost('/home/Com/doCollect', {book_id: book_id, event: event}, function () {
            switch (event) {
                case 'join':
                    $(obj).attr('onclick', 'javascript:doCollect(' + book_id + ',\'remove\',this);');
                    $(obj).text('移除书架');
                    break;
                case 'remove':
                    $(obj).attr('onclick', 'javascript:doCollect(' + book_id + ',\'join\',this);');
                    $(obj).text('加入书架');
                    break;
            }
        });
    }else {
        showBox(1);
    }
}

function createHtml(list){
    var str = '';
    if(list){
        $.each(list,function(i,item){
            str += '<div class="book">';
            str += '<a href="/home/book/info.html?book_id='+item.id+'"><img class="cover fl" src="'+item.cover+'"></a>';
            str += '<div class="info fr">';
            str += '<p class="name"><a href="/home/book/info.html?book_id='+item.id+'">'+item.name+'</a></p>';
            str += '<p class="author">作者：'+item.author+'</p>';
            str += '<p class="author">最新章节：'+item.best_new_chapter+'</p>';
            if (item.type == 1){
                str += '<p class="author">字数：'+item.word_number+'</p>';
            }
            str += '<p class="author">更新时间：'+item.edit_time+'</p>';
            str += '<p class="author">状态：'+item.over_type+'</p>';
            str += '<p class="introduce">'+item.summary+'</p>';
            str += '<div class="bottom_bar">';
            if(item.category.length > 0){
                for(var i=0;i<item.category.length;i++){
                    str += '<div class="fl type">'+item.category[i]+'</div>';
                }
            }
            if(parseInt(item.is_collect) == 2){
                str += '<a href="javascript:void(0);" class="join fr" onclick="javascrpt:doCollect('+item.id+',\'join\',this)">加入书架</a>';
            }else{
                str += '<a href="javascript:void(0);" class="join fr" onclick="javascrpt:doCollect('+item.id+',\'remove\',this)">移除书架</a>';
            }
            str += '<a href="/home/book/info.html?book_id='+item.id+'" class="read fr">立即阅读</a>';
            str += '</div>';
            str += '</div>';
            str += '</div>';
        });
    }
    $('.showBox').html(str);
}

//检测是否限免页面
function IsFreePage(){
    var res = false;
    var name = window.location.pathname;
    str = name.toLowerCase();
    if(str.indexOf("free") != -1){
        res = true;
    }
    return res;
}