var dotoken = '',pwd_time = 0;
	function doCheckPhone(){
		$('.right').hide();
		$('#phoneBox').show();
	}
	
	//检查原密码
	function doCheckOldPwd(){
		var password = $('input[name="user_old_pwd"]').val();
		if(password.length == 0){
			layer.msg('请输入原密码');
			return false;
		}
		if(password.length < 6){
			layer.msg('原密码至少6位');
			return false;
		}
		ajaxPost('/home/user/checkOldPwd',{password:password},function(res){
			$('.right').hide();
			dotoken = res.token;
			$('#setPwdBox').show();
		});
	}
	
	function doCheckBindPhone(){
		var phone = $('input[name="user_pwd_phone"]').val();
		if(!/^1[3456789]\d{9}$/.test(phone)){
			layer.msg('手机号格式不正确');
			pwd_time = 0
			return false;
		}
		var code = $('input[name="user_pwd_code"]').val();
		if(!/^\d{4}$/.test(code)){
			layer.msg('验证码格式不正确');
			pwd_time = 0
			return false;
		}
		ajaxPost('/home/user/checkPwdPhone',{phone:phone,code:code},function(res){
			$('.right').hide();
			dotoken = res.token;
			$('#setPwdBox').show();
		});
	}
	
	function sendPwdCode(obj){
		if(pwd_time !== 0){
			return false;
		}
		pwd_time = 120
		var phone = $('input[name="user_pwd_phone"]').val();
		if(!/^1[3456789]\d{9}$/.test(phone)){
			layer.msg('手机号格式不正确');
			pwd_time = 0
			return false;
		}
		$.ajax({
			type : 'post',
			url : '/home/Com/sendCode',
			data : {phone:phone,type:3},
			dataType : 'json',
			success : function(res){
				if(res.code == 1){
					setPwdTime(obj);
				}else{
					pwd_time = 0;
					layer.msg(res.msg);
				}
			},
			error : function(){
				pwd_time = 0;
				layer.msg('网络错误,请稍后再试');
			}
		});
	}

	function setPwdTime(obj){
		if (pwd_time == 0) { 
			  $(obj).text("重新获取"); 
			  return;
		  } else { 
			  $(obj).text(pwd_time + "s"); 
			  pwd_time--; 
		  } 
		  setTimeout(function() { 
			  setPwdTime(obj); 
		  },1000); 
	}
	
	function doSetNewPwd(){
		if(dotoken.length === 0){
			layer.msg('非法操作');
			return false;
		}
		var pwd = $('input[name="user_new_pwd"]').val();
		var re_pwd = $('input[name="user_re_pwd"]').val();
		if(pwd.length == 0){
			layer.msg('请输入原密码');
			return false;
		}
		if(pwd.length < 6){
			layer.msg('原密码至少6位');
			return false;
		}
		if(pwd !== re_pwd){
			layer.msg('两次输入密码不一致');
			return false;
		}
		ajaxPost('/home/user/doSetPwd',{token:dotoken,pwd:pwd,re_pwd:re_pwd},function(res){
			$('.right').hide();
			dotoken = '';
			$('#sucBox').show();
		});
	}
	
	function showPhoneBox(){
		$('.right').hide();
		$('#phoneBox').show();
	}