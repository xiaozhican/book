var reward_key = '',reward_money = 0,reward_number=1,comment_cate = 1,comment_page = 1;
function doCollect(book_id,obj){
	if(checkLogin()){
		var thisKey = $(obj).attr('data-key');
		var nextkey = thisKey === 'join' ? 'remove' : 'join';
		var nexttext = thisKey === 'join' ? '已在书架' : '加入书架';
		ajaxPost('/home/Com/doCollect',{book_id:book_id,event:thisKey},function(){
			$(obj).attr('data-key',nextkey);
			$(obj).attr('onclick','javascript:doCollect('+book_id+',this);');
			$(obj).text(nexttext);
		});
	}else{
		showBox(1);
	}
}
function showReward(gift_key,name,src,money,obj){
	if(!checkLogin()){
		showBox(1);
	}else{
		reward_key = gift_key;
		reward_money = parseInt(money);
		reward_number = 1;
		$('#number').text(reward_number);
		$('#reward_popup').find('.gift_icon').attr('src',src);
		$('#reward_popup').find('.gift_name').text(name);
		$('#reward_popup').find('.gift_number').text(money+'书币');
		$('#reward_popup').find('.total_money').text(money);
		$('#reward_popup').show();
	}

}
function hideReward(){
	$('#reward_popup').hide();
}

function doGiftNumber(obj,flag){
	switch(flag){
		case 'inc':
			reward_number ++;
			break;
		case 'dec':
			if(reward_number == 1){
				return false;
			}
			reward_number --;
			break;
	}
	total_money = reward_money * reward_number;
	$('#number').text(reward_number);
	$('#reward_popup').find('.total_money').text(total_money);
}

function doRewardPay(book_id){
	if(checkLogin()){
		if(!reward_key){
			layer.msg('您尚未选中礼物');
			return false;
		}
		totalMoney = reward_money * reward_number;
		user_money = Number($('#user_money').text());

		if(user_money < totalMoney){
			layer.msg('您的书币余额不足');
			return false;
		}
		var data = {pid:book_id,gift_key:reward_key,count:reward_number,money:totalMoney};
		ajaxPost('/home/Com/doReward',data,function(e){
			var last_money = user_money - totalMoney;
			$('#user_money').text(last_money);

			var old_fen = parseInt($(".fen").html());
			$(".fen").html(old_fen + totalMoney);

			$('#reward_popup').hide();
			$("#reward_suc").show()
		});
	}else{
		showBox(1);
	}
}

function hideSuc(){
	$("#reward_suc").hide();
}

function checkCate(obj,val){
	if($(obj).hasClass('active')){
		return false;
	}
	$(obj).addClass('active').siblings().removeClass('active');
	comment_cate = val;
	comment_page = 1
	getData();
}

function getData(){
	var data = {cate:comment_cate,page:comment_page,book_id:book_id};
	ajaxPost('/home/Com/getCommentsList',data,function(res){
		createHtml(res.list);
		if(comment_page === 1 && res.count > 1){
			myPageInit({
				pages: res.count,
				currentPage: 1,
				element: '.my-page',
				callback: function(page) {
					if(parseInt(page) !== comment_page){
						comment_page = parseInt(page);
						getData();
					}
				}
			});
		}
	});
}

function createHtml(list){
	var str = '';
	if(list){
		$.each(list,function(i,item){
			str += '<li class="rate_item">';
			str += '<img class="headimg fl" src="'+item.headimgurl+'">';
			str += '<div class="right fr">';
			str += '<p class="name">'+item.nickname+'</p>';
			str += '<p class="main">'+item.content+'</p>';
			str += '<div class="bottom_bar">';
			str += '<p class="time fl">'+item.create_time+'</p>';
			str += '<div class="fabulous fr">';
			str += '<span class="fabulous_num fr">'+item.zan_num+'</span>';
			str += '<input class="fabulous_input" type="checkbox" id="1" hidden ';
			if(parseInt(item.is_zan) == 1){
				str += 'checked="checked"';
			}
			str +=  '>';
			str += '<label class="fabulous_img fr" for="1" onclick="javascript:doZan(this,'+item.id+');"></label>';
			str += '</div>';
			str += '</div>';
			str += '</div>';
			str += '</li>';
		});
	}
	$('.listbox').html(str);
}

function doZan(obj,pid){
	ajaxPost('/home/Com/doZan',{pid:pid},function(){
		var num = parseInt($(obj).parent().find('.fabulous_num').text());
		num += 1;
		$(obj).parent().find('.fabulous_num').text(num);
		$(obj).parent().find('input').prop('checked',true);
	});
}

function clearContent(){
	$('#content').val('');
}

function doComments(){
	if(!checkLogin()){
		showBox(1);
	}else{
		var content = $('#content').val();
		if(content.length == 0){
			layer.msg('请输入评论内容');
			return false;
		}
		var data = {pid:book_id,content:content};
		ajaxPost('/home/Com/doComments',data,function(){
			layer.msg('提交成功,评论审核中');
			clearContent();
		});
	}
}