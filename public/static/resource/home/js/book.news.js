var list_page = 1;
var param = getQueryParam();
makeParam(param);
var is_free_page = IsFreePage();
getData();
function makeParam(data){
	for(key in data){
		var dom_class = '.key_'+key;
		if($(dom_class)){
			$(dom_class).find('.span_item').removeClass('blue');
			$(dom_class).find('.span_item[data-val="'+data[key]+'"]').addClass('blue');
		}
	}
}

function doChooseType(obj,type){
	$('.category_item').hide();
	$(obj).addClass('blue').siblings().removeClass('blue');
	$('category_item,.type'+type).show();
	param['type'] = type;
	$('.type'+type).find('.span_item').removeClass('blue');
	$('.type'+type).find('.span_item:eq(0)').addClass('blue');
	if('category' in param){
		delete param['category'];
	}
	clearPage();
	getData();
}

function doChooseItem(obj,key,val){
	if(val){
		param[key] = val;
	}else{
		if(key in param){
			delete param[key];
		}
	}
	$(obj).addClass('blue').siblings().removeClass('blue');
	$(obj).addClass('blue');
	clearPage();
	getData();
}

function getData(){
	param['page'] = list_page;
	ajaxPost('',param,function(res){
		createHtml(res.list);
		if(list_page === 1 && res.count > 1){
			myPageInit({
			    pages: res.count,
			    currentPage: 1,
			    element: '.my-page',
			    callback: function(page) {
			    	if(parseInt(page) !== list_page){
			    		list_page = parseInt(page);
			    		getData();
			    	}
			    }
			});
		} 
	});
}

function clearPage(){
	list_page = 1;
	$('.my-page').html('');
}

function doCollect(book_id,event,obj){
	ajaxPost('/home/Com/doCollect',{book_id:book_id,event:event},function(){
		switch(event){
		case 'join':
			$(obj).attr('onclick','javascript:doCollect('+book_id+',\'remove\',this);');
			$(obj).text('移除书架');
			break;
		case 'remove':
			$(obj).attr('onclick','javascript:doCollect('+book_id+',\'join\',this);');
			$(obj).text('加入书架');
			break;
		}
	});
}

function createHtml(list){
	var str = '';
	if(list){
		$.each(list,function(i,item){
			str += '<div class="book" style="padding: 15px 0">';
			str += '<div class="info fr" style="width: 1050px">';
			str += '<div class="bottom_bar" style="margin-top: 0">';
			str += '<div class="fl" style="line-height: 32px;"><a target="_blank" href="/home/book/newsContent?new_id='+item.id+'" style="color: #000000;">'+item.title+'</a></div>';
			str += '<a class="fr" style="line-height: 32px;margin-right: 50px">'+item.create_time+'</a>';
			str += '</div>';
			str += '</div>';
			str += '</div>';
		});
	}
	$('.showBox').html(str);
}

//检测是否限免页面
function IsFreePage(){
	var res = false;
	var name = window.location.pathname;
	str = name.toLowerCase();
	if(str.indexOf("free") != -1){
		res = true;
	}
	return res;
}