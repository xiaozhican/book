
/**
 * 获取get参数
 * @param variable
 * @returns
 */
function getQueryParam(variable){
	var param = {};
    var query = window.location.search.substring(1);
    var vars = query.split("&");
    for (var i=0;i<vars.length;i++) {
       var pair = vars[i].split("=");
       if(pair.length === 2){
    	   param[pair[0]] = decodeURI(pair[1]);
       }
    }
   return param;
}

//ajax提交
function ajaxPost(url,data,callback,is_load){
	is_load = is_load !== true ? false : true;
	var loadindex;
	$.ajax({
		type : 'post',
		url : url,
		data : data,
		dataType : 'json',
		beforeSend : function(){
			if(is_load){
				loadindex = layer.load(1, {shade: [0.4,'#000']});
			}
		},
		success : function(res){
			switch(res.code){
			case 1:
				callback(res.data);
				break;
			case 2:
				layer.msg(res.msg);
				break;
			case 3:
				showBox(1);
				break;
			case 4:
				layer.confirm(res.msg,{btn: ['确定','取消']},function(index){
					layer.close(index);
				});
				break;
			}
		},
		complete: function(){
			if(is_load){
				layer.close(loadindex);
			}
		},
		error : function(){
			if(is_load){
				layer.close(loadindex);
			}
		}
	});
}

$(".main_item").mouseover(function(){
    $(this).children(".sub_box").show();
});
$(".main_item").mouseleave(function(){
    $(this).children(".sub_box").hide();
});

$('#search_input').bind('keyup', function(event) {
    if (event.keyCode == "13") {
        var val = $(this).val();
        if(val.length > 0){
        	window.location.href = '/home/search/index.html?keyword='+encodeURI(val);;
        }
    }
});

$('#search_btn').click(function(){
	var val = $('#search_input').val();
	if(val.length > 0){
    	window.location.href = '/home/search/index.html?keyword='+encodeURI(val);;
    }
});

//检测章节
function checkChapter(book_id,book_number){
	ajaxPost('/home/Book/checkChapter',{book_id:book_id,number:book_number},function(res){
		window.location.href = res.url;
	});
}

function checkLogin(){
	var res = false;
	var token = $.cookie('LOGIN_TOKEN');
	if(token){
		res = true;
	}
	return res;
}

function checkJump(url){
	if(!checkLogin()){
		showBox(1);
	}else{
		window.location.href = url;
	}
}
