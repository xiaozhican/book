var list_page = 1;
getData();
function getData(){
	ajaxPost('',{page:list_page},function(res){
		createHtml(res.list);
		if(list_page === 1){
			if(res.count > 0){
				$('.not_book').hide();
				$('.buttom_bar').show();
				if(res.count > 1){
					myPageInit({
						pages: res.count,
						currentPage: 1,
						element: '.my-page',
						callback: function(page) {
							if(parseInt(page) !== list_page){
								list_page = parseInt(page);
								getData();
							}
						}
					});
				}
			}
		}
	});
}

function createHtml(list){
	var str = '';
	if(list){
		$.each(list,function(i,item){
			str += '<li>';
			str += '<input type="checkbox" name="choose_book" class="choose_book" value="'+item.id+'" id="list_'+item.id+'" hidden>';
			str += '<label class="choose_book_label fl" for="list_'+item.id+'"></label>';
			str += '<img class="cover fl" src="'+item.cover+'">';
			str += '<div class="name_bar fl">';
			str += '<p class="name textEllipsis">'+item.name+'</p>';
			str += '<p class="author textEllipsis">作者：'+item.author+'</p>';
			str += '</div>';
			str += '<p class="main fl">'+item.summary+'</p>';
			str += '<a href="javascript:void(0);" class="read fr" onclick="javascript:checkChapter('+item.id+','+item.number+');" >继续阅读</a>';
			str += '</li>';
		});
	}
	$('.book_list').html(str);
}

//全选
$("#choose_all").on("change", function () {
	var value = $("#choose_all").is(":checked");
	if (value) { // 全选
		$("input[name='choose_book']").each(function () {
			$(this).prop("checked", true);
		});
	} else {
		$("input[name='choose_book']").each(function () {
			$(this).prop("checked", false);
		});
	}
});

$("body").on('click','.choose_book',function () {
	check_len = $("input[name='choose_book']:checked").length;
	total_len = $("input[name='choose_book']").length;
	if (check_len == total_len){
		$("#choose_all").prop("checked", true);
	}else {
		$("#choose_all").prop("checked", false);
	}
})


function doRemoveBook(flag){
	var ids = [];
	$('input[name="choose_book"]:checked').each(function(){
		var val = $(this).val();
		ids.push(val);
	});
	if(ids.length === 0){
		layer.msg('您尚未选择要移除的书籍');
		setTimeout(function () {
			$('.delete_popup').hide();
		},3000)
		return false;
	}
	var book_ids = ids.join(',');
	ajaxPost('/home/User/doRemoveBook',{book_ids:book_ids,flag:flag},function(){
		$('input[name="choose_book"]:checked').parent().remove();
		$('.delete_popup').hide();
		if ($("#choose_all").is(":checked")){
			window.location.href='';
		}
	});
}

$("#delete").on("click", function () {
	$('.delete_popup').show();
});

$('.close,#close').on("click", function () {
	$('.delete_popup').hide();
});