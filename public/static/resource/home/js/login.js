var reg_time = 0,forget_time = 0;
function showBox(flag){
	$('.login_box').hide();
	switch(flag){
	case 1:
		$('#login_box').show();
		break;
	case 2:
		$('#reg_box').show();
		break;
	case 3:
		$('#forget_box').show();
		break;
	case 4:
		$('#logout_box').show();
		break;
	}
}

function closeBox(){
	$('.login_box').hide();
}

//登录
function doLogin(obj){
	var parentObj = $(obj).parent();
	var phone = parentObj.find('input[name="phone"]').val();
	var password = parentObj.find('input[name="password"]').val();
	if(!/^1[3456789]\d{9}$/.test(phone)){
		layer.msg('手机号格式不正确');
		return false;
	}
	if(password.length < 6){
		layer.msg('登录密码为6-16位数字');
		return false;
	}
	var data = {phone:phone,password:password};
	ajaxPost('/home/Login/doLogin',data,function(user_msg){
		layer.msg('登录成功');
		closeBox();
		var str = '';
		str += '<a href="javascript:void(0);" class="user_item" onclick="javascript:showBox(4);">退出</a>';
		str += '<span class="header_line"></span>';
		str += '<a href="javascript:void(0);" class="user_item" onclick="javascript:showBox(1);">切换</a>';
		str += '<span class="header_line"></span>';
		str += '<a href="javascript:void(0);" onclick="javascript:checkJump(\'/home/user/bookself.html\');" class="user_item" >'+user_msg.nickname+'</a>';
		str += '<img class="default_headimg" onclick="javascript:checkJump(\'/home/user/bookself.html\');" src="'+user_msg.headimgurl+'">';
		$('#show_user').html(str);
	});
}

//注册
function doRegister(obj){
	var parentObj = $(obj).parent();
	var phone = parentObj.find('input[name="phone"]').val();
	var code = parentObj.find('input[name="code"]').val();
	var password = parentObj.find('input[name="password"]').val();
	if(!/^1[3456789]\d{9}$/.test(phone)){
		layer.msg('手机号格式不正确');
		return false;
	}
	if(!/^\d{4}$/.test(code)){
		layer.msg('验证码为4位数字');
		return false;
	}
	if(password.length < 6){
		layer.msg('登录密码为6-16位数字');
		return false;
	}
	var is_agree = parentObj.find('input[name="is_agree"]').is(':checked');
	if(!is_agree){
		layer.msg('请阅读并同意用户协议');
		return false;
	}
	var data = {phone:phone,code:code,password:password};
	ajaxPost('/home/Login/doRegister',data,function(user_msg){
		layer.msg('注册成功');
		closeBox();
		var str = '';
		str += '<a href="javascript:void(0);" class="user_item" onclick="javascript:showBox(1);">切换</a>';
		str += '<span class="header_line"></span>';
		str += '<a href="javascript:void(0);" class="user_item" >'+user_msg.nickname+'</a>';
		str += '<img class="default_headimg" src="'+user_msg.headimgurl+'">';
		$('#show_user').html(str);
	});
}

function sendRegCode(obj){
	if(reg_time !== 0){
		return false;
	}
	reg_time = 120
	var phone = $(obj).parents('.login_main').find('input[name="phone"]').val();
	if(!/^1[3456789]\d{9}$/.test(phone)){
		layer.msg('手机号格式不正确');
		reg_time = 0;
		return false;
	}
	$.ajax({
		  type : 'post',
		  url : '/home/Com/sendCode',
		  data : {phone:phone,type:1},
		  dataType : 'json',
		  success : function(res){
			  if(res.code == 1){
				  setRegTime(obj);
			  }else{
				  reg_time = 0;
				  layer.msg(res.msg);
			  }
		  },
		  error : function(){
			  reg_time = 0;
			  layer.msg('网络错误,请稍后再试');
		  }
	});
}

function setRegTime(obj){
	if (reg_time == 0) { 
		  $(obj).text("重新获取"); 
		  return;
	  } else { 
		  $(obj).text(reg_time + "s"); 
		  reg_time--; 
	  } 
	  setTimeout(function() { 
		  setRegTime(obj); 
	  },1000); 
}

function sendForgetCode(obj){
	if(forget_time !== 0){
		return false;
	}
	forget_time = 120
	var phone = $(obj).parents('.login_main').find('input[name="phone"]').val();
	if(!/^1[3456789]\d{9}$/.test(phone)){
		layer.msg('手机号格式不正确');
		forget_time = 0
		return false;
	}
	$.ajax({
		type : 'post',
		url : '/home/Com/sendCode',
		data : {phone:phone,type:2},
		dataType : 'json',
		success : function(res){
			if(res.code == 1){
				setForgetTime(obj);
			}else{
				forget_time = 0;
				layer.msg(res.msg);
			}
		},
		error : function(){
			forget_time = 0;
			layer.msg('网络错误,请稍后再试');
		}
	});
}

function setForgetTime(obj){
	if (forget_time == 0) { 
		  $(obj).text("重新获取"); 
		  return;
	  } else { 
		  $(obj).text(forget_time + "s"); 
		  forget_time--; 
	  } 
	  setTimeout(function() { 
		  setFotgetTime(obj); 
	  },1000); 
}

//忘记密码
function doForgetPwd(obj){
	var parentObj = $(obj).parent();
	var phone = parentObj.find('input[name="phone"]').val();
	var code = parentObj.find('input[name="code"]').val();
	var password = parentObj.find('input[name="password"]').val();
	var repassword = parentObj.find('input[name="repassword"]').val();
	if(!/^1[3456789]\d{9}$/.test(phone)){
		layer.msg('手机号格式不正确');
		return false;
	}
	if(!/^\d{4}$/.test(code)){
		layer.msg('验证码为4位数字');
		return false;
	}
	if(password.length < 6){
		layer.msg('登录密码为6-16位数字');
		return false;
	}
	if(repassword !== password){
		layer.msg('两次输入密码不一致');
		return false;
	}
	var data = {phone:phone,code:code,password:password};
	ajaxPost('/home/Login/doForget',data,function(user_msg){
		layer.msg('登录成功');
		closeBox();
		var str = '';
		str += '<a href="javascript:void(0);" class="user_item" onclick="javascript:showBox(1);">切换</a>';
		str += '<span class="header_line"></span>';
		str += '<a href="javascript:void(0);" class="user_item" >'+user_msg.nickname+'</a>';
		str += '<img class="default_headimg" src="'+user_msg.headimgurl+'">';
		$('#show_user').html(str);
	});
}

//退出登录
function doLogout(obj){
	ajaxPost('/home/Login/logout',{},function(){
		layer.msg('退出成功');
		setTimeout(function () {
			window.location.href='/home';
		},2000);
		closeBox();
	});
}