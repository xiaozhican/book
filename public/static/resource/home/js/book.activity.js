var list_page = 1;
var param = getQueryParam();
makeParam(param);
var is_free_page = IsFreePage();
getData();
function makeParam(data){
	for(key in data){
		var dom_class = '.key_'+key;
		if($(dom_class)){
			$(dom_class).find('.span_item').removeClass('blue');
			$(dom_class).find('.span_item[data-val="'+data[key]+'"]').addClass('blue');
		}
	}
}

function doChooseType(obj,type){
	$('.category_item').hide();
	$(obj).addClass('blue').siblings().removeClass('blue');
	$('category_item,.type'+type).show();
	param['type'] = type;
	$('.type'+type).find('.span_item').removeClass('blue');
	$('.type'+type).find('.span_item:eq(0)').addClass('blue');
	if('category' in param){
		delete param['category'];
	}
	clearPage();
	getData();
}

function doChooseItem(obj,key,val){
	if(val){
		param[key] = val;
	}else{
		if(key in param){
			delete param[key];
		}
	}
	$(obj).addClass('blue').siblings().removeClass('blue');
	$(obj).addClass('blue');
	clearPage();
	getData();
}

function getData(){
	param['page'] = list_page;
	ajaxPost('',param,function(res){
		createHtml(res.list);
		if(list_page === 1 && res.count > 1){
			myPageInit({
			    pages: res.count,
			    currentPage: 1,
			    element: '.my-page',
			    callback: function(page) {
			    	if(parseInt(page) !== list_page){
			    		list_page = parseInt(page);
			    		getData();
			    	}
			    }
			});
		} 
	});
}

function clearPage(){
	list_page = 1;
	$('.my-page').html('');
}

function doCollect(book_id,event,obj){
	ajaxPost('/home/Com/doCollect',{book_id:book_id,event:event},function(){
		switch(event){
		case 'join':
			$(obj).attr('onclick','javascript:doCollect('+book_id+',\'remove\',this);');
			$(obj).text('移除书架');
			break;
		case 'remove':
			$(obj).attr('onclick','javascript:doCollect('+book_id+',\'join\',this);');
			$(obj).text('加入书架');
			break;
		}
	});
}

function createHtml(list){
	var str = '';
	if(list){
		$.each(list,function(i,item){
			str += '<div class="book">';
			str += '<img class="cover fl" src="/static/resource/home/images/bg.png">';
			str += '<div class="info fr">';
			str += '<p class="name">活动标题：'+item.name+'</p>';
			str += '<p class="author">活动内容：'+item.content+'</p>';
			str += '<p class="author">活动时间：'+item.between_time+'</p>';
			if (item.show_btn == 1) {
				str += '<div class="bottom_bar">';
				str += '<a href="javascript:void(0);" class="read fr" onclick="checkJumps('+item.id+');">参与活动</a>';
				str += '</div>';
			}
			str += '</div>';
			str += '</div>';
		});
	}
	$('.showBox').html(str);
}

//检测是否限免页面
function IsFreePage(){
	var res = false;
	var name = window.location.pathname;
	str = name.toLowerCase();
	if(str.indexOf("free") != -1){
		res = true;
	}
	return res;
}

function checkJumps(id){
	if (!checkLogin()){
		showBox(1);
	}else {
		ajaxPost('/home/charge/doActivityCharge',{activity_id:id},function(d){
			window.location.href = d.url;
			// window.location.href = "http://www.baidu.com";
		});
	}
}