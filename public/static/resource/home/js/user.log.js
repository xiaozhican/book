var list_page = 1;
getData();
function getData(){
	ajaxPost('',{page:list_page},function(res){
		var get = getQueryParam('type');
		pageType = 'type' in get ? parseInt(get.type) : 1;
		switch(pageType){
		case 1:
			createType1Html(res.list);
			break;
		case 2:
			createType2Html(res.list);
			break;
		case 3:
			createType3Html(res.list);
			break;
		}
		if(list_page === 1 && res.count > 1){
			myPageInit({
			    pages: res.count,
			    currentPage: 1,
			    element: '.my-page',
			    callback: function(page) {
			    	if(parseInt(page) !== list_page){
			    		list_page = parseInt(page);
			    		getData();
			    	}
			    }
			});
		} 
	});
}

function createType1Html(list){
	var str = '';
	if(list){
		$.each(list,function(i,item){
			str += '<li>';
			str += '<p class="w326">'+item.summary+'</p>';
			str += '<p class="red w326">'+item.money+'</p>';
			str += '<p class="w326">'+item.create_time+'</p>';
			str += '</li>';
		});
	}
	$('.consumption_list').html(str);
}

function createType2Html(list){
	var str = '';
	if(list){
		$.each(list,function(i,item){
			str += '<li>';
			str += '<p class="w290 textEllipsis fl">'+item.book_name+'</p>';
			str += '<div class="img_box w200 fl">';
			str += '<img src="'+item.gift.src+'">';
			str += '</div>';
			str += '<p class="w200 fl">'+item.count+'</p>';
			str += '<p class="w290 fl">'+item.create_time+'</p>';
			str += '</li>';
		});
	}
	$('.consumption_list').html(str);
}

function createType3Html(list){
	var str = '';
	if(list){
		$.each(list,function(i,item){
			str += '<li>';
			str += '<p class="w290 textEllipsis fl">'+item.summary+'</p>';
			str += '<p class="w290 textEllipsis fl">'+item.chapter_title+'</p>';
			str += '<p class="w110 fl">-'+item.money+'</p>';
			str += '<p class="w290">'+item.create_time+'</p>';
			str += '</li>';
		});
	}
	$('.consumption_list').html(str);
}