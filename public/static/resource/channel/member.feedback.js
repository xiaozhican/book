layui.use(['jquery','layer','table','form','laydate'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	laydate = layui.laydate,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
		    {field: 'uid', title: '粉丝ID',align:'center',minWidth:120},
		    {field: 'nickname', title: '粉丝昵称',align:'center',minWidth:120},
		    {field: 'phone', title: '联系电话',align:'center',minWidth:120},
		    {field: 'content', title: '反馈内容',align:'center',minWidth:300},
		    {field: 'reply', title: '回复内容',align:'center',minWidth:200},
		    {field: 'create_time', title: '反馈时间',align:'center',minWidth:160},
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	laydate.render({elem:'#between_time',type:'datetime',range:'~'});
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});