layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	table.render({
		elem : '#table-block',
		url : window.location.href,
		loading : true,
		cols : [[
			{title: '序号',type:'numbers'},
			{title: '渠道号',field:'channel_id',width:140,align:'center'},
		    {title: '用户名称',align:'left',minWidth:120,templet:function(d){
		    	var name = d.name;
		    	if(parseInt(d.is_cur) === 1){
		    		name += '<font class="text-red">(当前账号)</font>'
		    	}
		    	return name;
		    }},
		    {field: 'create_time', title: '添加时间',align:'center',minWidth:200},
		    {title: '操作',align:'center',minWidth:320,templet:function(d){
		    	var str = '';
		    	if(parseInt(d.is_cur) === 2){
		    		str += '<a class="layui-btn layui-btn-normal layui-btn-sm" lay-event="location">切换渠道</a>';
			    	str += '<a class="layui-btn layui-btn-danger layui-btn-sm" lay-event="delete">取消关联</a>';
		    	}else{
		    		str += '--';
		    	}
		    	return str;
		    }}
		]],
		page : true,
		height : 'full-220',
		response: {statusCode:1}, 
		id : 'table-block',
	    done:function (res) {
	    	layer.closeAll();
	    }
	});
	
	$('#bindChannel').click(function(){
		var url = U('doBind');
		layer.open({
			  type: 2,
			  title: '绑定渠道',
			  closeBtn: 1,
			  shade: [0],
			  area: ['500px','300px'],
			  offset : '100px',
			  time: 0,
			  anim: 2,
			  scrollbar :false,
			  content: [url,'yes'],
			  btn : ['确定','取消'],
			  yes : function(index,layero){
				  var iframeWin = window[layero.find('iframe')[0]['name']];
				  iframeWin.layui.form.on('submit(dosubmit)', function(obj){
					  ajaxPost(url,obj.field,'确定要提交绑定吗？',function(d){
						    layer.close(index);
							table.reload('table-block');
					  });
		          });  
				  iframeWin.document.getElementById("dosubmit").click();
			  }
		});
	});
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		var data = {id:field.channel_id,event:obj.event};
		var url = U('doAccountEvent');
		switch(obj.event){
			case 'location':
				ajaxPost(url,data,'确定要跳转到该账号吗？',function(){
					layOk('切换成功');
					setTimeout(function(){
						window.top.location.reload();
					},1500);
				});
				break;
			case 'delete':
				ajaxPost(url,data,'确定要将该取消关联该账号？',function(){
					layOk('操作成功');
					table.reload('table-block');
				});
				break;
		}
	});
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});