layui.use(['jquery','layer'],function(){
	var myChart = echarts.init(document.getElementById('memberCharts'),'macarons');
	ajaxPost(U('getUserChartData'),{},'',function(res){
		var option = {
			    tooltip : {
			        trigger: 'axis'
			    },
			    legend: {
			        data:['新增用户','关注用户']
			    },
			    toolbox: {
			        show : true,
			        feature : {
			            mark : {show: true},
			            dataView : {show: true, readOnly: false},
			            magicType : {show: true, type: ['line', 'bar']},
			            restore : {show: true},
			            saveAsImage : {show: true}
			        }
			    },
			    calculable : true,
			    xAxis : [
			        {
			            type : 'category',
			            boundaryGap : false,
			            data : res['key']
			        }
			    ],
			    yAxis : [
			        {
			            type : 'value',
			            axisLabel : {
			                formatter: '{value}'
			            }
			        }
			    ],
			    series : [
			        {
			            name:'新增用户',
			            type:'line',
			            data:res['add'],
			            
			            markPoint : {
			                data : [
			                    {type : 'max', name: '最大值'},
			                    {type : 'min', name: '最小值'}
			                ]
			            }
			        },
			        {
			            name:'关注用户',
			            type:'line',
			            data:res['sub'],
			            markPoint : {
			                data : [
			                	{type : 'max', name: '最大值'},
			                    {type : 'min', name: '最小值'}
			                ]
			            }
			        }
			    ]
			};
		myChart.setOption(option); 
	});
	
	
});