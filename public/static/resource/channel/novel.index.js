layui.use(['jquery','layer','table','form'],function(){
	var $ = layui.jquery,
	layer = layui.layer,
	table = layui.table,
	form = layui.form;
	layLoad('数据加载中...');
	
	var dataurl = window.location.href;
	renderTable(dataurl);
	function renderTable(url,where){
		if(!where){
			where = {};
		}
		table.render({
			elem : '#table-block',
			url : dataurl,
			where : where,
			loading : true,
			cols : [[
				{title: '封面',minWidth:100,align:'center',templet:function(d){
					var $html = '<font class="text-red">暂无封面</font>';
					if(d.cover){
						$html = '<img src="'+d.cover+'" style="width:50px;" />';
					}
					return $html;
				}},
			    {title: '小说名称',align:'left',minWidth:260,templet:function(d){
			    	var str = d.name+'&nbsp;'
			    	str += '<a class="layui-btn layui-btn-xs layui-btn-primary" lay-event="copy" title="点击复制链接"><i class="layui-icon layui-icon-link"></i></a>';
			    	var is_first = 1;	
			    	if(parseInt(d.is_god) === 1){
			    		is_first = 0;
				    	str += '<br />'
			    		str += '<a class="layui-btn layui-btn-danger layui-btn-xs">神书</a>'
			    	}
			    	if(parseInt(d.is_top) === 1){
			    		if(is_first){
			    			str += '<br />';
			    		}
			    		str += '<a class="layui-btn layui-btn-danger layui-btn-xs">顶</a>'
			    	}
		    		if(d.free_time){
		    			str += '<br />'
		    			str += '<a class="layui-btn layui-btn-xs layui-btn-danger">限免:'+d.free_time+'</a>';
		    		}
			    	return str;
			    }},
			    {field: 'author', title: '作者',align:'center',minWidth:120},
			    {field: 'total_chapter',title: '总章节',align:'center',minWidth:120},
			    {title: '更新状态',align:'center',minWidth:100,templet:function(d){
			    	var str = '--';
			    	switch(parseInt(d.over_type)){
			    	case 1:str='连载中';break;
			    	case 2:str='已完结';break;
			    	}
			    	return str;
			    }},
			    {title: '收费属性',align:'center',minWidth:80,templet:function(d){
			    	var str = '--';
			    	switch(parseInt(d.free_type)){
			    	case 1:str='免费';break;
			    	case 2:str='收费';break;
			    	}
			    	return str;
			    }},
			    {title: '读者性别',align:'center',minWidth:80,templet:function(d){
			    	var str = '--';
			    	switch(parseInt(d.gender_type)){
			    	case 1:str='男频';break;
			    	case 2:str='女频';break;
			    	}
			    	return str;
			    }},
			    {title: '小说状态',align:'center',minWidth:120,templet:function(d){
			    	var str = '';
			    	switch(parseInt(d.status)){
				    	case 1:str = '正常';break;
				    	case 2:str = '下架可阅';break;
			    	}
			    	return str;
			    }},
			    {title: '操作',align:'center',minWidth:400,templet:function(d){
			    	var str = '--';
			    	if(parseInt(d.total_chapter) > 0){
			    		if(parseInt(d.is_share) === 1){
			    			str = '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="share">分享话术</a>';
			    		}
				    	str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.guide_url+'" target="_blank">生成文案</a>';
				    	str += '<a class="layui-btn layui-btn-normal layui-btn-xs" lay-event="spread">推广链接</a>';
				    	str += '<a class="layui-btn layui-btn-normal layui-btn-xs" href="'+d.customer_url+'">客服消息</a>';
			    	}
			    	return str;
			    }}
			]],
			page : true,
			height : 'full-400',
			response: {statusCode:1}, 
			id : 'table-block',
		    done:function (res) {
		    	layer.closeAll();
		    }
		});
	}
	
	$('.search-item').click(function(){
		if($(this).hasClass('layui-btn-normal')){
			return false;
		}
		$(this).addClass('layui-btn-normal').removeClass('layui-btn-primary');
		$(this).siblings().removeClass('layui-btn-normal').addClass('layui-btn-primary');
		var where = getWhere();
		console.log(where);
		layLoad('数据加载中...');
		renderTable(dataurl,where);
	});
	
	function getWhere(){
		var data = {};
		$('.search-block').each(function(){
			var val = $(this).find('.search-item.layui-btn-normal').attr('data-val');
			if(val){
				var name = $(this).attr('data-name');
				data[name] = val;
			}
		});
		return data;
	}
	
	
	table.on('tool(table-block)',function(obj){
		var field = obj.data;
		switch(obj.event){
			case 'spread':
				createLink(field.spread_url);
				break;
			case 'share':
				layPage('分享话术【'+field.name+'】',field.share_url,'800px','500px')
				break;
			case 'copy':
				layPage('复制小说链接【'+field.name+'】',field.copy_url,'800px','500px')
				break;
		}
	});
	
	function createLink(url){
		layer.open({
			  type: 2,
			  title: '生成推广链接',
			  closeBtn: 1,
			  shade: [0],
			  area: ['800px','630px'],
			  offset : '100px',
			  time: 0,
			  anim: 2,
			  scrollbar :false,
			  content: [url,'yes'],
			  btn : ['生成推广链接'],
			  yes : function(index,layero){
				  var iframeWindow = $(layero).find('iframe')[0].contentWindow,
		            submitID = 'pageSubmit',
		            submit = layero.find('iframe').contents().find('#'+ submitID);
		            iframeWindow.layui.form.on('submit('+ submitID +')', function(obj){
		              var data = obj.field;
		              ajaxPost(url,data,'确定要生成推广链接吗？',function(res){
		            	  layOk('已为'+res.num+'个渠道生成推广链接');
	            		  layer.close(index);
		              });
		            });  
		            submit.trigger('click');
			  }
		});
	}
	
	form.on('submit(table-search)',function(obj){
		var where = obj.field;
		layLoad('数据加载中...');
		table.reload('table-block',{
			where : where,
			page: {curr:1}
		});
	});
});