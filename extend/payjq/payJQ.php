<?php
namespace payjq;


use site\myHttp;

class payJQ{
	
	public static $config;
	
	//获取微信支付参数
	public static function getPayMsg($param){
		$url = 'https://payjq.cn/api/jsapi';
		$data = ['mchid' => self::$config['mchid']];
		$data = array_merge($data,$param);
		$data['sign'] = self::getSign($data);
		$res = myHttp::doPost($url,$data);
		if($res && isset($res['return_code']) && $res['return_code'] == 1){
			return $res['jsapi'];
		}else{
			return false;
		}
	}
	
	//获取签名
	public static function getSign($data){
		$param = [];
		foreach ($data as $k=>$v){
			if(strlen($v) > 0){
				$param[$k] = $v;
			}
		}
		ksort($param);
		$sign = strtoupper(md5(urldecode(http_build_query($param)).'&key='.self::$config['api_key']));
		return $sign;
	}
}