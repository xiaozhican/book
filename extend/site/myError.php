<?php


namespace site;


use think\Db;

class myError
{
    //记录系统错误日志
    public static function errorLog($content,$file='',$line=''){
        Db::name('Error')->insert([
            'content' => $content,
            'file' => $file,
            'line' => $line,
            'date_time' => date('Y-m-d H:i:s')
        ]);
    }
}