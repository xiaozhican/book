<?php
namespace site;

use think\Db;

class myPush{
	
	private static $app_key = '00babd14604e2cecced4c312';
	
	private static $master_secret = '7ebd355c9adfbc64605e1a25';
	
	private static $url = 'https://api.jpush.cn/v3/push';
	
	
	//书籍更新推送
	public static function updateBookPush($bookId){
		$book = myCache::getBook($bookId);
		if($book){
			$uids = Db::name('MemberCollect')->where('book_id',$bookId)->column('uid');
			if($uids){
				$msg = [
					'title' => '书籍更新通知',
					'content' => '您收藏的书籍《'.$book['name'].'》更新啦，点我继续阅读吧',
					'extras' => [
						'page' => "2",
						'book_id' => ''.$book['id'],
						'book_type' => ''.$book['type']
					]
				];
				self::doPushMore($uids, $msg);
			}
		}
	}
	
	//热门书籍推送
	public static function HotBookPush(){
        $redis = self::redisConnect();
		$key = 'HotBookPush';
		$is_key = $redis->exists($key);
		if($is_key == 0){
		    //key不存在或列表中无数据
            self::HotBookData($key);//获取数据
        }
        $length = $redis->lLen($key);//获取长度
        if($length > 0){
            //说明列表里面有数据
            $book_id = $redis->lPop($key);
            $book_push = Db::name('Book')->where('id',$book_id)->field('id,type,name')->find();//推送的书籍
            $msg = [
                'title' => '热门书籍推荐',
                'content' => '今日为您推荐《'.$book_push['name'].'》，点我阅读吧',
                'extras' => [
                    'page' => "2",
                    'book_id' => ''.$book_push['id'],
                    'book_type' => ''.$book_push['type']
                ]
            ];
            self::doPush('all', $msg);
        }
	}

	//热门书籍推送的备份
    public static function HotBookPushCopy(){
        $book = Db::name('Book')->where('status',1)->order('hot_num','desc')->field('id,type,name')->find();
        if($book){
            $msg = [
                'title' => '热门书籍推荐',
                'content' => '今日为您推荐《'.$book['name'].'》，点我阅读吧',
                'extras' => [
                    'page' => "2",
                    'book_id' => ''.$book['id'],
                    'book_type' => ''.$book['type']
                ]
            ];
            self::doPush('all', $msg);
        }
    }

    private static function HotBookData($key){
	    $length = 10;
        $books = Db::name('Book')->where('status',1)->order('hot_num','desc')->field('id,type,name')->limit($length)->select();//取出人气高的前10条数据
        shuffle($books);//打乱数组
        $books = array_slice($books,0,$length);//取出数据
        $redis = self::redisConnect();
        //$key = 'HotBookPush';
        foreach ($books as $book) {
            //循环存入redis中
            $redis->rPush($key,$book['id']);
        }
    }
    //连接redis
    private static function redisConnect(){
        $redis = new \Redis();
        $redis->connect('127.0.0.1',6379);
        return $redis;
    }
	
	//推送多个指定用户
	public static function doPushMore($uids,$msg){
		$code = 4;
		$jpush_reg_ids = self::getJpushRegIds($uids);
		if($jpush_reg_ids){
			$uidList = array_chunk($jpush_reg_ids, 1000);
			$num = $suc = 0;
			foreach ($uidList as $v){
				$num++;
				$res = self::doPush($jpush_reg_ids,$msg);
				if($res){
					$suc++;
				}
			}
			if($suc != 0){
				if($num == $suc){
					$code = 1;
				}else{
					$code = 2;
				}
			}
		}else{
			$code = 3;
		}
		return $code;
	}
	
	//处理推送
	public static function doPush($jpush_reg_ids,$msg){
		if(is_array($jpush_reg_ids)){
			$audience = ['registration_id'=>$jpush_reg_ids];
		}else{
			if($jpush_reg_ids === 'all'){
				$audience = ['tag_and'=>['bookuser']];
			}else{
				$audience = ['registration_id'=>explode(',', $jpush_reg_ids)];
			}
		}
		$data = [
			'platform' => 'all',
			'audience' => $audience
		];
		$data['notification'] = [
			"android" => [
				"alert" => $msg['content'],
				"title" => $msg['title'],
				"builder_id" => 1,
				"extras" => $msg['extras']
			],
			"ios" => [
				"alert" => $msg['content'],
				"badge" => "1",
				"sound" => "default",
				"extras" => $msg['extras']
			]
		];
		$data['options'] = [
			"sendno" => time(),
			"time_to_live" => 86400,
			"apns_production" => true, //false开发环境，true生产环境
		];
		$result = false;
		$res = self::push_curl(json_encode($data));
		if($res){  
			$res_arr = json_decode($res, true);
			if(isset($res_arr['error'])){
				if($res_arr['error']['code'] == '200'){
					$result = true;
				}
			}else{
				$result = true;
			}
		}
		return $result;
	}
	
	//获取极光ID
	public static function getJpushRegIds($uids){
		$data = [];
		if($uids){
			foreach ($uids as $uid){
				$jpush_reg_id = myCache::JpushRegId($uid);
				if($jpush_reg_id){
					$data[] = $jpush_reg_id;
				}
			}
		}
		return $data;
	}
	
	//提交推送请求
	private static function push_curl($data) {
		$authorization = self::$app_key.':'.self::$master_secret;
		$base64 = base64_encode($authorization);
		$header = ["Authorization:Basic $base64","Content-Type:application/json"];
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,self::$url);
		curl_setopt($ch, CURLOPT_HEADER, 0);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_HTTPHEADER,$header);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, FALSE);
		$result = curl_exec($ch);                               
		curl_close($ch);
		return $result;
	}
}