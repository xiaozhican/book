<?php
namespace site;

use Endroid\QrCode\QrCode;

class myImg{
	
	//获取邀请图片
	public static function getShareImg($uid,$invite_code){
		$url = '';
		$config = getMyConfig('site_share');
		if($config && $config['bg']){
			$site = getMyConfig('website');
			if($site && $site['download_url']){
				$name = $config['bg'].'#'.$invite_code.'#'.$site['download_url'];
				$file = './files/share/'.$uid.'/'.md5($name).'.png';
				if(!@is_file($file)){
					$path = './files/share/'.$uid;
					if(!is_dir($path)){
						mkdir($path,0777,true);
					}
					if(is_dir($path)){
						$share_url = $site['download_url'];
						$share_url .= strpos($share_url, '?') !== false ? '&' : '?';
						$share_url .= 'invite_code='.$invite_code;
						$share_name = md5($share_url).'.png';
						$share_file = $path.'/'.$share_name;
						if(!@is_file($share_file)){
							$share_file = self::createQrCode($share_url, $share_file);
						}
						if($share_file){
							$bg_file = './files/bg/'.md5($config['bg']).'.jpg';
							if(!@is_file($bg_file)){
								$bgData = self::getImageContent($config['bg']);
								if($bgData){
									$myfile = fopen($bg_file,"w");
									fwrite($myfile,$bgData);
									fclose($myfile);
								}
							}
							if(@is_file($bg_file)){
								$img = imagecreatefromstring(file_get_contents($bg_file));
								$qrImg = imagecreatefrompng($share_file);
								$imgsize = getimagesize($share_file);
								imagecopyresized ($img, $qrImg,200, 500, 0, 0, 200, 200, $imgsize[0], $imgsize[1]);
								imagepng($img,$file);
								imagedestroy($img);
								imagedestroy($qrImg);
								if(is_file($file)){
									$url = 'http://'.$site['url'].ltrim($file,'.').'?t='.time();
								}
							}
						}
					}
				}else{
					$url = 'http://'.$site['url'].ltrim($file,'.').'?t='.time();
				}
			}
		}
		return $url;
	}
	
	//拉取远程图片
	public static function getImageContent($img_url,$referer=''){
		$header = array('User-Agent: Mozilla/5.0 (Windows NT 6.1; Win64; x64; rv:45.0) Gecko/20100101 Firefox/45.0', 'Accept-Language: zh-CN,zh;q=0.8,en-US;q=0.5,en;q=0.3', 'Accept-Encoding: gzip, deflate', );
		$curl = curl_init();
		curl_setopt($curl, CURLOPT_URL, $img_url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
		if($referer){
			curl_setopt($curl, CURLOPT_REFERER,$referer);
		}
		curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($curl, CURLOPT_ENCODING, 'gzip');
		curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
		$data = curl_exec($curl);
		$code = curl_getinfo($curl, CURLINFO_HTTP_CODE);
		curl_close($curl);
		return $data;
	}
	
	//生成二维码
	public static function createQrCode($content,$filename){
		$qrcode = new QrCode();
		$qrcode->setText($content);
		$qrcode->setSize(400);
		$qrcode->writeFile($filename);
		$res = @is_file($filename) ? $filename : '';
		return $res;
	}
	
}

