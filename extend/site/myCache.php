<?php
namespace site;

use think\Db;
use think\facade\Cache;
use app\common\model\mdPlatform;

class myCache{
	
	/**
	 * 清理所有缓存
	 */
	public static function clearAll(){
		Cache::clear();
	}
	
	/**
	 * 获取code信息
	 * @param string $code 编码
	 * @return mixed
	 */
	public static function getKcCodeInfo($code){
		$key = 'kc_code_'.$code;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = myDb::getcur('Code', [['code','=',$code]]);
			if($data){
				$data['info'] = json_decode($data['info'],true);
				cache($key,$data);
			}
		}
		return $data;
	}
	
	
	/**
	 * 获取渠道代理缓存
	 * @param number $channel_id 渠道代理ID
	 * @return array 渠道代理信息
	 */
	public static function getChannel($channel_id){
		$key = 'channel_info_'.$channel_id;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = myDb::getById('Channel', $channel_id);
			if($data){
				cache($key,$data);
			}
		}
		return $data;
	}
	
	/**
	 * 更新渠道信息
	 * @param number $channel_id 渠道ID
	 * @param mixed $field 字段名，批量更新为二维数组
	 * @param mixed $value 字段值
	 */
	public static function doChannel($channel_id,$field,$value=''){
		$key = 'channel_info_'.$channel_id;
		if(cache('?'.$key)){
			$data = cache($key);
			if(!$data){
				cache($key,null);
			}else{
				if(is_array($field)){
					foreach ($field as $k=>$v){
						if(array_key_exists($k, $data)){
							$data[$k] = $v;
						}
					}
				}else{
					if(array_key_exists($field, $data)){
						$data[$field] = $value;
					}
				}
				cache($key,$data);
			}
		}
		return true;
	}
	
	/**
	 * 清除渠道代理缓存
	 * @param number $channel_id 渠道代理ID
	 */
	public static function rmChannel($channel_id){
		$key = 'channel_info_'.$channel_id;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取代理code
	 * @param number $id
	 */
	public static function getAgentCode($id){
		$key = 'agent_code_'.$id;
		if(cache('?'.$key)){
			$code = cache($key);
		}else{
			$code = Db::name('Code')->where('cid',$id)->where('type',3)->value('code');
			if($code){
				cache($key,$code);
			}
		}
		return $code;
	}
	
	/**
	 * 获取渠道代理菜单
	 * @param number $type 2渠道3代理
	 * @return array 菜单信息
	 */
	public static function getChannelMenu($type=2){
		$key = $type === 2 ? 'CHANNEL_LOGIN_MENU' : 'AGENT_LOGIN_MENU';
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$temp = $data = [];
			$list = Db::name('Nodes')->where('type','=',$type)->where('status','=',1)->field('id,pid,name,url,icon')->order('sort_num','desc')->select();
			if($list){
				foreach ($list as $v){
					$temp[] = ['id'=>$v['id'],'pid'=>$v['pid'],'name'=>$v['name'],'icon'=>$v['icon'],'url'=>$v['url']];
				}
				$data = list_to_tree($temp,'id','pid','child');
				cache($key,$data);
			}
		}
		return $data;
	}
	
	/**
	 * 清除渠道代理菜单
	 * @param number $type 2渠道3代理
	 */
	public static function rmChannelMenu($type=2){
		$key = $type === 2 ? 'CHANNEL_LOGIN_MENU' : 'AGENT_LOGIN_MENU';
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取活动缓存
	 * @param number $id 活动ID
	 * @return array 活动信息
	 */
	public static function getActivity($id){
		$key = 'activity_info_'.$id;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = myDb::getById('Activity', $id);
			if($data){
				cache($key,$data);
			}
		}
		return $data;
	}
	
	/**
	 * 清理活动缓存
	 * @param number $id 活动ID
	 */
	public static function rmActivity($id){
		$key = 'activity_info_'.$id;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
    public static function rmNotice($id){
        $key = 'notice_info_'.$id;
        if(cache('?'.$key)){
            cache($key,null);
        }
    }
	
	/**
	 * 获取渠道禁用活动ID集
	 * @param number $channel_id
	 * @return array
	 */
	public static function getActivityDelIds($channel_id){
		$key = 'activity_del_ids_'.$channel_id;
		if(cache('?'.$key)){
			$data = cache($key); 
		}else{
			$data = Db::name('ActivityStatus')->where('channel_id',$channel_id)->where('status',2)->column('act_id');
			$data = $data ? : [];
			cache($key,$data);
		}
		return $data;
	}
	
	/**
	 * 删除渠道禁用活动ID集
	 * @param number $channel_id
	 * @return array
	 */
	public static function rmActivityDelIds($channel_id){
		$key = 'activity_del_ids_'.$channel_id;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取活动编码
	 * @param number $actId 活动ID
	 * @param number $channelId 渠道ID
	 * @return number|unknown|mixed|string|bool
	 */
	public static function getActivityCode($actId,$channelId=0){
		$key = 'act_code_'.$actId.'_'.$channelId;
		if(cache('?'.$key)){
			$code = cache($key);
		}else{
			$code = '';
			$table = 'Code';
			$code = myDb::getValue($table,[['cid','=',$channelId],['aid','=',$actId]],'code');
			if(!$code){
				$info = mdPlatform::getLevelInfo($channelId);
				if(!$info){
					res_api('数据异常，请联系管理员');
				}
				$data = [
					'code' => myDb::createCode('Code'),
					'cid' => $channelId,
					'aid' => $actId,
					'type' => 2,
					'info' => json_encode($info)
				];
				$re = myDb::add($table,$data);
				if($re){
					$code = $data['code'];
				}
			}
		}
		return $code;
	}
	
	/**
	 * 获取最后发布的一条活动信息
	 * @return mixed
	 */
	public static function getLastActivityId(){
		$key = 'near_activity';
		$time = time();
		if(cache('?'.$key)){
			$activity_id = cache($key);
		}else{
			$activity_id = Db::name('Activity')
			->where('status','=',1)
			->where('start_time','<',$time)
			->where('end_time','>',$time)
			->order('id','desc')
			->value('id');
			$activity_id = $activity_id ? : 0;
			cache($key,$activity_id,86400);
		}
		if($activity_id){
			$data = self::getActivity($activity_id);
			if(!$data || !($data['start_time'] < $time && $data['end_time'] > $time)){
				cache($key,null);
				$activity_id = 0;
			}
		}
		return $activity_id;
	}
	
	/**
	 * 清理活动缓存
	 * @param number $id 活动ID
	 */
	public static function rmLastActivity(){
		$key = 'near_activity';
		if(cache('?'.$key)){
			cache($key,null);
		}
	}

    /**
     * 清除广告缓存
     */
    public static function rmLastNotice(){
        $key = 'near_notice';
        if(cache('?'.$key)){
            cache($key,null);
        }
    }
	
	/**
	 * 获取活动充值用户列表
	 * @param number $id 活动ID
	 * @return array
	 */
	public static function getActivityUids($id){
		$key = 'activity_uids_'.$id;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('Order')
			->where('status',2)
			->where('type',2)
			->where('pid',$id)
			->column('uid');
			$data = $data ? : [];
			cache($key,$data,86400);
		}
		return $data;
	}
	
	/**
	 * 新增用户活动参与列表
	 * @param number $id 活动ID
	 * @param number $uid 用户ID
	 */
	public static function addActivityUids($id,$uid){
		$key = 'activity_uids_'.$id;
		if(cache('?'.$key)){
			$data = cache($key);
			if(is_array($data)){
				if(!in_array($uid, $data)){
					$data[] = $uid;
					cache($key,$data,86400);
				}
			}else{
				cache($key,null);
			}
		}
	}
	
	//获取书籍评论缓存
	public static function getBookComments($bookId){
		$key = 'book_comments_'.$bookId;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$field = 'id,uid,content,create_time';
			$data = Db::name('Comments')->where('type',1)->where('pid',$bookId)->where('status',1)->field($field)->order('id','desc')->select();
			if($data){
				foreach ($data as &$v){
					$member = self::getMember($v['uid']);
					if($member){
						$v['nickname'] = $member['nickname'];
						$v['headimgurl'] = $member['headimgurl'];
					}else{
						$v['nickname'] = '游客';
						$v['headimgurl'] = '/static/templet/default/headimg.jpeg';
					}
					$v['create_time'] = date('Y-m-d H:i',$v['create_time']);
				}
			}else{
				$data = 0;
			}
			cache($key,$data);
		}
		return $data;
	}
	
	//删除评论缓存
	public static function rmBookComments($bookId){
		$key = 'book_comments_'.$bookId;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取用户点赞评论ID集
	public static function getZanIds($uid){
		$key = 'comments_zan_id_'.$uid;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('CommentsZan')->where('uid',$uid)->column('pid');
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//新增评论点赞
	public static function addZanIds($uid,$id){
		$key = 'comments_zan_id_'.$uid;
		if(cache('?'.$key)){
			$data = cache($key);
			if($data){
				if(!in_array($id, $data)){
					$data[] = $id;
					cache($key,$data);
				}
			}else{
				cache($key,null);
			}
		}
	}
	
	//获取评论点赞数量
	public static function getCommentsZannum($pid){
		$key = 'comments_zan_'.$pid;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('Comments')->where('id',$pid)->value('zan_num');
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//点赞数量自增
	public static function incCommentsZan($pid){
		$key = 'comments_zan_'.$pid;
		if(cache('?'.$key)){
			Cache::inc($key);
		}
	}
	
	/**
	 * 获取书籍打赏信息
	 * @param number $bookId
	 * @return mixed
	 */
	public static function getRewardCache($bookId){
		$key = 'book_reward_'.$bookId;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('OrderReward')->where('type',1)->where('pid',$bookId)->field('sum(money) as money,uid')->group('uid')->limit(3)->order('money','desc')->select();
			if($data){
				foreach ($data as &$v){
					$member = self::getMember($v['uid']);
					if($member){
						$v['nickname'] = $member['nickname'];
						$v['headimgurl'] = $member['headimgurl'];
					}else{
						$v['nickname'] = '游客';
						$v['headimgurl'] = '/static/templet/default/headimg.jpeg';
					}
				}
			}else{
				$data = 0;
			}
			cache($key,$data);
		}
		return $data;
	}
	
	//清空打赏缓存
	public static function rmRewardCache($bookId){
		$key = 'book_reward_'.$bookId;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取收藏书籍id集
	 * @param number $uid 用户ID
	 * @return mixed
	 */
	public static function getCollectBookIds($uid){
		$key = 'member_collect_bookids_'.$uid;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('MemberCollect')->where('uid','=',$uid)->order('id','desc')->column('book_id');
			$data = $data ? $data : [];
			cache($key,$data);
		}
		return $data;
	}
	
	/**
	 * 清空用户收藏书籍ID集
	 * @param number $uid 用户ID
	 */
	public static function rmCollectBookIds($uid){
		$key = 'member_collect_bookids_'.$uid;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取用户缓存
	 * @param number $uid 用户ID
	 * @return array
	 */
	public static function getMember($uid){
		$key = 'member_info_'.$uid;
		//cache($key,null);die;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$field = 'id,openid,wx_id,channel_id,agent_id,invite_code,phone,sex,nickname,headimgurl,subscribe,subscribe_time,viptime,money,status,spread_id,is_charge,is_auto,create_time,channel,old_phone';
			$data = myDb::getById('Member',$uid,$field);
			if($data){
				if($data['status'] != 1){
					res_api('用户信息异常');
				}
				cache($key,$data);
			}
		}
		return $data;
	}
	
	/**
	 * 清除用户缓存
	 * @param number $uid 用户ID
	 */
	public static function rmMember($uid){
		$key = 'member_info_'.$uid;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取用户极光ID
	public static function JpushRegId($uid,$jpush_reg_id=''){
		$key = 'jpush_reg_id_'.$uid;
		if($jpush_reg_id){
			cache($key,$jpush_reg_id);
		}else{
			if(cache('?'.$key)){
				$data = cache($key);
			}else{
				$data = Db::name('MemberInfo')->where('uid',$uid)->value('jpush_reg_id');
				$data = $data ? : 0;
				cache($key,$data);
			}
			return $data;
		}
	}
	
	
	//获取用户最后评论是否采纳
	public static function getMemberIsAdopt($uid){
		$key = 'member_feedback_adopt_'.$uid;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = 'no';
			$cur = Db::name('Feedback')->where('uid',$uid)->order('id','desc')->field('id,is_adopt,adopt_time')->find();
			if($cur && $cur['is_adopt'] == 1 && ($cur['adopt_time']+172800) > time()){
				$data = 'yes';
			}
			cache($key,$data,172800);
		}
		return $data;
	}
	
	//清除用户采纳情况
	public static function setMemberIsAdopt($uid,$flag='no'){
		$key = 'member_feedback_adopt_'.$uid;
		if(cache('?'.$key)){
			cache($key,$flag,172800);
		}
	}
	
	//清除用户采纳情况
	public static function rmMemberIsAdopt($uid){
		$key = 'member_feedback_adopt_'.$uid;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取用户账户余额
	 * @param unknown $uid
	 * @return number
	 */
	public static function getMemberMoney($uid){
		$key = 'member_money_'.$uid;
		if(cache('?'.$key)){
			$money = cache($key);
		}else{
			$user = Db::name('Member')->where('id',$uid)->field('id,money')->find();
			if($user){
				$money = $user['money'];
				cache($key,$money);
			}else{
				$money = 0;
			}
		}
		return $money;
	}

    /**
     * 判断缓存数据是否跟数据库数据一致
     * @param $uid
     */
	public static function changeMember($uid){
        $user = Db::name('Member')->where('id',$uid)->field('id,money,viptime')->find();
        if($user){
            $key_money = 'member_money_'.$uid;
            if(cache('?'.$key_money)){
                $money = cache($key_money);
                if($user['money'] != $money){
                    cache($key_money,$user['money']);
                }
            }
            $key_info = 'member_info_'.$uid;
            if(cache('?'.$key_info)){
                $user_info = cache($key_info);
                if($user['viptime'] != $user_info['viptime']){
                    cache($key_info,null);
                }
            }
        }
    }

    public static function delMoney($uid){
        $key_money = 'member_money_'.$uid;
        if('?'.$key_money){
            cache($key_money,null);
        }
    }
    public static function delInfo($uid){
        $key = 'member_info_'.$uid;//用户信息缓存
        if(cache('?'.$key)){
            cache($key,null);
        }
    }
	
	/**
	 * 增减用户账户余额
	 * @param number $uid 用户ID
	 * @param number $value 数量
	 * @param string $opt inc增加dec减少
	 */
	public static function doMemberMoney($uid,$value,$opt='inc'){
		$key = 'member_money_'.$uid;
		if(cache('?'.$key)){
			switch ($opt){
				case 'inc':
					Cache::inc($key,$value);
					break;
				case 'dec':
					Cache::dec($key,$value);
					break;
			}
		}
	}
	
	/**
	 * 根据openid获取用户id
	 * @param string $openid 微信openID
	 * @return number
	 */
	public static function getUidByOpenId($openid){
		$uid = 0;
		if($openid){
			$key = $openid.'_userid';
			if(cache('?'.$key)){
				$uid = cache($key);
			}else{
				$uid = myDb::getValue('Member', [['openid','=',$openid]], 'id');
				if($uid){
					cache($key,$uid);
				}
			}
		}
		return $uid;
	}
	
	/**
	 * 根据邀请码获取用户ID
	 * @param string $openid 微信openID
	 * @return number
	 */
	public static function getUidByCode($code){
		$uid = 0;
		if($code){
			$key = $code.'_userid';
			if(cache('?'.$key)){
				$uid = cache($key);
			}else{
				$uid = myDb::getValue('Member', [['invite_code','=',$code]],'id');
				if($uid){
					cache($key,$uid);
				}
			}
		}
		return $uid;
	}
	
	/**
	 * 清除用户Openid缓存
	 * @param string $openid 用户openID
	 */
	public static function rmMembeOpenid($openid){
		$key = $openid.'_userid';
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 修改用户信息
	 */
	public static function updateMember($uid,$field,$value=''){
		$key = 'member_info_'.$uid;
		if(cache('?'.$key)){
			$data = cache($key);
			if(is_array($field)){
				foreach ($field as $k=>$v){
					if(array_key_exists($k, $data)){
						$data[$k] = $v;
					}
				}
			}else{
				if(array_key_exists($field, $data)){
					$data[$field] = $value;
				}
			}
			cache($key,$data);
		}
	}
	
	/**
	 * 获取书籍缓存
	 * @param number $bookId 书籍ID
	 * @return array
	 */
	public static function getBook($bookId){
		$key = 'book_'.$bookId;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$field = 'id,type,money,name,author,cover,summary,category,free_type,over_type,gender_type,free_chapter,share_title,share_desc,status,word_number';
			$data = myDb::getById('Book',$bookId,$field);
			if($data){
				if($data['category']){
					$data['category'] = explode(',',trim($data['category'],','));
				}else{
					$data['category'] = [];
				}
				cache($key,$data);
			}
		}
		return $data;
	}
	
	/**
	 * 更新书籍缓存
	 * @param number $bookId 书籍ID
	 * @return array
	 */
	public static function doBook($bookId,$field,$value){
		$key = 'book_'.$bookId;
		if(cache('?'.$key)){
			$data = cache($key);
			if(isset($data[$field])){
				$data[$field] = $value;
				cache($key,$data);
			}
		}
	}
	
	/**
	 * 删除书籍缓存
	 * @param number $bookId 书籍ID
	 */
	public static function rmBook($bookId){
		$key = 'book_'.$bookId;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取新发布区域精品推荐缓存
	public static function getRecommendIds($type,$gender_type){
		$key = 'area_recommend_ids'.$type.$gender_type;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$where = [
				['type','=',$type],
				['status','=',1],
				['gender_type','=',$gender_type],
				['is_recommend','=',1]
			];
			$data = Db::name('Book')->where($where)->order('hot_num','desc')->column('id');
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//获取新发布区域本周热搜缓存
	public static function getReadTopIds($type,$gender_type){
		$key = 'area_readtop_ids'.$type.$gender_type;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$week = date('N');
			$end_time = strtotime(date('Y-m-d')) - (($week - 1) * 86400) ;
			$start_time = $end_time - 7 * 86400;
			$end_time -= 1;
			$where = [
				['a.type','=',$type],
				['a.status','=',1],
				['a.gender_type','=',$gender_type],
			];
			$list = Db::name('Book a')
			->join('read_history b','a.id=b.book_id')
			->where($where)
			->field('a.id,count(b.id) as readnum')
			->group('a.id')
			->order('readnum','desc')
			->select();
			$data = $list ? array_column($list, 'id') : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//获取热门推荐书籍
	public static function getHotIds($type,$gender_type){
		$key = 'area_hot_ids'.$type.$gender_type;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$where = [
				['type','=',$type],
				['status','=',1],
				['gender_type','=',$gender_type],
			];
			$data = Db::name('Book')->where($where)->order('hot_num','desc')->column('id');
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//获取新书推荐
	public static function getUpdateIds($type,$gender_type){
		$key = 'area_update_ids'.$type.$gender_type;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
		    $date = date('Y-m-d');
			$where = [
				['a.type','=',$type],
				['a.status','=',1],
				['a.gender_type','=',$gender_type],
                ['b.create_time','between',[strtotime($date.' 00:00:00'),strtotime($date.' 23:59:59')]]
			];
			$list = Db::name('Book a')
			->join('book_cat_log b','a.id=b.book_id')
			->where($where)
			->field('a.id,max(b.create_time) as create_time')
			->group('a.id')
			->order('create_time','desc')
			->select();
			$data = $list ? array_column($list, 'id') : 0;
			cache($key,$data);
		}
		return $data;
	}

	//备份
    public static function getUpdateIds_bak($type,$gender_type){
        $key = 'area_update_ids'.$type.$gender_type;
        if(cache('?'.$key)){
            $data = cache($key);
        }else{
            $where = [
                ['a.type','=',$type],
                ['a.status','=',1],
                ['a.gender_type','=',$gender_type],
            ];
            $list = Db::name('Book a')
                ->join('book_chapter b','a.id=b.book_id')
                ->where($where)
                ->field('a.id,max(b.create_time) as create_time')
                ->group('a.id')
                ->order('create_time','desc')
                ->select();
            $data = $list ? array_column($list, 'id') : 0;
            cache($key,$data);
        }
        return $data;
    }
	
	//清理新书发布区域缓存
	public static function clearAreaIds($type){
		$keys = [];
		for ($b=1;$b<=2;$b++){
			$keys[] = 'area_recommend_ids'.$type.$b;
			$keys[] = 'area_readtop_ids'.$type.$b;
			$keys[] = 'area_hot_ids'.$type.$b;
			$keys[] = 'area_update_ids'.$type.$b;
		}
		foreach ($keys as $key){
			if(cache('?'.$key)){
				cache($key,null);
			}
		}
	}
	
	//获取书籍排行缓存
	public static function getBookRankIds($flag,$type,$gender_type){
		$key = 'book_rank_ids_'.$type.'_'.$gender_type.'_'.$flag;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			switch ($flag){
				case 1: //热销
					$field = 'a.book_id,sum(a.money) as money';
					$list = Db::name('MemberConsume a')->join('book b','a.book_id=b.id and b.status=1 and b.type='.$type.' and b.gender_type='.$gender_type)->where('a.book_id','>',0)->field($field)->group('a.book_id')->order('money','desc')->limit(10)->select();
					break;
				case 2://新书
					$field = 'id as book_id';
					$list = Db::name('Book')->where('status',1)->where('type',$type)->where('gender_type',$gender_type)->field($field)->order('create_time','desc')->order('hot_num','desc')->limit(10)->select();
					break;
				case 3://收藏
					$field = 'a.book_id,count(a.id) as count';
					$list = Db::name('MemberCollect a')->join('book b','a.book_id=b.id and b.status=1 and b.type='.$type.' and b.gender_type='.$gender_type)->field($field)->group('a.book_id')->order('count','desc')->limit(10)->select();
					break;
				case 4://点击
					$list = Db::name('Book')->where('status',1)->where('type',$type)->where('gender_type',$gender_type)->field('id as book_id')->order('hot_num','desc')->limit(10)->select();
					break;
			}
			$data = $list ? array_column($list, 'book_id') : 0;
			cache($key,$data,86400);
		}
		return $data;
	}
	
	//获取书籍排行缓存
	public static function rmBookRankIds($flag,$type,$gender_type){
		$key = 'book_rank_ids_'.$type.'_'.$gender_type.'_'.$flag;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取书架推荐书籍列表
	public static function getBookRecommendIds(){
		$key = md5('book_recommend_ids_'.date('Ymd'));
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('Book')->where('status',1)->order('hot_num','desc')->order('id','desc')->column('id');
			$data = $data ? : 0;
			cache($key,$data,86400);
		}
		return $data;
	}
	
	//清除书架推荐书籍列表
	public static function rmBookRecommendIds(){
		$key = md5('book_recommend_ids_'.date('Ymd'));
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	
	
	//出版书籍缓存
	public static function getBookPublishIds($type,$gender_type){
		$key = md5('book_publish'.$type.$gender_type);
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('Book')->where('type',$type)->where('status',1)->where('free_type',1)->where('gender_type',$gender_type)->order('hot_num','desc')->order('id','desc')->column('id');
			$data = $data ? : 0;
			cache($key,$data,86400);
		}
		return $data;
	}
	
	//清除书籍id缓存
	public static function rmBookPublishIds($type,$gender_type){
		$key = md5('book_publish'.$type.$gender_type);
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取书籍推广分享详情
	public static function getBookShareInfo($bookId){
		$key = 'book_share_info'.$bookId;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('BookShare')->where('book_id',$bookId)->find();
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//删除书籍分享详情
	public static function rmBookShareInfo($bookId){
		$key = 'book_share_info'.$bookId;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}

    //完结书籍缓存
    public static function getBookOverType2Ids($type,$gender_type,$overtype){
        $key = md5('book_overtype2'.$type.$gender_type.$overtype);
        if(cache('?'.$key)){
            $data = cache($key);
        }else{
            $data = Db::name('Book')->where('type',$type)->where('status',1)->where('over_type',$overtype)->where('gender_type',$gender_type)->order('hot_num','desc')->order('id','desc')->column('id');
            $data = $data ? : 0;
            cache($key,$data,86400);
        }
        return $data;
    }

    //清除书籍id缓存
    public static function rmBookOverType2Ids($type,$gender_type,$overtype){
        $key = md5('book_overtype2'.$type.$gender_type.$overtype);
        if(cache('?'.$key)){
            cache($key,null);
        }
    }

    //连载书籍缓存
    public static function getBookOverType1Ids($type,$gender_type,$overtype){
        $key = md5('book_overtype1'.$type.$gender_type.$overtype);
        if(cache('?'.$key)){
            $data = cache($key);
        }else{
            $data = Db::name('Book')->where('type',$type)->where('status',1)->where('over_type',$overtype)->where('gender_type',$gender_type)->order('hot_num','desc')->order('id','desc')->column('id');
            $data = $data ? : 0;
            cache($key,$data,86400);
        }
        return $data;
    }

    //删除书籍缓存
    public static function rmBookOverType1Ids($type,$gender_type,$overtype){
        $key = md5('book_overtype1'.$type.$gender_type.$overtype);
        if(cache('?'.$key)){
            cache($key,null);
        }
    }
	
	/**
	 * 获取视频信息
	 * @param unknown $videoId
	 * @return array|\site\NULL
	 */
	public static function getVideo($videoId){
		$key = 'video_'.$videoId;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = myDb::getById('Video',$videoId);
			if($data){
				cache($key,$data);
			}
		}
		return $data;
	}
	
	/**
	 * 删除书籍缓存
	 * @param number $videoId 视频ID
	 */
	public static function rmVideo($videoId){
		$key = 'video_'.$videoId;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取书籍章节数量
	 * @param number $book_id 书籍ID
	 * @return number|string
	 */
	public static function getBookChapterNum($book_id){
		$key = 'book_chapter_num_'.$book_id;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = myDb::getCount('BookChapter',[['book_id','=',$book_id]]);
			cache($key,$data);
		}
		return $data;
	}
	
	/**
	 * 删除章节数量缓存
	 * @param number $book_id 书籍ID
	 */
	public static function rmBookChapterNum($book_id){
		$key = 'book_chapter_num_'.$book_id;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取书籍热度
	 * @param number $book_id 书籍ID
	 * @return number
	 */
	public static function getBookHotNum($book_id){
		$key = 'book_hot_num_'.$book_id;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = myDb::getValue('Book', [['id','=',$book_id]], 'hot_num');
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	/**
	 * 增加书籍热度数量
	 * @param number $book_id 书籍ID
	 */
	public static function incBookHotNum($book_id){
		$key = 'book_hot_num_'.$book_id;
		if(cache('?'.$key)){
			Cache::inc($key);
		}
	}
	
	/**
	 * 删除书籍热度缓存
	 * @param number $book_id 书籍ID
	 */
	public static function rmBookHotNum($book_id){
		$key = 'book_hot_num_'.$book_id;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取书籍章节列表缓存
	 * @param number $book_id 书籍ID
	 * @return number
	 */
	public static function getBookChapterList($book_id){
		$key = 'book_chapter_list_'.$book_id;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('BookChapter')->where('book_id','=',$book_id)->field('id,name,number,src,money,page_notice')->order('number','asc')->select();
			if($data){
				$data = array_column($data, null,'number');
				cache($key,$data);
			}
			$data = $data ? : 0;
		}
		return $data;
	}
	
	/**
	 * 删除书籍列表章节缓存
	 * @param number $book_id 书籍ID
	 */
	public static function rmBookChapterList($book_id){
		$key = 'book_chapter_list_'.$book_id;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取用户阅读记录缓存
	public static function getReadCache($uid){
		$key = 'member_read_'.$uid;
		if(cache('?'.$key)){
			$list = cache($key);
		}else{
			$list = [];
			$temp = Db::name('ReadHistory')->where('uid',$uid)->where('status',1)->field('book_id,number,create_time')->order('create_time','desc')->select();
			if($temp){
				foreach ($temp as $v){
					$ck = 'book_'.$v['book_id'];
					if(isset($list[$ck])){
						$list[$ck]['numbers'][] = $v['number'];
						if($v['create_time'] > $list[$ck]['last_time']){
							$list[$ck]['last_number'] = $v['number'];
							$list[$ck]['last_time'] = $v['create_time'];
						}
					}else{
						$list[$ck] = [
							'book_id' => $v['book_id'],
							'last_number' => $v['number'],
							'last_time' => $v['create_time'],
							'numbers' => [$v['number']]
						];
					}
				}
			}
			cache($key,$list);
		}
		return $list;
	}
	
	//删除阅读缓存
	public static function rmReadCache($uid){
		$key = 'member_read_'.$uid;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	
	/**
	 * 检测用户是否阅读过该章节
	 * @param number $uid 用户ID
	 * @param number $book_id 书籍ID
	 * @param number $number 章节数量
	 * @return boolean 是否已阅读过
	 */
	public static function checkIsRead($uid,$book_id,$number){
		$list = self::getReadCache($uid);
		$res = false;
		if($list){
			$lkey = 'book_'.$book_id;
			if(isset($list[$lkey])){
				if(in_array ($number,$list[$lkey]['numbers'])){
					$res = true;
				}
			}
		}
		return $res;
	}
	
	/**
	 * 添加阅读记录
	 * @param number $uid 用户ID
	 * @param number $book_id 书籍ID
	 * @param number $number 章节数量
	 */
	public static function addRead($uid,$book_id,$number,$time){
		$key = 'member_read_'.$uid;
		if(cache('?'.$key)){
			$list = cache($key);
			$ck = 'book_'.$book_id;
			if(isset($list[$ck])){
				if(!in_array($number, $list[$ck]['numbers'])){
					$list[$ck]['last_time'] = $time;
					$list[$ck]['last_number'] = $number;
					$list[$ck]['numbers'][] = $number;
					cache($key,$list);
				}
			}else{
				$cur = [
					'book_id' => $book_id,
					'last_number' => $number,
					'last_time' => $time,
					'numbers' => [$number]
				];
				$list[$ck] = $cur;
				cache($key,$list);
			}
		}
	}
	
	/**
	 * 获取推广缓存
	 * @param number $id 推广ID
	 * @return array
	 */
	public static function getSpread($id){
		$key = 'spread_'.$id;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$field = 'id,name,channel_id,book_id,chapter_number,number,click_type';
			$data = myDb::getById('Spread', $id,$field);
			if($data){
				cache($key,$data);
			}
		}
		return $data;
	}
	
	/**
	 * 删除推广缓存
	 * @param number $id 推广ID
	 */
	public static function rmSpread($id){
		$key = 'spread_'.$id;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	
	/**
	 * 获取缓存的url信息
	 * @param string $server_url 当前链接
	 * @return array
	 */
	public static function getUrlInfo($server_url=''){
		$server_url = $server_url ? : $_SERVER['HTTP_HOST'];
		$key = md5($server_url.'#info');
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$site = getMyConfig('website');
			if(!$site){
				res_error('尚未配置站点信息');
			}
			$is_site = false;
			$is_wx = 0;
			if($server_url === $site['url']){
				$is_wx = 1;
				$is_site = true;
			}else{
				if($site['location_url'] && $site['location_url'] === $server_url){
					if($site['is_location'] != 1){
						res_error('该域名尚未启用');
					}
					$is_site = true;
				}
			}
			$data = [
				'name' => $site['name'],
				'url' => $site['url'],
				'is_location' => $site['is_location'],
				'location_url' => $site['location_url'],
				'wx_id' => 0,
				'channel_id' => 0,
				'agent_id' => 0
			];
			if(!$is_site){
				$field = 'id,name,type,is_wx,parent_id,url,is_location,location_url';
				$channel = Db::name('Channel')->where('url|location_url','=',$server_url)->field($field)->find();
				if($channel['url'] !== $server_url){
					if($channel['is_location'] != 1){
						res_error('该域名尚未启用');
					}
				}
				if($channel['type'] == 1){
					if($channel['is_wx'] == 1){
						$data['name'] = $channel['name'];
						$data['channel_id'] = $channel['id'];
						$data['wx_id'] = $channel['id'];
						$data['url'] = $channel['url'];
						$data['is_location'] = $channel['is_location'];
						$data['location_url'] = $channel['location_url'];
					}else{ 
						$data['is_location'] = 1;
						$data['location_url'] = $channel['url'];
						$data['channel_id'] = $channel['id'];
					}
				}else{
					$parent = self::getChannel($channel['parent_id']);
					if(!$parent){
						res_api('渠道信息异常');
					}
					$data['is_location'] = 1;
					$data['location_url'] = $channel['url'];
					$data['channel_id'] = $parent['id'];
					$data['agent_id'] = $channel['id'];
					if($parent['is_wx'] == 1){
						$data['name'] = $parent['name'];
						$data['url'] = $parent['url'];
						$data['wx_id'] = $parent['id'];
					}
				}
			}
			cache($key,$data);
		}
		return $data;
	}
	
	/**
	 * 获取当前url微信信息
	 * @param string $server_url 当前链接
	 * @return array
	 */
	public static function getWeixinInfo($server_url=''){
		$server_url = $server_url ? : $_SERVER['HTTP_HOST'];
		$key = md5($server_url.'#weixin');
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$info = self::getUrlInfo();
			if($info['wx_id'] > 0){
				$channel = self::getChannel($info['wx_id']);
				if(!$channel){
					res_error('渠道信息异常');
				}
				if(!$channel['appid'] || !$channel['appsecret']){
					res_error('该渠道尚未配置微信公众号信息');
				}
				$data = [
					'qrcode' => $channel['qrcode'],
					'appid' => $channel['appid'],
					'appsecret' => $channel['appsecret'],
					'apptoken' => $channel['apptoken'],
				];
			}else{
				$data = getMyConfig('weixin_param');
				if(!$data){
					res_error('尚未配置站点微信信息');
				}
			}
			cache($key,$data);
		}
		return $data;
	}
	
	/**
	 * 删除url缓存
	 * @param string $url 链接
	 */
	public static function rmUrlInfo($url){
		$key1 = md5($url.'#info');
		$key2 = md5($url.'#weixin');
		if(cache('?'.$key1)){
			cache($key1,null);
		}
		if(cache('?'.$key2)){
			cache($key2,null);
		}
	}
	
	/**
	 * 缓存用户签到情况
	 * @param number $uid 用户ID
	 * @return string|mixed|\think\cache\Driver|boolean|string[]|unknown[]
	 */
	public static function getUserSignInfo($uid){
		$cur_date = date('Ymd');
		$cacheKey = md5($cur_date.'_signuser_'.$uid);
		$re = cache('?'.$cacheKey);
		if($re){
			$data = cache($cacheKey);
		}else{
			$siteConfig = getMyConfig('site_sign');
			if(!isset($siteConfig['is_sign']) || $siteConfig['is_sign'] != 1){
				return '签到未启用';
			}
			$config = $siteConfig['sign_config'];
			$checkConfig = false;
			$total = 0;
			foreach ($config as $k=>$v){
				$day = ltrim($k,'day');
				if(!$v){
					$checkConfig = true;
				}
				$total += $v;
				$temp[$k] = ['money'=>$v,'day'=>intval($day),'is_sign'=>'no'];
			}
			if($checkConfig){
				return '签到参数配置有误，请联系客服';
			}
			$cur_sign = 'no';
			$cur = Db::name('MemberSign')->where('uid','=',$uid)->where('date','=',$cur_date)->field('id,days')->find();
			if($cur){
				$cur_sign = 'yes';
				for($i=0;$i<$cur['days'];$i++){
					$key = 'day'.($i+1);
					$temp[$key]['is_sign'] = 'yes';
				}
			}else{
				$yesterday = date('Ymd',strtotime('yesterday'));
				$yesterday_msg = Db::name('MemberSign')->where('uid','=',$uid)->where('date','=',$yesterday)->field('id,days')->find();
				$yesterdayNum = $yesterday_msg['days'] == 7 ? 0 : $yesterday_msg['days'];
				if($yesterdayNum){
					for($i=0;$i<$yesterdayNum;$i++){
						$key = 'day'.($i+1);
						$temp[$key]['is_sign'] = 'yes';
					}
				}
			}
			$data = [
				'cur_sign' => $cur_sign,
				'total' => $total,
				'list' => $temp
			];
			cache($cacheKey,$data,86400);
		}
		return $data;
	}
	
	//获取充值数据
	public static function getSiteCharge(){
		$key = 'site_charge';
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = [];
			$list = Db::name('Charge')->where('channel_id',0)->field('id,type,money,content,pay_times')->order('sort_num','desc')->select();
			if($list){
				foreach ($list as $v){
					$v['content'] = json_decode($v['content'],true);
					$data[$v['id']] = $v;
				}
				cache($key,$data);
			}
		}
		return $data;
	}
	
	//获取套餐对应ID
	public static function getChargePid($charge_id){
		$key = 'charge_id_'.$charge_id;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$pid = Db::name('Charge')->where('id',$charge_id)->value('pid');
			$data = $pid ? $pid : $charge_id;
			cache($key,$data);
		}
		return $data;
	}
	
	//获取渠道充值套餐
	public static function getCharge($channelId){
		$key = 'channel_charge_'.$channelId;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('Charge')->where('channel_id',$channelId)->where('status',1)->field('id,type,pid,is_check,is_hot')->order('sort_num','desc')->select();
			$data = $data ? $data : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//删除充值套餐
	public static function rmCharge($channelId){
		$key = 'channel_charge_'.$channelId;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//删除充值数据
	public static function rmSiteCharge(){
		$key = 'site_charge';
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取用户套餐充值次数
	public static function getChargeNum($uid,$chargeId){
		$key = 'member_charge_num_'.$uid.'_'.$chargeId;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('Order')->where('uid',$uid)->where('status',2)->where('charge_id',$chargeId)->count();
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//新增用户套餐充值次数
	public static function incChargeNum($uid,$chargeId){
		$key = 'member_charge_num_'.$uid.'_'.$chargeId;
		if(cache('?'.$key)){
			Cache::inc($key);
		}
	}
	
	
	
	//获取渠道vip账号列表
	public static function getVipChannelIds($vipId){
		$key = 'vip_channel_ids_'.$vipId;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('VipChannel')->where('vip_id',$vipId)->column('channel_id');
			if($data){
				cache($key,$data);
			}
		}
		return $data;
	}
	
	//根据渠道ID获取vipID
	public static function getVipIdByChannelId($channel_id){
		$key = 'vip_id_by_'.$channel_id;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('VipChannel')->where('channel_id',$channel_id)->value('vip_id');
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//保存vipID缓存
	public static function saveVipIdByChannelId($channel_id,$vip_id){
		$key = 'vip_id_by_'.$channel_id;
		cache($key,$vip_id);
	}
	
	//删除渠道vip账号列表
	public static function rmVipChannelIds($vipId){
		$key = 'vip_channel_ids_'.$vipId;
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取渠道vipID
	public static function getChannelVipId($channelId){
		$key = 'channel_vip_id_'.$channelId;
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = Db::name('VipChannel')->where('channel_id',$channelId)->value('vip_id');
			$data = $data ? : 0;
			cache($key,$data);
		}
		return $data;
	}
	
	//获取渠道可提现余额
	public static function getChannelAmount($channel_id){
		$date = date('Y-m-d');
		$key = md5('channel_amount_'.$date.'_'.$channel_id);
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$where = [['create_date','<',$date],['channel_id','=',$channel_id],['status','=',2]];
			$data = Db::name('ChannelChart')->where($where)->sum('income_money');
			$data = $data ? : 0;
			cache($key,$data,86400);
		}
		return $data;
	}
	
	//删除渠道提现余额
	public static function rmChannelAmount($channel_id){
		$date = date('Y-m-d');
		$key = md5('channel_amount_'.$date.'_'.$channel_id);
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	/**
	 * 获取渠道指定数据
	 * @param number $channel_id 渠道ID
	 * @param number $type 1今日关注2今日充值3今日收入
	 * @return number
	 */
	public static function getChannelTodayMsg($channel_id,$type=1){
		$date = date('Y-m-d');
		$key = md5('channel_'.$date.'_'.$type.'_'.$channel_id);
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$channel = self::getChannel($channel_id);
			$field_key = $channel['type'] == 1 ? 'channel_id' : 'agent_id';
			$start_time = strtotime('today');
			switch ($type){
				case 1:
					$data = Db::name('Member')->where('subscribe_time','>=',$start_time)->where('subscribe',1)->where($field_key,$channel_id)->count();
					break;
				case 2:
					$data = Db::name('Order')->where('create_time','>=',$start_time)->where('status',2)->where($field_key,$channel_id)->where('is_count',1)->sum('money');
					break;
				case 3:
					$data = 0;
					$list = Db::name('Order')->where('create_time','>=',$start_time)->where('status',2)->where($field_key,$channel_id)->where('is_count',1)->field('id,count_info')->select();
					if($list){
						$count_key = $channel['type'] == 1 ? 'channel' : 'agent';
						foreach ($list as $v){
							$count_info = json_decode($v['count_info'],true);
							$data += $count_info[$count_key]['money'];
						}
					}
					break;
			}
			$data = $data ? : 0;
			cache($key,$data,86400);
		}
		return $data;
	}
	
	/**
	 * 新增渠道指定数据
	 * @param number $channel_id 渠道ID
	 * @param number $type 1今日关注2今日充值
	 * @param number $value 增加数值
	 */
	public static function incChannelTodayMsg($channel_id,$type,$value){
		$date = date('Y-m-d');
		$key = md5('channel_'.$date.'_'.$type.'_'.$channel_id);
		if(cache('?'.$key)){
			Cache::inc($key,$value);
		}
	}
	
	//获取渠道统计ID
	public static function getChannelChartId($channel_id,$date=null){
		$date = $date ? $date : date('Y-m-d');
		$key = md5('channel_chart_'.$date.'_'.$channel_id);
		if(cache($key)){
			$data = cache($key);
		}else{
			$data = Db::name('ChannelChart')->where('create_date',$date)->where('channel_id',$channel_id)->value('id');
			if($data){
				cache($key,$data,86400);
			}
		}
		return $data;
	}
	
	//当日充值统计ID
	public static function getSpreadChartId($spread_id,$date=null){
		$date = $date ? $date : date('Y-m-d');
		$key = md5('spread_chart_'.$spread_id.'_'.$date);
		if(cache($key)){
			$data = cache($key);
		}else{
			$data = Db::name('SpreadChart')->where('sid',$spread_id)->where('create_date',$date)->value('id');
			if($data){
				cache($key,$data,86400);
			}
		}
		return $data;
	}
	
	//获取用户订单记录信息
	public static function getMemberChargeId($uid,$updateData=null){
		$key = 'member_charge_'.$uid;
		if($updateData){
			cache($key,$updateData);
		}else{
			if(cache('?'.$key)){
				$data = cache($key);
			}else{
				$data = Db::name('MemberCharge')->where('uid',$uid)->find();
				if(!$data){
					$res = Db::name('MemberCharge')->insert(['uid'=>$uid]);
					if($res){
						$data = Db::name('MemberCharge')->where('uid',$uid)->find();
						if($data){
							cache($key,$data);
						}
					}
				}
			}
			return $data;
		}
	}
	
	//检测书籍是否限免
	public static function checkBookFree($book_id){
		$data = self::getFreeBookIds();
		return ($data && isset($data[$book_id])) ? true : false;
	}
	
	//获取限免书籍ID
	public static function getFreeBookIds(){
		$date = date('Y-m-d');
		$key = md5('free_book_ids_'.$date);
		if(cache('?'.$key)){
			$data = cache($key);
		}else{
			$data = [];
			$cache = ['all' => [],'11' => [],'12'=>[],'21'=>[],'22'=>[]];
			$list = Db::name('BookFree a')
			->join('book b','a.book_id=b.id')
			->where('a.start_date','<=',$date)
			->where('a.end_date','>=',$date)
			->field('a.book_id,a.start_date,a.end_date,b.type,b.gender_type')
			->select();
			if($list){
				foreach ($list as $v){
					$all_key = $v['book_id'];
					$child_key = $v['type'].$v['gender_type'];
					$cache['all'][$all_key] = 1;
					$cache[$child_key][] = ['id'=>$v['book_id'],'start_date'=>$v['start_date'],'end_date'=>$v['end_date']];
				}
			}
			foreach ($cache as $k=>$val){
				if($k === 'all'){
					$data = $val ? : 0;
					cache($k,$data,86400);
				}else{
					$child_cache_key = md5($key.$k);
					$child_data = $val ? : 0;
					cache($child_cache_key,$child_data,86400);
				}
			}
		}
		return $data;
	}
	
	//删除限免书籍缓存
	public static function rmFreeBookIds(){
		$date = date('Y-m-d');
		$key = md5('free_book_ids_'.$date);
		if(cache('?'.$key)){
			cache($key,null);
		}
	}
	
	//获取限免书籍分类ID集
	public static function getBookFreeIds($type,$gender_type){
		$date = date('Y-m-d');
		$key = md5(md5('free_book_ids_'.$date).$type.$gender_type);
		$data = null;
		if(cache('?'.$key)){
			$data = cache($key);
		}
		return $data;
	}

	//获取连载,完结书籍id集
    public static function getBookOverTypeIds($type,$gender_type,$over_type)
    {
        $date = date('Y-m-d');
        $key = md5(md5('over_type_book_ids_'.$date).$type.$gender_type.$over_type);
        $data = null;
        if($type == 2){
            //漫画
            cache($key,null);
        }
        //cache($key,null);die;
        if(cache('?'.$key)){
            $data = cache($key);
        }else{
            $data = Db::name('Book')->where(['type'=>$type,'gender_type'=>$gender_type,'over_type'=>$over_type])->order('hot_num desc')->select();
            $data = $data?:0;
            cache($key,$data);
        }
        return $data;
    }

    //获取完结书籍id集
//    public static function getBookOverType2Ids($type,$gender_type,$over_type2)
//    {
//        $date = date('Y-m-d');
//        $key = md5(md5('over_type2_book_ids_'.$date).$type.$gender_type.$over_type2);
//        if($type==2){
//            cache($key,null);
//        }
//        $data = null;
//        //cache($key,null);die;
//        if(cache('?'.$key)){
//            $data = cache($key);
//        }else{
//            $data = Db::name('Book')->where(['type'=>$type,'gender_type'=>$gender_type,'over_type'=>$over_type2])->order('hot_num desc')->select();
//            $data = $data?:0;
//            cache($key,$data);
//        }
//        return $data;
//    }

    //获取友情链接列表
    public static function getLinksList(){
        $key = 'links_list';
        if(cache('?'.$key)){
            $data = cache($key);
        }else{
            $data = Db::name('Links')->where([])->order('sort asc')->select();
            $data = $data ? : 0;
            cache($key,$data);
        }
        return $data;
    }

    //删除友情链接列表
    public static function rmLinksList(){
        $key = 'links_list';
        if(cache('?'.$key)){
            cache($key,null);
        }
    }

    //获取pc顶部导航栏列表
    public static function getPcNavList(){
        $key = 'pc_nav_list';
        //dump(cache($key));die;
        if(cache('?'.$key)){
            $data = cache($key);
        }else{
            $pcNavs = Db::name('pc_nav')->where(['pid'=>0])->order('id asc')->select();
            $data = [];
            foreach ($pcNavs as $k =>$v){
                $data[$v['key']] = ['name'=>$v['name'],'is_show'=>$v['is_show'],'url'=>$v['url'],'click'=>""];
                $childNavs = Db::name('pc_nav')->where(['pid'=>$v['id']])->order('id asc')->select();
                foreach ($childNavs as $key1=>$val){
                    $data[$v['key']]['child'][] = ['key'=>$val['key'],'name'=>$val['name'],'is_show'=>$val['is_show'],'url'=>$val['url']];
                }
            }
            $data = $data ? : 0;
            cache($key,$data);
        }
        return $data;
    }

    //删除pc顶部导航栏列表
    public static function rmPcNavList(){
        $key = 'pc_nav_list';
        if(cache('?'.$key)){
            cache($key,null);
        }
    }
}