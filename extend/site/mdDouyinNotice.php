<?php


namespace site;


class mdDouyinNotice
{
    public static function createNotice()
    {
        $client = new http\Client;
        $request = new http\Client\Request;

        $body = new http\Message\Body;
        $body->append('{
            "advertiser_id": {{advertiser_id}},
            "campaign_id": 1234,
            "budget_mode": "BUDGET_MODE_DAY",
            "budget": 1234,
            "name": "test2",
            "external_url": "xxxx",
            "schedule_type": "SCHEDULE_START_END",
            "flow_control_mode": "FLOW_CONTROL_MODE_FAST",
            "start_time":"2019-04-07 00:00",
            "end_time":"2019-09-08 23:59",
            "bid":10,
            "pricing": "PRICING_CPM"
        }');
        
        $request->setRequestUrl('https://ad.oceanengine.com/open_api/2/ad/create/');
        $request->setRequestMethod('POST');
        $request->setBody($body);
        
        $request->setHeaders(array(
            'Access-Token' => '{{access_token}}',
            'Content-Type' => 'application/json'
        ));
        
        $client->enqueue($request)->send();
        $response = $client->getResponse();
        
        echo $response->getBody();
    }

    public function getAccessToken()
    {
        $client = new http\Client;
        $request = new http\Client\Request;

        $body = new http\Message\Body;
        $body->append('{
            "app_id": 0,
            "secret": "",
            "grant_type": "auth_code",
            "auth_code": ""
        }');
        
        $request->setRequestUrl('https://ad.oceanengine.com/open_api/oauth2/access_token/');
        $request->setRequestMethod('POST');
        $request->setBody($body);
        
        $request->setHeaders(array(
                'Content-Type' => 'application/json'
        ));
        
        $client->enqueue($request)->send();
        $response = $client->getResponse();
        
        echo $response->getBody();
    }
}