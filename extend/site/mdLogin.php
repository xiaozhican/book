<?php


namespace site;


use think\Db;

class mdLogin
{
    //保存h5和pc端登录日志
    public static function saveLog($login_name,$login_type,$error=''){
        $status = $error ? 2 : 1;
        $is_online = $error ? 3 : 1;
        $data = [
            'username' => $login_name,
            //'type' => $type,
            'status' => $status,
            'login_type'=>$login_type,
            'remark' => $error,
            //'login_ip' => request()->ip(),
            'create_time' => time(),
            'is_online'=>$is_online
        ];
        Db::name('Log')->insert($data);
    }
}