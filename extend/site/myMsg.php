<?php
namespace site;

use think\facade\Log;

class myMsg{


    //赛邮短信发送
    public static function saiyouSend($phone){
        $config = getMyConfig('saiyou_message');
        if(!$config){
            res_api('短信平台配置异常');
        }
        $res = false;
        $key = $phone.'_code';
        $code = cache($key);
        if(!$code){
            $code = mt_rand(1000,9999);
        }
        //$url = 'https://api.mysubmail.com/message/send';
        $url = 'https://hk-api.mysubmail.com/message/send';
        $data = [
            'appid' => $config['appid'],
            'to' => $phone,
            'content' => '【'.$config['sign'].'】'.str_replace('CODE', $code, $config['content']),
            'timestamp' => time(),
            'sign_type' => 'normal',
            'signature' => $config['appkey']
        ];
        $re = myHttp::doPost($url,$data);
        if(isset($re['status']) && $re['status'] === 'success'){
            cache($key,$code,120);
            $res = true;
        }
        return $res;
    }

    //dxton短信发送
    public static function dxtonSend($phone){
        Log::info('进去短信发送');
        $config = getMyConfig('dxton_message');//application/common.php     select * from sy_config where `key`='dxton_message' and status=1;
        if(!$config){
            res_api('短信平台配置异常');
        }
        $res = false;
        $key = $phone.'_code';
        $code = cache($key);
        if(!$code){
            $code = mt_rand(1000,9999);
        }
        Log::info('验证码为：'.$code);
        $url = 'http://sms.106jiekou.com/utf8/sms.aspx';
        $data = [
            'account' => $config['account'],
            'password'=> $config['password'],
            'mobile'=>$phone,
            'content'=> rawurlencode(str_replace('CODE',$code,$config['content']))
        ];

        $curl_data = urldecode(http_build_query($data));
        $re = myHttp::dxtonPost($curl_data,$url);
        Log::info('短信发送状态为：'.$re);
        if($re == 100){
            cache($key,$code,120);
            $res = true;
        }else{
            $res = $re;
        }
        Log::info('结束短信发送-----------');
        return $res;
    }
	
	//检测验证码是否正确
	public static function check($phone,$code){
		if($code == '1111'){
			return true;
		}
		$key = $phone.'_code';
		$cache_code = cache($key);
		if(!$cache_code){
			return '验证码已过期';
		}
		if($cache_code != $code){
			return '验证码不正确';
		}
		cache($key,null);
		return true;
	}
}