<?php
namespace site;

use app\common\model\mdBook;

class myBlock{
	
	/**
	 * 更新首页缓存
	 * @param number $type 书籍类型
	 * @param str $templet 模版名称
	 * @return string
	 */
	public static function createIndexHtml($type,$gender_type,$templet){
		$str = '';
		$areaMoreUrl = '/index/book/more.html?type='.$type.'&gender_type='.$gender_type;
		$genderMoreUrl = '/index/book/genderMore.html?type='.$type.'&gender_type='.$gender_type;
		$list = mdBook::getAreaBookList($type, $gender_type);
		if($list){
			foreach ($list as $v){
				$str .= '<div class="box">';
				$str .= '<div class="title">';
				$str .= '<h2>'.$v['area'].'</h2>';
				$str .= '<a href="'.$areaMoreUrl.'&area_id='.$v['area_id'].'">查看更多<img src="/static/resource/index/images/next_left.png"></a>';
				$str .= '</div>';
				$str .= '<div class="row_content">';
				foreach ($v['book_list'] as $val){
					$hot_num = formatNumber($val['hot_num']);
					$str .= '<a href="/index/Book/info.html?book_id='.$val['id'].'" class="item">';
					$str .= '<img class="cover" src="'.$val['cover'].'">';
					$str .= '<p class="name">'.$val['name'].'</p>';
					$str .= '<div class="read">';
					$str .= '<img src="/static/resource/index/images/eye.png">';
					$str .= '<span>'.$hot_num.'</span>';
					$str .= '</div>';
					$str .= '</a>';
				}
				$str .= '</div>';
				$str .= '</div>';
			}
		}
		$selectedIds = myCache::getHotIds($type, $gender_type);
		if($selectedIds){
			$title = $gender_type == 1 ? '男生精选' : '女生精选';
			$str .= '<div class="box">';
			$str .= '<div class="title">';
			$str .= '<h2>'.$title.'</h2>';
			$str .= '<a href="'.$genderMoreUrl.'">查看更多<img src="/static/resource/index/images/next_left.png"></a>';
			$str .= '</div>';
			$str .= '<div class="column_content">';
			$indexSelectedIds = array_slice($selectedIds, 0,6);
			foreach ($indexSelectedIds as $sbook_id){
				$book = myCache::getBook($sbook_id);
				if($book && $book['status'] == 1){
					$str .= '<a href="/index/Book/info.html?book_id='.$book['id'].'" class="item">';
					$str .= '<img class="cover" src="'.$book['cover'].'">';
					$str .= '<div class="right">';
					$str .= '<p class="name">'.$book['name'].'</p>';
					$str .= '<p class="main">'.$book['summary'].'</p>';
					if($book['category']){
						$str .= '<p class="article_type">';
						foreach ($book['category'] as $cateStr){
							$str .= '<span>'.$cateStr.'</span>';
						}
						$str .= '</p>';
					}
					$str .= '</div>';
					$str .= '</a>';
				}
			}
			$str .= '</div>';
			$str .= '</div>';
		}
		if($str){
			saveBlock($str, $templet,'other');
		}
	}
	
	/**
	 * 创建书籍排行
	 */
	public static function createBookRank($type,$gender_type){
		for ($flag=1;$flag<5;$flag++){
			myCache::rmBookRankIds($flag, $type, $gender_type);
			$ids = myCache::getBookRankIds($flag, $type, $gender_type);
			if($ids){
				$list = [];
				foreach ($ids as $book_id){
					$book = myCache::getBook($book_id);
					if($book && $book['status'] == 1){
						$list[] = [
							'book_id' => $book['id'],
							'name' => $book['name'],
							'cover' => $book['cover'],
							'summary' => $book['summary'],
							'category' => $book['category'],
						];
					}
				}
				$str = self::createRankHtml($list);
				$templet = 'book_rank_'.$type.$gender_type.$flag;
				saveBlock($str, $templet,'other');
			}
		}
	}
	
	/**
	 * 创建排行html
	 * @param array $list
	 * @return string
	 */
	private static function createRankHtml($list){
		$str = '';
		if($list){
			$str .= '<div class="column_content">';
			$num = 1;
			foreach ($list as $v){
				$str .= '<a href="/index/Book/info.html?book_id='.$v['book_id'].'" class="item">';
				$str .= '<img class="cover" src="'.$v['cover'].'">';
				$str .= '<div class="right">';
				$str .= '<p class="name">'.$v['name'].'</p>';
				$str .= '<p class="main">'.$v['summary'].'</p>';
				if($v['category']){
					$str .= '<p class="article_type">';
					foreach ($v['category'] as $cv){
						$str .= '<span>'.$cv.'</span>';
					}
					$str .= '</p>';
				}
				$str .= '</div>';
				$str .= '<div class="rink">';
				$str .= '<img class="rink_bg" src="/static/resource/index/images/ranking_bg.png">';
				if($num < 4){
					$str .= '<img class="rink_img" src="/static/resource/index/images/ranking_'.$num.'.png">';
				}else{
					$str .= '<span class="rink_text">'.$num.'</span>';
				}
				$str .= '</div>';
				$str .= '</a>';
				$num++;
			}
			$str .= '</div>';
		}
		return $str;
	}
	
	
}