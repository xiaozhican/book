<?php
namespace site;

class myLinks{
    
    public static function getAll(){
        $list = [
            'novel' => [
                'name' => '小说相关链接',
                'links' => [
                    ['url'=>'/','summary'=>'首页'],
                	['url'=>'/index/Book/category.html','summary'=>'书库','param'=>['type'=>1]],
                	['url'=>'/index/Book/rank.html','summary'=>'榜单','param'=>['type'=>1]],
                	['url'=>'/index/Book/publish.html','summary'=>'出版（男频）','param'=>['type'=>1,'gender_type'=>1]],
                	['url'=>'/index/Book/publish.html','summary'=>'出版（女频）','param'=>['type'=>1,'gender_type'=>2]],
                	['url'=>'/index/Book/free.html','summary'=>'限免（男频）','param'=>['type'=>1,'gender_type'=>1]],
                	['url'=>'/index/Book/free.html','summary'=>'限免（女频）','param'=>['type'=>1,'gender_type'=>2]]
                ]
            ],
            'cartoon' => [
                'name' => '漫画相关链接',
                'links' => [
                    ['url'=>'/index/index/cartoon.html','summary'=>'首页'],
                	['url'=>'/index/Book/category.html','summary'=>'书库','param'=>['type'=>2]],
                	['url'=>'/index/Book/rank.html','summary'=>'榜单','param'=>['type'=>2]],
                	['url'=>'/index/Book/publish.html','summary'=>'出版（男频）','param'=>['type'=>2,'gender_type'=>1]],
                	['url'=>'/index/Book/publish.html','summary'=>'出版（女频）','param'=>['type'=>2,'gender_type'=>2]],
                	['url'=>'/index/Book/free.html','summary'=>'限免（男频）','param'=>['type'=>2,'gender_type'=>1]],
                	['url'=>'/index/Book/free.html','summary'=>'限免（女频）','param'=>['type'=>2,'gender_type'=>2]]
                ]
            ],
            'other' => [
                'name' => '其他链接',
                'links' => [
                    ['url'=>'/index/User/index.html','summary'=>'个人中心'],
                    ['url'=>'/index/User/readHistory.html','summary'=>'阅读历史'],
                    ['url'=>'/index/User/bookshelf.html','summary'=>'我的书架'],
                    ['url'=>'/index/User/feedback.html','summary'=>'意见反馈'],
                    ['url'=>'/index/User/consume.html','summary'=>'消费记录'],
                    ['url'=>'/index/User/charge.html','summary'=>'充值记录'],
                    ['url'=>'/index/User/sign.html','summary'=>'签到页面'],
                    ['url'=>'/index/Charge/index.html','summary'=>'书币充值']
                ]
            ],
        ];
        return $list;
    }
}
